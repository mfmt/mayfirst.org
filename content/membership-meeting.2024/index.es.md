+++ 
date = "2024-01-04T14:47:48Z"
description = "Bienvenidos la Reunión de la Membresía de May First" 
title = "La votación ha comenzado" 
tags = [ "callout" ]
+++

**Íhdice**

* [La votación ha comenzado](#vote)
* [Introducción](#introduction)
* [Reuniones de intercambio](#discussion-meetings)
* [Elecciones a la Junta Directiva](#board-elections)
* [Reunión principal (o asamblea)](#main-meeting)
* [Periodo de votación](#voting-period)
* [Información legal](#legal-details)


## La votación ha comenzado{#vote}                                                                                                                                                                                           
La votación ha comenzado y durará hasta el jueves 14 de marzo. Los resultados se anunciarán la semana del 18 de marzo.

Si no ha recibido una papeleta de voto, envíe un correo electrónico a info@mayfirst.org.

## Introducción {#introduction}

El año pasado estuvo marcado por la trágica y continua violencia en Gaza. También atestiguamos una inmensa movilización contra la guerra y la ocupación en todo nuestro movimiento, incluyendo los esfuerzos de la membresía de May First y las coaliciones en las que May First participa.
 
En el continente americano, asistimos a movimientos populistas de derechas sin precedentes que compiten por el poder nacional -algunos pierden y otros ganan-, mientras se avecinan otras elecciones decisivas en 2024.
 
En el mundo de la tecnología, la manipulación y la especulación de las Big Tech salen a la luz una y otra vez, una realidad ampliamente extendida incluso en los medios dominantes. Sin embargo, nuevas tecnologías como Chat GPT se posicionan como nuevas vías para alimentar peligrosos sistemas de vigilancia.

**Dado nuestro entorno político, ¿qué ha estado haciendo May First en 2023? ¿Cuáles son nuestros planes para 2024? Participa de las reuniones anuales de membresía para pensar y y ayudar a planificar nuestro futuro!**

<!--
<a href="https://mayfirst.coop/img/2022-MFMT-membership-meeting.es.png" target="_blank">
  <img width="300" height="600" border="0" align="left" hspace="20" src="/img/2022-MFMT-membership-meeting.es.png">
</a>
-->

<!--
Haz clic sobre la imágen para abrir nuestra infografía de la reunión.
<br clear="left" />
-->

Puedes inscribirte a las reuniones en nuestra sección de [registro a eventos](https://outreach.mayfirst.org/civicrm/event/register?id=90&reset=1).

Consulta nuestro programa y asegúrate de **asistir al menos a una sesión de intercambio y a la reunión principal de membresía.**

## Reuniones de intercambio {#discussion-meetings} 

*Todas las actividades tendrán una duración de una hora.*

Además de la reunión principal (véase más abajo), celebraremos cuatro sesiones de intercambio. Puedes asistir a todas las sesiones o sólo a las que consideres más relevantes para tu trabajo organizativo.

Cada actividad se desarrollará en línea mediante nuestro sistema de videoconferencia (Jitsi Meet) a través de <https://i.meet.mayfirst.org/MembershipMeeting>. *Nota: Para obtener los mejores resultados, conéctate a través de un teléfono Android o una computadora utilizando el navegador web Chrome o el navegador web Firefox. La mayoría de los iPhones y iPads no son capaces de utilizar nuestro sistema de interpretación.*

Todas las actividades contarán con interpretación simultánea en inglés y español.

Habilitaremos un hilo de conversación específico en el [Foro Discourse](https://comment.mayfirst.org/) para las personas que no puedan asistir a la sesión en línea o para quienes deseen continuar la conversación por este medio. Además, en breve contaremos con informes escritos de evaluación sobre nuestro trabajo. 

Agradecemos nos hagas saber las [reuniones a las que tienes previsto asistir](https://outreach.mayfirst.org/civicrm/event/register?id=90&reset=1).

### Calendario de las sesiones en línea 

* **Jueves 25 de enero, 14:00 - 15:00** (Nueva York), 13:00 - 14:00 (Ciudad de México), 19:00 - 20:00 (UTC) \
  *Infraestructura y servicios tecnológicos* \
  Si quieres saber más sobre nuestra tecnología y servicios actuales, ayudar a planificar formas para que el movimiento participe en su desarrollo y planificar nuestro desarrollo futuro, acompáñanos y conoce al equipo de TIAS. De esta reunión saldrán prioridades para el próximo año. \
  [Descargar archivo de calendario 📅](tias.ics) | [Enlace Google Calendar](https://calendar.google.com/calendar/render?action=TEMPLATE&text=May%20First:%20Technology,%20Infrastructure%20and%20Services&dates=20240125T190000Z/20240125T200000Z&details=If%20you%20want%20to%20learn%20more%20about%20our%20current%20technology%20and%20services,%20help%20plan%20how%20to%20engage%20the%20movement%20in%20their%20development,%20and%20plan%20our%20future%20development,%20join%20us%20and%20meet%20the%20TIAS%20team.%20This%20meeting%20will%20produce%20about%206%20-%208%20priorities%20for%20the%20coming%20year.%20&location=https://i.meet.mayfirst.org/MembershipMeeting&trp=true) | [Participar a través del foro en línea](https://comment.mayfirst.org/t/2024-membership-meeting-tias-reunion-de-miembros-2024-tias/2564)
* **Jueves 8 de febrero, 14:00 - 15:00** (Nueva York), 13:00 - 14:00 (Ciudad de México, 19:00 - 20:00 UTC) \
  *Participación y comunicación*
  ¿Cómo conseguimos la participación del movimiento y transmitimos la importancia de la tecnología guiada por los propios movimientos? ¿Cómo alineamos nuestros proyectos con el momento político actual? Colabora en la elaboración de estrategias. De esta reunión saldrán prioridades para este año. \
  [Descargar archivo de calendario 📅](engagement.ics) | [Enlace Google Calendar](https://calendar.google.com/calendar/render?action=TEMPLATE&text=May%20First:%20Engagement%20and%20Communications&dates=20240208T190000Z/20240208T200000Z&details=How%20do%20we%20engage%20the%20movement%20and%20communicate%20the%20importance%20of%20movement-controlled%20technology?%20How%20do%20we%20align%20our%20projects%20with%20the%20current%20political%20moment?%20Help%20us%20strategize%20for%20next%20year.%20This%20meeting%20will%20produce%20about%206%20-%208%20priorities%20for%20the%20coming%20year.%20&location=https://i.meet.mayfirst.org/MembershipMeeting&trp=true) | [Participar a través del foro en línea](https://comment.mayfirst.org/t/2024-membership-meeting-engagement-and-communication-reunion-de-la-mebresia-2024-participacion-y-comunicacion/2565)
* **Jueves 15 de febrero, 14:00 - 15:00** (Nueva York), 13:00 - 14:00 horas (Ciudad de México), 19:00 - 20:30 (UTC)
  *Análisis del contexto político actual* \
  Participa en un taller colaborativo para debatir los aspectos más importantes del momento político actual y ayúdanos a planificar lo que está por venir. \
  [Descargar archivo de calendario 📅](political-environment.ics) | [Enlace Google Calendar](https://calendar.google.com/calendar/render?action=TEMPLATE&text=Assessing%20the%20current%20political%20environment&dates=20240215T190000Z/20240215T200000Z&details=Join%20us%20in%20a%20collaborative%20workshop%20to%20discuss%20the%20most%20important%20aspects%20of%20the%20current%20political%20moment%20and%20help%20us%20plan%20for%20what’s%20to%20come.%20&location=https://i.meet.mayfirst.org/MembershipMeeting&trp=true) | [Participar a través del foro en línea](https://comment.mayfirst.org/t/2024-membership-meeting-political-environment-reunion-de-la-membresia-2024-entorno-politico/2566)
* **Jueves 29 de febrero, 14:00 - 15:00** (Nueva York), 13:00 - 14:00 (Ciudad de México), 19:00 - 20:00 (UTC) \
  *¿Quieres presentarte a la Junta Directiva?* \
Si te interesa participar en la dirección de May First, esta reunión es para ti. Compartiremos información para todas las personas interesadas en formar parte de la junta directiva sobre el funcionamiento de la dirección de May First, las expectativas de quienes forman parte de la junta, la historia de la organización y el compromiso de la organización con la lucha contra el racismo y el sexismo. Por favor, [revise nuestros materiales de orientación para la junta directiva](https://mayfirst.coop/es/orientación/). \
  [Descargar archivo del calendario 📅](run-for-board.ics) | [Enlace Google Calendar](https://calendar.google.com/calendar/render?action=TEMPLATE&text=Running%20for%20the%20May%20First%20Board&dates=20240229T190000Z/20240229T200000Z&details=If%20you%20are%20interested%20in%20helping%20direct%20May%20First,%20this%20workshop%20is%20for%20you.%20We%20will%20share%20information%20for%20all%20prospective%20board%20members%20about%20how%20May%20First’s%20leadership%20operates,%20expectations%20of%20board%20members,%20the%20history%20of%20the%20organization%20and%20the%20organization’s%20commitment%20to%20combating%20racism%20and%20sexism.%20Please%20review%20our%20board%20orientation%20materials:%20https://mayfirst.coop/en/orientation/&location=https://i.meet.mayfirst.org/MembershipMeeting&trp=true) | [Participar a través del foro en línea](https://comment.mayfirst.org/t/2024-membership-meeting-orientation-reunion-de-la-membresia-2024-orientacion/2567)

[Regístrete](https://outreach.mayfirst.org/civicrm/event/register?id=90&reset=1)!

## Elecciones a la Junta Directiva {#board-elections}

De acuerdo con nuestros estatutos, las personas trabajadoras eligen 5 puestos de la Junta Directiva y la membresía 20. De los 20, cada año elegimos aproximadamente 1/3 los cuales cumplen periodos de 3 años.

Además, destinamos un número proporcional de puestos a la membresía que pagan cuotas en México (alrededor del 21%).

Este año hay 5 puestos en la junta que pueden ser ocupados por cualquier
membresía de la organización (incluida la membresía mexicana). Sin embargo,
dada la proporción actual de la membresía mexicana (21%), todos los puestos de
la Junta *reservados* para la membresía en México están ocupados.

El periodo de nominación para la Junta Directiva será del 1 de febrero al 1 de marzo. Y las elecciones serán del 7 al 14 de marzo.

Si deseas postularte para la Junta Directiva, por favor asiste a la sesión de orientación de la Junta Directiva (ver arriba). También puede interesarte consultar nuestro [material de orientación para la Junta](/es/orientación).

A partir del 1 de febrero, puedes [nominarte a ti mismo o a otra persona miembro de May First que haya aceptado presentarse a través de este formulario.](https://vote.mayfirst.org/es/bienvenido).

## La reunión principal (o asamblea) {#main-meeting}

Por ley, es obligatorio celebrar una asamblea "oficial" a la que asistan al menos el 10% de nuestra membresía (alrededor de 68 integrantes este año).

**La reunión oficial de membresía tendrá lugar el jueves 7 de marzo, de 14:00 a 15:00 (Nueva York), 13:00 a 14:00 (Ciudad de México), 19:00 a 20:00 (UTC).**

[Descargar el archivo de calendario 📅](main.ics) | [Enlace Google Calendar](https://calendar.google.com/calendar/render?action=TEMPLATE&text=Main%20May%20First%20Membershp%20Meeting&dates=20240307T190000Z/20240307T200000Z&details=The%20main%20May%20First%20membership%20meeting%20for%202022.&location=https://i.meet.mayfirst.org/MembershipMeeting&trp=true) | [Participar a través del foro en línea](https://comment.mayfirst.org/t/2024-membership-meeting-main-meeting-reunion-de-la-membresia-2024-reunion-principal/2568)

Una vez que se haya registrado la asistencia y comprobado que tenemos quórum, podremos hacer propuestas para la aprobación de la membresía. A la hora de votar, las membresías individuales tienen un voto y las membresías organizacionales tienen dos votos. Aunque su organización envíe 10 personas a la reunión, sólo dispondrá de dos votos.

La agenda será:

1. **Propuesta sobre elecciones a la Junta Directiva**. Esta propuesta pedirá a las organizaciones miembros que aprueben la elección de la Junta Directiva mediante un sistema de votación en línea de una semana de duración para garantizar la participación del mayor número posible de representantes (en lugar de permitir votar únicamente a quienes estén presentes en ese momento). Véase a continuación para más detalles. 
2. **Reportes**. Habrá un informe financiero y dos informes de equipo, basados en las conversaciones mantenidas en las sesiones de intercambio previas a la reunión general. Los informes de los equipos harán hincapié en las prioridades de las que se hayan hecho eco en sus respectivas sesiones o en las líneas generales de la propuesta presentada.
   * Informe financiero
   * Participación y comunicaciones
   * Infraestructura y servicios tecnológicos

## Periodo de votación {#voting-period}

De manera constante May First enfatiza la importancia de la democracia interna y no creemos que una única reunión en línea sea la forma más inclusiva de elegir a quienes integrarán la Junta Directiva, ya que no es posible que toda la membresía asista.

Por ello, la Junta propondrá en la reunión de membresía que llevemos a cabo las elecciones a través de una votación en línea durante una semana después de la reunión: del 7 al 14 de marzo (utilizando el mismo [sitio de votación](https://vote.mayfirst.org/) donde hemos llevado a cabo nuestras elecciones durante la mayor parte de nuestra historia).

En esa votación para las elecciones a la Junta, también se pedirá a la membresía que clasifiquen las prioridades que se generaron durante las sesiones de intercambio.

## Información legal {#legal-details}

Nuestros nuevos estatutos exigen una asamblea anual en la que el 10% de nuestra membresía activa alcance el quórum. Esto significa que necesitamos la asistencia de al menos 68 integrantes de la membresía (el número exacto se determinará el 28 de febrero, fecha límite para votar). Para evaluar este requisito, contaremos la membresía, no las personas presentes en la asamblea. Invitamos a los grupos miembros a que traigan a tantas personas de su organización como deseen; sin embargo, contabilizaremos cada membresía una sola vez.

Este quórum es un requisito de la ley del Estado de Nueva York bajo la cual está constituida nuestra cooperativa.
