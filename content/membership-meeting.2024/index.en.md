+++ 
date = 2024-01-04T14:47:48Z
description = "Welcome to the May First Movement Technology Members' Meeting" 
title = "Voting has started" 
tags = [ "callout" ]
+++

**Table of Contents**

* [Voting has started](#vote)
* [Introduction](#introduction)
* [Discussion Meetings](#discussion-meetings)
* [Board Elections](#board-elections)
* [Main Meeting](#main-meeting)
* [Voting Period](#voting-period)
* [Legal Details](#legal-details)

## Voting has started {#vote}

Voting has started and will run through Thursday, March 14th. Results will be announced the week of March 18th.

If you have not received a voting token, please email `info@mayfirst.org`.

## Introduction {#introduction}

The past year stands out for the tragic and continuing violence in Gaza. But also, we have witnessed a tremendous mobilization against the war and occupation throughout our movement, including efforts by May First members and coalitions in which May First participates.
 
In the Americas, we are seeing unprecedented right wing populist movements vying for national power - some losing and some winning, with critical elections approaching in 2024.
 
In the technology world, the manipulation and profiteering of Big Tech is being continuously exposed, a reality firmly established even in the mainstream. However, new technologies such as Chat GPT provide yet more ways to feed dangerous systems of surveillance.

**Given our political environment, what has May First been doing in 2023? What are our plans for 2024? Join us at our membership meetings to find out and help plan our future!**

You can register for all events on our [event registration
page](https://outreach.mayfirst.org/civicrm/event/register?id=90&reset=1).

Please take a look at our schedule and **commit to attending at least one
discussion session and the main membership meeting**.

<!--
<a href="https://mayfirst.coop/img/2022-MFMT-membership-meeting.en.png" target="_blank">
  <img width="300" height="600" border="0" align="left" hspace="20" src="/img/2022-MFMT-membership-meeting.en.png">
</a>
-->


<!--
Click on the image to open our membership meeting infographic.
<br clear="left" />
-->

## Discussion Meetings {#discussion-meetings}

*All activities are scheduled for 1 hour.*

In addition to the main meeting (see below), we will hold the following four
discussion sessions. Members are welcome to attend all sessions, or you can
choose to attend just the sessions you consider most relevant to your
organizing work.

Each activity will take place online via our video conference system (Jitsi
Meet) via <https://i.meet.mayfirst.org/MembershipMeeting>. *Please note: For
best results, please connect via an Android phone or a desktop computer using
either the Chrome web browser or the Firefox web browser. Most iPhones and
iPads are not capable of using our interpretation system.*

All activities will have live, simultaneous interpretation between english and
spanish.

We will also have a corresponding forum conversation via
[discourse](https://comment.mayfirst.org/) for people unable to attend the
online session or who wish to continue the conversation.

Additionally, written reports evaluating our work will be available soon.

Please [let us know which activities you plan to attend](https://outreach.mayfirst.org/civicrm/event/register?id=90&reset=1).

### Schedule of Online Sessions {#schedule}

* **Thursday, January 25th, 2:00 - 3:00 pm** (New York), 1:00 pm - 2:00 pm (Mexico City), 7:00 - 8:00 pm (UTC) \
  *Technology Infrastructure and Services* \
  If you want to learn more about our current technology and services, help plan how to engage the movement in their development, and plan our future development, join us and meet the TIAS team. This meeting will produce priorities for the coming year. \
  [Download calendar file 📅](tias.ics) | [Google Calendar Link](https://calendar.google.com/calendar/render?action=TEMPLATE&text=May%20First:%20Technology,%20Infrastructure%20and%20Services&dates=20240125T190000Z/20240125T200000Z&details=If%20you%20want%20to%20learn%20more%20about%20our%20current%20technology%20and%20services,%20help%20plan%20how%20to%20engage%20the%20movement%20in%20their%20development,%20and%20plan%20our%20future%20development,%20join%20us%20and%20meet%20the%20TIAS%20team.%20This%20meeting%20will%20produce%20about%206%20-%208%20priorities%20for%20the%20coming%20year.%20&location=https://i.meet.mayfirst.org/MembershipMeeting&trp=true) | [Participate via the online forum](https://comment.mayfirst.org/t/2024-membership-meeting-tias-reunion-de-miembros-2024-tias/2564) 
* **Thursday, February 8th, 2:00 - 3:00 pm** (New York), 1:00 - 2:00 pm (Mexico City), 7:00 - 8:00 pm (UTC) \
 *Engagement and Communications* \
  How do we engage the movement and communicate the importance of movement-controlled technology? How do we align our projects with the current political moment? Help us strategize for next year. This meeting will produce priorities for the coming year. \
  [Download calendar file 📅](engagement.ics) | [Google Calendar Link](https://calendar.google.com/calendar/render?action=TEMPLATE&text=May%20First:%20Engagement%20and%20Communications&dates=20240208T190000Z/20240208T200000Z&details=How%20do%20we%20engage%20the%20movement%20and%20communicate%20the%20importance%20of%20movement-controlled%20technology?%20How%20do%20we%20align%20our%20projects%20with%20the%20current%20political%20moment?%20Help%20us%20strategize%20for%20next%20year.%20This%20meeting%20will%20produce%20about%206%20-%208%20priorities%20for%20the%20coming%20year.%20&location=https://i.meet.mayfirst.org/MembershipMeeting&trp=true) | [Participate via the online forum](https://comment.mayfirst.org/t/2024-membership-meeting-engagement-and-communication-reunion-de-la-mebresia-2024-participacion-y-comunicacion/2565)
* **Thursday, February 15th, 2:00 - 3:00 pm** (New York), 1:00 - 2:00 pm (Mexico City), 7:00 - 8:00 pm (UTC) \
  *Assessing the current political environment* \
  Join us in a collaborative workshop to discuss the most important aspects of the current political moment and help us plan for what's to come. \
  [Download calendar file 📅](political-environment.ics) | [Google Calendar Link](https://calendar.google.com/calendar/render?action=TEMPLATE&text=Assessing%20the%20current%20political%20environment&dates=20240215T190000Z/20240215T200000Z&details=Join%20us%20in%20a%20collaborative%20workshop%20to%20discuss%20the%20most%20important%20aspects%20of%20the%20current%20political%20moment%20and%20help%20us%20plan%20for%20what’s%20to%20come.%20&location=https://i.meet.mayfirst.org/MembershipMeeting&trp=true) | [Participate via the online forum](https://comment.mayfirst.org/t/2024-membership-meeting-political-environment-reunion-de-la-membresia-2024-entorno-politico/2566)
* **Thursday, February 29, 2:00 - 3:00 pm** (New York), 1:00 - 2:00 pm (Mexico City), 7:00 - 8:00 pm (UTC) \
  *Are you interested in running for the Board of Directors?* \
  This year, 5 seats are up for re-election! If you are interested in helping direct May First, this workshop is for you. We will share information for all prospective board members about how May First's leadership operates, expectations of board members, the history of the organization and the organization's commitment to combating racism and sexism. Please [review our board orientation materials](https://mayfirst.coop/en/orientation/). \
  [Download calendar file 📅](orientation.ics) | [Google Calendar Link](https://calendar.google.com/calendar/render?action=TEMPLATE&text=Running%20for%20the%20May%20First%20Board&dates=20240229T190000Z/20240229T190000Z&details=If%20you%20are%20interested%20in%20helping%20direct%20May%20First,%20this%20workshop%20is%20for%20you.%20We%20will%20share%20information%20for%20all%20prospective%20board%20members%20about%20how%20May%20First’s%20leadership%20operates,%20expectations%20of%20board%20members,%20the%20history%20of%20the%20organization%20and%20the%20organization’s%20commitment%20to%20combating%20racism%20and%20sexism.%20Please%20review%20our%20board%20orientation%20materials:%20https://mayfirst.coop/en/orientation/&location=https://i.meet.mayfirst.org/MembershipMeeting&trp=true) | [Participate via the online forum](https://comment.mayfirst.org/t/2024-membership-meeting-orientation-reunion-de-la-membresia-2024-orientacion/2567) 

Please [register](https://outreach.mayfirst.org/civicrm/event/register?id=90&reset=1)!

## Board Elections {#board-elections}

According to our bylaws, 5 board seats are chosen by workers and 20 are elected by the membership. Of the 20, we elect approximately 1/3 each year for 3 year terms.

In addition, we reserve a proportionate number of seats for members who pay dues in Mexico (about 21 percent).

This year **there are five board seats that can be filled by any member of the organization** (including members in
Mexico). Given the current ratio of Mexican members (21%), all *reserved* board
seats for a member paying dues in Mexico are filled.

The board nomination period will run from February 1st through March 1st. And the election will run from March 7th - March 14th.

If you would like to run for the board, please attend the board/orientation session (see above). You may also be interested in reviewing our [board orientation materials](/orientation).

Starting February 1, you can [nominate yourself or another May First member who has agreed to run via this form](https://vote.mayfirst.org/).

## The Main Meeting {#main-meeting}

By law, we are required to hold an "official" membership meeting with attendance by at least 10% of our members (about 68 members this year).

**The official membership meeting will take place on Thursday, March 7th, from 2:00 - 3:00 pm (New York), 1:00 - 2:00 pm (Mexico City), 7:00 - 8:00 pm (UTC)**

[Download calendar file 📅](main.ics) | [Google Calendar Link](https://calendar.google.com/calendar/render?action=TEMPLATE&text=Main%20May%20First%20Membershp%20Meeting&dates=20240307T190000Z/20240307T200000Z&details=The%20main%20May%20First%20membership%20meeting%20for%202022.&location=https://i.meet.mayfirst.org/MembershipMeeting&trp=true) | [Participate via the online forum](https://comment.mayfirst.org/t/2024-membership-meeting-main-meeting-reunion-de-la-membresia-2024-reunion-principal/2568)

Once attendance is taken and we have determined that we have quorum, we will be able to make proposals for member approval. When voting, individual members get one vote and organizational members get two votes. Even if your organization sends 10 people to the meeting, you still only get two votes.

The agenda is:

1. **Proposal on Board elections.** This proposal will ask the members to approve electing the board through a one week online ballot system to ensure as many members as possible can participate (rather than only allowing the members present on the call to vote). See below for more details.
2. **Reports.** There will be one financial report and two team reports, based on the conversations at the member discussion sessions that precede the Members' meeting. The team reports will emphasize the priorities echoed in their respective sessions or the general outline of the proposal being put forward.
   * Financial Report
   * Engagement and Communications
   * Technology Infrastructure and Services

## The Voting Period {#voting-period}

May First has always emphasized the importance of internal democracy and we don't feel a single online meeting is the most inclusive way to elect Board members since not all members will be able to attend.

So the Board will propose to the membership meeting that we conduct the elections via an online ballot (using the exact same [voting site](https://vote.mayfirst.org/) where we have conducted our elections for most of our history) for a week following the meeting: March 7th through March 14th.

In that Board elections ballot, members will also be asked to rank the priorities that were generated during discussion sessions.

## Legal Details {#legal-details}

Our new bylaws require a yearly meeting of members at which 10 percent of our active membership makes up a quorom. This means we need attendance by at least 68 members (the exact number will be determined on February 28th, the cut-off date for voter eligibility). To evaluate this requirement, we will be counting memberships, not people on the call. We invite members to bring as many people from your organization as you want, however, we can only count each membership once when taking attendance.

This quorum is a requirement of New York State law under which our cooperative is incorporated.
