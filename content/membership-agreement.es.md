+++
date = 2015-10-01T13:54:39Z
title = "Acuerdo de membresía"
slug = 'acuerdo-membresia'
[menu]
  [menu.more]
    parent = "Membresía"
    weight = 30
+++

Primero de Mayo/Enlace del Pueblo es una organización muy diversa y nuestra
valores y así como el documento de Beneficios para l@s miembros
reflejan nuestro objetivo de unir a l@s activistas por la justicia social del
movimiento progresista. En ese contexto, nuestr@s miembros esperan algunas
cosas de l@s demás, así como del personal técnico. ¿Qué pueden esperar los
miembros de nuestra organización y del personal técnico:

 * Nuestra organización actualiza, mantiene y amplia todos los equipos y
   software de uso compartido considerando las necesidades de l@s miembros y la
   capacidad de la infraestructura.
 * Nuestra organización se asegura de que todas las redes y los sistemas son
   funcionales y que, en el caso de mal funcionamiento de un servidor o un
   problema similar, nuestro personal dará prioridad a las reparaciones,
   correcciones y sustituciones de equipo cuando sea necesario.
 * Nuestro personal brinda apoyo técnico, de manera pronta y respetuosa a
   través de nuestro sistema público de seguimiento de solicitudes.
 * Nuestr@s directores/as se comunican con tod@s l@s miembros de manera regular
   y abierta sobre cuestiones de organización y desarrollo. También les
   reportan cualquier situación extraordinaria, el estado de operación y lo que
   estamos haciendo para atender emergencias cuando éstas surgen.
 * Nuestra organización protegerá sus datos y opondrá fuerte resistencia a
   cualquier intento (legal o no-legal) no autorizado de acceder a ellos.
   Mientras que para contar con la información debidamente resguardada se
   ejecutan todas las noches copias de seguridad por duplicado en el sitio
   físico que aloja los servidores y fuera de éste.

Lo que nuestra organización y el personal espera de nuestr@s miembros:

 * Como miembros, nosotr@s respetamos los límites del buen gusto, la decencia y
   el respeto a todas las personas y los pueblos y esperamos que ese respeto se
   refleje en el contenido de los sitios web y otras comunicaciones.
 * L@s miembros pagan sus cuotas a tiempo como una de nuestras principales
   formas de apoyar a nuestra organización (véase la política de pago y
   cobertura de las cuotas más abajo).
 * May First envía avisos a los afiliados por correo electrónico, incluyendo
   mensajes técnicos, de facturación y de organización política. Los miembros
   son responsables de mantener su información de contacto de correo
   electrónico actualizada y asegurarse de que el correo electrónico de May
   First se canalice adecuadamente a las personas adecuadas.
 * L@s miembros apoyan entre sí el trabajo de otr@s miembros cuando y de la
   manera en la que podemos hacerlo, ofreciendo los programas que hemos
   desarrollado y entrelazando estrategias, metas y recursos.
 * L@s miembros tratan a nuestro equipo técnico con respeto y sensibilidad
   sobre la enorme cantidad de trabajo con la que contribuyen a nuestra
   organización.
 * L@s miembros participan en el crecimiento de nuestra organización invitando
   a otras personas afines a unirse.
 * L@s miembros aceptan la responsabilidad de mantener, mejorar, arreglar y
   modificar el software y sitios web que operan para restringirlos al tipo de
   membresía que han asumido.

Si tienes preguntas, comentarios o desacuerdos, después de leer ésto, por favor
no dudes en hacérnoslo saber.

## Política de pago de cuotas

Cuando se trata de cuotas, May First da prioridad a la comunicación. Por favor,
háganos saber si no puede pagar sus cuotas y podemos elaborar un plan. Si las
cuotas Si las cuotas son demasiado altas, la junta de May First tiene un Comité
de Uso de Recursos diseñado para para atender estas necesidades.

May First envía recordatorios de pago por correo electrónico a partir de 2
semanas antes de su fecha de vencimiento de pago y enviará al menos 4
recordatorios de vencimiento.

May First ofrece un generoso período de gracia de 3 meses para los miembros
atrasados en sus durante el cual sus servicios de alojamiento seguirán siendo
totalmente funcionales.

Después de tres meses, May First desactivará sus servicios de alojamiento. Si
te pones en contacto Si te pones en contacto con nosotros para establecer una
línea de comunicación, volveremos a habilitar tus servicios mientras se
resuelve un plan de pago.

Si no tenemos noticias suyas durante 3 meses después de desactivar su
membresía, cancelaremos su cuenta y eliminaremos sus datos. cancelaremos su
cuenta y eliminaremos sus datos.

## Política de alojamiento

Los miembros de May First tienen derecho a alojar tantos sitios web,
direcciones de correo electrónico bases de datos, etc., siempre que no superen
el espacio de disco asignado. designado.

Sin embargo, como organización democrática de miembros, necesitamos que todas
las organizaciones y que utilicen nuestros recursos sean miembros por derecho
propio. Si los miembros albergan recursos de otras organizaciones o individuos
bajo su propia afiliación socava nuestro modelo democrático y nos impide
recaudar los recursos que necesitamos para construir y mantener nuestra
infraestructura.

Nuestra política es flexible. Por ejemplo, los miembros individuales son
bienvenidos a alojar sitios de otros miembros de la familia y los
desarrolladores web deben sentirse libres de alojar sitios sitios de clientes
bajo sus membresías.

Sin embargo, pedimos a todos los miembros que consideren la mejor manera de
apoyar a la organización cuando tomen estas decisiones.

