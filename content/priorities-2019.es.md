+++
date = 2018-12-24T14:47:48Z
description = "Prioridades"
title = "Prioridades"
+++

A diferencia de los años anteriores, tuvimos un referéndum especial para los miembros de sólo unos pocos
los artículos que se aprobaron con los siguientes votos:

1. Formar un comité para explorar la posibilidad de convertir nuestra organización en un
   Cooperativa de titulares:

sí: 162
no: 9

2. Formar un comité para explorar formas de hacer crecer significativamente la organización a un
   un tamaño más sostenible

sí: 160
no: 11

3. Posponer las elecciones del comité de liderazgo hasta que votemos sobre el Coop.

sí: 136
no: 35
