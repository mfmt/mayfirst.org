+++ 
title = "AMC 2020 Session - Who's Internet? Our Internet!" 
date = 2020-07-24T13:53:34Z
+++


What is the relationship between our movement and technology? It's been
complex, challenging and enormously productive. It's now also being threatened
at the very moment when its potential is greatest. *To protect it from the
attacks against it and to realize the potential of our relationship with
technology, we need to study how this relationship got to this place and what
that means for our movement and for technology.*

At the 2020 Allied Media Conference over 50 activists came together online to
build a collective timeline documenting this relationship - the past, present
and even the future.

We first reviewed an [initial timeline](https://progressivetech.org/timeline)
which documents some of the most significant milestones in our movement's
relationship with the Internet.

Next, we asked participants to break into small groups and complete the
timeline adding all the important events in the past and also imagined events
in the future that have and will affect our movement's relationship to the
Internet. Participants used the [excalidraw](https://excalidraw.com/)
collaborative drawing program to edit the canvas together in real time.

Please [browse the collective visual timeline we created](/files/amc2020-timeline.pdf):

[![Timeline Thumbnail](/files/amc2020-timeline.png)](/files/amc2020-timeline.pdf)

Or, watch the session!

<video controls width="512" height="289">
  <source src="/media/en/video/amc-2020-whose-internet-presentation.webm">
</video>


