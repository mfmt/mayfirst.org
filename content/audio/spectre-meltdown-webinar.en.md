+++
date = "2018-01-25T01:33:49Z" 
title = "Spectre Meltdown (Need to Know)"
webm = "media/en/audio/spectre-meltdown-webinar.webm"
mp3 = "media/en/audio/spectre-meltdown-webinar.mp3"
type = "audio"
podcast_duration = "1:04:56"
bytes = "41329055"
+++

May First/People Link technologists Jaime Villareal and Jamie McClelland explain this latest major vulnerability, ways to deal with it and what we’re doing about it.

