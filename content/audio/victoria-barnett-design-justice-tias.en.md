+++
date = "2021-06-25T04:25:08Z"
title = "Victoria Barnett - Design Justice"
mp3 = "media/en/audio/victoria-barnett-design-justice-tias.mp3"
webm = "media/en/audio/victoria-barnett-design-justice-tias.webm"
type = "audio"
podcast_duration = "0:50:15"
bytes = "41385880"
+++

### Victoria Barnett - Design Justice

**2021-06-18:** 
Victoria Barnett introduces us to the Design Justice Principles and her work supporting the growth of the [Design Justice Network](https://designjustice.org) . [Follow the pdf presentation](https://share.mayfirst.org/s/THxippnkHrdbppQ)

![Victoria Barnett](/img/victoria.jpg "Victoria") 

[Victoria Barnett](http://victoriabarnett.com/) is a digital graphic designer, facilitator, community organizer and collaborator at the service of social justice initiatives.  Her work is based on the Principles of Design Justice.


Additional references:

* [How the design justice principles were generated](https://designjustice.org/news-1/2016/05/02/2016-generating-shared-principles), See venn diagram exercise.
* [Useful questions to ask](https://designjustice.org/s/DESIGNJUSTICEZINE_ISSUE2.pdf), see page 7
* [Tips on how to organize a local node](https://designjustice.org/how-to-organize-local-node) Can apply to Working group
* [Zines](https://designjustice.org/zines)

<p>

---