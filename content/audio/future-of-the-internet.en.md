+++
date = "2020-07-08T20:10:08Z" 
title = "The Internet: Planning the Future"
mp3 = "media/en/audio/future-of-the-internet.mp3"
webm = "media/en/audio/future-of-the-internet.webm"
type = "audio"
podcast_duration = "0:58:45"
bytes = "32154881"
+++

The crises in our society have made clear not only how important the Internet
is but how we must change it. Our culture of large meetings and conferences,
our dependence on corporate communications and our lack of vision for the
future now show themselves as serious political problems. We simply can’t go on
doing things the way we are used to doing them and people in all our movements
are identifying the challenges and discussing ways to deal with them.

How we organize and how we communicate is going to have to change in
fundamental ways and those changes will not only allow us to continue our work
more effectively but give us an opportunity to build visioning bridges into the
future.

In this July Need to Know, activists involved in answering these challenges
discuss with the attendees: the changes in collaboration today, including
meeting and converging; developing an alternative to the Internet; and
sketching a future based on this technology.

It’s an exercise in visioning based on the very real challenges we are all
facing.
