+++
date = "2021-01-28T22:33:10Z" 
title = "The Congressional Invasion, the Biden Presidency: What happens now?"
mp3 = "media/en/audio/congressional-invasion-and-biden.mp3"
webm = "media/en/audio/congressional-invasion-and-biden.webm"
type = "audio"
podcast_duration = "0:39:44"
bytes = "23161086"
+++

Biden is President. The Congress is over-run by right-wing thugs. We are in a
new era. Now what? What do we demand of this new administration? What should we
expect from this new situation? What do we think this new administration is
going to do...what should we be doing?

Presentations by Jerome Scott, Maritza Arrastia and Ken Montenegro, followed by
small group discussions (not recorded) followed by report backs.

[Unfortunately the beginning of the session is missing. It picks up just after
Jerome begins his presentation.]
 
