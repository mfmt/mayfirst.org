+++
date = "2021-08-22T00:00:11Z" 
title = "Puerto Rican Socialist Party Oral History Need To Know"
mp3 = "media/en/audio/Puerto-Rican_Socialist-Party-Oral-History-Need-to-Know.mp3"
webm = "media/en/audio/Puerto-Rican_Socialist-Party-Oral-History-Need-to-Know.webm"
type = "audio"
podcast_duration = "1:07:02"
bytes = "36603787"
+++

During the 1970's the Puerto Rican Socialist Party was the largest and most
powerful Puerto Rican revolutionary organization in the U.S. and a major force
within the U.S. Left. A recently published book ([*Revolution Around the
Corner: Voices from the Puerto Rican Socialist Party in the United
States*](https://outreach.mayfirst.org/civicrm/mailing/url?u=1032&qid=1117613)),
written by people involved in the PSP then, offers a lesson-filled oral history
of the organization and its work. This Need to Know features some of those
people.

This is an event every person interested in changing this country should
attend. The lessons of that period and that organization are of great value
today.

The slide show referenced in the talk is [available for
download](/files/psp-need-to-know-slide-show.pdf).
