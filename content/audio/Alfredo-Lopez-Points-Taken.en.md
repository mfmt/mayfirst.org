+++
date = "2018-06-06T19:55:07Z" 
title = "Alfredo Lopez Points Taken"
mp3 = "media/en/audio/Alfredo-Lopez-Points-Taken.mp3"
webm = "media/en/audio/Alfredo-Lopez-Points-Taken.webm"
type = "audio"
podcast_duration = "0:29:40"
bytes = "17425377"
+++

Lucas talks with May First/People Link Co-Founder Alfredo Lopez about technology, how it affects and is affected by the political situation in the country, and what plans are being made to address these issues.

