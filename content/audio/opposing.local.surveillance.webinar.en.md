+++
date = "2016-07-13T01:33:49Z" 
title = "Opposing Local Surveillance Webinar"
webm = "media/en/audio/opposing.local.surveillance.webinar.webm"
mp3 = "media/en/audio/opposing.local.surveillance.webinar.mp3"
type = "audio"
podcast_duration = "1:09:31"
bytes = "42759424"
+++

Webinar featuring: 

 * Hamid Khan – Stop LAPD Spying Coalition
 * Dennis Flores – El Grito de Sunset Park
 * Jacobo Najera – May First/People Link - Mexico
 * Manisha Desai – May First/People Link and University of Connecticut
