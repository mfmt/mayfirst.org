+++
date = 2024-05-12
title = "Webinar: El impacto de la inteligencia artificial en los movimientos sociales"
slug = "ia-movimientos-sociales"
mp3 = "media/es/audio/ai-social-movements.es.mp3"
webm = "media/es/audio/ai-social-movements.es.webm"
type = "audio"
podcast_duration = "00:53:11"
bytes = "46634160"
tags = [ "callout "]
+++

**Grabado: Jueves 30 de mayo, 2024**

<img src="paola.png" style="float:left;max-width:300px" alt="Paola picture">

Cada tecnología, incluidas las digitales, son producto de una ideología
política. La mayoría de las veces invisibilizada.

Durante 2023 las **inteligencias artificiales** estaban por todos lados. Escuchamos
hablar de ellas en diversidad de espacios: artísticos y de creación, de
investigación social, médicos, laborales, estatales. Escuchamos sobre sus
potencialidades, alcances, sesgos, problemáticas, aspiraciones, así como de la
"inevitabilidad" de su presencia.

*¿Cómo y dónde se posicionan los movimientos sociales frente a estos temas? ¿Qué
tan cercanos nos resultan sus problemáticas pero también sus impactos y
oportunidades?* En este conversatorio invitamos a **Paola Ricaurte** quien recupera
la noción de la colonialidad del poder para proponer una visión del desarrollo
de IA que articule los aspectos macro y micropolíticos del desarrollo
tecnológico como parte de un **proyecto descolonial y feminista.**

Paola Ricaurte es investigadora del Departamento de Medios y Cultura Digital
del Tecnológico de Monterrey. Además, es profesora asociada del Berkman Klein
Center for Internet & Society de la Universidad de Harvard y cofundadora de
Tierra Común, una red de académicos, profesionales y activistas interesados en
la descolonialidad y los datos. Coordina el nodo de América Latina y El Caribe
de la Red Feminista de Investigación en Inteligencia Artificial.

## Preguntas y comentarios

A continuación se recogen algunas de las preguntas y comentarios que les participantes compartieron en el chat y documento de trabajo, durante la sesión:

> ¿Existe un término mejor que IA, ya que ésta no es inteligencia? ¿memoria integrada?

IA es un término muy desafortunado para nuestra época, pero muy productivo para
la industria y para quienes están interesados en construir un imaginario de la
IA como una tecnología unificada y superpoderosa. Este término refleja los
deseos y aspiraciones de los hombres blancos a los que a menudo se atribuye la
creación del campo y el apoyo al desarrollo de estas tecnologías. Otros
términos no son tan poderosos a la hora de construir este imaginario. De hecho,
muchos proponen utilizar "aprendizaje automático" en lugar de IA, porque la
mayoría de las veces nos referimos a este tipo específico de sistemas de IA. La
IA como concepto es una simplificación excesiva de un conjunto de tecnologías
heterogéneas que contribuye a reforzar este imaginario construido.

> Es importante señalar que la "IA" actual se basa en modelos digitales
> (simulaciones) de cómo funciona el cerebro orgánico, biológico, es decir, la
> red neuronal. Esto no funciona por operaciones lógicas, sino por
> "entrenamiento", basado en la exposición repetida a muchas entradas, creando
> capacidad de reconocimiento de patrones, pero sin permitir conocer qué datos
> de entrenamiento contribuyeron al entrenamiento, ni cuánto contribuyeron, ni
> qué aspectos o elementos de los estímulos de los datos de entrenamiento
> permitieron reconocer el patrón. Esto se centra ahora en los "grandes modelos
> lingüísticos", o LLM, de procesamiento del lenguaje, que pueden entrenarse
> para que suenen como una persona inteligente hablando, pero que no son más
> que un conjunto estadísticamente construido de resultados similares a los del
> lenguaje. Emily Bender lo llama "loros mecánicos". Pero la tecnología podría
> prestarse a aplicaciones mucho más valiosas, si pensamos en ella como si
> entrenáramos a un perro (o a un bebé), ¿cómo podríamos entrenar a un modelo
> organizativo para que apoyara un comportamiento feminista igualitario en
> nuestro trabajo? ¿Cómo podríamos entrenar un sistema de este tipo para apoyar
> la gestión de enormes cantidades de datos generados por una comunidad, de tal
> manera que la comunidad pueda utilizarlos para aumentar la autoconciencia de
> la comunidad, apoyar la intencionalidad de la comunidad, reflejando los
> patrones que el sistema encuentra emergiendo en las interacciones colectivas
> de la comunidad, tanto económicas como sociales?

> ¿Ves algún potencial liberador para la IA? En caso afirmativo, ¿en qué casos?

Hay una realidad que no podemos ignorar: la industria de la IA está actualmente
controlada por un grupo muy reducido de personas con gran poder. Es difícil
siquiera imaginar cómo influyen estos individuos en la economía, la política,
la legislación, la investigación y la defensa. Para mí, la pregunta es:
¿podemos nosotros, nuestras comunidades, desarrollar tecnologías -incluida la
IA- que se basen en valores que no estén impulsados por el mercado y que
permitan a las personas ser agentes de su propio destino, alcanzando el derecho
a vivir con dignidad? Y creo que la respuesta es sí. El problema es cómo crear
las condiciones para que las personas participen en el desarrollo de las
tecnologías que necesitan. Hacen falta tecnologías gobernadas, diseñadas,
producidas y mantenidas por las personas que siempre han estado excluidas del
desarrollo tecnológico y de la sociedad en general.

> ¿Se imagina cómo una organización como Mayfirst podría utilizar la IA para
> construir comunidad?

Supongo que se trata de decidir e imaginar colectivamente cómo estas
tecnologías pueden mejorar el potencial de transformación de las comunidades.
Por ejemplo, estamos apoyando un proyecto en el que las comunidades indígenas
están recopilando datos de sus territorios para ayudarles a entender los
cambios en medio de una profunda escasez de agua. Están desarrollando
herramientas basadas en las formas comunitarias y ancestrales de gobernar el
agua y responder a sus necesidades específicas. Creo que cada comunidad debe
decidir qué tecnologías le convienen y para qué fines.

> ¿Puede alguien incluir en el chat o en el correo electrónico de seguimiento
> los nombres de los proyectos de IA que Paola compartió y que ella y otras
> comunidades están desarrollando?

Nuestros proyectos están publicados en el sitio web de la Alianza A+
(aplusalliance.org), pero algunos de ellos tienen sus propios sitios web. Por
ejemplo, [EDIA](https://huggingface.co/spaces/vialibre/edia), y Aymurai
[aquí](https://sites.google.com/view/aymurai-en) y
[aquí](https://github.com/AymurAI). Además, tenemos una red llamada [Tierra
Común](https://tierracomun.net) centrada en la lucha contra la colonización de
datos, y otro proyecto con la organización mexicana
[Sursiendo](https://sursiendo/) que explora [las consecuencias ambientales del
desarrollo tecnológico](https://tecnoafecciones.net).

> ¿Existen escuelas u organizaciones que ofrezcan formación técnica para que
> las comunidades aprendan sobre IA, web3, programación, UX, etc.? ¿Deberían
> las organizaciones invertir en este tipo de formación para trabajadores y
> participantes?

Son preguntas muy interesantes, y cada una de ellas son áreas de la tecnología
diferentes, aunque relacionadas. Dicho esto, hay organizaciones que han
trabajado en algunos aspectos de esta cuestión. Detroit Digital Justice y
Community Network Development Kit son algunos de los proyectos en los que ha
trabajado gente de nuestra órbita. En cuanto a si las organizaciones deben
invertir... Creo que este es un tema en el que el movimiento necesita invertir
en conocimiento e infraestructura, pero no sé si todo el mundo necesita esta
capacidad para hacer su trabajo, y los diferentes proyectos lo abordarán de
manera diferente en función de su trabajo. La otra cuestión de recursos es que
si queremos hacer cosas con IA en hardware perteneciente a los movimientos
sociales, vamos a necesitar encontrar más recursos para proyectos como
Mayfirst. Por último, he empezado a investigar cómo utilizar/entrenar/ampliar
la IA de código abierto, y quizá pueda compartir lo que aprenda).

Estoy planeando investigar la utilidad de los modelos de código abierto en
huggingface para ver si podríamos construir un modelo de servicio al cliente
inverso, donde la gente de la organización entrena colectivamente (a través de
la interacción y el refuerzo) un modelo para ayudarles en su trabajo.

> ¿Cuáles pueden ser las estrategias de resistencia contra el colonialismo de
> la IA que ataca nuestros pensamientos, vidas, cultura y comunidades y nos
> aliena del mundo real, la sociedad y la naturaleza?

Históricamente, hemos encontrado muchas formas de resistir al colonialismo, la
violencia y la exclusión. Personas de todo el mundo están resistiendo, a un
coste muy alto, y de diferentes maneras. Hay ejemplos asombrosos en todas
partes, todos los días, de gente que pone su energía en sostener la vida. Creo
que la resistencia es algo fluido, heterogéneo y dinámico. A veces significa
organizarnos y exigir nuestros derechos como movimientos en las calles, y a
veces significa hacer una pausa para cuidarnos y volver a reunirnos para unir
fuerzas de nuevo. Pero si formamos parte de un movimiento, debemos recordar que
la resistencia siempre tiene que ser un esfuerzo colectivo.

> ¿Realmente pensamos que existe una forma de "construir" una inteligencia
> artificial "buena"? ¿Es realmente posible imaginar una tecnología que no
> produzca desplazamientos, destrucción de la tierra, robo de datos,
> condiciones de trabajo similares a la esclavitud? Y si en lugar de
> rehacer/recuperar las herramientas del enemigo, ¿podríamos poner nuestras
> energías en reconstruir comunidades, relaciones, individuos que "necesitan"
> cada vez menos las tecnologías?

Estoy totalmente de acuerdo en que nuestras energías deberían centrarse
especialmente en reconstruir nuestras comunidades, nuestras relaciones entre
nosotras mismas, con las demás y con el planeta. Para mí, el principal problema
al que nos enfrentamos como humanidad es la pérdida de comprensión de que nos
necesitamos entre nosotres para sobrevivir en este planeta. Mi preocupación con
respecto a la IA es que estas herramientas se están promoviendo como el medio
dominante para controlar las sociedades y concentrar el poder y la riqueza. Si
no reaccionamos ante esto como movimientos sociales, si no nos oponemos a este
régimen de IA, corremos el riesgo de despertarnos un día sin la posibilidad de
vivir de una manera que sea buena para nosotras y nuestras comunidades.

> La usabilidad de la IA para la reterritorialización y expansiones urbanas,
> modelando territorios al servicio del gran capital financiero y urbano,
> afectando derechos colectivos y ambientales; utilizada sin control por
> gobiernos municipales y corporaciones empresariales sin cumplir con la
> función social y ecológica: sólo para ganar dinero.

> Pero qué podemos hacer, sería genial pensarlo en el marco de la cooperativa,
> no sólo en abstracto: ¿Cómo puede una cooperativa como May First aprovechar
> la IA?

El consejo más común que escucho en las luchas por la liberación es
simplemente "organizate, organizate, organizate": ¿qué oportunidades
potenciales podría haber para utilizar la IA para permitir y mejorar la
organización local y global?

> ¿En qué nos basaríamos para confiar en una IA alimentada y cuidada por los
> hipercapitalistas?

En mi opinión, no deberíamos confiar en esa IA.

> Hay tantas oportunidades para resolver problemas a nivel local/comunitario
> utilizando IA y otras herramientas tecnológicas/de diseño que nuestras
> comunidades están llenas de genios creativos. Organizaciones como MayFirst
> pueden ayudar a crear acceso a herramientas y conocimientos para que podamos
> apoyar este tipo de microproyectos. Es de esperar que las buenas ideas se
> extiendan a más personas y lugares.

Sugiero que la gente forme pequeños grupos de debate para ayudarse mutuamente a
aprender sobre esta compleja tecnología, ya que algunas personas no se sienten
cómodas haciendo preguntas en grupos grandes. Para ello utilizamos los canales
de chat de Signal.

> Hola, gracias por esta presentación. ¡Estoy compartiendo un enlace a un
> montón de artículos / informes / recomendaciones relacionadas con la IA
> generativa y los derechos humanos de WITNESS (donde trabajo) en caso de que
> alguien esté interesado: https://www.gen-ai.witness.org/, con ganas de
> continuar esta conversación y la organización!

Gracias.

