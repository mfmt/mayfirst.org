+++
date = "2021-08-22T00:05:11Z" 
title = "Historia oral del Partido Socialista Puertorriqueño que hay que conocer"
mp3 = "media/es/audio/Puerto-Rican_Socialist-Party-Oral-History-Need-to-Know.mp3"
webm = "media/es/audio/Puerto-Rican_Socialist-Party-Oral-History-Need-to-Know.webm"
type = "audio"
podcast_duration = "0:10:54"
bytes = "6955781"
+++

Durante la década de 1970, el Partido Socialista Puertorriqueño fue la mayor y
más organización revolucionaria puertorriqueña más grande y poderosa de los
EE.UU. y una fuerza importante dentro de la izquierda estadounidense. Un libro
recientemente publicado ([*Revolution Around the
Corner: Voices from the Puerto Rican Socialist Party in the United
States*](https://outreach.mayfirst.org/civicrm/mailing/url?u=1032&qid=1117613)), escrito por personas involucradas en del PSP de
entonces, ofrece una historia oral llena de lecciones sobre la organización y
su trabajo. Este Need to Know presenta a algunas de esas personas.
   
Este es un evento al que toda persona interesada en cambiar este país debería
asistir. Las lecciones de ese período y esa organización son de gran valor hoy
en día.

Nota: Debido a dificultades técnicas, sólo se grabaron los primeros 10 minutos
de la se grabaron los primeros 10 minutos.

La presentación de diapositivas a la que se hace referencia en la charla está
[disponible para descargar](/files/psp-need-to-know-slide-show.pdf).
