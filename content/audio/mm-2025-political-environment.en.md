+++
date = "2025-02-26T16:00:08Z"
title = "2025 May First membership meeting: Understanding the current political context through 20 years of media activism"
mp3 = "media/en/audio/mm2025-political-environment.en.mp3"
webm = "media/en/audio/mm2025-political-environment.en.webm"
type = "audio"
podcast_duration = "1:01:32"
bytes = "59079020"
+++

This year May First celebrates 20 years! In this time we have seen enormous
changes in the “Internet ecosystem.” The political environment in most of the
world has also shifted rightward. We are experiencing a time in which digital
technologies reinforce the accumulation of power by historically privileged
groups. We also recognize an increase in our movement's awareness of the
control and social surveillance exercised by both governments and corporations.

We are interested in continuing to think about the role of May First and others
who maintain autonomous technologies to strengthen spaces for social and
environmental justice. We invite all our members to listen to this webinar
recording, where we analyze the political context and review the
historical relationship between digital technologies and social movements with
a special focus on the actions developed from our technologies. We are
joined by special guests to outline some ideas and then open a collective
conversation about our next steps:

 * **Danielle Chynoweth,** co-founder of the [Urbana-Champaign Independent Media Center (UC-IMC)](https://www.ucimc.org/) and Cunningham Township Supervisor.
 * **Loreto Bravo Muñoz,** digital protection and care strategy coordinator for the [Mesoamerican Initiative of Women Human Rights Defenders (IM-Defensoras)](https://im-defensoras.org/en/).
 * **Gloria Muñoz Ramírez,** founder and director of [Desinformémonos](https://desinformemonos.org/), a digital and community journalism project in Mexico.

