+++
date = "2025-02-26T16:00:08Z"
title = "Reunión de membresía de May First 2025: Comprendiendo el contexto político actual a través de 20 años de activismo mediático"
mp3 = "media/es/audio/mm2025-political-environment.es.mp3"
webm = "media/es/audio/mm2025-political-environment.es.webm"
type = "audio"
podcast_duration = "58:42"
bytes = "30229580"
+++

Este año May First cumple 20 años de existencia. En este tiempo hemos visto
enormes cambios en lo que se conoce como el "ecosistema de Internet". El
contexto político también ha virado hacia una derechización de las formas de
vida política y social. Asistimos a tiempos en los que las tecnologías
digitales refuerzan la acumulación de poder de grupos históricamente
privilegiados. Podemos reconocer también un aumento de la conciencia social y
ciudadana respecto al control y la vigilancia social que ejercen tanto
gobiernos como corporaciones.

Nos interesa seguir pensando el rol que tienen May First y otros grupos que
mantienen tecnologías autónomas en fortalecer los espacios de justicia social y
ambiental. Invitamos a toda nuestra membresía a a escuchar la grabación de este webinar que
analizará el contexto político. Haremos un repaso histórico sobre la relación
entre tecnologías digitales y movimientos sociales con un foco especial en las
acciones desarrolladas desde nuestras tecnologías. Contaremos con la presencia
de invitadas especiales para delinear algunas ideas y luego abrir paso a una
conversación colectiva sobre nuestros siguientes pasos. Nos acompañarán:

 * **Danielle Chynoweth,** co-fundadora de [Urbana-Champaign Independent Media Center (UC-IMC)](https://www.ucimc.org/). Actualmente supervisa el trabajo de Cunningham Township.
 * **Loreto Bravo Muñoz,** Coordinadora de la estrategia de [Protección y Cuidados Digitales de la Iniciativa Mesoamericana de Mujeres Defensoras de DDHH (IM-Defensoras)](https://im-defensoras.org/en/).
 * **Gloria Muñoz Ramírez,** fundadora y directora de [Desinformémonos](https://desinformemonos.org/), un proyecto de periodismo digital y comunitario de México.

