+++
date = "2023-06-29T16:00:08Z"
title = "The Role of Technology in The Rising Majority"
mp3 = "media/en/audio/tech.and.rising.majority.mp3"
webm = "media/en/audio/tech.and.rising.majority.webm"
type = "audio"
podcast_duration = "0:57:28"
bytes = "45300724"
tags = [ "news", "callout" ]
+++

*A conversation with Alice Aguilar and Jerome Scott on the role of technology
in movement strategy.*

![Black woman at edge of frame with title: The Rising Majority News Brief. A list of headlines includes: The US Struggles to combat Coronavirus, particisan battle over supreme court justice RBG's seat has begin, and others.](https://outreach.mayfirst.org/sites/default/files/civicrm/persist/contribute/images/rising-majority.jpg)

What if we could collectively develop a strategy that recognized the autonomy
and differences of our diverse movements, while unifying our resources and
energy toward a common goal? **What role would technology play in such a
strategy?**
 
The [Rising Majority](https://therisingmajority.com) formed in 2017 as "a
coalition that seeks to develop a collective strategy and shared practice that
will involve labor, youth, abolition, immigrant rights, climate change,
feminist, anti-war/anti-imperialist, and economic justice forces in order to
amplify our collective power and to build alignment across our movements." 
 
In 2020 May First joined The Rising Majority to support this effort by
contributing our experience with technology and movement building. Please help
us fullfil this task! Join May First Board members Alice Aguilar and Jerome
Scott in discussing the role of technology in building such a strategy. 
 
## Comments from participants

Here are some of the questions comments from the participants.

* Is there interest in placing a priority on 'supply-chain' and labor isues relating to the provision of critical materials for the infrastructure that supports web-related movement work? Thinking about worker and environmental conditions with cobalt & lithium mining, colonial legacies, the stuff that phones and laptops and servers are built from.. etc. 
* How does Rising Majority feel about FOSS? It denies 'realization' to capitalist tech companies. FOSS social media can also bypass attempts at censorship.
* Is Rising Majority only for US or has an ambition to go abroad?
* Information Technology has not only became a lifeline for Capitalism in crisis it has quickly become the primary driver of the neoliberal economy and it will be intricately connected to all other movement struggles.
* It is obviously a huge contradiction that Anti capitalist movement organizations have adopted en masse the worst of the worst of surveillance capitalism driven information technology platforms.  There is no denying that these tools have really helped movements expand their reach and facilitate their work in meaningful ways. But there is already and will continue to be a huge price for routing all movement communications through these corporations who are in full cooperation with the state when it comes to surveilling our movements and controlling public opinion.
* All technology that is not guided by movements for liberation and social justice will become carceral technology. This is why the May First project and others are so important. How can we inspire our comrades in Rising Majority to take interest in what we are doing and begin thinking about how they can chart a path out of those corporate internet services?
* Why doesn't RM notice technology as a critical issue? What's the need for us as a movement to
* get folx to notice that tech is a crucial element and always has been present as, a factor in oppression (eg the Luddites). Are we too overinvolved in our academic-type and identitarian focuses?
* I'm curious as to how RM sees its relationship to groups that are not 501c3 or c4 organizations with paid staff. I guess I"m assuming that the groups do have paid staff, is that true or partially true?
* There is SO MUCH to talk about technology and its impacts! Maybe we should concentrate on how techonology affects social movements and our relations..which I believe is basically the issues of privacy and autonomy..that is the right to organize and discuss privately and the right to communicate freely, that is on platforms that we control, with no commercial interference and data collection without consent
* The discourse of Power and revolution changed dramatically within movements in Mexico after the Zapatista uprising in 1994 and a grassroots strategy that seeks to "change the world without taking power" became a very influential political vision here.
* There is a difference between authoritarian "Power over something" and agency and autonomy, the "power to do something" which I want to believe movements who use a discourse of power are really talking about.
* People talk about global but the reality is the majority of the world remains unconnected... technology affects them in the ways that it the neoliberal economy it fuels displaces these people, consumes their natural resources and destroys their environments and forces them to migrate.
* Tech made it possible for capitalism to go global - the shift to computerization and high speed digital info transfers.
* When talking about technology being used for the rich, I think about how Cuba uses technology for the people. I need to raise this question to friends in Cuba who I'm still in contact with.
* y la tecnología digital no debe ser "obligatoria" o la única forma de acceso (vivas donde vivas)
* The Internet started based on open source tech, and it still runs on that, eg. the DNS system. We simply do not take advantage of this; the answer is to stop using tech that exploits us and move to using and creating more and more open tech.
* this is where it's great ot understand the history of tech and specifically how it relates to our movements - and how it's changed - we used to control our tech (for a brief moment)
* "For Indigenous nations to live, capitalism must die. And for capitalism to die, we must actively participate in the construction of Indigenous alternatives to it." - - Glen Sean Coulthard - Red Skin, White Masks: Rejecting the Colonial Politics of Recognition (p. 173)
* +1 to above, and noticing that we need to be building alternatives to capitalism too. We need anti-capitalist technology and we will have to create it ourselves.
* Yes, we can't just be constantly against something or other, taking it down, deconstructing it. We have to be for something, to be constructive. Where would we be if tomorrow, capitalism and authoritarian domination was gone, where would we be. Are we prepared for that eventuality?
* We will need to build on feminist and intersectional thinking that has evolved approaches to how we engage and interact with one another - human relations technology that can enable organizations that do not use hierarchy and domination, but do have conflict transformation capacities and egalitarian power-to instead of power-over. We need to build structures in which people can learn to live that way.
* What about dual power? Do we have to take over the existing structures begin creating our own autonomous solutions?
* I'm optimistic, but I don't think we'll get rid of capitalism in the near future. So, I think we need to figure how to use technology to move our social justice movements in this country now.
* Here's the link to Freedom Circles signup if anyone would like to join us! (Rising Majority) https://linktr.ee/freedomcircles
* I would narrow the scope about technology and a group like RM to technology that supports building movements (security and organizing needs)- as opposed to spending time on the "other" issues about technology.
* We need to be working more closely with comunity network projects.
* right on! love where this is going...it also makes me think about the impact of tech within the context of climate collapse & healing the planet. 
* I'm curious how AI will impact our technology and our movements. 
* there is SO MUCH to talk about technology and its impacts! Maybe we should concentrate on how techonology affects social movements and our relations..which I believe is basically the issues of privacy and autonomy..that is the right to organize and discuss privately and the right to communicate freely, that is on platforms that we control, with no commercial interference and  data collection without consent 
* yes how do we change the world without taking power! 
* What technologies do we use to fight capitalism? FaceBook? Twitter, email lists?? What social media can we use? 
* totalmente! ser solidaries sin paternalismos sino desde (re)vincularnos con esas luchas 
* my work experience in technology has led to opportunities to provide technical support for organizing outside the U.S.  I don't feel like organizers elsewhere need my opinion on their work 
* y la tecnología digital no debe ser "obligatoria" o la única forma de acceso (vivas donde vivas) 
* this is where it's great ot understand the history of tech and specifically how it relates to our movements - and how it's changed - we used to control our tech (for a brief moment) 
* some years ago, I visited a digital technology center in Havana. They were very current then and I suspect they are still are 
* We need to be working more closely with comunity network projects. 
