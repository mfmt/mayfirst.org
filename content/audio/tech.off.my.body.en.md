+++
date = "2022-06-27T16:00:08Z"
title = "Get the Tech Off My Body! Recording and resources"
mp3 = "media/en/audio/tech.off.my.body.mp3"
webm = "media/en/audio/tech.off.my.body.webm"
type = "audio"
podcast_duration = "1:20:59"
bytes = "65930100"
+++

Overturning Roe v. Wade in the United States and restricting access to abortion
throughout the Americas is part of a strategy of repression that is focused on
our bodies. The strategy not only targets reproductive justice, but also
transgender rights, the mobility of poor people and people of color, and access
to health care and other critical services. In this webinar, we’ll hear from
May First members involved in the struggle and discuss the relationship between
the struggles for reproductive justice, privacy, autonomy, and freedom from
surveillance. **How can our campaign to re-envision technology based on consent
and liberation contribute to our movement's struggle for reproductive
justice?**

On June 21st, 2022 May First, along with May First members the [Progressive
Technology Project](https://progressivetech.org), the [National Network of
Abortion Funds](https://abortionfunds.org), and the [Detention Watch
Network](https://detentionwatch.org) organized a webinar to discuss the
technology implications of over turning Roe v. Wade.

Listen by clicking the play button above, or see the collection of resources
the panelists and participants compiled during the webinar.

## Collected Resource from the webinar

 * [Riana's blog post about abortion privacy & encryption](http://cyberlaw.stanford.edu/blog/2022/05/end-roe-will-bring-about-sea-change-encryption-debate)
 * [EFF abortion privacy & security tips](https://www.eff.org/deeplinks/2022/05/digital-security-and-privacy-tips-those-involved-abortion-access)
 * [Talk about abortion criminalization, “Jailed by a Google Search”](https://www.youtube.com/watch?v=RcPpbWurw4U)
 * [Politico article about abortion privacy & security (6/20)](https://www.politico.com/news/2022/06/17/abortion-rights-advocates-digital-security-threats-00040654)
 * [“My Body, My Data Act”](https://sarajacobs.house.gov/news/documentsingle.aspx?DocumentID=542)
 * [Ale Link Tree](https://linktr.ee/emotionalgangzter)
 * [American Dragnet Data-Driven Deportation in the 21st Century](https://www.americandragnet.org/)
 * [Defunding the Dragnet](https://justfutureslaw.org/wp-content/uploads/2021/11/JFL_Report-DefendingTheDragnet_111521.pdf)
 * [End Abusive Surveillance of Immigrant, Black & Brown Communities](https://justfutureslaw.org/wp-content/uploads/2020/12/JFL-Mijente-Immig-Tech-Surveillance-Rec-1.pdf)
 * [Hacking//Hustling](https://hackinghustling.org/)
 * [EFF - Street Level Surveillance](https://www.eff.org/issues/street-level-surveillance)
 * [Detention Watch Network: Defund Hate](https://www.detentionwatchnetwork.org/defundhate)
 * [Mijente No Tech For ICE Campaign](https://notechforice.com)
 * [Time Magazine - Abortion Surveillance](https://time.com/6184111/abortion-surveillance-tech-tracking/)
 * [Digital Security for Abortio Poster](https://digitaldefensefund.org/ddf-artwork-zines/digital-security-for-abortion-and-pregnancy-privacy-poster)
 * [From Data Criminalization to Prison Abolition](https://abolishdatacrim.org/en)
 * [De-criminalize Abortion](https://www.interruptingcriminalization.com/decriminalize-abortion)
 * [Facebook tracking abortion](https://www.engadget.com/facebook-meta-pixel-tracking-websites-abortion-services-185249977.html)
 * [Shout your abortion](https://shoutyourabortion.com/ )
 * [Join Mayfirst](https://mayfirst.coop) or if a member, [get involved](https://mayfirst.coop/en/get-involved)

## Panelists

<table> 
<tr>
<td style="text-align: center">

![Alejandra Poblas head shot](/en/post/2022/tech-off-my-body/alejandra.jpg) 

</td>
<td> 

**Alejandra Poblas** is a
reproductive justice organizer and storyteller at the intersections mass
incarceration and immigration. She shares her incarceration and abortion story
as an act of resistance to fight abortion stigma and bring awareness to
abolition and racial justice.

</td>
</tr>
<tr>
<td style="text-align: center">

![Alice Aguilar head shot](/en/post/2022/tech-off-my-body/alice.png) 

</td>
<td> 

**Alice Aguilar's** (moderator) life work focuses in
supporting indigenous people's rights, environmental justice, and reproductive
justice issues. Alice's current work involves leading the fight against racism
and sexism in technology, bringing women, queer and Trans people of color into
movement technology, and winning respect for the people of color already doing
technology work within social justice movements. 

</td>
</tr>

<tr>
<td style="text-align: center">

![Riana Pfefferkorn head shot](/en/post/2022/tech-off-my-body/riana.jpg) 

</td>
<td>

**Riana Pfefferkorn** is a Research
Scholar at the Stanford Internet Observatory. She investigates the U.S. and
other governments' policies and practices for forcing decryption and/or
influencing the security design of online platforms and services, devices, and
products, both via technical means and through the courts and legislatures.
Riana also studies novel forms of electronic surveillance and data access by
U.S. law enforcement and their impact on civil liberties.

</td>
</tr>

<tr>
<td style="text-align: center">

![National Network of Abortion Funds logo](/en/post/2022/tech-off-my-body/abortionfunds.png) 

</td>
<td>

**Kim Varela-Broxson** works for the The National Network of Abortion Funds.
The National Network of Abortion Funds builds power with members to remove
financial and logistical barriers to abortion access by centering people who
have abortions and organizing at the intersections of racial, economic, and
reproductive justice.

</td>
</tr>

<tr>
<td style="text-align: center">

![May First logo](/en/post/2022/tech-off-my-body/mayfirst.png) 

</td>
<td>

**May First Movement Technology**
engages in building movements by advancing the strategic use and collective
control of technology for local struggles, global transformation, and
emancipation without borders.

</td>
</tr>

</table>

