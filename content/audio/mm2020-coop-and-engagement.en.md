+++
date = "2020-11-18T21:05:07Z" 
title = "2020 Membership Meeting on Coop And Engagement"
mp3 = "media/en/audio/mm2020-coop-and-engagement.mp3"
webm = "media/en/audio/mm2020-coop-and-engagement.webm"
type = "audio"
podcast_duration = "0:41:07"
bytes = "23278190"
+++

The Coop and Engagement membership  meeting discussion covered how best we can
engage our members in our organizing and operations work.
