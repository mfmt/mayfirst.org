+++
date = "2018-05-25T12:37:06Z" 
title = "Eyes On You Technology Of Surveillance"
mp3 = "media/en/audio/eyes-on-you-technology-of-surveillance.mp3"
webm = "media/en/audio/eyes-on-you-technology-of-surveillance.webm"
type = "audio"
podcast_duration = "1:08:17"
bytes = "51154828"
+++

The government’s answer to a failing society? Lock up the people who are most affected and most likely to rebel. There are over 3 million people in prison today; 8 directly affected by prison, parole and probation. There is no place in the world with a larger imprisoned populations. We are the world’s leading imprisoner and our government is the world’s most active prison warden.

They could never do this without information technology. Cameras that watch us, systems that record our every move, computer systems that track people for arrest and process them once they’re arrested. And the massive computer systems that keep track of people who are detained and hold their publicly accessible records forever.

We are under attack using the technology we effectively created and should control. What should we do? How bad is it? Can it be reversed?

In this month’s Need to Know, we experts in this area – people who are organizing this fight and know its ins and outs – explaining the problem and discussing, with you, some solutions.

Stop LAPD Spying Coalitions’s Hamid Khan is one of the country’s best-known activists working against police surveillance and its various impacts. Myaisha Hayes is the National Organizer on Criminal Justice and Technology for the Center for Media Justice. Jude Ortiz is on the National Lawyers Guild’s Mass Defense Steering Committee and has lots of experience in radical grassroots legal support.
