+++
date = "2018-03-01T01:33:49Z" 
title = "Jerome Scott (Points Taken)"
webm = "media/en/audio/jerome-scott-points-taken.webm"
mp3 = "media/en/audio/jerome-scott-points-taken.mp3"
type = "audio"
podcast_duration = "0:26:46"
bytes = "15230605"
+++

Veteran activist Jerome Scott talks to Lucas about the state of our society and the challenges we face in saving ourselves and our world. A facinating conversation with one of our outstanding leaders.


