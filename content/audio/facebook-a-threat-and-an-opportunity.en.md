+++
date = "2018-04-23T01:25:23Z" 
title = "Facebook A Threat And An Opportunity (Need to Know)"
webm = "media/en/audio/facebook-a-threat-and-an-opportunity.webm"
mp3 = "media/en/audio/facebook-a-threat-and-an-opportunity.mp3"
type = "audio"
podcast_duration = "1:07:46"
bytes = "47174147"
+++

Sainaba Ali and Praveen Sinha from [Equality Labs](https://www.equalitylabs.org), Nathan ‘nash’ Sheard from the [Electronic Frontier Foundation](https://eff.org/) and Jamie McClelland from [May First/People Link](https://mayfirst.org/) and the [Progressive Technology Project](https://progressivetech.org/) are on board to the latest scandal surrounding Facebook: The personal information of 50 million Facebook users was captured or transferred, legally and transparently, by a right-wing consulting firm called Cambridge Analytics.

How dangerous is this? As dangerous as can be. What do we do about it? Precisely the question lots of activists are now asking themselves and we discuss in this latest [Need to Know](https://mayfirst.org/en/audio) from [May First/People Link](https://mayfirst.org/) and the [Progressive Technology Project](https://progressivetech.org/).

