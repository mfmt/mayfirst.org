+++
date = "2022-06-21T16:00:08Z"
title = "Sacar la tecnología de mi cuerpo: grabación y recursos"
mp3 = "media/es/audio/tech.off.my.body.es.mp3"
webm = "media/es/audio/tech.off.my.body.es.webm"
type = "audio"
podcast_duration = "1:21:27"
bytes = "78205431"
+++

Anular el caso Roe v. Wade en Estados Unidos y restringir el acceso al aborto
en todo el continente americano es parte de una estrategia de represión que se
centra en nuestres cuerpos. La estrategia no sólo afecta la justicia
reproductiva, sino también los derechos de las personas transgénero, la
movilidad de las personas pobres y de color, y el acceso a la atención
sanitaria y otros servicios críticos. En este seminario web, escucharemos a
personas afiliadas de May First en esta lucha y discutiremos la relación entre
las luchas por la justicia reproductiva, la privacidad, la autonomía y la
libertad de la vigilancia. **¿Cómo puede nuestre campaña para replantear la
tecnología basada en el consentimiento y la liberación contribuir a la lucha de
nuestre movimiento por la justicia reproductiva.**

El 21 de junio de 2022 May First, juntes con personas miembres de May First, el
[Progressive Technology Project](https://progressivetech.org), la [National
Network of Abortion Funds](https://abortionfunds.org), y la [Detention Watch
Network](https://detentionwatch.org), organizamos un seminario web para
discutir las implicaciones tecnológicas de la anulación de Roe v. Wade.

Clic arriba para escuchar, o vea la colección de recursos que las panelistas y
les participantes recopilaron durante el seminario web.

## Recopilación de recursos del seminario web

 * [Riana's blog post about abortion privacy & encryption](http://cyberlaw.stanford.edu/blog/2022/05/end-roe-will-bring-about-sea-change-encryption-debate)
 * [EFF abortion privacy & security tips](https://www.eff.org/deeplinks/2022/05/digital-security-and-privacy-tips-those-involved-abortion-access)
 * [Talk about abortion criminalization, “Jailed by a Google Search”](https://www.youtube.com/watch?v=RcPpbWurw4U)
 * [Politico article about abortion privacy & security (6/20)](https://www.politico.com/news/2022/06/17/abortion-rights-advocates-digital-security-threats-00040654)
 * [“My Body, My Data Act”](https://sarajacobs.house.gov/news/documentsingle.aspx?DocumentID=542)
 * [Ale Link Tree](https://linktr.ee/emotionalgangzter)
 * [American Dragnet Data-Driven Deportation in the 21st Century](https://www.americandragnet.org/)
 * [Defunding the Dragnet](https://justfutureslaw.org/wp-content/uploads/2021/11/JFL_Report-DefendingTheDragnet_111521.pdf)
 * [End Abusive Surveillance of Immigrant, Black & Brown Communities](https://justfutureslaw.org/wp-content/uploads/2020/12/JFL-Mijente-Immig-Tech-Surveillance-Rec-1.pdf)
 * [Hacking//Hustling](https://hackinghustling.org/)
 * [EFF - Street Level Surveillance](https://www.eff.org/issues/street-level-surveillance)
 * [Detention Watch Network: Defund Hate](https://www.detentionwatchnetwork.org/defundhate)
 * [Mijente No Tech For ICE Campaign](https://notechforice.com)
 * [Time Magazine - Abortion Surveillance](https://time.com/6184111/abortion-surveillance-tech-tracking/)
 * [Digital Security for Abortio Poster](https://digitaldefensefund.org/ddf-artwork-zines/digital-security-for-abortion-and-pregnancy-privacy-poster)
 * [From Data Criminalization to Prison Abolition](https://abolishdatacrim.org/en)
 * [De-criminalize Abortion](https://www.interruptingcriminalization.com/decriminalize-abortion)
 * [Facebook tracking abortion](https://www.engadget.com/facebook-meta-pixel-tracking-websites-abortion-services-185249977.html)
 * [Shout your abortion](https://shoutyourabortion.com/ )
 * [Join Mayfirst](https://mayfirst.coop) or if a member, [get involved](https://mayfirst.coop/en/get-involved)

## Panelistas

<table> <tr>
  <td style="text-align: center">
                                                                                                                                                                                                                     
  ![Alejandra Poblas head shot](/en/post/2022/tech-off-my-body/alejandra.jpg)
                                                                                                                                                                                                                     
  </td>
  <td>
                                                                                                                                                                                                                     
  **Alejandra Poblas** es una
  organizadora de la justicia reproductiva y narradora en las intersecciones de la
  encarcelamiento masivo e inmigración. Ella comparte su historia de encarcelamiento y aborto
  como un acto de resistencia para luchar contra el estigma del aborto y concienciar sobre la
  la abolición y la justicia racial.                                                                                                                                                                                      
                                                                                                                                                                                                                     
  </td>
  </tr>

  <tr>
  <td style="text-align: center">
  
  ![Alice Aguilar foto de cabeza](/en/post/2022/tech-off-my-body/alice.png)
  
  </td>
  <td>
  
  **El trabajo de Alice Aguilar** (moderadora) se centra en
  apoyar los derechos de los pueblos indígenas, la justicia ambiental y la justicia reproductiva
  reproductiva. El trabajo actual de Alice consiste en liderar la lucha contra el racismo
  racismo y el sexismo en la tecnología, incorporando a las mujeres, a las personas queer y trans de color al movimiento
  y ganarse el respeto de la gente de color que ya trabaja en tecnología dentro de los movimientos de justicia social.
  de color que ya están trabajando en tecnología dentro de los movimientos por la justicia social.
  
  </td>
  </tr>                                                                                                                                                                                                                    
  <tr>
  <td style="text-align: center">
                                                                                                                                                                                                                     
  ![Riana Pfefferkorn foto de cabeza](/en/post/2022/tech-off-my-body/riana.jpg)
                                                                                                                                                                                                                     
  </td>
  <td>
  
  **Riana Pfefferkorn** es una investigadora
  Scholar en el Observatorio de Internet de Stanford. Investiga las políticas y prácticas de Estados Unidos y
  y otros gobiernos para forzar el descifrado o influir en el diseño
  influir en el diseño de la seguridad de las plataformas y los servicios, dispositivos y productos
  productos en línea, tanto por medios técnicos como a través de los tribunales y las legislaturas.
  Riana también estudia las nuevas formas de vigilancia electrónica y acceso a los datos por parte de
  de datos por parte de las fuerzas del orden de Estados Unidos y su impacto en las libertades civiles.
  
  </td>
  </tr>
  
  <tr>
  <td style="text-align: center">
  
  ![Logotipo de May First](/en/post/2022/tech-off-my-body/mayfirst.png)
  
  </td>
  <td>
  
  **Tecnología del movimiento May First**
  se involucra en la construcción de movimientos mediante el avance del uso estratégico y el control colectivo
  de la tecnología para las luchas locales, la transformación global y la
  emancipación sin fronteras.
  
  </td>
  </tr>
  
  <tr>
  <td style="text-align: center">
  
  ![Logotipo de la Red Nacional de Fondos para el Aborto](/en/post/2022/tech-off-my-body/abortionfunds.png)
  
  </td>
<td>
  
  **Kim Varela-Broxson** trabaha con la National Network of Abortion Funds. La
  National Network of Abortion Funds construye el poder con los miembros para
  eliminar las barreras financieras y logísticas para el acceso al aborto
  mediante centrando a las personas que tienen abortos y organizando en las
  intersecciones de justicia racial, económica y reproductiva.
  
  </td>
  </tr>
  </table>


