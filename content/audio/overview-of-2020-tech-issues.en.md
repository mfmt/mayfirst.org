+++
date = "2020-02-20T21:45:07Z" 
title = "The Internet - where we are and what we do"
mp3 = "media/en/audio/overview-of-2020-tech-issues.mp3"
webm = "media/en/audio/overview-of-2020-tech-issues.webm"
type = "audio"
podcast_duration = "1:16:56"
bytes = "45865408"
+++

To kick off 2020, a panel and audience discussion of the current status of the
Internet, its freedom, the issues challenging our use of it, its security and
the alternatives to corporate control.

