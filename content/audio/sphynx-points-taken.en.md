+++
date = "2018-03-28T01:33:49Z" 
title = "Sphynx (Points Taken)"
webm = "media/en/audio/sphynx-points-taken.webm"
mp3 = "media/en/audio/sphynx-points-taken.mp3"
type = "audio"
podcast_duration = "1:09:52"
bytes = "39272830"
+++

Ambazonian activist, videographer and technologist Sphynx is a highly experienced and respected voice and authority on Africa’s situation and its liberation struggles. He speaks with Lucas about the situation in his home country and the region surrounding it and what people in the United States can do in solidarity. An information-filled interview that anyone interested in this topic should check out.

