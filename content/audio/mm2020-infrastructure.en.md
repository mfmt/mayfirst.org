+++
date = "2020-10-29T21:29:55Z" 
title = "2020 Membership Meeting discussion on Technology Infrastructure and Services"
mp3 = "media/en/audio/mm2020-infrastructure.mp3"
webm = "media/en/audio/mm2020-infrastructure.webm"
type = "audio"
podcast_duration = "0:48:22"
bytes = "28560789"
+++

Our 2020 Membership meeting discussion on Technology Infrastructure and
Services took place on October 27th. You can click above to hear the recording
and participate in the
[discussion](https://comment.mayfirst.org/t/technology-infrastructure-and-services/1849).
