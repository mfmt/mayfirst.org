+++
date = "2019-08-18T00:25:12Z" 
title = "Fighting Off Ddos Attacks"
mp3 = "media/en/audio/fighting-off-ddos-attacks.mp3"
webm = "media/en/audio/fighting-off-ddos-attacks.webm"
type = "audio"
podcast_duration = "1:13:39"
bytes = "42305115"
+++

Support team leaders Jamie McClelland and Jaime Villareal and Leadership
Committee member Maritza Arrastía lead a conversation about our recent
distributed denial of service attack (DDOS) for ransom, other types of DDOS
attacks and how to resist them.
