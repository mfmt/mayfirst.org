+++
date = "2018-02-28T01:33:49Z" 
title = "Internet Under Trump (Need to Know)"
webm = "media/en/audio/internet-under-trump-webinar.webm"
mp3 = "media/en/audio/internet-under-trump-webinar.mp3"
type = "audio"
podcast_duration = "0:59:11"
bytes = "44083791"
+++

Highlander Research and Education Center’s Samir Hazboun, Free Press’s Jessica Gonzalez and Center for Media Justice’s Steven Renderos in a lively conversation about the Challenges to Our Internet Under Trump.

