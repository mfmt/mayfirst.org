+++
date = "2018-07-26T14:50:15Z" 
title = "Google and Its Destructive Future"
mp3 = "media/en/audio/google.path.to.destruction.webinar.mp3"
webm = "media/en/audio/google.path.to.destruction.webinar.webm"
type = "audio"
podcast_duration = "0:57:36"
bytes = "38065980"
+++

Google is very much a part of your life and our movement's. Its email program is the most popular among movement activists. We all use its search engine. Major organizations use its storage and data sharing capabilities.

It has taken over our online lives and it is moving us toward disaster. Google's business plan is complete control of your communications and, since it is a partner of the U.S. intelligence services, it will destroy our movements if things continue as they are.

How real is this theat? How complicit is our movement? What should we do about this?

We have brought together a panel of experts, including [Electronic Frontier Foundation](https://eff.org/) Legal Director Corynne McSherry and veteran activist Jerome Scott, to talk about all this and then have a conversation with you about what's going on and what to do about it. You need to catch this one!

Corynne is a highly regarded and recognized litigator and advocate. At EFF, she specializes in intellectual property, open access, and free speech issues and has been lead attorney on many major cases involving copyright and access. She has recently been writing on Google and what it represents: the topic of this Need to Know.

Long-time revolutionary and organizer, Jerome was an auto worker in Detroit when he (and several other activists) founded the [League of Revolutionary Black Workers](https://www.revolutionaryblackworkers.org/), he was later a founder of [Project South](https://projectsouth.org/) and is currently a leader in the [League of Revolutionaries for a New America](http://lrna.org/). He's a member of the Leadership Committee of [May First/People Link](https://mayfirst.org/).


