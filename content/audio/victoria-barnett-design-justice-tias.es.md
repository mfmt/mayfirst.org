+++
date = "2021-06-25T04:25:08Z"
title = "Victoria Barnett - Justicia de Diseño"
mp3 = "media/es/audio/victoria-barnett-design-justice-tias.mp3"
webm = "media/es/audio/victoria-barnett-design-justice-tias.webm"
type = "audio"
podcast_duration = "0:50:15"
bytes = "42300072"
+++

### Victoria Barnett - Justicia de Diseño

**2021-06-18:** 
Victoria Barnett nos presenta los Principios de Justicia de Diseño y su trabajo apoyando el crecimiento de la [Red de Justicia de Diseño](https://designjustice.org).  . [Sigue la presentación pdf](https://share.mayfirst.org/s/THxippnkHrdbppQ)

![Victoria Barnett](/img/victoria.jpg "Victoria")

[Victoria Barnett](http://victoriabarnett.com/) es diseñadora de gráfica digital, facilitadora, organizadora comunitaria y colaboradora al servicio de iniciativas de justicia social. Su trabajo se basa en los Principios de Justicia de Diseño.

Referencias adicionales 

* [Cómo se generaron los principios de justicia de diseño](https://designjustice.org/news-1/2016/05/02/2016-generating-shared-principles), Véase el ejercicio del diagrama de Venn.
* [Preguntas útiles para hacer](https://designjustice.org/s/DESIGNJUSTICEZINE_ISSUE2.pdf), ver página 7
* [Consejos sobre cómo organizar un nodo local](https://designjustice.org/how-to-organize-local-node) Puede aplicarse al grupo de trabajo
* [Zines](https://designjustice.org/zines)

<p>

---