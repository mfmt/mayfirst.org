+++
date = "2021-07-02T11:45:12Z" 
title = "Nechari Riley - Human Centered Research"
mp3 = "media/en/audio/Nechari-Riley-Human-Centered-Research-TIAS.mp3"
webm = "media/en/audio/Nechari-Riley-Human-Centered-Research-TIAS.webm"
type = "audio"
podcast_duration = "0:52:37"
bytes = "38362888"
+++

### Nechari Riley - Human Centered Research

**2021-07-25:** 
Nechari Riley shares insights from her journey to becoming a "people powered" UX Design researcher.
[Follow the pdf presentation](https://share.mayfirst.org/s/PPjtSL5rQpHFZpX)

![Nechari Riley](/img/nechari.jpg "Nechari")

[Nechari Riley](https://www.nechari.com/about-me/) is a mixed-methods user researcher and strategist who works with organizations to develop research-informed projects that are inclusive, accessible, and engaging.

<p>

---
