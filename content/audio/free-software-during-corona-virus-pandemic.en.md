+++
date = "2020-03-27T01:22:52Z" 
title = "Free Software During Corona Virus Pandemic"
mp3 = "media/en/audio/free-software-during-corona-virus-pandemic.mp3"
webm = "media/en/audio/free-software-during-corona-virus-pandemic.webm"
type = "audio"
podcast_duration = "1:04:26"
bytes = "36181290"
+++

A conversation about the movement's software needs during this period of social
distanting.

The virus and the fact that so many activities must be cancelled as a result
has underscored the importance of free software (or what is frequently called
Free and Open Source Software). If you're going online with what were your
face to face activities, you really have to listen to what our panelists have
to say.

Micky Metts of Agaric, Jaime Villareal and Jamie McClelland of May First staff,
and activist, writer and teacher Melanie Bush

They'll answer the questions many have been asking about free software, what is
"open source" and, most immediately, how do we cope with this crisis while
continuing our organizing work?
