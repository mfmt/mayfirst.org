+++
date = "2018-02-23T01:33:49Z" 
title = "Sage Crump (Points Taken)"
webm = "media/en/audio/sage-crump-points-taken.webm"
mp3 = "media/en/audio/sage-crump-points-taken.mp3"
type = "audio"
podcast_duration = "0:24:13"
bytes = "13461262"
+++

Sage Crump is a cultural strategist, one of this country's foremost activists working at the intersection of art and social justice. She works in and with a variety of organizations, many in the South, including the National Performance Network where she coordinates the Leveraging a Network for Equity program. She works with the Southern Arts Federation and is on the Board of the Center for Media Justice. In this Points Taken, she talks with Lucas about her work, the situation of art in our communities and its role in our struggles.

