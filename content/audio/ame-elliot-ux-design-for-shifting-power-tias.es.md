+++
date = "2021-07-13T12:15:11Z" 
title = "Ame Elliot Ux Design For Shifting Power Tias"
mp3 = "media/es/audio/ame-elliot-ux-design-for-shifting-power-tias.mp3"
webm = "media/es/audio/ame-elliot-ux-design-for-shifting-power-tias.webm"
type = "audio"
podcast_duration = "0:53:26"
bytes = "45768941"
+++

### Diseño de UX para el cambio de poder

**2021-07-02**: 
Ame Elliot habla sobre su trabajo en Simply Secure ofreciendo soporte de diseño y asesoramiento para proyectos tecnológicos que centran y protegen a las poblaciones vulnerables. 
[Sigue la presentación pdf](https://share.mayfirst.org/s/SgcoXEEm3WNts2F)

![Ame elliot](/img/ame.jpg "Ame")

[Ame Elliot](https://simplysecure.org/who-we-are/ame.html) es Directora de Diseño de Simply Secure, una organización educativa sin ánimo de lucro conformando una comunidad que sitúan a las personas en el centro de la privacidad, la seguridad, la transparencia y la ética.

Referencias adicionales:

* [Biblioteca de patrones para la descentralización](https://decentpatterns.xyz/)
* [Base de conocimientos de Simply Secure](https://simplysecure.org/knowledge-base/)
* [Derechos de para personas participantes](https://simplysecure.org/blog/participant-rights)
* [Maquetas](https://simplysecure.org/blog/wireframing-intro)
* [Hazlo físico, imprímelo, fíjalo con alfileres](https://simplysecure.org/assets/blog/2016/architecture.jpg)

<p>

---