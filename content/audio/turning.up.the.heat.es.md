+++
date = "2022-11-15T16:00:08Z"
title = "Aumentando la presión sobre las grandes empresas tecnológicas"
mp3 = "media/en/audio/Turning-up-the-Heat-on-Big-Tech.es.mp3"
webm = "media/es/audio/Turning-up-the-Heat-on-Big-Tech.es.webm"
type = "audio"
bytes = "62281007"
+++

La violencia del apartheid en los Territorios Ocupados Palestinos, las brutales
represalias contra los trabajadores de los almacenes, la criminalización y la
violencia contra las comunidades racializadas en todo el mundo dependen de la
infraestructura del capitalismo de vigilancia, en gran parte alimentada por los
contratos militares y policiales de Google y Amazon. Acompáñanos en un panel
que reunirá a las voces del movimiento que están denunciando el papel de
Google, Amazon, Zoom, Facebook y otros en el mantenimiento y la expansión de la
violencia estatal.

Los ponentes serán Matyos Kidane (Stop LAPD Spying), el Dr. Rabab Abdulhadi y
Jonathan Bailey (Amazon International).

![](https://outreach.mayfirst.org/sites/default/files/civicrm/persist/contribute/images/turning-up-the-heat.es.png)
