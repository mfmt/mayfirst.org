+++
date = "2016-12-03T01:33:49Z" 
title = "2016 Membership Meeting Panel"
webm = "media/en/audio/mm-2016-panel.webm"
mp3 = "media/en/audio/mm-2016-panel.mp3"
type = "audio"
podcast_duration = "0:47:29"
bytes = "29821545"
+++

Presenters: Alfredo Lopez (moderators), Laura Flanders, Ashley Abbott, Chinyere Tutashinda, Jerome Scott, Emily Kawano, and Georgia Bullen 

