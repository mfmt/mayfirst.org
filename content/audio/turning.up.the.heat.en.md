+++
date = "2022-11-15T16:00:08Z"
title = "Turning up the heat on Big Tech"
audiofiles = [ 
  { mp3 = "media/en/audio/Turning-up-the-Heat-on-Big-Tech-pt1.en.mp3", webm = "media/es/audio/Turning-up-the-Heat-on-Big-Tech-pt1.en.webm"},
  { mp3 = "media/en/audio/Turning-up-the-Heat-on-Big-Tech-pt2.en.mp3", webm = "media/es/audio/Turning-up-the-Heat-on-Big-Tech-pt2.en.webm"} 
]
type = "audio"
+++

*NOTE: The english recording is split into two recordings representing the
introduction and start of the webinar and the rest of the webinar. Due to
technical difficulties, the quality of part one (the player on the left) is
poor. The quality of part two (the player on the right) is better.*

Ongoing apartheid violence in occupied Palestine, brutal anti-worker
retaliation against organizing warehouse workers, and criminalization and
violence against communities of color globally all depend on surveillance
capitalism’s infrastructure – largely fueled by Amazon’s and Google’s military
and police contracts. Join us for a panel bringing together movement voices who
are turning up the heat on the unapologetic role of Google, Amazon, Zoom,
Facebook and others in upholding and expanding the reach of state violence.

The speakers featured Matyos Kidane (Stop LAPD Spying), Dr. Rabab
Abdulhadi, and Jonathan Bailey (Amazon International).

![](https://outreach.mayfirst.org/sites/default/files/civicrm/persist/contribute/images/turning-up-the-heat.png)
