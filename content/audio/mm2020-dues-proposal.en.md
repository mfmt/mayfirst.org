+++
date = "2020-11-18T21:23:39Z" 
title = "2020 Membership Meeting Dues Proposal"
mp3 = "media/en/audio/mm2020-dues-proposal.mp3"
webm = "media/en/audio/mm2020-dues-proposal.webm"
type = "audio"
podcast_duration = "1:01:27"
bytes = "36301784"
+++

This presentation covers the proposal for re-structuring how we price the dues for membership.
