---
date: 2024-12-05
title: "Cutting the Cord: Survey Results"
mp3: "media/en/audio/cutting-the-cord.mp3"
webm: "media/en/audio/cutting-the-cord.en.webm"
type: "audio"
podcast_duration: "00:54:54"
bytes: "36859872"
tags: [ "callout"]
---

*We asked activists to help us make leaving Gmail and other Google services
easier and the reaction was unbelievable.* We received more than twice the
number of expected responses from our survey, and many people reached out to
express their excitement and support for this project.

**Clearly, as a movement, we want to leave Google behind. But how?**

![sometimes you have to cut the cord](/en/post/2024/gmail-survey/cutting-the-cord.png)

In this webinar on December 6th, 2024, we discussed the results. The slides are also
[available](slide-show.pdf).

The survey was conducted by [May First Movement
Technology](https://mayfirst.coop/) and the [Progressive Technology
Project](https://progressivetech.org/), in collaboration with the [Infrared
Network](https://in.fra.red/), and with funding from the [Association for
Progressive Communications](https://apc.org).

## Questions and Answers

1. Is the webinar being recorded?  

>	Audio is recorded! Stay tuned for an announcement of where you can find it.

2. Is there a resource guide for more secure platforms and what they offer? 
	
>	Electronic Frontier Foundation has one of the best lists of resources out there: https://ssd.eff.org/

3. It feels like the collaborations as biggest obstacle answer is kind of a cop out(was that option an easy out/pick?).. everyone else hasn't moved so I haven't moved.. this coming from someone who picked collaboration as a big obstacle

>	I see it as my org saying "I work with ppl who I don't know how to train on Nextcloud, so we just use Google Docs, since they already know it." This is an indication of "intent" which provides additional opportunities The features available on alternatives have increased over time, so that is an improvement overall

4. What is the primary reason for people to want to leave Google? 

>	We didn't directly ask this question in the survey, but we will get more info in the follow up interviews. The focus was about what are the obstacles to leaving, rather than why folx want to transition.

5. Are there updated resource guides for the nuts-and-bolts of making the migrations happen? For those who can admin in big tech dashboards, how can they learn to admin in the FLOSS world? And what timelines are needed for migrations to happen?

> The movement hosting networks are working towards this, seeking funding and resources towards more documentation, better guides and more capacity for helping groups transition to movement-aligned tech. Through the rest of the Cutting the Cord campaign, we'll be developing strategies togehter for this work.

6. Can you address the security advantages of leaving corporate hosting?
	
> Sometimes security is not a black and white issue. In all honesty, Google can be very secure for groups in many ways and this needs to be weighed within the overall context of how secure it is to have your data and communications hosted with a corporation that you are fighting, or like one that you are fighting.
	
>	An excellent resource on understanding sec overall: https://holistic-security.org/ 

7. As long as "collaboration" forces me to use gmail - by having to write to gmail addresses, I consider this an obstacle. Does anybody else feel this way? I guess the presenters cannot really answer this. 

>	Many people posted in the chat that they felt the same.

8. What are the cost differences in moving from big tech to these options? If May First and PTP are hosting tools for us to move to, are we cost/resource sharing?

> Sometimes there are costs to using open source or movement-aligned tools vs. google tools. And sometimes the costs aren't monetary but, for example, have to do with things like optics. If an organization is running a campaign against google for its contracts with the Israeli military, and using google docs to run the campaign, that can be counter-productive in several different ways. Also, the cost of Corporate Tech comes in the form of surveillance and marketing. Many corporate tech platforms are essentially built for advertising. 
	
>	Sometimes the costs of alternative tech are subsidized by the orgs and groups that provide the tech and with more resources, these tools could be better. A lot to think about here.
	
>	Thinking about https://opencollective.com/ for group fundraising/cost sharing

9. Could we organize a practical webinar to hand-hold and simply guide people in using alternatives like nextcloud or whatever alternative, that avoids technical jargon that poses a barrier to those unfamiliar with or intimidated by various open source alternatives? The main barrier to leaving googledocs in my organization is colleagues' impression that alternatives are too confusing/don't work as well/not a big deal since we're working on bigger picture systemic change. My efforts at pushback are in vain so far. I think showing the equal or superior efficacy and usability of the alternatives would be very helpful. <3

>	While we don't have fully automated guides, or we don't know all of the answers, there are a lot of options and opportunities for more folx to be doing this "hand holding" work. Within our movement technology organizations, we're looking for more funding to do this more in-depth support with transitions.
	Corps have the advantage that their software is taught starting school... So actually, having trainings is a must. In whatever mean that seems attractive and "effortless" in order to be able to compete.

10. How does mayfirst help orgs move to mayfirst and nextcloud? Are there step by step explainers about how to best move from google to mayfirst/nextcloud?

>	May First has an extensive set of help documents on Nextcloud: https://help.mayfirst.org/en/reference/about-nextcloud and there are a lot of resources on the Internet since it is a wildly popular platform. However, when it comes to moving, there is no one size fits all because every organization uses Google in a different way with different features.

11. What is the sensitivity of this deck (TLP preferred)? Can we share within our organization and/or direct partner network? 

>	Yes please share!

12. A comment, not a question: this is quite important and my proposal is to do the survey every year.

>	We agree and are trying to secure resources to do so!

>	If possible, broading territory and intersectionality (i.e. for gender, you can ask cis male, cis female, trans male, trans female, trans, nb... as an example, not the best, it can be others...)

13. I wonder what would have been the difference if you allowed people to choose there own obstacle instead of having a pre-established list.

> We did! But it was not included in the report because the answers were mostly complete sentences that didn't make a lot sense in a word cloud. We will be summarizing these responses in the final report. 

14. I'm curious - what is the additional infrastructure that needs to be built (with appropriate resourcing) to transition (e.g. technical assistance) and for the long-term e.g. does a quality community controlled tool that could replace google doc exist?

> I don't think we can really answer this question. We can't replace Google with a non profit Google. Instead, I think our goal should be to build more of an eco-system of providers so we can all build relationships with the human beings providing these services and think of our technology as something we "organize" rather than something we buy.

15. Question from Roberto: What calendar apps do you recommend? I see that Proton has a calendar. They offer VPN which I use 

>	answer from Redon (thank you Redon!!): Protonmail is amazing in terms of how they communicate they service and they have spent a lot of time with the UX of their service (which is rare for privacy oriented services). The issue is that it is another locked in ecosystem. I would say it is a better Google, but is that what we need? 

16. What's are possibilities (funding/labour contingent of course) for doing an expansion of the survey (after this round of the research ends). In many respects your results are the preliminary proof of concept and further research with groups/orgs/networks/mvts could be really amazing. 

>	Yes! The possibilities are high. Funders seem to love surveys. PTP has some applications in and we are hoping to continue researching this issue.

17. Has a thorough review of google drive/gmail/calendar third party lock down options been explored (ie. imposing encryption at rest/transit with self hosted encryption keys, chain of custody of docs and control of access for external recipients/collaborators, etc.),even ruling in or out the possibility of keeping the data protected against bad actors operating from within Google who may have conflicts of interest? A handful of solutions to both keep gmail but lock it down from even google appear to exist.
	
>	No. I'm not aware of any such review. Also, I would prefer to see our resources spent building an eco-system of autonomous providers rather than zeroing in on such a narrow need.
	
>> thank you for your response. I'm not sure I see agree that the need is narrow, quite the opposite, especially as if googles services can be used without google themselves having access to the data - if that's what possible under the framework I suggest above,then orgs would I suppose be able to use and collaborate on google without the inertia of migrating while still being able to be compliant with potentially even strict security policies. Kind of wondering why this may not have been explored as a first option?

18. Can mayfirst organize a space for organizations already committed to migration to talk about challenges and best practices?

>	Yes - please come to the May First TIAS group to help! https://mayfirst.coop/en/get-involved/

19. What recommendations do you have for small nonprofits with very limited budgets? We have sensitive data about people's immigration cases (are not lawyers though) and advocate for immigrant justice. 

>	First I encourage you to keep on organizing! We can't let Trump or any digital privacy activists intimidate us or scare us into curtailing our actions.

>	Second, I suggest reviewing all the data you are tracking and ask yourself: how are we using this data? Do we need to track it at all? If we do, how long do we need to keep it? If we had to turn it over, what would we tell the people whose data was exposed?

>	Many organizations make a habit of asking for information like gender identity, race identity, immigration status, etc without a plan to use it. If you need it, then definitely ask for it, but if you don't have plans to use it, don't track it.

>	You might consider writing a data policy that is based on your political values and emphasizes a commitment to minimize data collection. This will be valuable if you receive a subpoena requesting data that you don't keep - you can point to your data policy and turn over nothing.

>	No provider can help you if you receive a subponea - it doesn't matter where your data is hosted.

20. Future research? How many orgs/individiuals are able, willing, or aware of how much they could/need to pay to support other services (mvt infrastructure providers or "better than" google services (i.e. proton, cryptpad, etc)?

>	My understanding is that corporate tech feeds off of multi-million dollar organizations that pay enormous sums of money for "enterprise" features. I don't think we'll get there any time soon. But, if all the small organizations that want to move to movement hosting were able to get the help they need to make the transition, I think we would have a very healthy eco-system.

21. Jamie, can you or anyone  else from MayFirst speak about their personal experience in movig off of G, as an anecdotal example of what worked for them? 

>	I have at least 5 gmail accounts, but have never actually used it as a regular service so can't speak to moving off of it.

22. digital sovereignty is a concern that is becoming more common. What does Mayfirst think about physically hosting outside of the "empire" - i.e. outside of the USA/European Union. Does Mayfirst have any hosting options outside of the US. How can we change the domination of the USA in centralised internet hosting/services provision?

>	We're developing relationships with movement hosting providers in Canada and the Netherlands to explore options for more geographic diversity, especially in the face of a total political meltdown in the US and for resiliancy against climate disaster.

>	However, from a legal standpoint, the country your hosting provider is registered in is more important than the location of the servers. If May First gets a subpoena from the US government that we can't wiggle out of, it doesn't matter if our servers were located in North Korea. We would get a huge fine for every day we failed to turn over the data. 

>	Recommended reading about state/foreign policy discourse shifts around terms like "digital sovereignty and digital solidarity" 
	Digital solidarity vs. digital sovereignty: Which side are you on? (2024, July 10). _Security Intelligence_. https://securityintelligence.com/articles/digital-solidarity-vs-digital-sovereignty/

>	Note to whoever posted the above, "security intelligence" is run by IBM, so hardly recommended reading in relation to moving away from big tech. (also, that page is chock full of google tracking and does not even load with tracking blockers.)

> sorry! not "recommeded reading" because we agree, instead it's to know how the state departement, etc is co-opating terms like solidarity, etc.  +1 gosh that is really transparent, "rules based order" stuff. 🤮

23. If another webinar is organised to get people together, could the conference room remain open after the alloted time? Even if mods and admin need to go about their other business, i don't see why conversation could not continue among others, Of couse,somebody might have to volunteer to mod/rotate mods, or it could get chaotic, but people would always be free to leave.

> It would be great to have a space for people to continue the conversation and we would welcome anyone proposing a new link to take that conversation. We take responsibility for any conversation that happens as part of our webinar, so we wouldn't be able to responsibly leave the room while the conversation continued, but anyone is welcome to propose another venue and people are welcome to follow you to that venue to keep talking.

## Shared Resources

1. This is a resource from a network of French alt infra providers, Chatons.org that demonstrates an ecosystem approach to finding alternatives to different corporate/proprietary providers. It's just a database that connects your search request to options available but is something that shows a variety of alternatives to different tools, software, etc.  https://www.chatons.org/search/by-service

2. Electronic Frontier Foundation's Surveillance Self-Defense - https://ssd.eff.org/

3. Tactical Technology Collective's Holistic Security manual - https://holistic-security.org/

4. For group fundraising/cost sharing - https://opencollective.com/

5. Aspiration's Nonprofit Developers Summit - not just for developers! - https://aspirationtech.org/events/devsummit

6. Recent article on Google contract with Israeli government on Project Nimbus - https://theintercept.com/2024/12/02/google-project-nimbus-ai-israel/

7. Critique of "Meeting people where they live" - https://seizethemeans.communitarium.org/baslow/breaking-free-why-21st-century-activism-cant-just-meet-people-where-they-live
