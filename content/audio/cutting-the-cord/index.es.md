---
date: 2024-12-05
title: "Webinar Cortar el hilo: resultados"
mp3: "media/en/audio/cutting-the-cord.mp3"
webm: "media/en/audio/cutting-the-cord.en.webm"
type: "audio"
podcast_duration: "00:54:54"
bytes: "36859872"
tags: [ "callout"]
---

[Lamentablemente, la interpretación no se grabó correctamente, por lo que el audio que se comparte está solo en inglés]


*Preguntamos a diversos activistas si podían ayudarnos a hacer más fácil la
salida de Gmail y otros servicios de Google, y la reacción fue increíble.* Hemos
recibido más del doble de respuestas de las esperadas en nuestra encuesta, y
muchas personas se han puesto en contacto directo para expresar su entusiasmo y
apoyo a este proyecto.

**Está claro que, como movimiento, queremos dejar atrás Google. ¿Pero cómo?**

En el seminario web del 6 de diciembre de 2024 compartimos los resultados.
También puedes revisar las diapositivas [por aquí](slide-show.pdf).

![a veces solo hay que cortal el hilo](/es/post/2024/gmail-survey/cortar-el-hilo.png)

Este investigación es realizada por [May First](https://mayfirst.coop/) y
[Progressive Technology Project](https://progressivetech.org/) en colaboración
con [Infrared](https://in.fra.red/). Cuenta con apoyo financiero de la
[Association for Progressive Communications (APC)](https://apc.org/).

## Preguntas y respuestas

1.- ¿Se grabará esta conversación?

> Así es, el audio está siendo grabado. Les informaremos dónde pueden encontrarlo.


2.- ¿Existe una guía de recursos sobre plataformas más seguras y lo que pueden ofrecer?

> La Electronic Frontier Foundation tiene una de las mejores listas de recursos que existen: https://ssd.eff.org/es/


3.- Me da la sensación de que las herramientas de colaboración, lo cual ha sido nombrado como el mayor obstáculo, es una especie de evasiva (¿fue esa opción una excusa fácil?) todas las personas no se han movido, así que yo no me he movido... y lo dice alguien que eligió la colaboración como el mayor obstáculo.

> Como parte de una organización yo me veo diciendo «Trabajo con gente que no sé cómo enseñar a usar Nextcloud, por lo que sólo podemos usar Google Docs, porque es lo que ya conocen». Se trata de una señal de «intención» que proporciona oportunidades adicionales. Las funciones disponibles en las alternativas han mejorado con el tiempo, lo que supone una mejora general.

4.- ¿Cuál es el principal motivo por el que las personas dicen querer dejar Google?

> No hicimos esta pregunta en la encuesta, pero obtendremos más información en las entrevistas de seguimiento. Nos centramos más en los obstáculos que impiden abandonar la empresa que en los motivos por los que la gente quiere hacer la transición.


5.- ¿Existen referencias actualizadas con detalles prácticos para realizar una migración? Quienes pueden administrar grandes volúmenes de datos tecnológicos, ¿cómo pueden aprender a administrar en el mundo del software libre? ¿Y qué plazos son necesarios para que se produzcan las migraciones?

> Las organizaciones de alojamiento web que apoyan a los movimientos sociales están trabajando en este sentido, buscando financiamiento y recursos para conseguir más documentación, mejores guías y más capacidad para ayudar a los grupos en la transición a tecnologías cercanas a los movimientos sociales. Durante el resto de la campaña «Cortar el hilo» queremos desarrollar conjuntamente estrategias para este trabajo.

6.- ¿Podrían hablarnos de las ventajas respecto al tema de seguridad supone abandonar el hospedaje corporativo?

> A veces la seguridad no es una cuestión de blanco o negro. Sinceramente, Google puede ser muy seguro para algunos grupos en muchos aspectos y esto debe sopesarse dentro del contexto general de lo seguro que es tener tus datos y comunicaciones alojados en una corporación contra la que luchas, o similar a una contra la que luchas.

> Un recurso excelente para entender la seguridad en general: https://holistic-security.org/

7.- Mientras la «colaboración» me obligue a utilizar Gmail, teniendo que escribir a direcciones de Gmail, lo considero un obstáculo. ¿Alguien más opina lo mismo? Supongo que las personas de la moderación no pueden responder a esta pregunta.

> Muchas personas comentaron en el chat que pensaban esto mismo.

8.- ¿Cuáles son las diferencias de precios que suponen pasar de las grandes tecnológicas a estas opciones? Si May First y PTP alojan herramientas a las que nos podemos trasladar, ¿estamos compartiendo costes/recursos?

> A veces el uso de herramientas de código abierto o afines a los movimientos sociales tiene un costo monetario en comparación con las herramientas de Google. Y a veces los costes no son monetarios sino que, por ejemplo, tienen que ver con cosas como la perspectiva. Si una organización lleva a cabo una campaña contra Google por sus contratos con el ejército israelí y utiliza Google Docs para desarollarla, puede ser contraproducente en varios sentidos. Además, el precio de la tecnología corporativa viene en forma de vigilancia y marketing. Muchas plataformas de tecnología corporativa se diseñan esencialmente para la publicidad.

> Otras veces, los costos de la tecnología alternativa está subvencionada por las organizaciones y grupos que proporcionan la tecnología y, con más recursos, estas herramientas podrían ser mejores. Hay mucho que pensar al respecto.

> Para recaudación coletiva de fondos o para compartir gastos - https://opencollective.com/

9.- ¿Podríamos organizar un seminario web práctico para ayudar y guiar a las personas en el uso de alternativas como nextcloud o cualquier otra alternativa, evitando los tecnicismos que suponen una barrera para quienes no están familiarizados con las distintas alternativas de código abierto o se sienten intimidados por ellas? En mi organización la principal barrera para dejar Google Docs es la "sensación" de que las alternativas son demasiado confusas / no funcionan tan bien / no es tan necesario, ya que estamos trabajando en un cambio sistémico de mayor alcance. Hasta ahora, mis esfuerzos por rebatirlo han sido en vano. Creo que demostrar la eficacia igual o superior y la facilidad de uso de las alternativas sería muy útil. <3

> Si bien no tenemos guías totalmente automatizadas, o no sabemos todas las respuestas, hay un montón de opciones y oportunidades para que más gente haga este trabajo «de la mano». Dentro de nuestras organizaciones dedicadas a temas tecnológicos, estamos buscando más financiación para llevar a cabo este apoyo en profundidad durante las transiciones. Las corporaciones tienen la ventaja de que su software se enseña desde las escuelas... Así que, en realidad, tener formaciones es una necesidad. Y es necesario hacerlo con cualquier medio que parezca atractivo y «sin esfuerzo» para poder "competir".

10.- ¿Cómo ayuda May First a otras organizaciones en la migración a su infraestructura y uso de Nextcloud? ¿Existen tutoriales paso a paso sobre la mejor manera de pasar de Google a la instancia Nextcloud de May First?

> May First dispone de una extensa documentación de ayuda sobre Nextcloud: https://help.mayfirst.org/en/reference/about-nextcloud y hay muchos recursos en Internet, ya que se trata de una plataforma muy popular. Sin embargo, cuando se trata de trasladarse, no hay una única solución válida para cada caso, ya que cada organización utiliza Google de una manera diferente y con características distintas.

11.- ¿Cuál es la sensibilidad de la plataforma que mencionan (preferiblemente siguiendo una clasificación de semáforo o Traffic Light Protocol/TLP por sus siglas en inglés)? ¿Podemos compartirla dentro de nuestra organización y/o redes aliadas?

> Sí, por favor, ¡compártala!

12.- Tengo un comentario más que una pregunta: esta investigación es bastante importante y mi propuesta sería realizar la encuesta cada año.

> Estamos de acuerdo y trabajamos en conseguir recursos para hacerlo.

> De ser posible, nos interesa repetirla explorando diferentes territorios e interseccionalidad (por ejemplo, para género, se puede preguntar hombre cis, mujer cis, hombre trans, mujer trans, etc... como ejemplo quizás no es lo mejor, pueden ser otros...)

13.- Me pregunto si hubiera habido diferencia de haber permitido a la gente elegir su propio obstáculo en lugar de tener una lista preestablecida.

> ¡Lo hicimos! Pero no se incluyó en este informe porque las respuestas eran en su mayoría frases completas que no tenían mucho sentido en una nube de palabras. Resumiremos estas respuestas en el informe final.

14.- Tengo curiosidad por saber cuál es la infraestructura adicional que hay que crear (con los recursos adecuados) para la transición (por ejemplo, asistencia técnica) y a largo plazo (por ejemplo, ¿existe alguna herramienta de calidad gestionada por la comunidad que pueda sustituir a Google Doc?

> No creo que podamos responder a esta pregunta. No podemos sustituir a Google por un Google sin ánimo de lucro. En su lugar, creo que nuestro objetivo debería ser construir un ecosistema de proveedores para que podamos establecer relaciones con las personas que prestan estos servicios y pensar en nuestra tecnología como algo que «organizamos» en lugar de algo que compramos.

15.- Pregunta de Roberto: ¿Qué aplicaciones de calendario me recomiendan? Veo que Proton tiene un calendario. De hecho, ofrecen un servicio VPN que yo uso

> Respuesta de Redon (¡gracias Redon!): Protonmail es increíble en términos de cómo comunican su servicio y han dedicado mucho tiempo a la UX de su servicio (que es raro para los servicios orientados a la privacidad). El problema es que se trata de otro ecosistema cerrado. Yo diría que es un Google mejorado, entonces ¿es eso lo que necesitamos?

16.- ¿Cuáles son las posibilidades (en función de la financiación y la carga de trabajo, por supuesto) de ampliar la encuesta (una vez finalizada esta ronda de investigación)? En muchos aspectos, sus resultados son una prueba conceptual preliminar, y una investigación más profunda con grupos/organizaciones/redes/movimientos podría ser realmente valiosa.

> Sí. Las posibilidades son enormes. Parece que a las financiadoras les encantan las encuestas. PTP tiene algunas solicitudes en marcha y esperamos seguir investigando este tema.

17.- ¿Se ha explorado una revisión exhaustiva de las opciones de bloqueo de terceros para Google Drive/Gmail/Calendar (es decir, imponer el cifrado en reposo/tránsito con claves de cifrado autoalojadas, cadena de custodia de documentos y control de acceso para destinatarios/colaboradores externos, etc.), incluso descartando la posibilidad de mantener los datos protegidos contra actores malintencionados que operan desde dentro de Google y que pueden tener conflictos de intereses? Parece que existen un puñado de soluciones tanto para mantener Gmail como para bloquearlo incluso desde Google.

> No. No tengo conocimiento de ninguna revisión de este tipo. Además, preferiría que nuestros recursos se emplearan en crear un ecosistema de proveedores autónomos en lugar de centrarnos en una necesidad tan limitada.

> *Gracias por esa respuesta. No se si estoy de acuerdo en que esta necesidad es limitada, sino todo lo contrario, sobre todo si los servicios de Google se pueden utilizar sin que el propio Google tenga acceso a los datos - si eso es posible en el marco que sugiero más arriba, entonces supongo que las organizaciones serían capaces de utilizar y colaborar en Google sin la inercia de la migración, sin dejar de ser capaces de cumplir con las políticas de seguridad, incluso potencialmente estrictas. Me pregunto por qué esto no se ha explorado como primera opción.*

18.- ¿ Mayfirst podría organizar un espacio para que las organizaciones ya comprometidas con la migración hablen sobre los retos y las mejores prácticas?

> Sí - ¡ven al grupo TIAS de May First para ayudar! https://mayfirst.coop/en/get-involved/

19.- ¿Qué recomendaciones tiene para las organizaciones sin ánimo de lucro con presupuestos muy limitados? Tenemos datos confidenciales sobre casos de migración (aunque no somos abogados) y defendemos la justicia para las personas migrantes.

> En primer lugar, ¡les animo a seguir organizándose! No podemos dejar que Trump o cualquier activista de la privacidad digital nos intimide o nos asuste para que reduzcamos nuestras acciones.

> En segundo lugar, sugiero que se revisen todos los datos que estás registrando y nos preguntemos cómo estamos utilizando estos datos ¿Necesitamos registrar cada uno de ellos? Si es así, ¿durante cuánto tiempo debemos conservarlos? Si tuviéramos que entregarlos, ¿qué diríamos a las personas cuyos datos se han visto expuestos?

> Muchas organizaciones tienen la costumbre de pedir información como la identidad de género, la identidad racial, el estatus migratorio, etc. sin un plan para utilizarla. Si la necesitas, no dudes en pedirla, pero si no tienes planes de utilizarla, no la recojas.

> Puedes plantearte redactar una política de datos basada en tus valores políticos y que haga hincapié en el compromiso de minimizar la recopilación de datos. Esto será muy útil si recibes una citación judicial solicitando datos que no guardas: puedes remitirte a tu política de datos y no entregar nada.

> Ningún proveedor podría ayudarles si reciben una citación: no importa dónde estén alojados sus datos.

20.- ¿Futuras investigaciónes? ¿Cuántas organizaciones/individuos son capaces, tienen disposición o conciencia sobre cuánto podrían/necesitarían pagar para dar apoyo a otros servidores (proveedores de infraestructura tecnológica alineados con los movimientos sociales o «mejores que» los servicios de Google (por ejemplo, proton, cryptpad, etc.)?

> Según tengo entendido, la tecnología corporativa se nutre de organizaciones multimillonarias que pagan enormes sumas de dinero por prestaciones «empresariales». No creo que lleguemos a eso pronto. Pero, si todas las pequeñas organizaciones que quieren cambiar sus servicios tecnológicos a infraestructuras más alineadas con sus valores fueran capaces de obtener la ayuda que necesitan para hacer la transición, creo que tendríamos un ecosistema muy saludable.

21.- Jamie, ¿podrías tú o cualquier otra persona de MayFirst hablar sobre su experiencia personal en la migración desde Google, como un ejemplo anecdótico de lo que funcionó para ustedes?

> Tengo al menos 5 cuentas de gmail, pero nunca lo he utilizado como servicio habitual, por lo que no puedo hablar de cómo dejarlo.

22.- La soberanía digital es una preocupación cada vez más común. ¿Qué opina May First del alojamiento físico fuera del «imperio», es decir, fuera de EE.UU./Unión Europea? ¿Tiene May First alguna opción de alojamiento fuera de EE.UU.? ¿Cómo podemos cambiar la centralidad de EE.UU. en la oferta de servicios de alojamiento/servicios de Internet?

> Estamos explorando opciones con proveedores de servicios tecnológicos aliados en Canadá y los Países Bajos con la intención de contar con una mayor diversidad geográfica, especialmente de cara a un colapso político en los EE.UU. y pensando en acciones resilientes contra el desastre climático.

> Sin embargo, desde un punto de vista legal, el país en el que está registrado el proveedor de servicios web es más importante que la ubicación de los servidores. Si May First recibe una citación del gobierno estadounidense de la que no podemos librarnos, no importa que nuestros servidores estén ubicados en Corea del Norte. Nos impondrían una multa enorme por cada día que no entregáramos los datos solicitados.

> Una lectura recomendada sobre los cambios discursivos en política exterior/estatal: Solidaridad digital frente a soberanía digital: ¿De qué lado estás? (2024, 10 de julio). La inteligencia de la seguridad. https://securityintelligence.com/articles/digital-solidarity-vs-digital-sovereignty/

> *Nota para quien haya publicado este artículo: «Security Intelligence» está dirigida por IBM, por lo que no es una lectura recomendable para alejarse de las grandes empresas tecnológicas (además, esa página está repleta de rastreo de Google y ni siquiera se carga con bloqueadores de rastreo).*

> *Lo siento, la intención de la lectura recomendada no es mostrar acuerdo con ella sino para saber cómo el Departamento de Estado [de Estados Unidos], etc. está cooptando términos como solidaridad, etc.*

> *+1 vaya, eso es realmente claro, cosas del orden basado en las «reglas establecidas». 🤮*

23.- Si se organiza otro seminario web para reunir a la gente, ¿podría permanecer abierta la sala de conferencias después de la hora programada?  Incluso si las personas moderadoras y administradores tuvieran que ocuparse de otros asuntos, no veo por qué no podría continuar la conversación entre las demás personas asistentes. Puede que alguien tuviera que ofrecerse para moderar o se rotaría la moderación para evitar que haya caos, pero la gente siempre sería libre de marcharse.

> Sería genial tener un espacio para que las personas continuaran platicando sobre el tema y agradeceríamos que alguien propusiera un nuevo enlace para hacerlo. Asumimos la responsabilidad de cualquier conversación que se produzca como parte de nuestro seminario web, por lo que no podríamos abandonar la sala de forma responsable mientras la conversación continuara allí, pero podría proponerse otra sala y las personas interesadas podrían mudarse allí para continuar la conversación.

## Recursos compartidos

- Este es un recurso de una red francesa de proveedores de infraestructura alternativa, Chatons.org, que muestra un enfoque ecosistémico para encontrar alternativas a diferentes proveedores corporativos/propietarios. Es sólo una base de datos que conecta su solicitud de búsqueda con las opciones disponibles, pero es algo que muestra una variedad de alternativas a diferentes herramientas, software, etc. https://www.chatons.org/search/by-service

- Electronic Frontier Foundation’s Surveillance Self-Defense - https://ssd.eff.org/es/

- Manual de seguridad holística de Tactical Technology Collective - https://holistic-security.org/

- Recaudación coletiva de fondos o para compartir gastos - https://opencollective.com/

- Aspiration's Nonprofit Developers Summit - ¡no sólo para desarrolladores! - https://aspirationtech.org/events/devsummit

- Artículo reciente sobre el contrato de Google con el gobierno israelí en relacionado al Proyecto Nimbus - https://theintercept.com/2024/12/02/google-project-nimbus-ai-israel/

- Crítica de «Meeting people where they live» - https://seizethemeans.communitarium.org/baslow/breaking-free-why-21st-century-activism-cant-just-meet-people-where-they-live
