+++
date = "2020-05-08T17:00:07Z" 
title = "Covid 19 Membership Checkin"
mp3 = "media/en/audio/covid-19-membership-checkin.mp3"
webm = "media/en/audio/covid-19-membership-checkin.webm"
type = "audio"
podcast_duration = "0:25:17"
bytes = "15188879"
+++

All members were invited to this quarterly convergence of members to confer
with the Board on decisions it must make in its upcoming meeting. 

The membership consultation took the form of small group discussions focused on
two rounds of questions: 

1) Think about what you, your organization, and the people and or movements you
work closest with need most right now? What does this world need? What changes
are necessary for this to happen?  

2) What role can you see May First Movement Technology playing in helping bring
about those changes? Should this alter our plans and priorities? If so, how so?

The recording captured the summary session - where all small groups reported
back on their discussions.

In addition, members participated via our [online discussion
system](https://comment.mayfirst.org/t/members-thinking-about-what-we-should-do/1715).
Notes from the live meeting are posted to the online discussion.

