+++
date = "2020-11-13T21:00:14Z" 
title = "2020 Membership Meeting: Board Orientation"
mp3 = "media/en/audio/mm2020-board-orientation.mp3"
webm = "media/en/audio/mm2020-board-orientation.webm"
type = "audio"
podcast_duration = "1:32:56"
bytes = "54074215"
+++

This session provides an overview of the organization, specifically intended for anyone interested in joining the Board.
