+++
date = "2021-07-13T12:10:08Z" 
title = "Ame Elliot - Ux Design For Shifting Power"
mp3 = "media/en/audio/ame-elliot-ux-design-for-shifting-power-tias.mp3"
webm = "media/en/audio/ame-elliot-ux-design-for-shifting-power-tias.webm"
type = "audio"
podcast_duration = "0:53:26"
bytes = "42598614"
+++

### Ame Elliot - UX Design for shifting power

**2021-07-02**: 
Ame Elliot talks about her work at Simply Secure providing design support and coaching for technology projects that center and protect vulnerable populations.
[Follow the pdf presentation.](https://share.mayfirst.org/s/SgcoXEEm3WNts2F)

![Ame elliot](/img/ame.jpg "Ame")

[Ame Elliot](https://simplysecure.org/who-we-are/ame.html) is the Design Director for Simply Secure, an educational nonprofit building a community of professional practitioners who put people at the center of privacy, security, transparency, and ethics. 

Additional references:

* [Pattern Library for Decentralization](https://decentpatterns.xyz/)
* [Simply Secure's Knowledge Base](https://simplysecure.org/knowledge-base/)
* [Participant Rights](https://simplysecure.org/blog/participant-rights)
* [Wire framing](https://simplysecure.org/blog/wireframing-intro)
* [Make it physical, print it out. Pin it up. Look at it](https://simplysecure.org/assets/blog/2016/architecture.jpg)

<p>

---
