+++
date = "2018-04-11T01:33:49Z" 
title = "Klast (Points Taken)"
webm = "media/en/audio/klast-points-taken.webm"
mp3 = "media/en/audio/klast-points-taken.mp3"
type = "audio"
podcast_duration = "0:35:29"
bytes = "21515978"
+++
In one of his Blacklist Podcasts, Lucas talks with Klast whose rare and intensive knowledge of gang and street culture combines scholarship with personal experience: a rare combination and a fascinating conversation. This is street culture coming from people who lived it.

