+++
date = "2018-07-26T14:55:18Z" 
title = "Everything Is Changing: May First/People Link Infrastructure Plans Webinar"
mp3 = "media/en/audio/everything.is.changing.infrastructure.plans.webinar.mp3"
webm = "media/en/audio/everything.is.changing.infrastructure.plans.webinar.webm"
type = "audio"
podcast_duration = "0:45:28"
bytes = "31775348"
+++

Every May First member needs to join us at a special members webinar/discussion about the online systems you use to communicate every day. The political changes all over the world, the crises human beings face and the rapid shifts and developments in the movements in which we work demand that May First respond more quickly and effectively than ever before. Yet the underlying architecture of May First/People Link's hosting has stayed the same for 15 years. That simply has to change and it's about to with major repercussions for our systems and our members. Your online communications systems are going to become more stable and more flexible than ever before and you need to know about it.

Our new plan is documented on our wiki:

https://support.mayfirst.org/wiki/infrastructure-2018

Come to the webinar to learn about it, ask questions and make suggestions.

Please note: Due to technical difficulties a portion of Jaime Villarreal's presentation was cut because the recording was inaudible.

