+++
date = "2021-07-02T11:45:12Z" 
title = "Nechari Riley - Investigación centrado en el ser humano"
mp3 = "media/es/audio/Nechari-Riley-Human-Centered-Research-TIAS.mp3"
webm = "media/es/audio/Nechari-Riley-Human-Centered-Research-TIAS.webm"
type = "audio"
podcast_duration = "0:52:37"
bytes = "43758173"
+++

### Nechari Riley - Investigación centrado en el ser humano

**2021-07-25:** 
Nechari Riley nos comparte reflexiones sobre su viaje para convertirse en una investigadora de diseño UX del "poder popular".
[Sigue la presentatión pdf](https://share.mayfirst.org/s/PPjtSL5rQpHFZpX)

![Nechari Riley](/img/nechari.jpg "Nechari") 

[Nechari Riley](https://www.nechari.com/about-me/) es una investigadora y estratega de métodos mixtos que trabaja con organizaciones para desarrollar proyectos basados en la investigación que sean inclusivos, accesibles y atractivos.

<p>

---