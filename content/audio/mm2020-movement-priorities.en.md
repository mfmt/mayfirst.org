+++
date = "2020-11-06T23:05:06Z" 
title = "2020 Membership Meeting on Movement Priorities"
mp3 = "media/en/audio/mm2020-movement-priorities.mp3"
webm = "media/en/audio/mm2020-movement-priorities.webm"
type = "audio"
podcast_duration = "0:52:21"
bytes = "30263613"
+++

At this meeting we discussed our Movement organizing priorities for the coming year.
