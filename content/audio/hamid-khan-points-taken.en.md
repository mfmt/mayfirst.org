+++
date = "2018-04-16T12:27:54Z" 
title = "Hamid Khan (Points Taken)"
webm = "media/en/audio/hamid-khan-points-taken.webm"
mp3 = "media/en/audio/hamid-khan-points-taken.mp3"
type = "audio"
podcast_duration = "0:44:57"
bytes = "26842285"
+++
One of the leaders in the movement against police abuse and spying, Hamid Khan heads up the Los Angeles-based End LAPD Spying Coalition. He is one of the country's foremost experts on the developing police state and how we can stop it from overtaking us and Lucas Lopez talks with him about this critically important subject.

