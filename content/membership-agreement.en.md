+++
date = 2015-10-01T13:54:39Z
title = "Membership Agreement"
[menu]
  [menu.more]
    parent = "Membership"
    weight = 30
+++

May First Movement Technology is a very diverse organization and values and
membership benefits reflect our goal of uniting all progressive and social
justice activists. In that context, our members expect some things from each
other and our staff. What members can expect of our organization and staff:

 * Our organization will upgrade, maintain and expand all equipment and
   commonly shared software based on members' needs and our capability.
 * Our organization will assure that all our networks and systems are
   functional and that, in the case of an outage or problem, our staff will
   prioritize repairs, fixes and substitutions when necessary.
 * Our staff will provide full, responsive and respectful technical support via
   our public ticket tracking system.
 * Our directors will communicate with the entire membership regularly and
   openly on organization issues and developments and report any emergency
   situations, their status and what we're doing about them to the entire
   membership when an emergency arises.
 * Our organization will protect your data by fiercely resisting un-authorized
   attempts by others to access it (including legal and non-legal strategies)
   and by running nightly onsite and offsite backups.

What our organization and staff expects of our members:

 * As members, we respect the boundaries of taste, decency and respect for all
   peoples in the content of web sites and other communications.
 * Members pay our dues in a timely fashion as one of our key ways of
   supporting our organization (see dues payment and coverage policy below).
 * May First sends membership notices by email, including technical, billing
   and political organizing messages. Members are responsible for keeping their
   email contact information up to date and ensuring that May First email is
   properly channeled to the appropriate people.
 * Members support each others work when and however we can given our own
   programs, strategies, goals and resources.
 * Members treat our staff with respect and sensitivity to the enormous amount
   of work they contribute to our organization.
 * Members participate in the growth of our organization by encouraging other
   people they know to join.
 * Members assume responsibility for maintaining, upgrading, fixing and
   modifying the software and web sites specific to their membership. 

If you have questions, comments or, sure, disagreements after reading this,
please feel free to let us know.

## Dues Payment payment policy 

When it comes to dues, May First prioritizes communication. Please let us know
if you are not able to pay your dues and we can work out a plan. If the dues
are too high, May First's board has a Resource Usage Committee designed to
address these needs.

May First sends payment reminders by email starting 2 weeks prior to your
payment due date and will send at least 4 past due reminders.

May First offers a generous 3 month grace period for members behind on their
dues, during which time your hosting services will remain fully functional.

After three months, May First will disable your hosting services. If you reach
out to us to establish a line of communication, we will re-enable your services
as we figure out a payment plan.

If we do not hear from you for 3 months after disabling your membership, we
will cancel your account and remove your data.

## Hosting policy

May First members are entitled to host as many web sites, email addresses,
database etc. as they would like provided they stay under their designated disk
space allowance.

However, as a democratic membership organization, we need all organizations and
individuals using our resources to be members in their own right. If members
host other organizations' or individuals' resources under their own membership
it undermines our democratic model and prevents us from collecting the
resources we need to build and maintain our infrastructure.

Our policy is flexible. For example, individual members are welcome to host
other family member's sites and web developers should feel free to host staging
sites from clients under their memberships. 

However, we ask all members to consider how best to support the organization
when making these decisions.
