+++
date = 2020-09-21T14:47:48Z
description = "Welcome to the May First orientation page"
title = "May First Orientation"
tags = [ "" ]
[menu]
  [menu.more]
    parent = "Membership"
    weight = 40

+++

May First is an organization with a long history and many moving parts. This
page is a collection of links to help orient you to our history and how we
operate.

## Who we are: Our Members, Mission and Program

May First is a movement building membership organization that focuses on
technology.

As of February 2025, we have 759 members in good standing, most are based in the United
States, about 20% are based in Mexico and the rest spread out around the world.

About 60% of our members are organizations and the rest are individuals.

Our member organizations range from large organizations like Media Justice, CWA
Local 1180, National Network of Abortion Funds and more to small street medic
collectives, media production groups, and community organizations.

The issue areas range from universal healthcare to Palestinian rights to
immigrant freedom and many many more.

Our mission, as approved by our membership in 2015, is:

> *May First engages in building movements by advancing the strategic use and
> collective control of technology for local struggles, global transformation,
> and emancipation without borders.*

Our program, while complex, varied and changing, fundamentally flows from two
inter-related areas:

1. **Engagement and Communications:** engaging the broader left movement in
   conversation about the fundamental role of technology in our fight for a
   more just world and helping to develop a strategy for change that centers
   technology. 

2. **Technology infrastructure and services:** collectively running our own
   Internet-based technology services for our members to provide privacy and
   stability for critical movement activities and showcase and experiment with
   technology based on our values.

## History and culture

When did May First begin? Was is 1994? 2005? Read more about the [key points in 
our organization's history](https://share.mayfirst.org/s/4Fnb8MBX6ffPK2K).

Throughout our history, we have made a commitment to the leadership of people
traditionally excluded from technology, which was formalized into [a statement
of intentionality on combatting racism and
sexism](https://mayfirst.coop/en/intentionality/).

Our culture also includes an [agreement on how we interact with each
other](https://share.mayfirst.org/s/fdAEDg3xRGpXmqG) that identifies the
behaviors and practices we strive for and those we try to avoid and also [tips
on scheduling and running
meetings](https://share.mayfirst.org/s/X7yPJGFPEMWdsoP).

## Mexico

As you can read from our history, we have a special relationship with activists
and movement organizations in Mexico. The details are in the
[bylaws](https://mayfirst.coop/en/bylaws/) and the [history
document](https://share.mayfirst.org/s/LFmQx74wK3SQpEy), but here is a quick
summary.

May First is an *international* organization - we have members from all over
the world and welcome anyone no matter where you live.

However, May First started in the United States and most members are based in
the US. In 2011, the historic Mexican alternative Internet provider LaNeta
decided to stop providing hosting services and asked if May First would
consider integrating their members. We agreed, and the LaNeta members approved,
resulting in our first substantial number of members from a country other then
the US (about 20%).

This arrangement aligned with our organization's political vision - not only the
idea that borders do not stop our movement building, but specifically
recognizing the importance of the relationship between the movements of the US
and Mexico, given the border, the economic pressures, xenophobia and racism
that have distorted this relationship.

Although Mexican members are members just like anyone else, we have included in
our bylaws a provision to reserve a number of board members from Mexico
proportionate to the representation of Mexican members in our membership.

## Structure and decision making

We have one of the most diverse leadership of any technology organization. To
learn more, see our [board membership page](https://mayfirst.coop/en/who/)!

Our [bylaws](https://mayfirst.coop/en/bylaws/) define the minimal requirements
of our structure and decision making.

The bylaws state:

 * *The membership elects 80% of the board, and staff chooses the remaining 20%.*
   We currently have 23 [board members](https://mayfirst.coop/en/who).

 * *The Workers Review committee of the Board hires and fires workers* (workers
   include people who are paid and unpaid). Currently, we have three workers:
   Jaime Villarreal (paid), Jes Ciaci (paid) and Jamie McClelland (volunteer).

However, there is much more to our structure and decision making than what is
defined in the bylaws. 

For example, the board typically asks our members to help build a set of
**priorities for the coming year** and then rank them in terms of importance at our
annual membership meetings (see our [current priorities](/priorities/)).

### Teams and Committees

#### Program Teams 

May First engages our members in our work via two **program teams**:

 * **The Engagement and Communications Team:** Covers engagement with the
   broader movement, such as conference participation/webinars/presentations,
   participating in campaigns and coalitions, political/popular education.

 * **The Technology Infrastructure and Services Team:** Covers the management,
   development and support of all technology infrastructure and services.

All May First members (not just Board members) are encouraged to join these
teams, which meet regularly (at least monthly) at clearly posted times, with at
least one Board-appointed coordinator, preferable two.

The program coordinators are responsible for drafting a work plan, based on
feedback from the program team, for discussion by the coordination committee
and approval by the Board. Once a workplan is approved by the board, the
program coordinators are authorized to make any implementation decisions
consistent with the approved workplan.

The program teams are not formal decision making bodies - the coordinators
retain the right to make final decisions within their scope of the work plan.
Instead, they are designed to engage members in our work, brain storm and
discuss program ideas, provide initial feedback on workplans and evaluation
reports, and coordinate the work. Toward this end, they should be open,
inviting, and flexible.

Program coordinators are also responsible for submitting evaluation reports at
the end of the year (based on feedback from the program team). The reports
should be submitted to the Board for approval and then presented to the
membership during the annual membership meeting for discussion prior to the
selection of the following year's priorities.

#### Coordination Committee (CC)

  * **Purpose** - The purpose of the coordination committee is to connect and coordinate activities across all MFMT teams and processes and ensure decisions and commitments made in all parts of the organization are consistent with the direction of the Board and member priorities.

  * **Composition** - The coordination committee is limited to current board members. 

  *  **Responsibilities** - The Coordination Committee meets weekly with the following responsibilities:
     1.  **Information Sharing and coordination** - The CC receives reports on all activities of the organization and helps coordinate work between teams. The CC is also responsible for ensuring decisions and commitments made in all parts of the organization are consistent with the direction of the Board and member priorities.

     2. **Feedback and Decision Making** - The CC considers proposals and questions from: workers, other teams, and its own members. Depending on the nature of the issue, the coordination committee may:
        - *Provide feedback*: when the final decision should rest with the workers, team or person that raised the issue
        - *Make decisions*:  for operational or administrative issues too important for the team  that raised them to make on their own, but not important enough (or too  time sensitive) for the Board to make
        - *Escalate to the Board*: refine and forward the issue to the Board for more consequential decisions.

     3. **Board Support** - The CC assists the Board by:
        - Developing board meeting agendas
        - Discussing and preparing larger strategic decisions for the organization
        - Implementing or assigning decisions made by the Board

#### Additional Committees 

In addition to the program teams and the coordination committee, our bylaws
specify a Worker Review committee that meets when needed (see above). 

Other ad-hoc committees may also be formed, such as a membership meeting
planning committee, to take up particular tasks. All additional committees are
open to any board member and should be announced, along with a clear mandate,
to the full board prior to formation. 

### How are proposals made? How do I get involved? What decision making process is used?

Typically, most major decisions start with the program team that it is relevant
to. 

Next, they move to the coordination committee for discussion.

If the decision is minor, or clearly within the scope of the already defined
workplan, then it might be sent to a program committee to be implemented or to
the workers to implement. 

Otherwise, it goes to the board for a vote.

If the decision requires a change to the bylaws, then it must go to the
membership meeting for a final approval. 

Ultimately, final decisions can be made by the May First board via a simple
majority vote. However, the culture is to make every effort to achieve
consensus before resorting to a vote.

### Responsibilities of Board Members

Board members are elected for 3 year terms. The minimum requirements of being a
board member are:

 * Attend eight, 1.5 hour online board meetings per year
 * Spend up to 1 hour per week reading and responding to email
 * Attend the annual membership meeting.

In addition, while not required, board members are encouraged to participate in
at least one of the program committees.

## Finances

May First workers are responsible for presenting a financial report to the
board every year. See our [final 2023 Financial
Report](https://share.mayfirst.org/s/gBDCCQKgdKQaa2w).

The board is responsible for approving the budget, based on the workplan, each
year.

## Tools

The main communications tools we use:

 * Email lists: all board members are subscribed to an email discussion list.
   The coordination team also has a list. Other work teams employ their own
   communication tools.

 * Nextcloud: The board and all workgroups rely heavily on our
   [Nextcloud](https://support.mayfirst.org/wiki/nextcloud) document storage
   system. We have folders for all committees and take regular notes for all
   committee meetings. Most proposals are written and saved in Nextcloud and
   final reports are kept here as well.

 * Discourse: Our [Discourse server](https://comment.mayfirst.org/) is a
   web-based discussion forum for members to comment on topics and proposal. We
   heavily use Discourse during our member meetings - so members can choose
   either to come to a live meeting if the schedule lines up, or participate
   asynchronously via the Discourse system.

 * Jitsi Meet: We are increasingly using the [Jisti Meet videoconferencing system](https://support.mayfirst.org/wiki/web-conference) for live meetings.  

