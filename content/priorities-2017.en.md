+++
date = 2015-11-24T14:47:48Z
description = "Priorities (2017)"
title = "Priorities"
+++

The November 2016 Membership Meeting approved the following priorities for 2017.

See our [current year priorities](/en/priorities)

## Online Group

 * Investigate and educate members about technologies and systems architectures that are more distributed, as highly available as possible and easier to maintain in the face of high traffic or attacks, especially in collaboration with allied groups that have similar needs.
 * In light of Trump presidency and the Mexican/US base of MF/PL, target fund-raising efforts (with a slogan such as “we build networks while others build walls”) and educational outreach to groups that are particularly threatened. Our goals are both to help with tech education and to bring these groups in as paying members by exploring the security trade-offs we face when developing technology and building awareness of the choices we make in deciding which to use.
 * Dedicate human resources (new paid staff person, whether full or part-time, or volunteer) to focus on development (foundation or direct appeal, crowdfunding, or other outreach focusing on building paid membership).

## Brooklyln Group One

 * May First/People Link shall form an Education committee, for Technological & Political Education, which will be responsible for creating a curriculum covering revolution & technology, including significantly security and collaboration. This will be used to educate the leadership committee first, then the membership, and then (informed with this added knowledge and experience, and modified for a popular audience) the wider public, in particular technologists and movement organizers. Clarification: Education is all about organizing.
 * May First/People Link shall engage in a complete survey of its members, including one-on-one interviews with each, to learn their current technological and political approach, and pain points. This will inform both technological & political education and development of technology services and coalition-building. May First/People Link shall develop a database of technology and political providers by skills and costs as part of this.
 * May First/People Link shall form a Fundraising committee which will identify specific projects (or crises) suitable for crowdfunding, and also suggest when offering strategic technological & political services (in partnership with identified technology and political partners) to organizations is compatible with and can help fund technological & political education and improvements to the May First platform.

## Brooklyn Group Two

 * Share stories from members that answer the question of how they approach issues of technology & social change and share those learnings as stories with the membership. This could be a part of a larger communication or fundraising strategy. Specific ideas: broadband co-ops, youth programs, POC Techies program, Stories from Cointelpro, etc.
 * Focus on education broadly, specifically around issues of networked systems (e.g. IoT) and privacy, security, impact to people. It would ideal for MayFirst to be a cornerstone for resources for education – they don’t need to create the resources themselves, but could be a hub for resources that members within the organization have developed already, or for connecting members who could co-develop resources and popular education materials. Along these lines a 5 minute video about the issues could be good.
 * Given the new pricing model for membership, it would be helpful to have materials for outreach that make it clear what membership gives you e.g. $25 as an individual gets you ownCloud, email lists etc, $50 gets you XYZ. Members could then recruit other members and help build the network. Approaching foundations seems like a potential opportunity, but the Leadership Team needs to decide on a process for MayFirst to agree on what funding to pursue or not, and potentially activate members to help with doing fundraising from foundations or other funding sources, e.g. this could be a committee of members who have experience in fundraising. The understanding here is that this would be building on the existing movement work as part of a shift to Technology & Revolution as the focus of the movement work. This work would happen in movement spaces where MayFirst is already working – Left Forum, Social Forum, etc.

## Brooklyn Group Three

 * Invest in recruitment strategies for individual and organizational memberships. using the new funding structure, focusing on communities of color and those with diverse relationships to technology.
 * Investigate alternative revenue streams such as establishing a consulting service for activists/non-profits.

## México City combined group 1 and 2

 * Building strategies to improve the participation of members through campaigns and common projects, deepening our political development based on the reality of each center (N.Y. and Mexico), with diagnostic tools among other things.
 * Work toward the financial sustainability of the organization through the promotion of membership and the improvement of the system of dues payments.
 * Continue the installation of test servers project in other countries looking to augment and transfer technological knowledge.
 * International projection to create a third and fourth regional organizing center for May First/People Link.

