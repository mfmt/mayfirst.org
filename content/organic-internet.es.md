+++ 
title = "The Organic Internet" 
date = 2021-05-12T13:53:34Z
[menu]
  [menu.more]
    parent = "Acerca de Nosotros"
    weight = 900

+++

El *Internet orgánico* se publicó en 2007, dos años después del Primero de Mayo
Nace Movement Technology (entonces conocida como May First/People Link).
Proporcionaba nuestro análisis inicial de la relación entre el movimiento e
Internet y guió nuestra estrategia durante los años siguientes.

[Descargar la versión en PDF (en inglés)](/files/organicinternet.1.5.pdf)
