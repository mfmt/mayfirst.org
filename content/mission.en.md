+++
date = 2015-10-01T13:52:02Z
title = "Mission Statement"
[menu]
  [menu.more]
    parent = "About"
    weight = 20
+++

May First engages in building movements by advancing the strategic use and collective control of technology for local struggles, global transformation, and emancipation without borders. 

*Approved by the membership in November 2015 as part of our [official documents](/en/official-documents).*

