+++
date = 2019-08-21T14:47:48Z
description = "Bienvenido a la reunión de miembros de la tecnología del primer movimiento de mayo"
title = "Reunión de miembros 2019"
tags = [ "callout" ]
+++


**¡Gracias a todos los que asistieron a nuestra reunión de membresía 2019!**
  
Aprobamos la propuesta de votar por nuestra junta directiva a través de un sistema de votación en línea.
  
Hemos enviado a todos los miembros en regla un correo electrónico con un enlace para usar para emitir su voto.
                                                                                                                                        
Si no recibió un correo electrónico, envíe un correo electrónico a: support@mayfirst.org para solicitar su token.
                                                                                                                                        
El período de votación en línea se extenderá desde el martes 8 de octubre hasta
el lunes 14 de octubre (a la medianoche tiempo America/New_York).
                                                                                                                                        
Puedes ver una [lista de todos nominados]
(https://vote.mayfirst.org/es/nominations/2019) y también un [video presentación
de todos los nominados] (https://vote.mayfirst.org/es/node/171).

Esta página es la página oficial que documenta la reunión y se mantendrá
actualizada Fecha con la última información.

## Infographic

Nuestra reunión de membresía se resume en el siguiente gráfico de información.
¡Desplácese hacia abajo para obtener detalles adicionales!

![Meeting details info graphic](/img/mfmt_infographic_es.web.png)

## La reunión principal

Una vez que se tome la asistencia y hayamos determinado que tenemos quórum,
estaremos capaz de hacer propuestas para la aprobación de los miembros. Al
votar, los miembros individuales obtienen un voto y los miembros de la
organización obtienen dos votos. Incluso si tu organización envía 10 personas a
la reunión, todavía solo obtienes dos votos.
                                                                          
La agenda es:

1. **Propuesta para las elecciones de la Junta.** Esta propuesta pedirá a los
   miembros que aprueben la elección de la junta a través de un sistema de
   votación en línea de una semana para garantizar que puedan participar tantos
   miembros como sea posible (en lugar de permitir que los miembros presentes
   en la llamada a votar). Ver abajo para más detalles.

2. **Informes.** Habrá tres informes, todos basados en las conversaciones en
   las reuniones de consulta de miembros que preceden a la reunión de Miembros. 
 * *Informe de trabajo del movimiento:* este informe detalla las muchas áreas
   de trabajo con nuestros movimientos en los que estamos involucrados, así
   como algunas propuestas para el futuro ... todo adjunto a las estimaciones
   de costos 
 * *Informe de tecnología:* un informe sobre el estado de nuestra tecnología y
   nuestras prioridades para desarrollarla y el presupuesto que requerirá.
 * *Informe financiero:* como su nombre lo indica con aportes de la tercera
   convergencia previa a la reunión el 25 de septiembre

[¡Por favor regístrese para asistir ahora!](https://outreach.mayfirst.org/index.php?q=civicrm/event/register&id=60&reset=1)

## Calendario de fechas y eventos previos a la reunión

### Martes 4 de septiembre

Las nominaciones públicas para nuestro consejo de administración están abiertas
a través de nuestro [sitio de votación](https://vote.mayfirst.org/es/bienvenido) y se
anuncian a todos los miembros.

### Reuniones previas: semana del 11 de septiembre - semana del 25 de septiembre

Hay tres reuniones previas para prepararse para la reunión principal de miembros.

Todas las reuniones tienen lugar en línea a través de
[mumble](https://support.mayfirst.org/wiki/mumble.Es). Antes de cada reunión, un
el informe se publica a través de nuestra [discusión en línea
foro](https://comment.mayfirst.org/) permitiendo que los miembros no puedan asistir
participar en la discusión. Después de cada reunión, los resultados se publican en
el panel de discusión de la conversación puede continuar conduciendo a la página principal
reunión.

 * *Miercoles, 11 de septiembre, 14:00 - 15:30 Hora de Ciudad de Mexico:* Generar prioridades de movimiento para el próximo año. Por favor lea el [informe de movimiento](https://share.mayfirst.org/s/r4piD7X5qDKx4bo). Si no puede asistir a la reunión, [comente en nuestro foro](https://comment.mayfirst.org/t/2019-membership-meeting-movement-priorities/1519).
 * *Viernes, 20 de septiembre, 14:00 - 15:30 Hora de Ciudad de Mexico:* Discuta el plan de infraestructura tecnológica y las cuotas para
   cambios de servicios. Por favor, lea el [informe de tech](https://share.mayfirst.org/s/mXBzjx49QifTqAE). Si no puede asistir a la reunión, [comente en nuestro foro](https://comment.mayfirst.org/t/2019-membership-meeting-infrastructure/1520).
 * *Miercoles, 25 de septiembre, 14:00 - 15:30 Hora de Ciudad de Mexico:* Lo que significa ser una cooperativa, un [informe sobre finanzas](https://share.mayfirst.org/s/DCt9w2z2ijTks3o),
   responsabilidades de los miembros de la junta y el funcionamiento general de la cooperativa. Por favor, [comente en nuetro foro](https://comment.mayfirst.org/t/what-it-means-to-be-a-coop/1530.) 

[¡Por favor regístrese para asistir ahora!](https://outreach.mayfirst.org/index.php?q=civicrm/event/register&id=60&reset=1)

### Martes 1 de octubre

Fecha límite para nominaciones de la Junta. No se permiten nominaciones después
de esta fecha.

### Semana del 1 de octubre

Se pone a disposición de los miembros una presentación de video grabada de
todos los candidatos a la junta para que la vean y conozcan a los candidatos.

### Martes 8 de octubre

*14:00 - 15:00 Hora de Ciudad de Mexico*

La reunión de miembros se lleva a cabo a través de
[mumble](https://support.mayfirst.org/wiki/mumble.Es). Hora de anunciarse.

## La elección de la Junta

Primero de Mayo siempre ha enfatizado la importancia de la democracia interna y
no creemos que una sola reunión en línea sea la forma más inclusiva de elegir a
los miembros de la Junta, ya que no todos los miembros podrán asistir.

Por lo tanto, el Comité de Dirección actual propondrá a la reunión de miembros
que realicemos las elecciones a través de una boleta en línea (exactamente de
la misma manera que hemos realizado nuestras elecciones durante los últimos 5
años) durante una semana después de la reunión: del 8 de octubre al 15 de
octubre .

En esa boleta electoral de la Junta, a los miembros también se les pedirá que
clasifiquen las prioridades que se generaron durante la reunión del 11 de
septiembre. Tradicionalmente, la Reunión de Miembros termina estableciendo una
docena de prioridades y eso hace que el trabajo del liderazgo sea mucho más
difícil porque debe priorizar entre esas prioridades. Este año, les pedimos a
los miembros que lo hagan para que la Junta pueda dirigir más fácilmente
nuestro trabajo.

## Detalles legales

Nuestros nuevos estatutos requieren una reunión anual de miembros en la que el 10 por ciento de nuestros
la membresía activa constituye un quórom. Esto significa que necesitamos asistencia por lo menos
65 miembros. Para evaluar este requisito, estaremos contando membresías, no
personas en la llamada. Invitamos a los miembros a traer tantas personas de su
organización como desee, sin embargo, solo podemos contar cada membresía una vez cuando
tomando asistencia.

Este quórum es un requisito de la ley del estado de Nueva York bajo el cual nuestra cooperativa
está incorporado.



