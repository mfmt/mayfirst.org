+++
date = 2020-10-04T14:47:48Z
description = "Prioridades"
title = "Prioridades"

+++

En la reunión de miembros de 2019 se aprobaron y clasificaron las siguientes prioridades para 2020:

 * 388 votos: Reajustar nuestra solidaridad económica para asegurar que el personal sea respetado y pagado justamente.
 * 372 votos: Completar nuestra revisión tecnológica
 * 365 Votos: Continuar el trabajo con los movimientos de justicia social y cambio
 * 343 Votos: : Facilitar la participación en el desarrollo de los proyectos de infraestructura del Primero de Mayo y ampliar los recursos y servicios de nuevas tecnologías
 * 334 Votos: : Profundizar nuestra participación y trabajo político en los movimientos de economía cooperativa y alternativa
 * 327 Votos: : Involucrar a nuestros miembros en nuestro trabajo
 * 305 Votos: : Lanzamiento de la campaña para duplicar nuestra membresía en los próximos dos años
 * 299 Votos: : Investigar y explorar las opciones para hacer las operaciones del centro de datos "más verdes"

Prioridades del año anterior

 * [2016](/priorities-2016)
 * [2017](/priorities-2017)
 * [2018](/priorities-2018)
 * [2019](/priorities-2019)
