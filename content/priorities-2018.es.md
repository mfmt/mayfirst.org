+++
date = 2015-11-24T14:47:48Z
description = "Prioridades"
title = "Prioridades"
+++

## Prioridades del Comité de Liderazgo

En enero de 2018, el Comité de Dirección aprobó el siguiente resumen de las prioridades aprobadas por los Miembros:

1. Educar
 * Plan para un programa de entrenamiento de tecnología basado en la educación popular para organizadores y activistas del movimiento
 * Aumentar la capacidad de las personas tradicionalmente excluidas en el campo de la tecnología
 * Las organizaciones deben participar directamente en el diseño de las necesidades y prioridades de capacitación en cuestiones técnicas
 * Identificar las barreras para que la gente adopte la tecnología que sugerimos
 * Específicamente la tecnología utilizada por los niños
 * Dirigido específicamente a reporteros y fabricantes de medios de comunicación para enseñar tecnología y seguridad en el contexto del periodismo
1. Aumentar la participación de los miembros
 * Crear mejores y más oportunidades para la participación de los miembros en la organización
 * Fortalecer nuestras comunicaciones a nuestros miembros y a través de medios alternativos sobre nuestras actividades usando un lenguaje simple y amigable
 * Involucrar a las organizaciones miembros en el intercambio de lecciones y ejemplos que tengan de experiencia en tecnología de liberación
1. Fortalecer la capacidad tecnológica de la organización
 * Descentralizar nuestra infraestructura tecnológica, particularmente en el Sur global, en asociación con otras organizaciones;
 * Aumentar la investigación y el desarrollo de software y hardware libres.
1. Organizar
 * Llamado a un congreso de la izquierda sobre tecnología
 * Expandir las sesiones técnicas y de revolución para incluir más educación popular, para incluir teoría y práctica explícita de la historia del movimiento.
 * Organizar a los tecnólogos en torno al tema del peer to peer con un vínculo explícito a los organizadores y organizaciones del movimiento.

## Prioridades aprobadas por los miembros

En la reunión de miembros de diciembre de 2017 se aprobaron las siguientes prioridades para 2018.

#### 1: Programa de entrenamiento: Generar una iniciativa de formación técnica / educación popular

* Aumentar la capacidad de las personas excluidas en el campo de la tecnología, POC (Personas de color), Comunidades Indígenas / Primeras Naciones / Pueblos nativos, LGBTIQ, Personas con discapacidad / habilidades diferentes, además de otros ...
* Facilitar que los movimientos sociales, organizaciones de lucha, colectivos propongan a personas de sus propias organizaciones a participar en estas capacitaciones.
* Apoyo directo a las organizaciones de base
* Que las organizaciones participen directamente en el diseño de las necesidades y prioridades de capacitación en cuestiones técnicas
* Compartir temas de acción, metodologías, dinámicas, planes y capacidades para fortalecer las organizaciones y las luchas por conocer las necesidades de colaboración a diferentes niveles.
* Colaborar con otros proveedores de infraestructura, cooperativas de tecnología, etc.
* Participar en reuniones / encuentros presenciales // organizar una reunión de trabajo de varios días para organizar y definir esta propuesta
* Dedicarnos a planificar actividades durante el año 2018 para empezar a implementarlas en 2019

#### 2: Mejor organización de la participación voluntaria en el Primero de Mayo

* Identificación de las necesidades para proyectos específicos: por ejemplo, interpretación, documentación, diseño gráfico, programación, formación
* Lista de los voluntarios, en equipos definidos
* Espacios inclusivos para el voluntariado

#### 3: Descentralización geográfica progresiva de nuestra infraestructura: Fortalecer nuestra infraestructura en otros lugares.

* Mantener y terminar la instalación de los servidores en México.
* Explorar las posibilidades de colocar servidores en Costa Rica a través de Codigo Sur.
* Infraestructura para hacer colaboración técnica en otros espacios.

#### 4: Llevar a cabo una estrategia de comunicación con un lenguaje sencillo y amigable tanto para difundir que es la organización como también a los miembros para las convocatorias, difusión de recursos y herramientas, y procesos internos del PMEP.

#### 5: Establecer una estrategia con medios de comunicación libres y alternativos, periodistas, comunicadores, con el objetivo de discutir y promover los temas de seguridad y tecnología; y la importancia de que estos temas estén presentes en los medios de comunicación (Contextualizar la situación de vulnerabilidad que viven los periodistas en México)

#### 6: Desarrollar estrategias para el uso y operación crítica de todas las tecnologías para los niños.

#### 7: Fortalecer la comunicación con los miembros.

#### 8: Reforzar la capacidad tecnológica, reocultar las necesidades de apoyo técnico.
 
* Apoyar la investigación y el desarrollo de software y hardware libre.
* Enlazar y promover centros de investigación (Redcoop)

#### 9: Crear un grupo de trabajo para identificar las barreras para que la gente adopte la tecnología que sugerimos

Liderazgo de MFPL: 

* hablando entre sí sobre las barreras obvias que se nos presentan en su trabajo con los miembros e iniciando el proceso para responder a esas conversaciones.
* Hablar con los miembros sobre sus barreras organizativas para adoptar la tecnología de código abierto.
* Hablar con los miembros que no son de la FMPL (que están en el movimiento) sobre sus barreras para la adopción.

#### 10: Llamar a un congreso de la izquierda sobre tecnología

#### 11: Expandir las sesiones de tecnología y revolución para incluir más educación popular, para incluir teoría y práctica explícita de la historia del movimiento.

#### 12: Organizar a los tecnólogos en torno al tema de peer to peer con un vínculo explícito a los organizadores y organizaciones del movimiento.

#### 13: Involucrar a las organizaciones miembros en el intercambio de lecciones y ejemplos que tienen de la experiencia de la tecnología de liberación - ver la construcción del movimiento a través de la tecnología

#### Propuestas individuales que no fueron consensuadas por pequeños grupos

## No consentido

Estas propuestas se hicieron pero no fueron consensuadas durante la reunión de los miembros. Se presentan al Comité de Dirección para su posterior discusión y posible aplicación sin el pleno acuerdo de los miembros.

#### 1: Difundir el conocimiento acerca de cómo la búsqueda en Internet es controlada por las corporaciones e investigar todas las alternativas que los activistas de Internet trabajan y ayudar a la búsqueda en Internet no controlada por las corporaciones.

#### 2: Formalizar en la estructura binacional. El nombramiento de los administradores de los dos centros por el comité de liderazgo y el seguimiento de las finanzas y el control de los miembros

Véase también nuestras [prioridades 2016](/en/prioridades-2016) y [prioridades 2017](/en/prioridades-2017).

