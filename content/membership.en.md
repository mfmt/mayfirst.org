+++
date = 2015-09-21T14:47:48Z
description = "May First Movement Technology members pay dues to support our work and benefit from collective hosting."
featured_image = "membership.jpg"
tags = [ "featured" ]
tags_weight = 3
title = "Membership"
+++

Over 800 May First Movement Technology members pay dues to support our work. One important benefit of membership is access to our first class web, email and other hosting services, based entirely on free and open source software, hardened to defend against denial of service attacks, and with a track record of fighting corporate and government requests for data. 


