+++
date = 2015-10-01T13:52:02Z
title = "La lucha contra el racismo y el sexismo"
slug = 'intencionalidad'
[menu]
  [menu.more]
    parent = "Acerca de Nosotros"
    weight = 60
+++

Primero de Mayo/Enlace Popular es una organización de activistas y organizadores que hacen uso de Internet para conectar a las comunidades en el suelo con los miembros predominantemente en los EE.UU. y México.

Como organización del movimiento en que respondemos a las actividades y desarrollos dentro de nuestros movimientos para el cambio y tratamos de responder a esas actividades mediante el desarrollo y que se refleja en nuestra membresía y liderazgo esos movimientos demográficos y la cultura.

 * En este momento en los Estados Unidos, los principales movimientos para el cambio social son conducidos principalmente por la gente de color - la Mayoría Global. Para tener éxito, nuestra organización debe reflejar esta realidad.
 * El feroz ataque contra los derechos de las mujeres, junto con el sexismo persistente y profundamente arraigada en la cultura de la tecnología y la importancia fundamental de los movimientos de feminismo y de mujeres a todos los movimientos de cambio social hace urgente el mismo reflejo de las mujeres y los movimientos de mujeres en nuestra membresía, liderazgo y actividades.
 * El descuido y demasiado frecuente de la importancia política e histórica y el éxito de los movimientos en las regiones agrícolas mexicanos hace urgente que las conexiones se realicen con esos movimientos y que se reflejan en nuestra membresía y liderazgo.

El papel de la tecnología en todos estos movimientos es particularmente importante. Debido a que la tecnología de las comunicaciones representa la expresión más elocuente del espíritu democrático y la colaboración humana, es vital para el éxito de nuestros movimientos. Al mismo tiempo, debemos ser conscientes de que se ha hecho un esfuerzo especial para atraer a la gente y los movimientos de color en el uso de la propiedad de las empresas de medios sociales y otras herramientas propietarias y que impidieron que la Internet ha hecho poco para evitar eso y que, muchos de estos movimientos, el activismo en Internet no es una prioridad.

Estos son los desafíos que enfrentamos y no podemos enfrentarlos a menos que estemos compuesta predominantemente por y conducidos por personas de la Mayoría Global.

Por último, creemos que es esencial para reunir a los técnicos políticamente progresistas de todos los orígenes, en particular las personas de color, para comenzar a coordinar una estrategia de respuesta a estas medidas represivas y para avanzar en la protección de la neutralidad y el software libre desarrollo de Internet.

Por estas razones, vamos a realizar nuestro trabajo y orientar el trabajo de nuestra organización con esta intencionalidad. Específicamente:

 * Vamos a hacer circular enérgicamente esta declaración a todos aquellos con quienes tenemos una relación y rigurosamente aplicarlo a todo nuestro trabajo.
 * Todo gran representación de Primero de Mayo/Enlace Popular a los EE.UU. y los movimientos mundiales debe incluir al menos una persona de color y una mujer. Las excepciones a esta regla deben ser aprobados por la propia CD.
 * Los coordinadores de cada grupo de trabajo deben incluir al menos una persona de color o mujer.
 * Nos concentraremos nuestros esfuerzos de alcance y reclutamiento de las organizaciones de la Mayoría Global.
 * Llevaremos a cabo nuestras reuniones con métodos de educación popular como una base para reflejar la diversidad de nuestros miembros.

Pedimos a todos los que trabajan con nosotros para apoyarnos, desafiando nuestras actividades cuando no reflejan estos principios y darse cuenta de ellos cuando lo hacen.

*Aprobado por la membresía en noviembre 2015, como parte de los [documentos oficiales](/es/documentos-oficiales).*
