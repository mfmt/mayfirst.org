+++
date = 2020-10-04T14:47:48Z
description = "Prioridades"
title = "Prioridades"
slug = 'prioridades'

+++

En la reunión de miembros de 2021 se aprobaron y clasificaron las siguientes prioridades para 2022:

 * 390 Votas: Colaborar con los movimientos para proporcionarles recursos técnicos útiles y, al mismo tiempo, desarrollar su capacidad de acción en el uso de la tecnología.
 * 366 Votas: Reorganizar y mejorar la documentación para cubrir los vacíos, garantizar un lenguaje accesible y guiar la membresía al uso de las herramientas y servicios de May First.
 * 346 Votas: Fomentar la participación de la membresía en las programas de May First creando un entorno inclusivo que permite superar las exclusiones originadas por cualquier tipo de opresión.
 * 334 Votas: Promover múltiples métodos de orientación para la membresía recién inscrita para facilitar la participación en los procesos organizativos y el uso de los servicios.
 * 333 Votas: Profundizar en nuestra comprensión y conciencia interna de las metodologías globales de movimientos desde abajo y comprometernos con ellos a través de la construcción de relaciones, la confianza y la participación.
 * 332 Votas: Elevar nuestra comprensión colectiva del momento político y de cómo nos organizamos juntos para actuar hacia una transformación social a largo plazo.
 * 329 Votas: Cuestionar el vínculo entre las herramientas tecnológicas corporativas y la profesionalización de la organización de los movimientos, señalando la supremacía blanca, el patriarcado y otras formas de opresión inherentes a las herramientas corporativas.
 * 325 Votas: Aumentar la conciencia dentro de los movimientos globales sobre lo que está haciendo May First e internamente dentro de May First sobre lo que están haciendo los movimientos globales y locales.
 * 324 Votas: Encontrar la unidad y la causa común con otras organizaciones y movimientos a través de valores y principios compartidos.
 * 313 Votas: Centrar la participación de la membresía en los procesos de evaluación, renovación e introducción de los nuevos servicios y herramientas tecnológicas para asegurar que sean relevantes para el trabajo con movimientos así como sostenibles por nuestra cooper
 * 271 Votas: Hacer más transparente para la membresía los detalles y el uso de la infraestructura física de la cooperativa así como su consumo de energía.

Prioridades del año anterior

 * [2016](/priorities-2016)
 * [2017](/priorities-2017)
 * [2018](/priorities-2018)
 * [2019](/priorities-2019)
 * [2020](/priorities-2020)
 * [2021](/priorities-2021)
