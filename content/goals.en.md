+++
date = 2015-10-01T13:52:02Z
title = "Goals"
+++

## Building Technology Capacity

 * Build movement-led communications and technology infrastructure
 * Make access to technology affordable, especially for Global Majority and women
 * Build capacity of movement organizations and activists in the use of technology
 * Support members’ communications and technology needs
 * Enhance communication across international borders
 * Advance the use of and develop Free/Libre and Open Source Software
 * Contribute to the development of technologies to advance solidarity economies and other alternatives to capitalism and to build just relationships in places where people work.

## Movement Building

 * Enhance awareness, particularly among members, of political issues surrounding technology
 * Engage members in movement work
 * Promote and defend internet rights
 * Defend privacy, data sovereignty, and security
 * Promote respect for cultural rights and linguistic diversity

*Approved by the membership in November 2015 as part of our [official documents](/en/official-documents).*
 


