+++
date = 2018-12-24T14:47:48Z
description = "Priorities"
title = "Priorities"
+++

Unlike in previous years, we had a special membership referendum on just a few
items which were passed with the following votes:

1. Form a committee to explore converting our organization to a multi-stake
   holder Cooperative:

yes: 162
no: 9

2. Form a committee to explore ways to significantly grow the organization to a
   more sustainable size

yes: 160
no: 11

3. Postpone leadership committee elections until we vote on coop

yes: 136
no: 35


