+++
date = 2015-10-01T13:52:02Z
title = "Political Environment"
+++

## The Human Crisis and May First's Future

The human race is facing imminent extinction and, simultaneously, the most attainable opportunity to build a new, functional and sustainable world society. People, led by organizations and movements of the Global Majority, are searching for and starting to develop real answers to the threat.

Both the crisis and its solution are driven by technology and, without technology, this situation would never be possible.

With an economy that is no longer capable of sustaining human life, an environment that now clearly threatens human existence and a political culture that has reduced government to a cartoonish caricature of its stated purpose, we can see a crisis whose destructive impact is not only unprecedented but beyond repair. There is no reform that will ease this crisis. There is no manipulation that can hide its impact. There is no option for the human race but to replace the system that controls us and the society in which we live.

On the other hand, in the Americas, there are more left-wing governments in power today than ever before, mass movements struggle against oppression while examining and testing new forms of organization and production and there is a growing push toward unity across imposed national borders. There is a very high consciousness about climate change and a rejection of war as a human activity.

As part of that progressive development, the Internet has become the most popular communications technology in history - bringing together over two billion people.

Technology is the driving force in both this crisis and these progressive developments.

It has been used by ruling classes in destructive and repressive ways that offend its original, progressive purpose. It has replaced and reduced human work, accumulated and arranged data and smoothed the ability of governments to control individuals and societies. Technology has altered the role of human beings in this society from the forces whose labor drives society to the victims of its increasingly labor-less production. It has facilitated the theft of resources, including intellectual, from Mexico (and many other countries world-wide) to the United States and has been, itself, a resource stolen from the collaboration and thinking of people world-wide (particularly Mexico) to benefit a few corporations in the United States.

It has ushered in a non-sustainable society that will self-destruct and take the human race with it.

Internet and communications technology was created, however, to allow for communications among all humans = communications that are faster, more efficient, more comprehensive, more proactive and more fully expressive of each of our realities than any other communications technology in history. It was born of a collaboration by technologists world-wide and sustained by billions of people with the world's progressive movement playing a leading role. It has changed the concept of "news reporting" and "information" to a democratic, non-exclusive exercise by masses of people. It has allowed lightning organizing among people who, up to now, have had almost no access to media as anything but consumers. It has empowered people who have, up to now, considered themselves powerless.

It has changed the human race forever.

To save the human race, the world must create a new society characterized by cooperation, collaboration and human sharing. It can create that society only if we have a free, functional, fully distributed and robust communications technology - particularly the Internet.

At the same time, the world can benefit from the lessons in the development of the Internet, lessons that demonstrate how collaboration without profit works to produce superb technology; how systems of world-wide democratic collaboration can not only sustain this complex technology system but enhance, improve and popularize it; and how this type of collaboration among individuals not only improves work but enhances creative thinking.

In short, the Internet proves that the world we envision is not only possible but necessary and highly beneficial.

For these reasons, May First Movement Technology understands the importance of preserving and expanding technology for movement use and we believe our role in that is critical. To do that we must:

 1. Ensure that the Internet is owned and controlled by people
 1. Organize and/or participate in all campaigns and efforts designed to preserve and deepen that ownership and preserve the Internet's freedom
 1. Broaden and strengthen the collaboration of campaigns working for similar goals in countries throughout America and beyond
 1. Mobilize the only forces capable of realizing these goals - the left-wing movements in the United States and Mexico - by being part of those movements, working within them as a participant organization and recruiting the leadership organizations in those movements to May First Movement Technology or a unitary front it is part of.

These are the challenges we face in the upcoming period and we are prepared to confront them.

*Approved by the membership in November 2015 as part of our [official documents](/en/official-documents).*

