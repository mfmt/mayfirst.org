+++
date = 2015-10-01T13:52:02Z
title = "Misión"
slug = 'mision'
[menu]
  [menu.more]
    parent = "Acerca de Nosotros"
    weight = 20
+++

May First se compromete a la construcción de movimientos sociales mediante el impulso del uso estratégico y el control colectivo de la tecnología para las luchas locales, la transformación global, y la emancipación sin fronteras. 

*Aprobado por la membresía en noviembre 2015, como parte de los [documentos oficiales](/es/documentos-oficiales).*
