\+++ date = 2015-10-01T13:52:02Z title = "Values Statement" +++

May First Movement Technology's core values guide us in implementing our mission and ensure its integrity. We have identified the following values as critical to our work:

* Cooperation, collaboration, and sharing help us contribute to a movement broader than ourselves
* Transparency, openness, and honesty ensure that we are building a democratic organization
* Equality and respect guide us in building with the diversity of people we need to make a difference
* Conviction and discipline provide the strength to continue against overwhelming odds
* Solidarity, mutual aid, and sustainability to ensure we are building new ways to work together
* Humility, openness to criticism and commitment to conflict resolution create a culture of resilience for our organization and movements

*Approved by the membership in November 2015 as part of our [official documents](/en/official-documents).*