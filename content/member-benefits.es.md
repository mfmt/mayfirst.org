+++
date = 2015-10-01T13:52:02Z
aliases = "beneficio-nuevo"
title = "Beneficios de Membresía"
slug = 'beneficios'
[menu]
  [menu.more]
    parent = "Membresía"
    weight = 20
+++

<a id="servicios-de-hospedaje"></a>

### Acceso a Nuestra Infraestructura y Servicios Tecnológicos

Nuestra infraestructura tecnológica autónoma es un recurso comunitario
compartido para el uso de la membresía y se financia enteramente con las cuotas
de afiliación de nuestra membresía.

Haga clic en cualquiera de las siguientes secciones para ver más detalles:

<details>
  <summary><h4>Lista de Servicios Disponibles</h4></summary>
  <p>Las membresías de May First que se aprovechan de nuestros <strong>planes de
  alojamiento</strong> reciben acceso y soporte para la siguiente lista de
  servicios de alojamiento en línea mantenidos por nuestro personal técnico.</p>
  <ul class="accordion">
  <li>
    <details>
      <summary>Alojamiento de Sitios Web:</summary>
      <p>Proporcionamos instalaciones automatizadas de sistemas de gestión de contenidos
de sitios web como Wordpress, Drupal y Backdrop. Otro software popular para
sitios web puede ser instalado manualmente o migrado a nuestros servidores
compartidos.</p>
    </details>
  <li>
    <details>
      <summary>Bases de Datos:</summary>
      <p>Acceso a bases de datos MySQL y Postgres.</p>
    </details>
  <li>
    <details>
      <summary>Correo Electrónico:</summary>
      <p>Cuentas de correo electrónico para su propio dominio accesible a través de
nuestra interfaz de correo web o por la mayoría de aplicaciones de software de
correo electrónico y dispositivos que utilizan protocolos estándar (pop, imap,
smtp).</p>
    </details>
  <li>
    <details>
      <summary>Compartición de Archivos y Colaboración:</summary>
      <p>Nuestra instancia de Nextcloud facilita el uso compartido de archivos, la
edición colaborativa de documentos, calendarios compartidos, contactos y tareas
entre grupos, así como la sincronización de datos entre tus propios
dispositivos.</p>
    </details>
  <li>
    <details>
      <summary>Eventos y Reuniones Virtuales:</summary>
      <p>Nuestros servidores Jitsi Meet, Mumble e Icecast proporcionan soluciones para
reuniones web virtuales y transmisión de eventos en directo. Todas nuestras
plataformas de reuniones en directo incluyen herramientas para la
interpretación simultánea de idiomas.</p>
    </details>
  <li>
    <details>
      <summary>Listas de Correo:</summary>
      <p> Proporcionamos la configuración automática de listas de correo de estilo
 difusión o de discusión a través del listserv Mailman altamente configurable.
</p>
    </details>
  <li>
    <details>
      <summary>Chat Seguro:</summary>
      <p>Nuestro servidor XMPP/Jabber puede utilizarse para proporcionar chat cifrado en
 tiempo real con cuentas en cualquier servidor XMPP compatible y es compatible
 con múltiples programas de chat FOSS disponibles para su computadora y
 smartphone.
</p>
    </details>
  </ul>

  Para una descripción más completa de estos y otros servicios, consulte nuestra
  [servicios](/es/services).
</details>

<details>
  <summary><h4>Seguridad y privacidad</h4></summary>
<p>Construidos completamente con software libre y de código abierto y mantenidos
 con las necesidades y funciones de seguridad que necesitan los grupos de
 activistas y organizadores, nuestros servicios están diseñados para
 garantizar que el trabajo y las comunicaciones esenciales del movimiento
 permanezcan libres de vigilancia y censura.</p>
</details>

<details>
  <summary><h4>Límites de uso</h4></summary>
  <p>Los recursos tecnológicos derivados de la materia física de la tierra nunca
 deben tratarse como ilimitados y preferimos no utilizar esa palabra para
 describir nuestros servicios. Limitamos el uso total de espacio en disco, pero
 por lo demás nos inclinamos por la abundancia y creemos que tenemos recursos
 de sobra para toda la membresía. Pueden crear tantas cuentas, sitios web,
 bases de datos y listas como necesiten para sus propios proyectos. La reventa
 de la infraestructura y servicios de May First a terceros está prohibida.</p>
</details>

<details>
  <summary><h4>Detalles técnicos</h4></summary>
  <p>Nuestra infraestructura da soporte a todos los lenguajes de programación
 comunes (PHP, perl, python, ruby, etc), la mayoría de bases de datos comunes
 (MySQL, Postgres, SQLite), ssh y SFTP acceso completo, webmail via Roundcube y
 acceso seguro por IMAP o POP, el software de lista de email de Mailman, acceso
 a nuestros servidores de transmisión de correo masivo (cuál envía centenares
 de miles de mensajes de email diariamente), logins para nuestro servidor
 NextCloud compartido, cuentas en nuestro servicio de XMPP (Jabber) compartido y
 muchos más servicios. Nuestra membresía utiliza Drupal, Wordpress, Joomla,
 Wikimedia, CiviCRM y muchos otros sistemas de administración del contenido y
 bases de datos. Todos los servidores corren el sistema operative de Debian
 estable.</p>
</details>

<hr>

<a id="new-dues-structure"></a>
<a id="nueva-estructura-cuotas"></a>

### Planes de Hospedaje y Quotas de Membresía

 Nuestra cooperativa ha elegido un método distinto para calcular los "costes"
 de alojamiento en comparación con los proveedores de alojamiento
 tradicionales. Utilizamos un sistema de escala móvil basado en el rango de
 ingresos y el uso total de recursos de cada membresía para determinar el total
 de las cuotas de afiliación. Pruebe nuestra [Calculadora de cuotas de
 membresía](https://outreach.mayfirst.org/es/civicrm/public/calculate#/mayfirst/calculate)
 en línea para explorar de forma interactiva cuánto tendría que pagar
 utilizando diferentes planes de alojamiento según sus niveles de ingresos.

Haga clic en cualquiera de las siguientes secciones para ver más detalles:

<details>
<summary><h4>Membresía Básica</h4></summary>

 Una cuota anual de **afiliación básica** de 50 dólares estadounidenses es la
 contribución mínima necesaria para unirse a nuestra cooperativa con pleno
 derecho a voto. La membresía básica incluye los beneficios de una dirección de
 correo electrónico mail.mayfirst.org, una cuenta Nextcloud y una cuenta de
 chat XMPP/Jabber con 10 GB de almacenamiento entre todas. Pueden pasar de la
 membresía básica a uno de nuestros **planes de alojamiento** completo en
 cualquier momento.

</details>

<details>
<summary><h4>Planes de Hospedaje y Costos</h4></summary>

 Los miembros que se benefician de nuestros planes de alojamiento completo
 reciben acceso a todos nuestros servicios, múltiples cuentas y mayores
 cantidades de espacio de almacenamiento.

<table class="table">
  <tr>
    <th>Plan</th><th>Cuotas anuales totales (dólares EEUU)</th><th>Límite de almacenamiento</th>
  </tr>
  <tr>
    <td>Básico</td><td>$50.00</td><td>10G</td>
  </tr>
  <tr>
    <td>Plan 0</td><td>$62.00</td><td>40G</td>
  </tr>
  <tr>
    <td>Plan 1</td><td>$125.00</td><td>100G</td>
  </tr>
  <tr>
    <td>Plan 2</td><td>$250.00</td><td>232G</td>
  </tr>
  <tr>
    <td>Plan 3</td><td>$500.00</td><td>500G</td>
  </tr>
</table>

 El acceso a los planes anteriores está condicionado por un criterio de escala
 móvil basado en su nivel de ingresos.

 Haga clic en cualquiera de las siguientes preguntas para ver más detalles:

  <details>
    <summary>
    <strong>¿Cómo funciona la escala móvil basada en los ingresos?</strong>
    </summary>

 En nuestro sistema, el acceso a los niveles del plan de alojamiento depende del
 nivel de ingresos declarado por cada membresía. Las personas com membresía
 individual pueden utilizar sus ingresos anuales personales y las organizaciones
 pueden utilizar su presupuesto operativo anual para determinar su nivel de
 ingresos. Los planes de alojamiento más pequeños están disponibles para las
 membreśia con ingresos más bajos, pero las membresías con ingresos más
 altos solo pueden elegir un plan de alojamiento más grande.

 En el siguiente gráfico, los rangos de ingresos están representados por
 líneas horizontales de distintos colores. Los "puntos de entrada" indican el
 primer plan de alojamiento disponible para ese rango de ingresos.

 <a href="/img/dues-subway-map-es.png"><img class="img-responsive" alt="Mapa de
 quotas de membreśia" src="/img/dues-subway-map-thumb-es.png"></a>

  </details>

  <details>
    <summary>
    <strong>¿Cómo se calcula el rango de ingresos?</strong>
    </summary>

 En la tabla siguiente se detallan los rangos de ingresos de membresía en
 función de sus ingresos anuales o del presupuesto de su organización,
 expresados en dólares estadounidenses. Las personas deben utilizar sus
 ingresos anuales personales y las organizaciones utilizarán su presupuesto
 operativo anual para determinar su **rango de ingresos**.

  <table class="table">
    <tr>
      <th>Nivel de Rango</th><th>Ingresos Anuales (dólares estadounidenses)</th>
    </tr>
    <tr>
      <td>Nivel de Rango de Ingresos 1</td><td>menos de 25 mil dólares</td>
    </tr>
    <tr>
      <td>Nivel de Rango de Ingresos 2</td><td>entre 25 mil a 150 mil dólares</td>
    </tr>
    <tr>
      <td>Nivel de Rango de Ingresos 3</td><td>entre 150 mil a 500 mil dólares</td>
    </tr>
    <tr>
      <td>Nivel de Rango de Ingresos 4</td><td>más de 500 mil dólares</td>
    </tr>
  </table>
  </details>

  <details>
    <summary>
    <strong>¿Y si necesito más espacio de almacenamiento?</strong>
    </summary>

 El nivel de su **plan de alojamiento** comienza en su **nivel de ingresos**
 equivalente, pero no se limita a él. Siempre pueden añadir espacio de
 almacenamiento adicional o seleccionar un nivel de plan de alojamiento superior
 y contribuir más cuotas proporcionalmente, pero no pueden descender a un nivel
 de plan de alojamiento inferior a su nivel de ingresos.

  </details>

</details>

<details>
<summary><h4>Servidores Virtuales Dedicados</h4></summary>

Si desean administrar sus propios servidores privados virtuales podemos ofrecer dos opciones:

* VPS semigestionados - brindamos monitoreo básico y ayuda con las actualizaciones del sistema
* VPS no administrados - completamente privados

Un VPS sólo puede añadirse en combinación con un plan de hospedaje.

Cada servidor virtual privado incluye:

* 25 GB de disco de sistema
* 1 núcleo de CPU
* 1 GB de RAM

 El precio mensual base de cada servidor privado virtual también depende de su
 nivel de ingresos.

<table class="table">
  <tr>
    <th>Rango de Ingresos</th><th>Cuota mensual básica para VPS (dólares estadounidenses)</th>
  </tr>
  <tr>
    <td>Rango de Ingresos 1</td><td>$25.00</td>
  </tr>
  <tr>
    <td>Rango de Ingresos 2</td><td>$50.00</td>
  </tr>
  <tr>
    <td>Rango de Ingresos 3</td><td>$100.00</td>
  </tr>
  <tr>
    <td>Rango de Ingresos 4</td><td>$200.00</td>
  </tr>
</table>

 Puedan agregar núcleos de CPU, RAM y unidades de disco de estado sólido con
 los siguientes costos adicionales.

* **CPUs**: $10/por mes/por CPU (Máximo por VPS: 4)
* **RAM**: $10/por mes/por 1GB (Máximo por VPS: 8)
* **Unidades de estado sólido**: 20$/por mes/por 10GB (Máximo por VPS: 20GB)

 El espacio de almacenamiento de datos adicional para su VPS puede
 aprovisionarse desde el espacio de disco disponible en su plan de hospedaje
 seleccionado **sin coste adicional**. Por favor, indique la cantidad de
 almacenamiento que desea dedicar a su VPS en sus instrucciones.

 Si tienes más de un VPS, puedes mezclar y combinar tu asignación de todos los
 recursos anteriores como desees.

</details>


<details>
  <summary><h4>Formas de pago</h4></summary>

 Todas las cuotas de membresía se calculan en dólares estadounidenses y pueden
 pagarse a través de PayPal o tarjeta de crédito. **Las membresías establecidas
 en México** que requieran una **factura fiscal mexicana** pueden optar por pagar
 en pesos mexicanos a través de depósito bancario. Se calculará una tasa de
 cambio de pesos mexicanos por dólar basada en las cifras actuales del mercado.

</details>

<hr>

<a id="trabajo-del-movimiento"></a>

### Beneficios de nuestro trabajo de participación con los movimientos

 Afirmamos que nuestro trabajo político, que consiste en involucrar a los
 movimientos globales en conversaciones sobre el papel de la tecnología en
 nuestra lucha por un mundo más justo, representa un beneficio importante
 para nuestra membresía y para el movimiento de izquierda en general.

<details>
<summary><h4>La base de nuestro trabajo con los movimientos</h4></summary>

 Creemos que los movimientos por el cambio fundamental deben priorizar el uso,
 la protección y la democratización de la tecnología. Queremos que los
 líderes de nuestros movimientos reflexionen sobre esos objetivos y desarrollen
 un plan unificado para hacerlos realidad. Ésa es la base de nuestro trabajo y
 hace de May First una de las organizaciones tecnológicas más activas y
 destacadas de la izquierda de este país.

</details>

<details>
<summary><h4>Trabajo de coalición y participacion en encuentros</h4></summary>

 Como partidarios activos del foro social y de las coaliciones de medios y
 tecnología libres y abiertos, May First Movement Technology contribuye con
 nuestro pensamiento político y nuestras habilidades tecnológicas a estas
 importantes áreas.

 Somos una organización líder de <a href =
 "https://mediajustice.org/network/"> Media Justice Network </a>; uno de los
 fundadores de las <a href="https://radicalconnections.net"> Conexiones
 Radicales Red </a>; miembro de la <a href="https://apc.org/"> Association desde
 hace mucho tiempo para comunicaciones progresivas </a>; miembro de varias
 coaliciones de nuevas economías y redes y hemos estado involucrados en muchas
 coaliciones y redes establecidas para lidiar con problemas y luchas
 específicas. Participamos en muchas conferencias y grandes convergencias en el
 transcurso del año en los Estados Unidos, en México y internacionalmente.
 Nuestros representantes participarán en más de 20 eventos de este tipo.
 durante cualquier año.

 Nuestra tecnología <a href="https://techandrev.org"> única y sin precedentes
 y La serie Revolution </a> ha reunido a más de 1500 líderes de movimiento y
 activistas en los Estados Unidos y México en 24 convergencias desde 2017 para
 pensar y hablar sobre la intersección de la tecnología y la revolución y
 forjar un programa de trabajo que podemos hacer en torno a la tecnología.

</details>

<details>
<summary><h4>La defensa de nuestra membresía y sus datos</h4></summary>

 Ese compromiso con el trabajo del movimiento también se ve en nuestro
 compromiso con el protección de nuestros datos. Nuestros miembros son <a href
 = "https://support.mayfirst.org/wiki/legal"> golpea con frecuencia </a> con
 demandas para datos de gobiernos, avisos de eliminación de corporaciones o
 denegación de Ataques de servicio de enemigos políticos. Nos resistimos a
 todos. Tenemos resistió con éxito los ataques de denegación de servicio a
 los miembros que trabajan en justicia reproductiva, derechos palestinos y el
 movimiento Black Lives Matter. Nos negamos a dar detalles al FBI cuando exigió
 información de pago que habría identificado a los miembros de un sitio de
 Independent Media Center en Grecia. Nos hemos negado sistemáticamente a
 eliminar sitios web de sátiras por miembros como el Yes Men, que las
 corporaciones consideran violaciones de marcas registradas.

 Pero esos son solo los casos más dramáticos porque, día a día, ferozmente
 proteja los datos mediante encriptación, respaldo y monitoreo. Los datos de
 nuestros miembros son nuestra herramienta de movimientos y nuestros movimientos
 son nuestra prioridad.

</details>

<details>
<summary><h4>Contribuyendo al movimiento de tecnología libre</h4></summary>

 También contribuimos al desarrollo de nuevas tecnologías que son coherentes
 con nuestra visión política de una Internet libre y abierta, incluyendo apoyo
 al software libre y de código abierto y a los estándares abiertos de redes y
 datos.

</details>

<details>
<summary><h4>Nuestro propósito</h4></summary>

 Todo esto, nuestro trabajo de movimiento y nuestra protección de datos, tiene
 un propósito: Ayudar a nuestros movimientos a construir una nueva relación
 con la tecnología. Queremos nuestro movimiento para pensar en tecnología,
 desarrollar un programa para protegerla y expandirla y de hecho trabajar en ese
 programa. Queremos ser una plataforma para ese tipo de colaboración: el
 control democrático y la expansión de la tecnología como herramienta para
 supervivencia humana y la lucha para que eso suceda. Queremos construir eso y
 queremos modelarlo.

 </details>
