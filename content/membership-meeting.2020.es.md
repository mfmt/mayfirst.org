+++
date = 2020-09-21T14:47:48Z
description = "Bienvenido a la reunión de miembros de la tecnología del primer movimiento de mayo"
title = "Reunión de miembros 2020"
tags = [ "callout" ]
+++

## Calendario de fechas y reuniones

### Summary

**Bienvenidos a la página oficial de la reunión de miembros del Primero de Mayo de 2020!**
  
Mientras presenciamos la implosión de la presidencia, el peligroso cambio climático y el
con el surgimiento del fascismo, el papel de los titanes de la tecnología corporativa se hace más claro
y más cómplices cada día. 

Entonces, ¿qué es lo que hace diferente al Primero de Mayo?

No es sólo la política que defendemos, o el movimiento que estamos construyendo
comprometidos, o incluso el software libre y de código abierto que mantenemos
incansablemente: El Primero de Mayo es único en nuestro compromiso de
democratizar la propiedad de nuestros la tecnología. **Estamos aquí para
cambiar fundamentalmente la relación del movimiento a la tecnología.**

¡Y ahí es donde tú, un miembro del Primero de Mayo, tienes un papel importante que jugar!

Necesitamos que cada miembro del Primero de Mayo participe en nuestra membresía
anual reuniones, aunque sea lo único que hagas para el Primero de Mayo todo el
año. Necesitamos para escuchar nuestros informes y evaluaciones sobre lo bien
que cumplimos nuestros objetivos esta                    y para ayudar a dar
forma a nuestros planes para el próximo año basado en el constante cambio
ambiente político. Estos son tiempos sin precedentes, así que necesitamos toda
la información podemos reunirnos para tomar las decisiones correctas en el
futuro.

Nuestras reuniones de miembros tendrán lugar desde mediados de octubre hasta
mediados de diciembre e incluirá reuniones en línea, discusiones en el foro y
una semana de votación y punto.

Puede registrarse para todos los eventos en nuestro [registro de eventos
página](https://outreach.mayfirst.org/civicrm/event/register?id=67&reset=1).  

Por favor, eche un vistazo a nuestro horario y *comprométase a asistir al menos a una
y la reunión principal de los miembros* en diciembre.

Vea nuestra [infografía](/img/2020-membership-meeting-es.jpg) para los detalles
completos o lea más abajo.

[![Info graphic](/img/2020-membership-meeting-es-thumb.jpg)](/img/2020-membership-meeting-es.jpg)

### Reuniones de discusión

*Cada reunion durará una hora.*

Celebraremos las siguientes seis reuniones de debate. Los miembros son
bienvenidos a asistir a todas las reuniones, o puede asistir sólo a las
reuniones más relevantes para su organizando el trabajo. 

Cada reunión tendrá lugar en línea a través de la aplicación de audio
(https://support.mayfirst.org/wiki/mumble) y también tienen un
correspondiente debate en el foro a través de [discurso](https://comment.mayfirst.org/)
para las personas que no puedan asistir a la reunión o que deseen continuar el debate.

Por favor [revise todos nuestros informes](https://share.mayfirst.org/s/ZrsjayExTpAMpdo).                    
NOTA: Todos los informes con "en" están en inglés y los que tienen "es" son los
versiones en español.

Por favor [háganos saber a qué reuniones planea asistir](https://outreach.mayfirst.org/civicrm/event/register?id=67&reset=1).

#### Programa de reuniones

*Cada reunion durará una hora.*

 * *Viernes 23 de octubre, a la 1:00 pm América/Nueva York:* **Evaluando la corriente
   entorno político** El resultado previsto será de 5 a 7 puntos.
   que describen los aspectos más importantes de la situación política actual
   para que lo consideremos mientras planeamos nuestro trabajo futuro. [Foro
   Discusión](https://comment.mayfirst.org/t/political-environment/1848).

 * *Martes, 27 de octubre, a las 3:00 pm América/Nueva_York:* **Tecnología
   Infraestructura y servicios** El resultado previsto será de 5 a 7 puntos
   puntos que describen las prioridades de nuestra organización en la construcción de nuestra
   infraestructura. [Foro Discusión](https://comment.mayfirst.org/t/technology-infrastructure-and-services/1849).

 * *Viernes, 6 de noviembre, a las 4:00 pm hora de América/Nueva York* **Movimiento
   prioridades** El resultado previsto será de 5 a 7 puntos que describan nuestra
   las prioridades de la organización para 2021. [Foro
   Discusión](https://comment.mayfirst.org/t/movement-priorities/1850). 

 * *Martes, 10 de noviembre, a la 1:00 pm América/Nueva_York* **Propuesta sobre nuevas cuotas
   estructura** El resultado previsto será un debate para asegurar que todos
   entiende la propuesta y considerar los cambios propuestos antes de una
   la propuesta se somete a votación por nuestros miembros. [Foro
   Discusión](https://comment.mayfirst.org/t/new-dues-structure-proposal/1851).

 * *Viernes 13 de noviembre, a las 2:00 pm América/Nueva York* **Nuevo miembro de la Junta Directiva
   Orientación para cualquiera que quiera presentarse a la Junta Directiva** El
   El resultado previsto será compartir la información para todos los posibles miembros de la junta
   miembros sobre cómo funciona el liderazgo del Primero de Mayo, las expectativas de la junta
   miembros, la historia de la organización y el compromiso de la organización
   para combatir el racismo y el sexismo. [Foro
   Discusión](https://comment.mayfirst.org/t/new-board-member-orientation/1852).

 * *Miércoles, 18 de noviembre, a la 1:00 pm América/Nueva York.* **La estructura de la cooperativa
   y la participación de los miembros** El resultado previsto será de 5 a 7 balas
   puntos que describen las prioridades de nuestra organización en relación con los miembros
   participación. [Foro
   Discusión](https://comment.mayfirst.org/t/coop-and-membership-engagement/1853).


Por favor, [regístrese](https://outreach.mayfirst.org/civicrm/event/register?id=67&reset=1)!

### Elecciones de la Junta

De acuerdo con nuestros estatutos, 5 puestos de la junta directiva son elegidos por los trabajadores y 20 son
elegidos por los miembros. De los 20, elegimos aproximadamente 1/3 cada año para
3 años de mandato. 

Además, reservamos un número proporcional de asientos para los miembros que pagan
en México (alrededor del 20 por ciento).

Este año, dada la proporción actual de miembros mexicanos, un asiento en la junta directiva para un
miembro que paga cuotas en México y seis puestos en la junta directiva para todos los demás miembros están arriba
para la elección.

El período de nominación de la junta directiva se extenderá desde el 1 de
noviembre hasta el 30 de noviembre. Y la elección se llevará a cabo del 1 al 8
de diciembre.

Si desea presentarse a la junta, por favor regístrese en la junta orientación
(ver arriba). También puede estar interesado en revisar nuestra [orientación de
la junta materiales](/es/orientación).

Cuando estés listo, puedes [nombrarte a ti mismo o a otro miembro del Primero de Mayo que
ha accedido a correr a través de este formulario](https://vote.mayfirst.org/es).

## La Reunión Principal

Por ley, tenemos que celebrar una reunión "oficial" de los miembros con la asistencia de
al menos el 10% de nuestros miembros (unos 65 miembros).

**La reunión oficial de miembros tendrá lugar el martes 1 de diciembre en
3:00 pm América/Nueva York**

Una vez que se tome la asistencia y hayamos determinado que tenemos quórum, estaremos
capaz de hacer propuestas para la aprobación de los miembros. Al votar, los miembros individuales obtienen
un voto y los miembros de la organización tienen dos votos. Incluso si su organización
envía a 10 personas a la reunión, y aún así sólo consigues dos votos.

La agenda es:

1. **Propuesta sobre las elecciones de la Junta Directiva.** Esta propuesta pedirá a los miembros que aprueben
   elegir la junta a través de un sistema de votación en línea de una semana para asegurar que tantos
   miembros como sea posible pueden participar (en lugar de permitir sólo a los miembros
   presente en la llamada a votar). Véase más abajo para más detalles.

2. **Informes.** Habrá cuatro informes, todos basados en las conversaciones en
   las reuniones de consulta de los miembros que preceden a la reunión de los miembros. El
   los informes harán hincapié en las 5 - 7 prioridades que cada reunión acordó consensuar
   o el esquema general de la propuesta que se presenta.

   * Trabajo del movimiento
   * Infraestructura tecnológica
   * Estado de la cooperativa y compromiso de los miembros
   * Nueva propuesta de cuotas (esta propuesta incluirá cambios en nuestros estatutos)


## El período de votación 

El Primero de Mayo siempre ha enfatizado la importancia de la democracia interna y nosotros
no creen que una sola reunión en línea sea la forma más inclusiva de elegir la Junta
ya que no todos los miembros podrán asistir.

Así que la Junta propondrá a la reunión de miembros que llevemos a cabo la
elecciones a través de una boleta en línea (exactamente la misma [votación
sitio](https://vote.mayfirst.org/) hemos llevado a cabo nuestras elecciones para el último
6 años) durante una semana después de la reunión: Del 1 al 8 de diciembre. 

En esa boleta de elecciones de la Junta, también se pedirá a los miembros que clasifiquen la
prioridades que se generaron durante las reuniones de debate.

Además, se pedirá a los miembros que aprueben o rechacen un cambio propuesto
a los reglamentos relativos a las cuotas de los miembros.

## Detalles legales 

Nuestros nuevos estatutos requieren una reunión anual de miembros en la que el 10 por ciento de nuestros
La membresía activa constituye un quórum. Esto significa que necesitamos la asistencia de al menos
65 miembros (el número exacto se determinará el 31 de octubre, la fecha límite
fecha de elegibilidad de los votantes). Para evaluar este requisito, contaremos
miembros, no gente en la llamada. Invitamos a los miembros a que traigan a la mayor cantidad de gente
de su organización como quiera, sin embargo, sólo podemos contar cada miembro
una vez al tomar la asistencia.

Este quórum es un requisito de la ley del Estado de Nueva York bajo el cual nuestra cooperativa
se incorpora.
