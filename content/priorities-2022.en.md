+++
date = 2020-10-03T14:47:48Z
description = "Priorities"
title = "Priorities"

+++

The 2021 membership meeting approved and ranked the following priorities for 2022:

 * 390 Votes: Engage with movements to both provide useful technical resources while also developing their agency over their use of technology.
 * 366 Votes: Reorganize and improve documentation to fill gaps, ensure accessible language and guide members to using May First's tools and services.
 * 346 Votes: Encourage member participation in May First program teams by creating a welcoming environment that overcomes exclusions originated by any type of oppression.
 * 334 Votes: Promote multiple methods of orientation for new members to facilitate participation in organizational processes and use of services.
 * 333 Votes: Deepen our internal understanding and awareness of global grassroots methodologies and engage with movements via building relationships, trust, and participation.
 * 332 Votes: Elevate our collective understanding of the political moment and how we organize together to take action toward long term social transformation.
 * 329 Votes: Challenge the link between corporate technology tools and the professionalization of movement organizing by highlighting the white supremacy, patriarchy and other forms of oppression inherent in corporate tools.
 * 325 Votes: Raise consciousness within global movements about what May First is doing and internally within May First about what global and local movements are doing.
 * 324 Votes: Find unity and common cause with other organizations and movements via shared values and principles.
 * 313 Votes: Center participation of members in processes of evaluation, renewal, and introduction of new technology services and tools to ensure these are relevant to movement work and sustainable by our cooperative
 * 271 Votes: Work towards making the details and usage of the cooperative's physical infrastructure and energy consumption more transparent for members.

Previous year priorities:

 * [2016](/priorities-2016)
 * [2017](/priorities-2017)
 * [2018](/priorities-2018)
 * [2019](/priorities-2019)
 * [2020](/priorities-2020)
 * [2021](/priorities-2021)
