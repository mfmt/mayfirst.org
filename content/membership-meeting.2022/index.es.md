+++ 
date = "2022-09-14T14:47:48Z"
description = "Bienvenidos la Reunión de la Membresía de May First" 
title = "2022 Reunión de Membresía y proceso de votación" 
tags = [ "callout" ]
+++

**Table of Contents**

* [Introducción](#introduction)
* [Reuniones de Discusión](#discussion-meetings)
* [Elecciones de la Junta Directiva](#board-elections)
* [Reunión Principal](#main-meeting)
* [Periodo de Votación](#voting-period)
* [Detalles Legales](#legal-details)

## Introducción {#introduction}

<span style="font-size:1.em">Este año hemos sido testigos de cómo la vigilancia basada en la tecnología carcelaria y policial trabaja de la mano con las agendas de la derecha en todo el mundo, desde la revocación de nuestro derecho al aborto en Estados Unidos, hasta las redes de reconocimiento facial y el creciente uso de drones en todo el mundo.</span>

<span style="font-size: 1.em">Muchas de estas tecnologías son alimentadas por nuestro propio uso de servicios tecnológicos corporativos.</span>

<span style="font-size: 1.em">*¿Cómo podemos, como movimiento, resistir? ¿Cómo construimos nuestra fuerza y autonomía mientras reducimos nuestra dependencia de la tecnología corporativa?*</span>


<a href="https://mayfirst.coop/img/2022-MFMT-membership-meeting.es.png" target="_blank">
  <img width="300" height="600" border="0" align="left" hspace="20" src="/img/2022-MFMT-membership-meeting.es.png">
</a>

**Participa en nuestras reuniones de membresía este otoño para ayudar a definir nuestra estrategia.**

Informaremos sobre el progreso de nuestro equipo de Comunicaciones y Participación y de nuestro equipo de Tecnología, Infraestructura y Servicios, definiendo nuestras prioridades para el próximo año, y conociéndonos mientras construimos un poderoso movimiento por la tecnología autónoma.

Nuestras actividades de la reunión de la membresía tendrán lugar desde principios de octubre hasta principios de noviembre e incluirán sesiones en línea, discusiones en nuestro foro web y un periodo de votación de una semana.

Puedes inscribirte en todos los eventos en nuestra [página de registro de eventos] (https://outreach.mayfirst.org/civicrm/event/register?id=80&reset=1).

Por favor, eche un vistazo a nuestro programa y **comprométase a asistir al menos a una sesión de discusión y a la reunión principal de la membresía** en noviembre.

Haz clic sobre la imágen para abrir nuestra infografía de la reunión.
<br clear="left" />

## Reuniones de discusión {#discussion-meetings} 

*Todas las actividades están programadas para una hora o una hora y media.*

Celebraremos las siguientes cuatro sesiones de discusión. Todas las personas de la membresía interesadas pueden asistir a todas las sesiones, o pueden eleigir asistir sólo a las sesiones que consideran más relevantes para su trabajo de organización.

Cada actividad tendrá lugar en línea a través de nuestro sistema de videoconferencia (Jitsi Meet) mediante <https://i.meet.mayfirst.org/MembershipMeeting>. Nota: Para obtener los mejores resultados, conéctese a través de un teléfono Android o una computadora utilizando el navegador web Chrome o el navegador web Firefox. La mayoría de los iPhones y iPads no pueden utilizar nuestro sistema de interpretación.

Todas las actividades contarán con interpretación simultánea entre el inglés y el español.

También dispondremos de un foro de conversación correspondiente a través de [discurso](https://comment.mayfirst.org/) para las personas que no puedan asistir a la sesión en línea o que deseen continuar la conversación.

Además, pronto estarán disponibles los informes escritos de evaluación de nuestro trabajo.

Por favor, [infórmenos de las actividades a las que tiene previsto asistir](https://outreach.mayfirst.org/civicrm/event/register?id=80&reset=1).

### Agenda de Sesiones en Línea

* **Martes, 4 de octubre, a las 4:00 - 5:30 pm** (América/Nueva_York, 03:00 pm - 4:30 pm Ciudad de México, 8:00 - 9:30 pm UTC) \ - *Evaluación del entorno político actual* \
  Únase a nosotros en un taller de colaboración para discutir los aspectos más importantes del momento político actual y ayudarnos a planificar lo que está por venir. \
  [Descargar archivo de calendario 📅](political-environment.ics) | [enlace Google Calendar](https://calendar.google.com/calendar/render?action=TEMPLATE&text=Assessing%20the%20current%20political%20environment&dates=20221004T200000Z/20221004T213000Z&details=Join%20us%20in%20a%20collaborative%20workshop%20to%20discuss%20the%20most%20important%20aspects%20of%20the%20current%20political%20moment%20and%20help%20us%20plan%20for%20what’s%20to%20come.%20&location=https://i.meet.mayfirst.org/MembershipMeeting&trp=true) | [Participar a través del foro en línea](https://comment.mayfirst.org/t/2022-assessing-the-current-political-environment-evaluacion-del-ambiente-politico-actual/2338)
* **Martes, 11 de octubre, a las 2:00 - 3:00 pm** (América/Nueva_York, 1:00 - 2:00 pm Ciudad de México, 6:00 - 7:00 pm UTC) \
  *¿Estás interesado en presentarte como candidata a la Junta Directiva?* \
Si te interesa participar en la dirección de May First, este taller es para ti. Compartiremos información para todas las personas interesadas en formar parte de la junta directiva sobre el funcionamiento de la dirección de May First, las expectativas de quienes forman parte de la junta, la historia de la organización y el compromiso de la organización con la lucha contra el racismo y el sexismo. Por favor, [revise nuestros materiales de orientación para la junta directiva](https://mayfirst.coop/es/orientatión/). \
  [Descargue el archivo del calendario 📅](run-for-board.ics) | [Enlace Google Calendar](https://calendar.google.com/calendar/render?action=TEMPLATE&text=Running%20for%20the%20May%20First%20Board&dates=20221011T180000Z/20221011T190000Z&details=If%20you%20are%20interested%20in%20helping%20direct%20May%20First,%20this%20workshop%20is%20for%20you.%20We%20will%20share%20information%20for%20all%20prospective%20board%20members%20about%20how%20May%20First’s%20leadership%20operates,%20expectations%20of%20board%20members,%20the%20history%20of%20the%20organization%20and%20the%20organization’s%20commitment%20to%20combating%20racism%20and%20sexism.%20Please%20review%20our%20board%20orientation%20materials:%20https://mayfirst.coop/en/orientation/&location=https://i.meet.mayfirst.org/MembershipMeeting&trp=true) | [Participe a través del foro en línea](https://comment.mayfirst.org/t/2022-are-you-interested-in-running-for-the-board-of-directors-te-interesa-presentarte-como-candidata-a-la-junta-directiva/
2339)
* **Miércoles 19 de octubre, a las 15:00 - 16:30 horas** (América/Nueva_York, 14:00 - 15:30 horas Ciudad de México, 19:00 - 20:30 horas UTC)
  *Participación y Comunicación*
  ¿Cómo involucramos al movimiento y comunicamos la importancia de la tecnología bajo el control del movimiento? ¿Cómo alineamos nuestros proyectos con el momento político actual? Ayúdanos a elaborar estrategias para el próximo año. De esta reunión saldrán entre 6 y 8 prioridades
 para el próximo año. \
  [Descargar archivo de calendario 📅](engagement.ics) | [Enlace Google Calendar](https://calendar.google.com/calendar/render?action=TEMPLATE&text=May%20First:%20Engagement%20and%20Communications&dates=20221019T190000Z/20221019T203000Z&details=How%20do%20we%20engage%20the%20movement%20and%20communicate%20the%20importance%20of%20movement-controlled%20technology?%20How%20do%20we%20align%20our%20projects%20with%20the%20current%20political%20moment?%20Help%20us%20strategize%20for%20next%20year.%20This%20meeting%20will%20produce%20about%206%20-%208%20priorities%20for%20the%20coming%20year.%20&location=https://i.meet.mayfirst.org/MembershipMeeting&trp=true) | [Participar a través del foro online](https://comment.mayfirst.org/t/2022-engagement-and-communications-participacion-y-comunicaciones/2340)
* **Viernes, 28 de octubre, a las 13:00 - 14:30 horas** (América/Nueva_York, 12:00 - 13:30 horas Ciudad de México, 17:00 - 18:30 horas UTC ) \
  *Infraestructura y Servicios Tecnológicos* \
  Si quieres aprender más sobre nuestra tecnología y servicios actuales, ayudar a planificar cómo involucrar al movimiento en su desarrollo, y planificar nuestro desarrollo futuro, únete a nosotres y conoce al equipo de TIAS. De esta reunión saldrán entre 6 y 8 prioridades
para el año que viene. \
  [Descargue el archivo del calendario 📅](tias.ics) | [Enlace Google Calendar](https://calendar.google.com/calendar/render?action=TEMPLATE&text=May%20First:%20Technology,%20Infrastructure%20and%20Services&dates=20221028T170000Z/20221028T183000Z&details=If%20you%20want%20to%20learn%20more%20about%20our%20current%20technology%20and%20services,%20help%20plan%20how%20to%20engage%20the%20movement%20in%20their%20development,%20and%20plan%20our%20future%20development,%20join%20us%20and%20meet%20the%20TIAS%20team.%20This%20meeting%20will%20produce%20about%206%20-%208%20priorities%20for%20the%20coming%20year.%20&location=https://i.meet.mayfirst.org/MembershipMeeting&trp=true) | [Participe a través del foro en línea](https://comment.mayfirst.org/t/2022-technology-infrastructure-and-services-infraestructura-y-servicios-tecnologicos/2341)

¡Por favor, [regístrese](https://outreach.mayfirst.org/civicrm/event/register?id=80&reset=1)!

## Elecciones de la Junta Directiva {#board-elections}

De acuerdo con nuestros estatutos, 5 puestos de la junta directiva son elegidos por los trabajadores y 20 son elegidos por la membresía. De los 20, elegimos aproximadamente 1/3 cada año por períodos de 3 años.

Además, reservamos un número proporcional de puestos para la membresía que paga sus cuotas en México (alrededor del 20%).

Este año, dada la proporción actual de la membresía mexicana, se eligen dos puestos de la junta directiva par apersonas a la membresía que pagan cuotas en México y cuatro puestos de la junta directiva para el resto de la membresía.

El periodo de presentación de candidaturas a la junta directiva será del 1 al 31 de octubre. Y la elección será del 10 al 17 de noviembre.

Si quieres presentarte como persona candidata para la junta directiva, por favor asiste a la sesión de la junta directiva (ver arriba). También puede interesarle revisar nuestro [material de orientación de la junta](/orientación).

A partir del 1 de octubre, puedes [nominarte a ti mismo o a otra persona de May First que haya aceptado presentarse a través de este formulario](https://vote.mayfirst.org/).

## La reunión principal {#main-meeting}

Por ley, tenemos que celebrar una reunión "oficial" de la membresía con la asistencia de al menos el 10% de nuestra membresía (alrededor de 65 persona representantes este año).

**La reunión oficial de la membresía tendrá lugar el jueves 10 de noviembre, de 1:00 - 2:00 pm América/Nueva_York (12:00 - 1:00 pm Ciudad de México, 6:00 - 7:00 pm UTC)**

[Descargar archivo de calendario 📅](main.ics) | [Enlace Google Calendar](https://calendar.google.com/calendar/render?action=TEMPLATE&text=Main%20May%20First%20Membershp%20Meeting&dates=20221110T180000Z/20221110T190000Z&details=The%20main%20May%20First%20membership%20meeting%20for%202022.&location=https://i.meet.mayfirst.org/MembershipMeeting&trp=true) | [Participar a través del foro en línea](https://comment.mayfirst.org/t/main-membership-meeting-reunion-principal-de-los-miembros/2342)

Una vez que se haya tomado la asistencia y hemos determinado que tenemos quórum, podremos hacer propuestas para la aprobación por parte de la membresía. En las votaciones, las personas con membresías individuales tienen un voto y las organizaciones tienen dos votos. Aunque cuando su organización cuente con 10 personas en la reunión, sólo tendrá dos votos.


El orden del día es:

1. **Propuesta sobre elecciones de la junta directiva** Esta propuesta pedirá a la membresía que aprueben la elección de la junta directiva a través de un sistema de votación en línea durante un plazo de una semana. Esto para garantizar la participación del mayor número posible de personas de la membres[ia (en lugar de permitir que sólo voten las personas presentes). Ver abajo para más detalles.
2. **Informes** Habrá un informe financiero y dos informes de equipo, basados en las conversaciones de las sesiones de discusión de la membresía previas. Los informes de los equipos enfatizarán las 5 - 7 prioridades de las que se hizo eco en sus respectivas sesiones de discusión o las líneas generales de la propuesta que se presenta.
   * Informe financiero
   * Participación y comunicaciones
   * Infraestructura y servicios tecnológicos

## El periodo de votación {#voting-period}

May First siempre ha insistido en la importancia de la democracia interna y no creemos que una única reunión en línea sea la forma más inclusiva para elegir a las personas de la Junta Directiva, ya que no toda la membresía podrá asistir este día.

Por lo tanto, la Junta propondrá a la reunión de la membresía que llevemos a cabo las elecciones a través de una votación en línea (utilizando exactamente el mismo [sitio de votación](https://vote.mayfirst.org/) donde hemos llevado a cabo nuestras elecciones durante los últimos 6 años) durante una semana después de la reunión: Del 10 al 17 de noviembre.

En esa votación para las elecciones a la Junta, también se pedirá a la membresía que califique las prioridades que se generaron durante las sesiones de debate.

## Detalles legales {#legal-details}

Nuestros nuevos estatutos exigen una reunión anual de la membresía en la que el 10 por ciento de la membresía activa constituya el quórum. Esto significa que necesitamos la asistencia de al menos 65 representantes (el número exacto se determinará el 31 de octubre, fecha límite para la elegibilidad de los votantes). Para evaluar este requisito, contaremos el número de membresías representadas, no al número de  personas en la videollamada. Invitamos a toda la membresía a que traiga a todas las personas de su organización que desee, sin embargo, sólo podemos contar cada membresía una vez cuando se registra el conteo de asistencia.

Este quórum es un requisito de la ley del Estado de Nueva York bajo la cual está constituida nuestra cooperativa.
