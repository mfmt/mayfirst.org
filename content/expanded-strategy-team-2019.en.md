+++
date = 2019-04-23T14:47:48Z
title = "The Expanded Strategy Team of 2019"
tags = [ "callout" ]
+++

Thank you to all members who participated in our nine week expanded strategy
team process to hammer out a workplans for becoming a coop and expanding our
organization! 

The plans for becoming a coop *signal a major and historic shift in
how our organization functions and therefore require the approval of our
existing members* - so please read on to learn about what the committee has
recommend and read our proposed new bylaws.

The workplan for [expanding the
organization](/files/expanded-strategy-2019/expansion.en.pdf) has already been
approved by the Leadership Committee and is being implemented immediately.

## Summary of major decisions

* Previously, Media Jumpstart, Inc operated as a non-profit 501c3 organization
  in which the Board legally controlled the organization and appointed new
  board members. The Board members *voluntarily* allowed the elected Leadership
  Committee to make all decisions for the organization.  **We propose turning
  over legal control of the organization to the Members and Staff by retiring
  the Leadership Committee and empowering members and staff to elect the Board
  as a multi-stakeholder organization.** This decision means we have decided
  not to incorporate under the New York state Coop law because the law
  precludes a multi-stake holder operation. We have also chosen to continue as
  a non-profit, even though it means our members will not technically be
  "owners". By remaining a non-profit, we ensure that our organization cannot
  begin issuing dividends, distrubting earnings, or otherwise try to profit
  from its operation.

* Our legal name has always been Media Jumpstart, Inc and we have been
  operating under the name May First/People Link. We will continue as Media
  Jumpstart, Inc, but to help express the significance of our new bylaws, **we
  propose operating under the name "May First Movement Technology."**

* Previously, May First/People Link operated extra-legally as a collaboration
  between Media Jumpstart, Inc and Primero de Mayo (based in Mexico) in which
  both organizations operated under the power of the elected Leadership
  Committee. **We propose that each organization operate autonomously,** with
  Primero de Mayo members gaining automatic membership in May First Movement
  Technology, and a reserved number of seats on the Board of Directors. Primero
  de Mayo commits to making payments in kind or in cash to offset the costs of
  membership and program activity to support the movement building mission of
  the organization.


#### Please see the [proposed bylaws](/files/expanded-strategy-2019/bylaws-draft-legally-reviewed-lc-approved.en.pdf).

## Process

Based on nine weeks of discussion and hard work, the expanded strategy team
wrote a [plain language bylaws
draft](/files/expanded-strategy-2019/bylaws-expanded-strategy-team-approved.en.pdf).

Next, our generous volunteer lawyer from the [Urban Justice
Center](https://urbanjustice.org/) helped us shape that draft into a legally
sound document. While the resulting document has a lot of extra verbage, it
still retains the principles and goals of the original draft.

#### Our April 22, 2019, the Leadership Committee approved the draft and now we present it to our members for review: https://mayfirst.org/files/expanded-strategy-2019/bylaws-draft-legally-reviewed-lc-approved.en.pdf.

Between April 24th and May 10th, members are strongly encouraged to review the draft
and post comments and questions via our [discussion board](https://comment.mayfirst.org/t/discuss-the-proposed-coop-bylaws/1245).

On May 13th, the Leadership Committee will review all comments, incorporate
changes as necessary and then approve the final version.

After translation, the final version will be sent to all members for a vote.

Members will vote on three items:

 * Do you approve the new bylaws?
 * If the bylaws are approved, do you approve granting a special relationship
   to members of Primero de Mayo in Mexico to allow them to continue as members
   of the organization?
 * If the bylaws are approved, do you approve allowing the out-going
   Leadership Committee to continue governing the organization through the
   election of the new Board of Directors, which must happen by the end of
   June, 2019?

## Agendas

The full agendas of the expanded meetings are documented below for historical purposes.

 * *January 30*: Introductions, Goals and process
 * *February 6*: Unifying the Movement Building and Service provision poles ([read primer](/files/expanded-strategy-2019/poles.en.pdf))
 * *February 13*: Evaluating US/Mexico strategy ([read primer](/files/expanded-strategy-2019/us-mexico-strategy.en.pdf))
 * *February 20*: Discuss major issues around forming a coop ([read legal review](/files/expanded-strategy-2019/legal-entity-options.pdf))
 * *February 27*: Discuss proposals for growing the organization ([read proposal](/files/expanded-strategy-2019/expansion.en.pdf))
 * *March 6*: Review drafts of coop and growing the organization plans 
 * *March 13*: Review drafts of coop and growing the organization plans
 * *March 20*: Finalize coop and growing the organization plans

## Background

During the 2018 Membership Meeting, two proposals were overwhelming passed by
the membership:

 * [Legally converting to a cooperative](https://comment.mayfirst.org/t/mm-2018-form-a-committee-to-explore-converting-our-organization-to-a-multi-stake-holder-cooperative/910)
 * [Significantly grow the organization](https://comment.mayfirst.org/t/mm-2018-form-a-committee-to-explore-ways-to-significantly-grow-the-organization-to-a-more-sustainable-size/911)

Based on feedback during our two membership meeting discussions in December,
Leadership Committee voted to address both proposals together, in a single
body. 


