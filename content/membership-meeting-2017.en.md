+++
date = 2017-12-09T14:47:48Z
description = "May First/People Link Membership Meeting"
title = "2017 Membership Meeting Huge Success"
tags = [ "news","callout"]
+++

Thanks to all 2017 Membership Meeting participants - online, Brooklyn and Mexico!

![](/en/img/mm-2017.jpg)

See below for the results of the meeting.

### Friday, December 1, 2017: Pre-meeting Training and Presentation Day

Unedited full recording is available in [spanish](/es/media/es/audio/mm2017-es-day1.webm). For english, click links below.

* Introductions, Orientation, documents, history of MF/PL (<a href="/en/media/en/audio/mm2017-en-day1-orientation.webm">listen</a>)
* Email deliverability: Challenges with corporate take over of email (<a href="/en/media/en/audio/mm2017-en-day1-email.webm">listen</a>)
* Peer to peer technology: launch of organizing campaign in 2018 (<a href="/en/media/en/audio/mm2017-en-day1-p2p.webm">listen</a>)
* Technology and Revolution: What we have learned from the previous year (<a href="/en/media/en/audio/mm2017-en-day1-techandrev.webm">listen</a>)

### Saturday, December 2, 2017: Membership Meeting

 * Please see the [report on last year priorities](https://comment.mayfirst.org/t/mm-2017-evaluation-of-2017-priorities/55).

 * Technology and Revolution: Based on results of our year-long Technology and Revolution campaign, we broke into small groups to finalize movement strategies. See the [results](https://techandrev.org/#convergences).

 * Given our proposed movement-wide strategies from the morning, we chose the priorities of our organization for the coming year. See the [resulting priorities](/en/priorities).

### Election

The following candidates were elected to our Leadership Committee:

* Jamie McClelland, [Progressive Technology Project](https://progressivetech.org/) (returning LC member)
* Samir Rohlin Hazboun, [The Highlander Research and Education Center](http://highlandercenter.org/)
* Jerome Scott, the [US Social Forum](http://ussocialforum.org/) (returning LC Member)
* Maritza Arrastia, [The Writing Room](http://thewritingroom.org/) (returning LC Member)
* Damaris Rivera Mendoza, [Fundación PopolNa Nicaragua](https://popolna.org/)

And the [Proposal about transparency in rulemaking about the Internet Membership Meeting](https://comment.mayfirst.org/t/mm-2017-proposal-about-transparency-in-rulemaking-about-the-internet/28) was approved with 93% of the vote.
