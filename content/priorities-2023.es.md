+++
date = 2020-10-04T14:47:48Z
description = "Prioridades"
title = "Prioridades"
slug = 'prioridades-2023'

+++

En la reunión de miembros de 2022 se aprobaron y clasificaron las siguientes prioridades para 2023:

 * 257 Votas: Continuar el proyecto de actualización de la documentación de soporte 
 * 255 Votas: Priorizar el desarolló de las relaciones, la continuidad y la sostenibilidad 
 * 249 Votas: Difundir el mensaje sobre la importancia de la tecnología autónoma tanto en los espacios tecnológicos como en los del movimiento 
 * 247 Votas: Fomentar la participación de la membresía de May First y fortalecer nuestra comunidad 
 * 246 Votas: Colaborar con las organizaciones del movimiento para profundizar en nuestra comprensión colectiva del momento político actual 
 * 210 Votas: Facilitar la colaboración de la membresía en los proyectos de infraestructura 
 * 207 Votas: Investigar, seleccionar y probar un conjunto de herramientas para colaborar  
 * 205 Votas: Organizar sesiones mensuales para compartir habilidades y horarios de consulta 

Prioridades del año anterior

 * [2016](/priorities-2016)
 * [2017](/priorities-2017)
 * [2018](/priorities-2018)
 * [2019](/priorities-2019)
 * [2020](/priorities-2020)
 * [2021](/priorities-2021)
 * [2022](/priorities-2022)
