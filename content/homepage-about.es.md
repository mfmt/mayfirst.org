+++
date = 2019-11-14T13:52:02Z
title = "Acerca de May First Movement Technology"
homepage = "about" 
+++

May First Movement Technology es una organización de membresía sin fines de lucro que participa en la construcción de movimientos al avanzar en el uso estratégico y control colectivo de tecnología para luchas locales, transformación global y emancipación sin fronteras.

<div class = "faq-question"> <a href="#faq-answer-who" data-toggle="collapse"> ¿Quién eres? </a></div> <div class = "faq-answer collapse" id = "faq-answer-who">

<p>Somos una <strong>cooperativa de movimiento democráticamente dirigida sin fines de lucro organizaciones y activistas</strong> en los Estados Unidos y México. Hemos estado alrededor desde 2005 y nuestros 850 miembros (en su mayoría organizaciones y principalmente en los EE. UU. y México) aloja más de 10,000 direcciones de correo electrónico y más de 2,000 sitios web en nuestro hardware de propiedad colectiva y seguro que se ejecuta exclusivamente en cifrado discos.</p>

<p> Nuestros miembros dirigen nuestra organización a través de una <strong>raza y género diverso</strong> <a href="who"> cuerpo electo de activistas, organizadores y tecnólogos del movimiento </a> </p>

<p> Además de alojar esos datos, nuestra organización participa en redes, coaliciones y campañas que se organizan en torno a muchos temas relacionados con la tecnología y se ha distinguido por enfrentar muchas amenazas legales y hacer frente a citaciones. </p>

<p> Eso es lo que somos. Esto es lo que no somos: no somos una empresa o un servicio proveedor o un colectivo o una pequeña sin fines de lucro. Todas esas cosas están bien y Muchos de esos tipos de organizaciones realizan un trabajo excepcional. Solo hacemos algo más. </p> </div>

<div class = "faq-question"> <a href="#faq-answer-what" data-toggle="collapse"> ¿Qué haces? </a></div> <div class = "faq-answer collapse" id = "faq-answer-what"> <p> Creemos que los movimientos para el cambio fundamental deben priorizar el uso, protección y democratización de la tecnología. Queremos el liderazgo de nuestros movimientos pensar en esos objetivos y desarrollar un plan unificado para hacerlos realidad. Esa es la base de nuestro trabajo y hace que el Primero de Mayo sea uno de los más activos y organizaciones tecnológicas prominentes en la izquierda de este país. </p>

<p> Somos una organización líder de <a href = "https://mediajustice.org/network/"> Media Justice Network </a>; uno de los fundadores y líder de las <a href="https://radicalconnections.net"> Conexiones Radicales Red </a>; miembro de la <a href="https://apc.org/"> Association desde hace mucho tiempo para comunicaciones progresivas </a>; miembro de varias coaliciones de nuevas economías y redes y hemos estado involucrados en muchas coaliciones y redes establecidas para lidiar con problemas y luchas específicas. Participamos en muchas conferencias y grandes convergencias en el transcurso del año en los Estados Unidos, en México y internacionalmente. Nuestros representantes participarán en más de 20 eventos de este tipo. durante cualquier año. </p>

<p> Nuestra tecnología <a href="https://techandrev.org"> única y sin precedentes y La serie Revolution </a> ha reunido a más de 1500 líderes de movimiento y activistas en los Estados Unidos y México en 24 convergencias desde 2017 para pensar y hablar sobre la intersección de la tecnología y la revolución y forjar un programa de trabajo que podemos hacer en torno a la tecnología. </p>

<p> Ese compromiso con el trabajo del movimiento también se ve en nuestro compromiso con el protección de nuestros datos. Nuestros miembros son <a href = "https://support.mayfirst.org/wiki/legal"> golpea con frecuencia </a> con demandas para datos de gobiernos, avisos de eliminación de corporaciones o denegación de Ataques de servicio de enemigos políticos. Nos resistimos a todos. Tenemos resistió con éxito los ataques de denegación de servicio a los miembros que trabajan en justicia reproductiva, derechos palestinos y el movimiento Black Lives Matter. Nos negamos a dar detalles al FBI cuando exigió información de pago que habría identificado a los miembros de un sitio de Independent Media Center en Grecia. Nos hemos negado sistemáticamente a eliminar sitios web de sátiras por miembros como el Yes Men, que las corporaciones consideran violaciones de marcas registradas. </p>

<p> Pero esos son solo los casos más dramáticos porque, día a día, ferozmente proteja los datos mediante encriptación, respaldo y monitoreo. Los datos de nuestros miembros son nuestra herramienta de movimientos y nuestros movimientos son nuestra prioridad. </p>

<p> Todo esto, nuestro trabajo de movimiento y nuestra protección de datos, tiene un propósito: Ayudar a nuestros movimientos a construir una nueva relación con la tecnología. Queremos nuestro movimiento para pensar en tecnología, desarrollar un programa para protegerla y expandirla y de hecho trabajar en ese programa. Queremos ser una plataforma para ese tipo de colaboración: el control democrático y la expansión de la tecnología como herramienta para supervivencia humana y la lucha para que eso suceda. Queremos construir eso y queremos modelarlo. </p> </div>

<div class = "faq-question"> <a href="#faq-answer-cost" data-toggle="collapse"> ¿Cuánto cuesta? </a></div>
<div class = "faq-answer collapse" id = "faq-answer-cost">

Los miembros pagan cuotas en función de su capacidad de pago y de los recursos necesarios. Consulte nuestra <a href="/es/beneficios">página de ventajas para los miembros</a> para obtener más información.</p>.

 </div>

<div class = "faq-question"> <a href="#faq-answer-democracy" data-toggle="collapse"> ¿Cómo funciona la democracia? </a></div>
<div class="faq-answer collapse" id="faq-answer-democracy">

<p> Nuestros miembros toman nuestras decisiones clave a través de las reuniones.</p>

<p> La <strong> reunión anual de miembros </strong> generalmente tiene lugar en el otoño y toma al menos tres acciones:</p>

<ul> <li> desarrolla y luego clasifica las <a href="priorities"> prioridades </a> que describen la orientación general de nuestro trabajo para el próximo año </li> <li> la aprobación del informe financiero anual de la organización </li> <li> la elección de los miembros de nuestra <a href="who"> junta directiva de 21 a 25 personas </a> </li> </ul>

<p> La junta <strong> se reúne trimestralmente </strong>. La Junta dirige Primero de Mayo según el documento de prioridades y el presupuesto al aprobar planes de trabajo, nombrar grupos de trabajo y contratar personal. Esos grupos de trabajo y el personal llevan a cabo la implementación del plan de trabajo diario.

<p> Por cierto, cualquier miembro de Primero de Mayo puede asistir a una reunión de la Junta o una reunión de cualquiera de sus grupos de trabajo. No puede votar en la reunión de la Junta a menos que sea miembro de la Junta, pero puede participar. </p> </div>

<div class = "faq-question"> <a href="#faq-answer-racism-sexism" data-toggle="collapse"> ¿Puede explicar su política sobre la lucha contra el racismo y el sexismo? </a></div>
<div class = "faq-answer collapse" id = "faq-answer-racism-sexism">

<p> Esas enfermedades sociales han afectado mucho el trabajo tecnológico y son particularmente prevalente en la cultura de la tecnología del movimiento. Para toda nuestra existencia, nosotros han trabajado para combatir el racismo y el sexismo a través del reclutamiento consciente, representación, y un <a href="intentionality"> miembro aprobado declaración </a>. </p>

<p> La mayoría de los miembros de la Junta son personas de color y la mitad son mujeres. Todos mayores Representación de la tecnología del primer movimiento de mayo para los movimientos estadounidenses y mundiales debe incluir al menos una persona de color y una mujer. Excepciones a esto la regla debe ser aprobada por el grupo de trabajo de la Junta correspondiente. los los coordinadores de cada grupo de trabajo y comité permanente de la Junta deben incluir al menos una persona de color o mujer. </p>

<p> Concentramos nuestros esfuerzos de alcance y reclutamiento en organizaciones de Mayoría global y reuniones de conducta utilizando métodos de educación popular como Fundación para reflejar la diversidad de nuestros miembros. Todas las reuniones y público Los eventos se realizan en inglés y español con profesionales calificados y profesionales. interpretación. </p> </div>

<div class = "faq-question"> <a href="/es/sumate"> ¿Cómo me uno? </a> </div>
