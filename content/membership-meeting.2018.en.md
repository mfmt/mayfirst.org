+++
date = 2018-09-12T14:47:48Z
description = "Welcome to the May First/People Link Membership Meeting"
title = "2018 Membership Meeting"
+++

The 2018 Membership Meeting has ended!

Thank you to all the people who participated, particularly the record number of members who voted in our elections.

### Proposals 

All three proposals passed by wide margins:

 * [Legally converting to a cooperative](https://comment.mayfirst.org/t/mm-2018-form-a-committee-to-explore-converting-our-organization-to-a-multi-stake-holder-cooperative/910) (Yes: 160, No: 11)
 * [Significantly grow the organization](https://comment.mayfirst.org/t/mm-2018-form-a-committee-to-explore-ways-to-significantly-grow-the-organization-to-a-more-sustainable-size/911) (Yes: 162, No: 9)
 * [Postpone leadership committee elections](https://comment.mayfirst.org/t/mm-2018-postpone-leadership-committee-elections-until-we-vote-on-coop/912) (Yes: 136, No: 35)


