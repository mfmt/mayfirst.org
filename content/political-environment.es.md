+++
date = 2015-10-01T13:52:02Z
title = "Politico"
slug = 'politico'
+++

## La crisis humana y el futuro de Primero de Mayo

La raza humana se está enfrentando a su inminente extinción y, simultáneamente, a la oportunidad más significativa de su historia para crear una nueva sociedad mundial, funcional y sustentable. La gente, liderada por organizaciones y movimientos de la mayoría global, está buscando y empezando a articular respuestas reales a esta amenaza.

Tanto la crisis como su solución tienen su origen en la tecnología y, sin tecnología, esta situación nunca hubiera sido posible. 

Con una economía que ya no es capaz de sustentar la vida humana, un ambiente que claramente amenaza la existencia humana y una cultura política que ha reducido a los gobiernos a una caricatura de sus supuestos principios, podemos ver claramente que estamos ante una crisis cuyo impacto destructivo no es sólo inédito sino que está más allá de cualquier solución. No existe una reforma que pueda apaciguar esta crisis. No existe manipulación alguna que pueda ocultar su impacto. No existe una opción para la raza humana que no sea reemplazar el sistema que nos controla y la sociedad en que vivimos. 

Por otra parte, hoy en las Américas hay más gobiernos de izquierda en el poder que en toda su historia, los movimientos de masas luchan contra la opresión mientras que investigan y prueban nuevas formas de organización y producción, y hay un impulso creciente para la unidad más allá de fronteras nacionales impuestas. Hay una conciencia significativa acerca del cambio climático y un rechazo hacia la idea de la guerra como actividad humana. 

Como parte del ambiente progresista, internet se ha convertido en la herramienta de comunicación más popular de la historia, acercando a más de dos mil millones de personas. 

La tecnología es la fuerza impulsora de esta crisis y esos movimientos progresistas. 

Fue utilizada por las clases dominantes en formas destructivas y represivas que traicionaron su propósito progresista original. Reemplazó y redujo el trabajo humano, acumuló y procesó datos, y consolidó la habilidad de los gobiernos para controlar individuos y sociedades. La tecnología alteró el rol de los seres humanos en esta sociedad, tanto a las fuerzas de aquellos cuyo trabajo la sostiene como a las víctimas de una producción que cada vez requiere menos mano de obra. Facilitó el robo de recursos, incluyendo los intelectuales, de México (y muchos otros países de todo el mundo) por parte de Estados Unidos, y es, en sí misma, un recurso arrebatado al pensamiento y la colaboración de gente de todo el mundo (y en especial, México) para beneficiar a unas pocas empresas en Estados Unidos. 

Ha provocado una sociedad no sustentable que se autodestruirá y llevará consigo a la raza humana. 

Internet y las tecnologías de las comunicaciones fueron creadas, sin embargo, para permitir las comunicaciones entre todos los humanos: comunicaciones que son más rápidas, más eficientes, más exhaustivas, más proactivas y que más plenamente permiten expresar cada una de nuestras realidades que cualquier otra tecnología de comunicación en la historia. Nació de una colaboración de técnicos de todo el mundo y actualmente se mantiene gracias a miles de millones de personas, en un proceso en el que el movimiento progresista juega un rol de liderazgo. Cambió el concepto de circulación de noticias e información y los volvió una actividad más democrática e inclusiva llevada a cabo por masas de personas. Permitió la organización más efectiva de personas que, hasta ahora, casi no habían tenido acceso a los medios sino como consumidores. Empoderó a las personas que, hasta ahora, se consideraban desprovistas de poder. 

Cambió la raza humana para siempre. 

Para salvar a la raza humana, el mundo debe crear una sociedad nueva marcada por la cooperación, la colaboración y el intercambio humano. Puede crear esta sociedad únicamente si tenemos una tecnología de comunicaciones libre, funcional, realmente distribuida y robusta, especialmente internet. 

Al mismo tiempo, el mundo puede beneficiarse de lo aprendido con el desarrollo de internet, que demuestra cómo la colaboración sin ganancia trabaja para producir tecnología increíble; cómo algunos sistemas de colaboración democrática en todo el mundo no sólo mantienen este sistema tecnológico complejo sino que lo mejoran, fortalecen y popularizan; y cómo este tipo de colaboración entre individuos no sólo mejora el trabajo sino que promueve el pensamiento creativo.

Resumiendo, internet prueba que el mundo que imaginamos no sólo es posible sino necesario y altamente beneficioso. 

Por estas razones, Primero de Mayo/Enlace del Pueblo entiende la importancia de mantener y expandir la tecnología para que el movimiento la use y creemos que nuestro rol es crítico. Para eso necesitamos:

 * Asegurarnos de que internet esté en manos de la gente y sea controlado por ellos. 
 * Organizar y participar en todas las campañas y esfuerzos diseñados para perseverar y profundizar la propiedad colectiva y mantener la libertad de internet. 
 * Ampliar y fortalecer la colaboración de campañas que trabajan por objetivos similares en países de las Américas y más allá. 
 * Movilizar a las únicas fuerzas capaces de alcanzar estos objetivos, los movimientos de izquierda en Estados Unidos y México, participando de esos movimientos y trabajando con ellos como organización participante, así como invitando a las organizaciones líderes de esos movimientos a participar en Primero de Mayo/Enlace del Pueblo o el frente unitario del que seamos parte.

Estos son los desafíos que enfrentamos en el próximo período, y estamos preparados para afrontarlos. 

*Aprobado por la membresía en noviembre 2015, como parte de los [documentos oficiales](/es/documentos-oficiales).*
