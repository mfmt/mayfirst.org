+++
date = 2015-09-21T14:47:48Z
description = "May First Movement Technology is based in the US and Mexico."
featured_image = "binational.png"
tags = [ "featured" ]
tags_weight = 2
title = "Binational"
+++

May First Movement Technology is binational, based in the US and Mexico. Our member-elected Leadership Committee is drawn from both countries to allow us to build a strategic vision with an international perspective that matches the potential of movement as we continue to develop communication opportunities that cross borders.

