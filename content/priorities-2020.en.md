+++
date = 2020-10-03T14:47:48Z
description = "Priorities"
title = "Priorities"

+++

The 2019 membership meeting approved and ranked the following priorities for 2020:

 * 388 Votes: Realign our economic solidarity to ensure that staff is being respected and paid fairly.
 * 372 Votes: Complete our technology over-haul
 * 365 Votes: Continue work with the social justice and change movements
 * 343 Votes: : Facilitate participation in development of May First infrastructure projects and expand new technology resources and services
 * 334 Votes: : Deepening our involvement and political work in the cooperative and alternative economy movements
 * 327 Votes: : Engage our members in our work
 * 305 Votes: : Launch campaign to double our membership within the next two years
 * 299 Votes: : Investigate and explore options for making data center operations “greener”

Previous year priorities:

 * [2016](/priorities-2016)
 * [2017](/priorities-2017)
 * [2018](/priorities-2018)
 * [2019](/priorities-2019)
