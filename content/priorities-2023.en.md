+++
date = 2020-10-03T14:47:48Z
description = "Priorities"
title = "Priorities"

+++

The 2022 membership meeting approved and ranked the following priorities for 2023:

 * 257 Votes: Continue project to update support documentation 
 * 255 Votes: Prioritize relationship building, continuity and sustainability 
 * 249 Votes: Spread message about importance of autonomous technology in both technology and movement spaces 
 * 247 Votes: Increase member participation in May First and strengthen our community 
 * 246 Votes: Collaborate with movement organizations to deepen our collective understanding of the current political moment
 * 210 Votes: Facilitate member collaboration on the open-source infrastructure projects 
 * 207 Votes: Research, select, and test a suite of collaboration tools
 * 205 Votes: Host monthly member-led skillshare sessions and office hours

Previous year priorities:

 * [2016](/priorities-2016)
 * [2017](/priorities-2017)
 * [2018](/priorities-2018)
 * [2019](/priorities-2019)
 * [2020](/priorities-2020)
 * [2021](/priorities-2021)
 * [2022](/priorities-2022)
