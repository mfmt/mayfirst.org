+++
date = 2022-01-27T13:53:34Z
title = "Get Involved"
[menu]
  [menu.more]
    parent = "Membership"
    weight = 50
+++

## Not yet a member?

If you are not yet a member, why not [join now](/joinus)?

If you are not yet ready to join, feel free to [sign up for our mailing list](https://outreach.mayfirst.org/civicrm/profile/create?gid=2&reset=1).

## Already a member?

Great! If you'd like to get more involved, we have two work teams that meet regularly.

Both teams support our mission of engaging our members and the wider movement
in building and maintaining autonomous technology for organizing so we can win
our struggles for liberation.

 * **The Technology, Infrastructure and Services Team (TIAS)** focuses on the
   tools, including technical development as well as documentation, training
   and planning. Please see the [full invitation to
   join](https://comment.mayfirst.org/t/invitation-to-participate-in-the-technology-infrastructure-and-services-team-invitacion-a-participar-en-el-equipo-de-infraestructura-y-servicios-tecnologicos/1954). 

 * **The Engagement and Communicaitons Team** focuses on other ways to convey the
   importance of autonomous technology for the movement, including
   presentations, conferences, workshops, and others. To receive notifications
   of meetings, please fill out our
   [form](https://outreach.mayfirst.org/civicrm/profile/create?gid=35&reset=1).

Both teams meet with simultaneous interpretation to support speakers of either
English or Spanish.
