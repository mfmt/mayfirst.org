+++
date = 2015-09-21T14:47:48Z
description = "May First Movement Technology is committed to building the movement for a more just world."
featured_image = "movement.jpg"
tags = [ "featured" ]
tags_weight = 1
title = "Movement"
+++

May First Movement Technology is a member of the [Media Action Grassroots Network](http://mag-net.org/), the [Association for Progressive Communications](http://apc.org), the [US Social Forum](http://ussocialforum.org) and actively participates in the [Left Forum](http://leftforum.org), the [Allied Media Conference](http://alliedmedia.org/amc), the Mexican and International Coop movements, and other movement coalitions and gatherings. We are committed to building movements for global transformation and emancipation.

