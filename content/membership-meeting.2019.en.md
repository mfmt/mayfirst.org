+++
date = 2019-08-21T14:47:48Z
description = "Welcome to the May First Movement Technology Members' Meeting"
title = "2019 Members' Meeting and voting process"
tags = [ "callout" ]
+++

**Thank you to everyone who attended our 2019 membership meeting!**

We passed the proposal to vote for our board of directors via an online voting system.

We have sent all members in good standing an email with a link to use to cast your vote.

If you did not receive an email, please email: support@mayfirst.org to request your token.

The online voting period will run from Tuesday October 8 through Monday October
14 (at midnight America/New_York time).

You can see a [list of all
nominees](https://vote.mayfirst.org/nominations/2019) and also a [video
presentation from all nominees](https://vote.mayfirst.org/video/2019).

This page is the official page documenting the meeting and will be kept up to
date with the latest information.

## Infographic

Our membership meeting processed is summarized in the following info graphic.
Scroll down for additional details!

![Meeting details info graphic](/img/mfmt_infographic_en.web.png)

## The Main Meeting

Once attendance is taken and we have determined that we have quorum, we will be
able to make proposals for member approval. When voting, individual members get
one vote and organizational members get two votes. Even if your organization
sends 10 people to the meeting, you still only get two votes.

The agenda is:

1. **Proposal on Board elections.** This proposal will ask the members to approve
   electing the board through a one week online ballot system to ensure as many
   members as possible can participate (rather than only allowing the members
   present on the call to vote). See below for more details.

2. **Reports.** There will be three reports, all based on the conversations at the
   member consultation meetings that precede the Members' meeting.
 * *Movement Work Report:* this report details the many areas of work with our
   movements that we are involved in as well as some proposals for future
   involves...all attached to cost estimates
 * *Technology Report:* a report on the state of our technology and our
   priorities in developing it and the budget it will require.
 * *Financial Report:* as the name implies with input from the third
   pre-meeting convergence on Sept. 25

[Please register for the main meeting and all pre-meetings now!](https://outreach.mayfirst.org/index.php?q=civicrm/event/register&id=60&reset=1)

## Calendar of Dates and Pre-meeting Events

### Tuesday, September 4

Public nominations for our board of directors is open via our [voting
site](https://vote.mayfirst.org/) and announced to the full membership.

### Pre Meetings: Week of September 11 - Week of September 25

There are three pre-meetings to prepare for the main membership meeting.

All meetings take place online via
[mumble](https://support.mayfirst.org/wiki/mumble). Before each meeting, a
report is posted via our [online discussion
forum](https://comment.mayfirst.org/) allowing members unable to attend to
participate in the discussion. After each meeting, the results are posted to
the discussion board to the conversation can continue leading up to the main
meeting.

 * *Wednesday, September 11, 2:00 - 3:30 pm America/Mexico_City, 3:00 - 4:30 pm America/New_York:* Generate movement priorities for the coming year. Please read the [movement report](https://share.mayfirst.org/s/r4piD7X5qDKx4bo) and if you can't make the in person meeting, you can [comment on our forum](https://comment.mayfirst.org/t/2019-membership-meeting-movement-priorities/1519).
 * *Friday, September 20, 2:00 - 3:30 pm America/Mexico_city, 3:00 - 4:30 pm America/New_York:* Discuss technology infrastructure plan and dues for
   services changes. Please read the [technology report](https://share.mayfirst.org/s/mXBzjx49QifTqAE) and also the proposed [policy on changes in dues for extra resources](https://share.mayfirst.org/s/pYd7KXSfHM8koez) and if you can't make the meeting, you can [comment on our forum](https://comment.mayfirst.org/t/2019-membership-meeting-infrastructure/1520)
 * *Wednesday, September 25, 2:00 - 3:30 pm America/Mexico_city, 3:00 - 4:30 pm America/New_York:* What it means to be a coop, a [report on finances](https://share.mayfirst.org/s/DCt9w2z2ijTks3o), responsibilities of board membership and general coop functioning. Please feel free to [join the discussion](https://comment.mayfirst.org/t/what-it-means-to-be-a-coop/1530.) 

[Please register for the main meeting and all pre-meetings now!](https://outreach.mayfirst.org/index.php?q=civicrm/event/register&id=60&reset=1)

### Tuesday, October 1

Deadline for Board Nominations. No nominations allowed after this date.

### Week of October 1

A recorded video presentation of all board candidates is made available for members to watch to get to know the candidates.

### Tuesday, October 8

*3:00 - 4:00 pm America/New_York*

Members' meeting takes place via
[mumble](https://support.mayfirst.org/wiki/mumble).

## The Board Election

May First has always emphasized the importance of internal democracy and we
don't feel a single online meeting is the most inclusive way to elect Board
members since not all members will be able to attend.

So the current Leadership Committee will propose to the membership meeting that
we conduct the elections via an online ballot (the exact same way we have
conducted our elections for the last 5 years) for a week following the meeting:
October 8 to October 15.

In that Board elections ballot, members will also be asked to rank the
priorities that were generated during the September 11 meeting. Traditionally,
the Members' Meeting ends up setting a dozen priorities and that makes the
leadership's work much more difficult because it then must prioritize among
those priorities. This year, we are asking the members to do that so the Board
can more easily direct our work.

## Legal Details 

Our new bylaws require a yearly meeting of members at which 10 percent of our
active membership makes up a quorom. This means we need attendance by at least
65 members. To evaluate this requirement, we will be counting memberships, not
people on the call. We invite members to bring as many people from your
organization as you want, however, we can only count each membership once when
taking attendance.

This quorum is a requirement of New York State law under which our cooperative
is incorporated.

