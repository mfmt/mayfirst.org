+++ 
date = "2025-01-22T14:47:48Z"
description = "Bienvenidos la Reunión de la Membresía de May First" 
title = "Reunión de la Membresía de May First: febrero 2025" 
slug = "reunion-de-membresia" 
tags = [ "callout" ]
+++

**Íhdice**

* [Introducción](#introduction)
* [Reuniones de intercambio](#discussion-meetings)
* [Elecciones a la Junta Directiva](#board-elections)
* [Reunión principal (o asamblea)](#main-meeting)
* [Periodo de votación](#voting-period)
* [Información legal](#legal-details)


## Introducción {#introduction}

En 2024 se hizo evidente que las grandes empresas tecnológicas son una fuerza
determinante detrás de algunos de nuestros problemas más acuciantes. Ya se
trate del cambio climático, la expansión de movimientos violentos de derechas,
racistas y patriarcales, la expansión de tácticas policiales y militares
represivas o la subversión de procesos democráticos, los gigantes tecnológicos
desempeñan un papel cada vez más importante.

¿Qué rol ocupamos los proveedores de tecnología autónomos? ¿Cuál deberíamos
desempeñar? ¿Cómo podemos aprovechar el momento político actual para
movilizarnos en favor de un cambio sistémico, en lugar de reformas sin
trascendencia?

**Participa en las reuniones de membresía para profundizar en el tema y ayudar a
planificar nuestro futuro.**

Puedes inscribirte en cualquiera de nuestros encuentros a través de la página
de inscripción.

Eche un vistazo a nuestro programa y asegúrate **de asistir al menos a una
reunión de intercambio de opiniones y a la asamblea general.**

Puedes inscribirte a las reuniones en nuestra sección de [registro a eventos](https://outreach.mayfirst.org/es/civicrm/event/register?id=96&reset=1).

Consulta nuestro programa y asegúrate de **asistir al menos a una sesión de intercambio y a la reunión principal de membresía.**

## Reuniones de intercambio {#discussion-meetings} 

*Todas las sesiones tendrán una duración de una hora.*

Además de la asamblea general (véase más abajo), organizaremos tres sesiones
previas de intercambio. Las membresías están invitadas a asistir a todas las
sesiones, aunque también pueden optar por asistir a aquellas que consideren más
relevantes para su labor organizativa.

Cada sesión se desarrollará en nuestro sitio de videconferencias (Jitsi Meet).
*Nota: Para un mejor rendimiento, puedes conectarte a través de un teléfono
Android o una computadora utilizando los navegadores Chrome o Firefox. La
mayoría de los iPhones y iPads no son compatibles con nuestro sistema de
interpretación.*

Todas las actividades contarán con interpretación simultánea en inglés y
español.

También habilitaremos un espacio de conversación en nuestro foro [discourse](https://comment.mayfirst.org/t/2025-membership-meeting/2711) para
aquellas personas que no puedan asistir a las sesiones en línea o bien deseen
continuar los diálogos conversación.

Por otra parte, en breve estarán disponibles los informes de evaluación de
nuestro trabajo.

Por favor, haznos saber a qué actividades tienes [pensado asistir](https://outreach.mayfirst.org/es/civicrm/event/register?id=96&reset=1).

### Calendario de las sesiones en línea 

* **Martes 11 de febrero, 14:00 - 15:00** (Nueva York), 13:00 - 14:00 horas (Ciudad de México), 19:00 - 20:30 (UTC)
  *Análisis del contexto político actual* \
  Participa en el debate sobre los aspectos más importantes del momento político actual y ayúdanos a planificar lo que está por venir. \
  [Descargar archivo de calendario 📅](/en/membership-meeting/political-environment.ics) | [Enlace Google Calendar](https://calendar.google.com/calendar/render?action=TEMPLATE&text=Political%20Environment%20%7C%20Entorno%20pol%C3%ADtico&dates=20250211T190000Z/20250211T200000Z&details=&location=https%3A%2F%2Fi.meet.mayfirst.org%2FMembershipMeeting&ctz=UTC)
* **Viernes, 14 de febrero, 14:00 - 15:00** (Nueva York), 13:00 - 14:00 (Ciudad de México), 19:00 - 20:00 (UTC) \
  *Informes de Grupos y Prioridades* \
  Presentaremos los informes anuales del Personal y los Grupos de trabajo de Compromiso y Participación e Infraestructura y Servicios. Ayúdanos a evaluar el progreso alcanzado en la realización de las prioridades votadas el año anterior y a diseñar las prioridades siguientes. \
  [Descargar archivo de calendario 📅](/en/membership-meeting/teams.ics) | [Enlace Google Calendar](https://calendar.google.com/calendar/render?action=TEMPLATE&text=Team%20Reports%20and%20Priorities%20%7C%20Informes%20de%20los%20equipos%20y%20prioridades&dates=20250214T190000Z/20250214T200000Z&details=&location=https%3A%2F%2Fi.meet.mayfirst.org%2FMembershipMeeting&ctz=UTC)
* **Martes, 18 de febrero, 14:00 - 15:00** (Nueva York), 13:00 - 14:00 (Ciudad de México), 19:00 - 20:00 (UTC) \
  *¿Quieres postularte a la Junta Directiva?* \
Este año, hay siete puestos vacantes. Si tienes interés en ayudar a impulsar May First, este encuentro es para ti. Compartiremos información sobre el funcionamiento de la Junta Directiva de May First, las expectativas de sus integrantes, la historia de la organización y su compromiso con la lucha contra el racismo y el sexismo. Por favor, [revise nuestros materiales de orientación para la junta directiva](https://mayfirst.coop/es/orientación/). \
  [Descargar archivo del calendario 📅](/en/membership-meeting/orientation.ics) | [Enlace Google Calendar](https://calendar.google.com/calendar/render?action=TEMPLATE&text=How%20to%20run%20for%20the%20May%20First%20Board%20%7C%20C%C3%B3mo%20presentarse%20a%20la%20Junta%20Directiva%20de%20May%20First&dates=20250218T190000Z/20250218T200000Z&details=&location=https%3A%2F%2Fi.meet.mayfirst.org%2FMembershipMeeting&ctz=UTC)

[Inscríbete](https://outreach.mayfirst.org/es/civicrm/event/register?id=96&reset=1)!

## Elecciones a la Junta Directiva {#board-elections}

De acuerdo con nuestros estatutos, las personas trabajadoras eligen 5 puestos
de la Junta Directiva y la membresía 20. De estos 20, cada año elegimos
aproximadamente 1/3 para cumplir periodos de participación de 3 años.

Además, destinamos un número proporcional de puestos a la membresía que paga
cuotas en México (alrededor del 21%).

Este año tenemos 7 puestos en la Junta que pueden ser ocupados por cualquier
membresía de la organización (incluida la membresía mexicana). Nota: debido a
un empate, el año pasado admitimos temporalmente 26 representantes en la Junta
Directiva. Este año finalizan ocho puestos, pero solo admitiremos 25
integrantes, por lo que esta vez se cubrirán siete.

El periodo de nominación para la Junta Directiva será 27 de enero al 21 de
febrero. Las elecciones serán del 28 de febrero al 7 de marzo.

Si deseas postularte para la Junta Directiva, por favor asiste a la sesión de
orientación de la Junta Directiva (ver arriba). También puede interesarte
consultar nuestro [material de orientación para la Junta](/es/orientación).

A partir del 27 de enero, puedes puedes autonominarte o nominar a otra persona integrante de May First que haya aceptado presentarse a través de [este formulario.](https://vote.mayfirst.org/es/bienvenido).

## La reunión principal (o asamblea) {#main-meeting}

Por ley, es obligatorio celebrar una asamblea “oficial” a la que asistan al
menos el 10% de nuestra membresía (alrededor de 73 integrantes este año).

**La reunión oficial de membresía tendrá lugar el viernes 28 de febrero de 2pm a 3pm (Nueva York), 1pm a 2pm (Ciudad de México), 7pm a 8pm (UTC).**

[Descargar el archivo de calendario 📅](/en/membership-meeting/main.ics) | [Enlace Google Calendar](https://calendar.google.com/calendar/render?action=TEMPLATE&text=Main%20Membership%20Assembly%20%7C%20Asamblea%20principal%20de%20la%20membres%C3%ADa&dates=20250228T190000Z/20250228T200000Z&details=&location=https%3A%2F%2Fi.meet.mayfirst.org%2FMembershipMeeting&ctz=UTC) 

Una vez que se haya registrado la asistencia y comprobado que tenemos quórum,
podremos hacer propuestas para la aprobación de la membresía. A la hora de
votar, las membresías individuales tienen un voto y las membresías
organizacionales tienen dos votos. Aunque tu organización envíe 10 personas a
la reunión, sólo dispondrá de dos votos.

La agenda será:

1. **Propuesta sobre elecciones a la Junta Directiva.** Esta propuesta pedirá a las organizaciones miembros que aprueben la elección de la Junta Directiva mediante un sistema de votación en línea de una semana de duración para garantizar la participación del mayor número posible de representantes (en lugar de permitir votar únicamente a quienes estén presentes en ese momento). Véase a continuación para más detalles.

2. **Reportes.** Habrá un informe financiero y dos informes de equipo, basados en las conversaciones mantenidas en las sesiones de intercambio previas a la reunión general. Los informes de los equipos harán hincapié en las prioridades de las que se hayan hecho eco en sus respectivas sesiones o en las líneas generales de la propuesta presentada.
   * Informe financiero
   * Participación y comunicaciones
   * Infraestructura y servicios tecnológicos

## Periodo de votación {#voting-period}

De manera constante May First enfatiza la importancia de la democracia interna
y no creemos que una única reunión en línea sea la forma más inclusiva de
elegir a quienes integrarán la Junta Directiva ya que no es posible que toda la
membresía asista.

Por ello, la Junta propondrá en la reunión de membresía que llevemos a cabo las
elecciones a través de una votación en línea durante una semana después de la
reunión: del 28 de febrero al 7 de marzo (utilizando el mismo [sitio de
votación](https://vote.mayfirst.org/) donde hemos llevado a cabo nuestras
elecciones durante la mayor parte de nuestra historia).

En esa votación para las elecciones a la Junta, también se pedirá a la
membresía que clasifiquen las prioridades que se generaron durante las sesiones
de intercambio.

## Información legal {#legal-details}

Nuestros nuevos estatutos exigen una asamblea anual en la que el 10% de nuestra
membresía activa alcance el quórum. Esto significa que necesitamos la
asistencia de al menos 73 integrantes de la membresía (el número exacto se
determinará el 21 de febrero, fecha límite para votar). Para evaluar este
requisito, contaremos la membresía, no las personas presentes en la asamblea.
Invitamos a los grupos miembros a que traigan a tantas personas de su
organización como deseen; sin embargo, contabilizaremos cada membresía una sola
vez.

Este quórum es un requisito de la ley del Estado de Nueva York bajo la cual
está constituida nuestra cooperativa.
