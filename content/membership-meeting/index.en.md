+++
date = 2025-01-22T14:47:48Z
description = "Welcome to the May First Movement Technology Members' Meeting"
title = "May First Movement Technology Members' Meeting: February 2025"
tags = [ "callout" ]
+++

**Table of Contents**

* [Introduction](#introduction)
* [Discussion Meetings](#discussion-meetings)
* [Board Elections](#board-elections)
* [Main Meeting](#main-meeting)
* [Voting Period](#voting-period)
* [Legal Details](#legal-details)

## Introduction {#introduction}

In 2024 it became clear beyond a doubt: big tech is a driving force behind our most pressing issues. Whether it is climate change; the spread of violent right-wing, racist and patriarchal movements; the expansion of repressive police and military tactics; the subversion of democratic proceses - the corporate technology giants are playing an ever-increasing role.

What role are autonomous technology providers playing? What role should we be playing? How do we use the current political moment to mobilize for systemic change, rather than empty reforms?

**Join us at our membership meetings to find out more and help plan our future!**

You can register for all events on our [event registration
page](https://outreach.mayfirst.org/civicrm/event/register?id=96&reset=1).

Please take a look at our schedule and **commit to attending at least one
discussion session and the main membership meeting**.

## Discussion Meetings {#discussion-meetings}

*All activities are scheduled for 1 hour.*

In addition to the main assembly (see below), we will hold the following three
discussion sessions. Members are welcome to attend all sessions, or you can
choose to attend just the sessions you consider most relevant to your
organizing work.

Each activity will take place online via our video conference system (Jitsi
Meet) via <https://i.meet.mayfirst.org/MembershipMeeting>. *Please note: For
best results, please connect via an Android phone or a desktop computer using
either the Chrome web browser or the Firefox web browser. Most iPhones and
iPads are not capable of using our interpretation system.*

All activities will have live, simultaneous interpretation between english and
spanish.

We will also have a corresponding forum conversation via
[discourse](https://comment.mayfirst.org/t/2025-membership-meeting/2711) for
people unable to attend the online session or who wish to continue the
conversation.

Additionally, written reports evaluating our work will be available soon.

Please [let us know which activities you plan to attend](https://outreach.mayfirst.org/civicrm/event/register?id=96&reset=1).

### Schedule of Online Sessions {#schedule}

* **Tuesday, February 11th, 2:00 - 3:00 pm** (New York), 1:00 - 2:00 pm (Mexico City), 7:00 - 8:00 pm (UTC) \
  *Assessing the current political environment* \
  Join us to discuss the most important aspects of the current political moment and help us plan for what's to come. \
  [Download calendar file 📅](political-environment.ics) | [Google Calendar Link](https://calendar.google.com/calendar/render?action=TEMPLATE&text=Political%20Environment%20%7C%20Entorno%20pol%C3%ADtico&dates=20250211T190000Z/20250211T200000Z&details=&location=https%3A%2F%2Fi.meet.mayfirst.org%2FMembershipMeeting&ctz=UTC)
* **Friday, February 14th, 2:00 - 3:00 pm** (New York), 1:00 - 2:00 pm (Mexico City), 7:00 - 8:00 pm (UTC) \
 *Team Reports and Priorities* \
  Come hear full reports from the Workers, Engage and Communications and Technology, Infrastructure and Services team on work accomplished in the past year. Help us evaluate how well we met last year's priority and help shape priorities for hte coming year. \
  [Download calendar file 📅](teams.ics) | [Google Calendar Link](https://calendar.google.com/calendar/render?action=TEMPLATE&text=Team%20Reports%20and%20Priorities%20%7C%20Informes%20de%20los%20equipos%20y%20prioridades&dates=20250214T190000Z/20250214T200000Z&details=&location=https%3A%2F%2Fi.meet.mayfirst.org%2FMembershipMeeting&ctz=UTC)

* **Tuesday, February 18, 2:00 - 3:00 pm** (New York), 1:00 - 2:00 pm (Mexico City), 7:00 - 8:00 pm (UTC) \
  *Are you interested in running for the Board of Directors?* \
  This year, seven seats are up for re-election! If you are interested in helping direct May First, this workshop is for you. We will share information for all prospective board members about how May First's leadership operates, expectations of board members, the history of the organization and the organization's commitment to combating racism and sexism. Please [review our board orientation materials](https://mayfirst.coop/en/orientation/). \
  [Download calendar file 📅](orientation.ics) | [Google Calendar Link](https://calendar.google.com/calendar/render?action=TEMPLATE&text=How%20to%20run%20for%20the%20May%20First%20Board%20%7C%20C%C3%B3mo%20presentarse%20a%20la%20Junta%20Directiva%20de%20May%20First&dates=20250218T190000Z/20250218T200000Z&details=&location=https%3A%2F%2Fi.meet.mayfirst.org%2FMembershipMeeting&ctz=UTC)

Please [register](https://outreach.mayfirst.org/civicrm/event/register?id=96&reset=1)!

## Board Elections {#board-elections}

According to our bylaws, 5 board seats are chosen by workers and 20 are elected by the membership. Of the 20, we elect approximately 1/3 each year for 3 year terms.

In addition, we reserve a proportionate number of seats for members who pay dues in Mexico (about 21 percent).

This year **there are seven board seats that can be filled by any member of the
organization** (including members in Mexico). Given the current ratio of
Mexican members (21%), all *reserved* board seats for a member paying dues in
Mexico are filled. *Note: due to a tie, we temporarily allowed 26 members on
the board this past year. Eight board seats are expiring this year, but we will
only permit 25 board members, so only seven seats can be filled this round.*

The board nomination period will run from January 27th through February 21st. And the election will run from February 28th - March 7th.

If you would like to run for the board, please attend the board/orientation session (see above). You may also be interested in reviewing our [board orientation materials](/orientation).

Starting January 27th, you can [nominate yourself or another May First member who has agreed to run via this form](https://vote.mayfirst.org/).

## The Main Meeting {#main-meeting}

By law, we are required to hold an "official" membership meeting with attendance by at least 10% of our members (about 73 members this year).

**The official membership meeting will take place on Friday, February 28th, from 2:00 - 3:00 pm (New York), 1:00 - 2:00 pm (Mexico City), 7:00 - 8:00 pm (UTC)**

[Download calendar file 📅](main.ics) | [Google Calendar Link](https://calendar.google.com/calendar/render?action=TEMPLATE&text=Main%20Membership%20Assembly%20%7C%20Asamblea%20principal%20de%20la%20membres%C3%ADa&dates=20250228T190000Z/20250228T200000Z&details=&location=https%3A%2F%2Fi.meet.mayfirst.org%2FMembershipMeeting&ctz=UTC)

Once attendance is taken and we have determined that we have quorum, we will be able to make proposals for member approval. When voting, individual members get one vote and organizational members get two votes. Even if your organization sends 10 people to the meeting, you still only get two votes.

The agenda is:

1. **Proposal on Board elections.** This proposal will ask the members to approve electing the board through a one week online ballot system to ensure as many members as possible can participate (rather than only allowing the members present on the call to vote). See below for more details.
2. **Reports.** There will be one financial report and two team reports, based on the conversations at the member discussion sessions that precede the Members' meeting. The team reports will emphasize the priorities echoed in their respective sessions or the general outline of the proposal being put forward.
   * Financial Report
   * Engagement and Communications
   * Technology Infrastructure and Services

## The Voting Period {#voting-period}

May First has always emphasized the importance of internal democracy and we don't feel a single online meeting is the most inclusive way to elect Board members since not all members will be able to attend.

So the Board will propose to the membership meeting that we conduct the elections via an online ballot (using the exact same [voting site](https://vote.mayfirst.org/) where we have conducted our elections for most of our history) for a week following the meeting: February 28th through March 7th.

In that Board elections ballot, members will also be asked to rank the priorities that were generated during discussion sessions.

## Legal Details {#legal-details}

Our new bylaws require a yearly meeting of members at which 10 percent of our active membership makes up a quorom. This means we need attendance by at least 73 members (the exact number will be determined on February 21st, the cut-off date for voter eligibility). To evaluate this requirement, we will be counting memberships, not people on the call. We invite members to bring as many people from your organization as you want, however, we can only count each membership once when taking attendance.

This quorum is a requirement of New York State law under which our cooperative is incorporated.
