+++
date = 2015-11-24T14:47:48Z
description = "Prioridades (2017)"
title = "Prioridades (2017)"
slug = 'prioridades-2017'
+++

Estas prioridades son para fines de archivo. Por favor vea nuestras [prioridades del año actual](/es/prioridades).

La Reunión de Miembros de noviembre de 2016 aprobó las siguientes prioridades para 2017.

## Grupo en Línea

* Investigar y educar a los miembros sobre tecnologías y la construcción de los sistemas más comunes, de manera que los puedan tener tan disponibles como sea posible y que sean más fáciles de mantener ante el alto tráfico o los ataques, especialmente en colaboración con grupos aliados con necesidades similares.
* A la luz de la presidencia de Trump y la base mexicana / estadounidense de MF / PL, los esfuerzos de recaudación de fondos (con un eslogan como "construimos redes mientras que otros construyen muros") y el acercamiento educativo a grupos particularmente amenazados. Nuestros objetivos son tanto ayudar con la educación tecnológica, como incorporar a estos grupos como miembros activos, explorando los compromisos de seguridad que enfrentamos al desarrollar tecnología y crear conciencia de las decisiones que tomamos al decidir qué programas utilizar.
* Dedicar recursos humanos (nuevo personal remunerado, ya sea a tiempo completo o parcial, o personal voluntario en su defecto) para concentrarse en el desarrollo (promoción directa o una campaña de financiamiento centrada en la creación de membresía pagada y activa en el moviemiento).

## Brooklyln Grupo Uno

* May First / People Link formará un comité de Educación, para promover  Educación Tecnológica y Política, que será responsable de crear un plan de estudios que abarque temas sobre tecnología  y revolución, incluyendo significativamente la seguridad y la colaboración. Esto se utilizará para educar primero al comité de liderazgo, después a los miembros y, luego (informado y capacitado  con este conocimiento y ampliación de experiencias, y modificado para una audiencia popular) al público en general, en particular a los tecnólogos y organizadores de movimientos. Aclaración: el término educación se refiere a organizar.
* May First / People Link realizará una encuesta completa entre sus miembros, incluyendo entrevistas individuales con cada uno, para conocer su actual enfoque tecnológico y político y los puntos más vulnerables. Esto abarcará tanto la educación tecnológica y política como el desarrollo de servicios tecnológicos y la formación de coaliciones. May First / People Link desarrollará una base de datos de tecnología y proveedores políticos incluyendo habilidades y costos como parte del proceso.
* May First / People Link formará un comité de recaudación de fondos que identificará proyectos específicos (o necesidades) adecuados para una campaña de recaudación, y también sugiere que cuando se ofrecen servicios tecnológicos y políticos estratégicos (en asociación con tecnología identificada y aliados políticos) es compatible con, y puede Ayudar a financiar la educación tecnológica y política y  promover mejoras a la plataforma de Primero de mayo.

## Grupo Dos de Brooklyn
* Compartir historias de los miembros que respondan a la pregunta de cómo abordan temas de tecnología y cambio social y socializar esos aprendizajes como experiencias  entre los miembros. Esto podría ser parte de una estrategia de comunicación y/o recaudación de fondos más amplia. Ideas específicas: cooperativas de banda ancha, programas juveniles, programa POC Techies, Historias de Cointelpro, etc.
* Centrarse en la educación ampliamente, específicamente en torno a problemas de sistemas en red (por ejemplo, IoT) y privacidad, seguridad, y el impacto generado en las personas. Sería ideal para MayFirst ser una piedra angular para la obtención de recursos aplicables en educación - no necesariamente destinar  recursos ellos mismos, pero podrían ser un centro que comparta recursos que los miembros dentro de la organización han desarrollado ya, o para conectar a los miembros que podrían co-desarrollar nuevos Recursos y materiales de educación popular. A lo largo de estas líneas un video de 5 minutos sobre los temas podría ser muy útil.
* Dado el nuevo modelo de fijación de precios para la membresía, sería útil contar con materiales de divulgación que dejen claro los recursos disponibles para  la membresía, por ejemplo. $ 25 USD de aportación extras en la membresía individual les permitirá accesar  y hacer uso de  OwnCloud (la nube), las listas del email etc, $ 50 les permitirá contar con otros servicios extras. Los miembros podrían entonces participar en la promoción de membresías y reclutar a otros miembros y ayudar a construir una red más extensa de miembros. El acercamiento a las fundaciones parece una oportunidad potencial, pero el Equipo de Liderazgo debe decidir sobre un proceso para que MayFirst decida  a qué  tipo de financiamiento puede ser posible solicitar  y cuáles no sería conveniente solicitar, y potencialmente promover que los miembros colaboren con la recaudación de fondos de fundaciones u otras fuentes de financiamiento, p. Se podría crear un comité de miembros con experiencia en recaudación de fondos. En el entendido de que esto sería contribuir con el trabajo ya existente del movimiento, como parte de un cambio en el concepto de tecnología y revolución, enfocando más claramente el trabajo del movimiento. Este trabajo se daría en espacios compartidos con movimientos sociales donde MayFirst ya está trabajando - Foro Izquierdo, Foro Social, etc.

## Grupo Tres de Brooklyn
* Invertir en estrategias de reclutamiento para afiliaciones individuales y organizacionales. Utilizando la nueva estructura de financiación, centrándose en las comunidades de color y las que tienen diversas relaciones con la tecnología.
* Investigar flujos de ingresos alternativos, tales como establecer un servicio de consultoría para activistas / sin fines de lucro.

# Ciudad de México combinado grupo 1 y 2
* Construir estrategias para mejorar la participación de los miembros a través de campañas y proyectos comunes, profundizando nuestro desarrollo político basado en la realidad de cada centro (N.Y. y México), con herramientas de diagnóstico entre otras cosas.
* Trabajar hacia la sostenibilidad financiera de la organización a través de la promoción de la membresía y la mejora del sistema de pagos de cuotas.
* Continuar la instalación del proyecto de servidores de prueba en otros países que buscan aumentar y transferir conocimiento tecnológico.
* Proyección internacional para crear un tercer y cuarto centro de organización regional para May First / People Link.


