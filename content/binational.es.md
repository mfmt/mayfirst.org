+++
date = 2015-09-21T14:47:48Z
description = "Primero de Mayo/Enlace del Pueblo se basa en los Estados Unidos y México."
featured_image = "binational.png"
tags = [ "featured" ]
tags_weight = 2
title = "Binacional"
slug = 'binacional'
+++

Primero de Mayo/Enlace del Pueblo es binacional, con sede en los Estados Unidos y México. Nuestro Comité de Dirección, eligido por los miembros, se extrae de ambos países que nos permiten construir una visión estratégica con una perspectiva internacional que coincide con el potencial de movimiento como continuamos desarrollando oportunidades de comunicación que cruzan fronteras.
