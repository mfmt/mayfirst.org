+++
date = 2023-03-21T12:00:00Z
title = "Puestos Vacantes en May First"
+++


## ~~¡Estamos contratando!~~

**Este puesto ya no está disponible.**

* **Título del puesto:** Coordinación de la Membresía
* **Tipo de empleo:** Medio tiempo, 20 horas por semana
* **Experiencia:** Nivel inicial
* **Ubicación:** Trabajo remoto - zonas horarias de Norteamérica, Centroamérica o Sudamérica
* **Remuneración:** $24,000 dólares US al año

Lee la [descripción completa del vacante abajo](#descripción-del-puesto-coordinación-de-la-membresía):

Todas las personas interesadas deben enviar una carta de presentación y un currículum vitae antes del **viernes 14 de abril de 2023**.

Enviar carta de presentación y curriculum vitae en ambos **español e inglés** a: **job@mayfirst.org**

## Acerca de May First

**Misión:** May First se dedica a la construcción de movimientos mediante la promoción del uso estratégico y el control colectivo de la tecnología para las luchas locales, la transformación global y la emancipación sin fronteras.

Somos una cooperativa sin ánimo de lucro, democrática y multiparticipativa cuya membresía incluye una amplia gama de organizaciones e individuos vinculados a los movimientos por la justicia social y la liberación, incluyendo ONGs internacionales, pequeñas organizaciones locales, sindicatos, colectivos independientes, organizadores y activistas. La mayoría de su membresía se encuentra en Estados Unidos, con aproximadamente un 20% en México y otro 5% repartido por todo el mundo. El trabajo de May First se centra en dos áreas de programa interrelacionadas.

* Involucrar al movimiento de izquierda más amplio en una conversación sobre el papel crítico de la tecnología en nuestra lucha por un mundo más justo.
* Gestionar colectivamente nuestra propia infraestructura y servicios tecnológicos basados en Internet para nuestra membresía, con el fin de proporcionar privacidad y estabilidad a las actividades y comunicaciones críticas del movimiento. Estos servicios incluyen alojamiento de sitios web y correo electrónico, intercambio de archivos en línea al estilo de la "nube" y suite ofimática colaborativa, videoconferencias, etc.

### Valores de May First

* cooperación, colaboración e intercambio
* transparencia, apertura y honestidad
* igualdad y respeto
* convicción y disciplina
* compromiso contra el racismo, el sexismo y la explotación
* solidaridad, ayuda mutua y sostenibilidad
* humildad, receptividad a las críticas y compromiso con la resolución de conflictos

## Descripción del Puesto: Coordinación de la Membresía

### Resumen

El puesto de Coordinación de la Membresía servirá como punto de contacto principal para todas las personas afiliadas a May First, ayudará a planificar y coordinar eventos y comunicaciones tanto internas como públicas, y participará activamente como integrante del personal, los equipos de programa y el comité de coordinación para apoyar a la misión general de la organización. Nuestre candidate ideal combina habilidades de comunicación solidas con una pasión por el impulso de la tecnología para la justicia social y la liberación.

### Responsabilidades

* Servir de contacto principal para la membresía a través de la correspondencia por correo electrónico y el foro web de la membresía.
* Redactar y editar las comunicaciones con la membresía y supervisar las actividades de divulgación dirigidas a la membresía.
* Responder a preguntas sobre la afiliación y sus ventajas
* Tramitar las inscripciones de nuevas membresías y ayudar con la bienvenida y la incorporación de nuevas membresías a través del comité de bienvenida.
* Gestionar los pagos de los socios y otras tareas administrativas cotidianas.
* Realizar el seguimiento de los socios que están atrasados en el pago de sus cuotas.
* Participar en la coordinación de la asamblea anual de la membresía y las comunicaciones relacionadas.
* Participar en campañas de reclutamiento y comunicación con miembres potenciales.
* Gestionar las tareas administrativas de la organización
* Trabajar en estrecha colaboración con el personal y los equipos de programas para ayudar a planificar eventos y comunicaciones tanto internos como públicos
* Gestionar las cuentas de las redes sociales (por ejemplo, Mastodon).
* Redactar y editar correos electrónicos públicos, invitaciones a eventos y llamadas a la acción.

### Requisitos esenciales ( habilidades y aptitudes)

* Se requiere experiencia trabajando con movimientos: se pedirá a las personas candidatas que describan su experiencia como organizadoras o activistas y el papel que ha desempeñado la tecnología en su trabajo anterior.
* Se requiere experiencia o conocimientos tecnológicos básicos (por ejemplo, comodidad en el uso de plataformas de reuniones virtuales y colaboración en línea y disposición para aprender nuevas herramientas tecnológicas).
* No se requiere un conocimiento especializado o avanzado de la tecnología, sin embargo, una parte importante del trabajo es la disposición a aprender y utilizar alternativas de software libre para las tareas cotidianas y aprender algunas habilidades tecnológicas avanzadas con el fin de colaborar mejor con la membresía.
* Experiencia de trabajo en entornos en que haya diversidad racial, cultural y de género es una ventaja y una habilidad crítica para tener éxito en este puesto.
* Excelente nivel de comunicación escrito y verbal tanto en inglés como en español.

### Requisitos adicionales

* No es necesario ya ser una persona con membresía de May First, pero es una ventaja.
* Excelentes habilidades interpersonales y de comunicación
* Habilidades excepcionales de redacción y edición; capacidad para traducir ideas y estrategias complejas en narraciones claras, concisas y convincentes.
* Experiencia en gestión de redes sociales
* Se valorará la experiencia con las plataformas de software CiviCRM y Nextcloud.
* Espíritu de colaboración y capacidad para trabajar de forma proactiva e independiente.
* Fuertes habilidades organizativas, analíticas, de resolución de problemas y de gestión del tiempo demostradas a través de empleos anteriores, experiencia académica o práctica.
