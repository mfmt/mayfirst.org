+++
date = 2015-10-01T13:52:02Z
title = "Structure"
+++

May First/People Link is a democratic, membership-based organization with members from all parts of the world. There are two centers that are exclusively administrative and operational: Media Jumpstart (based in New York, collecting dues from all members not in Mexico) and La Cooperativa Primero de Mayo/Enlace de Pueblo (based in Mexico City, collecting dues from all members based in Mexico).

The organization's essential documents are: [The Political Environment](/en/political-environment), [Mission Statement](/en/mission), [Values](/en/values), [Goals](/en/goals), the [Statement of Intentionality](/en/intentionality), and this structure document.

The membership has the responsibility and authority to:

 * Elect a total of 15 members to the Leadership Committee to three year terms, with 5 members elected each year.
 * Define the priorities of the organization for the coming year, within the scope of the essential documents.
 * Approve changes to essential documents.
 * Present political, organizational, or other proposals to the Leadership Committee and receive a timely response.

Each center has the responsibility and authority to:

 * Collect membership dues for members within the center's regional area.
 * Organize the election of candidates for the leadership committee from the members in the center's region following the principles outlined in the Intentionality Statement, in order to ensure gender diverse representation from people of all backgrounds. Members of the Leadership Committee are elected in proportion to the number of members paying dues in their center, with no single center holding more than 2⁄3 of the positions. The entire membership elects nominated members or relects people whose period has expired this year.
 * Propose a budget for allocating resources collected from the membership or from external sources to the leadership committee for final approval.
 * Report on resources spent to the membership.
 * Promote political and organizational development in its environment to consolidate conditions of sustainability, as well as contribute to the formation of new centers for the future projection of the association, in accordance with the strategies designed by the Leadership Committee.
 * Submit proposals as a local instance to the Steering Committee and receive a timely response.

The leadership committee has the responsibility and authority to:

 * Organize and facilitate the membership meeting.
 * Provide political leadership and approve the allocation of resources within the scope of the essential documents and the priorities set by the membership at the prior membership meeting.
 * Turn priorities into projects once their technical, economic and political feasibility has been assessed with consideration to the centers and regions in which they will be applied.
 * Assist the administration and programming committees with the work of running the organization.

*Approved by the membership in November 2015, with modifications made in November 2016 as part of our [official documents](/en/official-documents).*

