+++
date = 2015-11-24T14:47:48Z
description = "Priorities"
title = "Priorities"
+++

## Leadership Committee Priorities

In January 2018, the Leadership Committee approved the following summary of the Member approved priorities:

1. Educate
 * Plan for a popular education based technology training program for movement organizers and activists
 * Increase the capacity of people traditionally excluded in the field of technology
 * Organizations should directly participate in designing needs and training priorities in technical issues
 * Identify barriers to people adopting technology we suggest
 * Specifically target technology used by children
 * Specifically target reporters and media makers to teach technology and security in the context of Journalism
1. Increase member involvement
 * Create better and more opportunities for member involvement in the organization
 * Strengthen our communications to our members and via alternative media about our activities using simple and friendly language
 * Engage member organizations in sharing lessons and examples they have of liberation technology experience
1. Strengthen Technology capacity of the organization
 * Decentralize our technological infrastructure, particularly in the global South, in partnership with other organizations;
 * Increase research and development of free software and free hardware
1. Organize
 * Call for a congress of the left on technology
 * Expand tech and rev sessions to include more popular education, to include explicit theory and practice from the movement’s history.
 * Organize technologists around the topic of peer to peer with an explicit link to movement organizers and organizations.

## Member approved priorities

The December 2017 Membership Meeting approved the following priorities for 2018.

#### 1: Training program: Generate a technical training / popular education initiative

* Increase the capacity of people excluded in the field of technology, POC (People of color), Indigenous Communities / First Nations / Native peoples, LGBTIQ, People with disabilities / different abilities, plus others ...
* Facilitate social movements, organizations of struggle, collectives propose people from their own organizations to participate in these trainings.
* Direct support to grassroots organizations
* That organizations directly participate in designing needs and training priorities in technical issues
* Share action topics, methodologies, dynamics, plans and capacities to strengthen organizations and struggles to know what are the needs of collaboration at different levels.
* Collaborate with other infrastructure providers, technology cooperatives, etc.
* Participate in meetings / face-to-face meetings // organize a multi-day working meeting to organize and define this proposal
* Dedicate ourselves to plan activities during 2018 to start implementing them in 2019

#### 2: Better Organization of voluntary participation in May First

* Identification of necessities for specific projects: e.g. interpretation, documentation, graphic design, programming, training
* List of those volunteering, in defined teams
* Inclusive spaces for volunteering

#### 3: Progressive geographic decentralization of our infrastructure: Strengthen our infrastructure in other places.

* Maintain and finished the installation of servers in Mexico.
* Explore the possibiltities of placing servers in Costa Rica through Codigo Sur.
* Infrastructure to do technical collaboration in other spaces.

#### 4: Carry out a communication strategy with a simple and friendly language both to spread that is the organization and also to the members for the calls, dissemination of resources and tools, and internal processes of PMEP.

#### 5: Establish a strategy with free, alternative media, journalists, communicators, with the objective of discussing and promoting safety and technology issues; and the importance of these issues being present in the media (Context the situation of vulnerability that journalists live in Mexico)

#### 6: Develop strategies for critical use and operation of all technologies for children.

#### 7: Strengthen communication with membership.

#### 8: Stengthen technological capacity, reocgnize the needs of technical support.
 
* Support the research and development of free software and free hardware.
* Link and promote reserach centers (Redcoop)

#### 9: Create working group to identify barriers to people adopting the technology we suggest

MFPL leadership: 

* talking to each other about the obvious barriers that are apparent to us from their work with members and iniciating process to respond to those conversations.
* Talk to members about their organizational barriers to adopting open-source technology.
* Talk to non-MFPL members (who are in the movement) about their barriers to adoption.

#### 10: Call for a congress of the left on technology

#### 11: Expand tech and rev sessions to include more popular education, to include explicit theory and practice from the movement's history.

#### 12: Organize technologists around the topic of peer to peer with an explicit link to movement organizers and organizations.

#### 13: Engaging member organizations in sharing lessons and examples they have of liberation technology experience - see movement building through technology

#### Individual Proposals that were not concensus of small groups

## Not consensed

These proposals were made but not consensed during the membership meeting. They are submitted to the Leadership Committee for further discussion and possible implementation without the full force of membership agreement.

#### 1:  Spread knowledge about how Internet search is corporate controlled and research all alternatives that internet activists working and assist non-corporate controlled Internet search.

#### 2: Formalize in the binational structure. The appointment of the administrators of the two centers by the leadership committee and the monitoring of finances and the control of the membership

See also our [2016 priorities](/en/priorities-2016) and [2017 priorities](/en/priorities-2017).
