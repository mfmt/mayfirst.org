+++
date = 2015-10-01T13:52:02Z
title = "Documentos Oficiales"
slug = 'documentos-oficiales'
+++

A partir de junio de 2019, la organización se rige por nuestros [estatutos](/es/bylaws) aprobados por los miembros.

El siguiente conjunto de documentos estuvo vigente desde 2015 hasta 2019 y se mantiene con fines históricos.

 * [Politico](/es/politico)
 * [Misión](/es/mision)
 * [Valores](/es/valores)
 * [Objetivos](/es/objetivos)
 * [Estructura](/es/estructura)
 * [La lucha contra el racismo y el sexismo](/es/intencionalidad)

*Aprobado por la membresía en noviembre 2015.*

A partir de 2006 - 2015, la organización utilizó una [declaración de la unidad](/es/declaración-de-unidad) que se proporciona con fines históricos.
