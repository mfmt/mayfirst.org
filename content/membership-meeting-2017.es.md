+++
date = 2017-12-09T14:47:48Z
description = "Reunión de membresía de Primero de Mayo/Enlace Popular"
title = "Membresía 2017 que se encuentra en un gran éxito"
tags = [ "callout" ]
+++

¡Gracias a todos los participantes de la Reunión de Membresía 2017!

![](/en/img/mm-2017.jpg)

Vea a continuación los resultados de la reunión.

## Viernes, 1 de diciembre de 2017: Día de capacitación y presentación previo a la reunión

La grabación completa no editada está disponible en [español](/es/media/es/audio/mm2017-es-day1.webm). Para inglés, haga clic en los enlaces a continuación.

* Presentaciones, orientación, documentos, historial de MF / PL (<a href="/en/media/en/audio/mm2017-en-day1-orientation.webm">listen</a>)
* Capacidad de entrega del correo electrónico: desafíos con la toma de control corporativo del correo electrónico (<a href="/es/media/es/audio/mm2017-en-day1-email.webm">listen</a>)
* Tecnología punto a punto: lanzamiento de la campaña de organización en 2018 (<a ​​href = " /es/media/es/audio/mm2017-en-day1-p2p.webm">listen </a>)
* Tecnología y revolución: lo que hemos aprendido del año anterior (<a href = "/en/media/en/audio/mm2017-en-day1-techandrev.webm">listen </a>)

## Sábado, 2 de diciembre de 2017: reunión de miembros

 * Consulte el [informe sobre las prioridades del año pasado] (https://comment.mayfirst.org/t/mm-2017-evaluation-of-2017-priorities/55).
 * Tecnología y revolución: con base en los resultados de nuestra campaña tecnológica y de revolución de un año, nos dividimos en pequeños grupos para finalizar las estrategias de movimiento. Ver los [resultados] (https://techandrev.org/es/#convergences).
 * Dadas nuestras estrategias propuestas para todo el movimiento desde la mañana, elegimos las prioridades de nuestra organización para el próximo año. Ver las [prioridades resultantes](/es/prioridades).

### Elección

Los siguientes candidatos fueron elegidos para nuestro Comité de Dirección:

* Jamie McClelland, [Progressive Technology Project](https://progressivetech.org/) (miembro de LC que regresa)
* Samir Rohlin Hazboun, [El Centro de Investigación y Educación de Highlander](http://highlandercenter.org/)
* Jerome Scott, el [Foro Social de EE. UU.](Http://ussocialforum.org/) (miembro de LC que regresa)
* Maritza Arrastia, [La sala de escritura](http://thewritingroom.org/) (miembro de LC que regresa)
* Damaris Rivera Mendoza, [Fundación PopolNa Nicaragua](https://popolna.org/)

Y la [Propuesta sobre la transparencia en la elaboración de normas sobre la Reunión de Membresía en Internet](https://comment.mayfirst.org/t/mm-2017-proposal-about-transparency-in-rulemaking-about-the-internet/28) fue aprobado con el 93% de los votos.

