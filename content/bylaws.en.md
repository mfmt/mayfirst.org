+++
date = 2019-06-14T13:52:02Z
title = "Bylaws"
[menu]
  [menu.more]
    parent = "About"
    weight = 20
+++

In June 2019, the membership of May First approved [bylaws](/files/bylaws.en.pdf) officially converting the organization into a nonprofit membership organization and effectively becoming a cooperative.
