+++
date = 2015-11-24T14:47:48Z
description = "Priorities for 2016"
title = "Priorities for 2016"
+++

The priorities are listed for archival reasons. See the [current year priorities](/en/priorities).

The November 2015 Membership Meeting approved the following priorities for 2016.

If you missed the meeting, the [full notes are available online](https://support.mayfirst.org/raw-attachment/wiki/projects/membership-meeting/2015/2015-annual-membership-meeting-notes.odt).

### Mesa on Human Communication Technology and Art

 * Given the current urgency for data sovereignty in the light of escalating cyber attacks on our movement, dialogue among activists and technologists is pressing. MFPL will conduct a campaign to promote dialogue between activists and technologists to together define technology & communication movement policy.
  * survey to identify what technology MFPL members currently use for communication beyond what MFPL offers (specifically use of social media; how concerned are members around data security)
  * popular education for members and the movement around race and class & cultural issues which get in the way of the conversation 
 * Continue & expand the techie of color training program
 * Develop & launch campaign of educational & informational videos around MFPL issues & services 

### Mesa on International / Earthcare

 * Using Jitsi and/or other open source tools, institute sessions where members across borders can share information on how Internet privacy threats are manifesting in their respective communities. Use content from these sessions to create political education resources, including international panels at various meeting such as the Left Forum.
 * Strengthen relationships with major international movements, including Internet Social Forum, World March of Women, WSF in Montreal, Climate Convergence
 * Coordinate learning from each MFPL member to learn what work they are doing, how they use MFPL technology, what gaps and needs they have, with a goal of identifying and building links between members and especially across borders 

### Mesa organized with various online participants
 * Link an "Internet privacy campaign" to local, day-to-day sureillence (street/building cameras, criminal records, police surveillence) 

### Mesa on Human rights, peoples rights, and democracy

 * Increase membership fees in a tiered and equitable manner that does not exclude anyone. This should be done for the purpose of growing Mayfirst's membership, infrastructure, capacity for outreach, and to provide resources for driving priority issues.
 * Engage members from marginalized communities for capacity building and education, to increase engagement and nurture leadership within the collective.
 * Build a voice to fight against the oppressive use of technology. This includes developing an analysis of how technology is used oppressively, and using that analysis to educate our membership base. 

### Mesa on Technology, Media, Gender, and Culture

 * May First must have as a political strategy for the collaboration and alliance and the strengthening of the alternative media movement and free media.
 * May First should promote the work of and messages from the organizations within it at the base, the grassroots, which will serve to strengthen the May First organization.
 * Make communications more clear with a deliberate campaign to communicate joint common interests and how these interests can be advanced/implemented through common tools.

 * May First should have as a priority to break the language barrier in its work.

 * May First should offer clarity about the way and logistics of documentation, and promote member participation.  Especially technical documentation and also organizational documentation.

 * Think of strategies of popular education that can generate resources to sustain the infrastructure.  It could be community parties to host workshops to share knowledge and promote linkages between our organizations that are members.

 * Due to the importance of MFPL, our organization should help shape main agendas of internet /related organizations and find common positions.

### Mesa on Countryside, Social development, social economy, and human rights.

 * We propose to design methodologies and diagnostics about information risk and security.  Should include analysis and case studies.

 * Grow MFPL infrastructure with the organization, assess server needs/operations in Mexico and in other countries.

 *  We believe it is necessary to promote Free Software tools and include training. To present to the members a table of the tools that MayFirst has as Free Software compared with those available in the commercial market, so we can see the differences.

 *  It is necessary to research to minimize the problems we've been having with communication systems, in particular Mezcla software.




