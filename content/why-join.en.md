+++
date = 2015-10-01T13:52:02Z
title = "Why you should join"
[menu]
  [menu.more]
    parent = "Membership" 
    weight = 20
+++

Your Internet work is vitally important to you and any responsible person or organization should think before moving services. That's why we're offering some reasons why we think it's critically important that you join May First Movement Technology right now.

1. **The Internet is Us**

    More that just a technology, the Internet is a massive network of people using computers to communicate with each other. It changes in size and character every second of the day as people log on and off and do what they log on to do. And it has a culture, a history and a vitally important future.

    That culture is based on freedom of communication: a place where everyone with an opinion or a story can express it.

    The history is one of collaboration where every major function of the Internet is driven by software that is free, developed by large groups of volunteers working together and maintained by teams of skilled volunteers, most of whom have never met personally.

    And the future is one of social change. The Internet has already played a critical role in combatting disinformation, expressing new ideas, delivering truth and bringing committed people together. It is among the the social justice movement's most potent communications tools.

    In short, it's ours and we should be using and developing it to the fullest. That's why we're building May First Movement Technology.

2. **We Need To Protect It**

    Many commercial providers and corporate interests don't want the Internet to belong to everyone. For them, the Internet is a way of selling your products and services and that vision dictates their approach to your use of the Internet and the development of its technology.

    Our movement has to resist the attempts by corporate interests to block political email in the name "fighting spam," prioritize commercial traffic over non-commercial traffic, and otherwise limit the use of the Internet as a tool for distributing information vital for our movements for social justice.

    But more than that, we have to develop the Internet in ways that empower us to use it and develop our use of it: to develop technologies, approaches to service and tools that enhance our movements' communications and organizing.

    It's time to join together to protect the Internet back from those who would misdirect, restrict and control its use.

    We think one way to do that is through May First Movement Technology.

3. **We're part of the same movement.**

    Although we think the quality of our services and staff rival those of any commercial provider, May First Movement Technology is not a company or a traditional Internet service provider and you are not "potential clients or customers."

    We're a social justice organization made up of activists and organizations who use the Internet. We're structured that way, we work that way and we plan our work that way.

    We also understand that your Internet work is political work. Your use of your website, email and other tools is, effectively, as viable politically as all the other work you do.

    And that's why we are actively advocate the use of the Internet by the social justice movement and are committed to building a vital communications system for it. As the right learned years ago, we will never succeed without creating our own infrastructure.

    May First Movement Technology and you are part of the same movement.

4. **You take control**

    Everything we do empowers the user: you are in control of your website, your email, your mailing lists and whatever online tools you need. Our role is to develop these tools, give you space to use them and provide support and help when you need us.

    We are also doing is working with coalitions, organizations and activists nationwide to think about what kinds of Internet tools we need to move forward as a movement and to develop and implement them.

5. **We offer outstanding service**

    Maybe the best argument we can make for joining May First Movement Technology is the provider you're currently using.

    Take a moment to compare what you currently have with what we offer. We think you'll find that:

    Our membership packages give members more technological power, more options and more tools for the money than anyone else in this industry.

    Our technical support is superior: available when you need it, staffed by the people who actually work on our network and on your sites, email and mail lists.

    Our equipment and technology is as good if not better: stable, powerful, reliable equipment and connections and all data backed up to remote servers every day.

    Our service is radically different because we're social justice activists and we relate to you that way. Our Case Studies describe the way we work with people. Get in touch with some of our members and ask them about their experiences with us.

    Think about it! Is THAT what you're now getting?

6. **We understand the importance of security**

    For social justice activists, security is a very real and often critical need. As activists, we know how important security is to you and we prioritize thinking about it and implementing security measures that will protect your data, identity and access. We believe that our approach to security and access is among the most thorough on the Internet and some the measures we take are unique.

7. **We try to practice what we preach**

    May First Movement Technology is committed to a work environment that respects the work and dignity of its staff. In the way we make decisions, the fact that we are a union shop, the physical environments we work in, and the way we deal with each other, we try to bring to realize, as much as possible, the principles of human interaction that we advocate for the world. That approach to non-alienated labor applies to the way we deal with members, whether we are providing support, helping with service issues or just giving advice. We treat you with the respect due any person, particularly a fellow activist, and we expect and usually get that kind of treatment from our members.

    That approach also drives our selection of staff and network "associates". We are working to create a diverse group of Internet workers who reflect the background and experience of our entire membership. That's a goal that we will always continue to work to meet.

8. **Reinvest in The Movement**

    You're probably already on the Internet with a service provider. So here's a question for you:

    Say there's another "provider" that can give you service, fees, and support equal to if not better than the quality you get from your current provider.

    And say that this other "provider" not only shares your commitment and goals but actually works with you to further your work.

    Finally, say that this other "provider" is building the kind of Internet organization you want to see and it needs your support to continue doing that.

    Which would you choose?

    Because THAT is the choice.

9. **We need you**

    None of this can work without you. No organization can grow without bringing in new members and ours requires support for its paid staff and the equipment, Internet lines and facilities we use.

    That comes from membership. We don't take foundation grants or other "donations". What we do is possible only through members' yearly fees. For us to keep going and continue growing, we need you. Now.

    Our capacity and capabilities grow with our size. As each new organization joins us, we increase what we are able to do for everyone.

    Those are seven reasons we think you should be part of May First Movement Technology.

    If they don't convince you, use this form to tell us why and we'll try to answer your concerns or questions.

    If they do convince you, please go to the Join section now! You don't have to move over until you're ready. We just want to make you part of our organization and have an idea when you will be ready.

