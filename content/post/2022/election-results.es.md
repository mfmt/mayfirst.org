---
date: 2022-11-22
title: "¡Ya están los resultados de las elecciones del May First!"
slug: election-results
tags: [ "news","callout"]
---


Gracias a todas las personas que han contribuido a la construcción de nuestro
proyecto democrático y participativo de tecnología para los movimientos. 

En primer lugar, damos la bienvenida a dos personas nuevas a la junta
directiva: [Brandon Forester](https://vote.mayfirst.org/es/node/258) de [Media
Justice](https://mediajustice.org) y [Santiago Navarro
Francisco](https://vote.mayfirst.org/es/node/247) de [Avispa
Midia](https://avispa.org/inicio/). También volvemos a dar la bienvenida a
cuatro miembros anteriores de la junta, Samantha Camacho, Jes Ciacci, Micky
Metts y Melanie Bush, que han sido elegidas para servir nuevos mandatos de tres
años.

Y en segundo lugar, aquí están las clasificaciones de las prioridades elegidas
por la membresía:

 * 257: Continuar el proyecto de actualización de la documentación de soporte    
 * 255: Priorizar el desarrollo de las relaciones, la continuidad y la sostenibilidad    
 * 249: Difundir el mensaje sobre la importancia de la tecnología autónoma tanto en los espacios tecnológicos como en los del movimiento    
 * 247: Fomentar la participación de la membresía de May First y fortalecer nuestra comunidad
 * 246: Colaborar con las organizaciones del movimiento para profundizar en nuestra comprensión colectiva del momento político actual
 * 210: Facilitar la colaboración de la membresía en los proyectos de infraestructura
 * 207: Investigar, seleccionar y probar un conjunto de herramientas para colaborar     
 * 205: Organizar sesiones mensuales para compartir habilidades y horarios de consulta

¿Qué es lo siguiente? En 2023 nuestra nueva junta directiva se reúne para
revisar las prioridades y empezar a planificar su implementación.
