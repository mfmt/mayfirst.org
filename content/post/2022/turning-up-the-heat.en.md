---
date: 2022-11-14
title: "Turning up the Heat on Big Tech: TODAY"
slug: turning-up-the-heat 
tags: [ "news","callout"]
---

It's not too late to join us today, Tuesday, November 15 at: 

 * 2:30 pm - 3:45 pm US (Pacific) / EE UU (Pacífico)
 * 4:30 pm - 5:45 pm US (Central)/Mexico City / EEUU (Central)/Ciudad de México
 * 5:30 pm - 6:45 pm US (Eastern) / EE UU (Este)
 * 10:30 pm - 11:45 pm (UTC)

**[Register here to get a link](https://outreach.mayfirst.org/civicrm/event/register?id=81&reset=1)**

Ongoing apartheid violence in occupied Palestine, brutal anti-worker
retaliation against organizing warehouse workers, and criminalization and
violence against communities of color globally all depend on surveillance
capitalism’s infrastructure – largely fueled by Amazon’s and Google’s military
and police contracts. Join us for a panel bringing together movement voices who
are turning up the heat on the unapologetic role of Google, Amazon, Zoom,
Facebook and others in upholding and expanding the reach of state violence.

The speakers will feature **Matyos Kidane** (Stop LAPD Spying), **Dr. Rabab
Abdulhadi,** and **Jonathan Bailey** (Amazon International).

*Interpretation between english and spanish, ASL and captions will be provided
Please register to receive a link to attend.*
