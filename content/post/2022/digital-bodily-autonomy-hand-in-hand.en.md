---
date: 2022-07-01
title: "The struggles for digital and bodily autonomy go hand in hand"
tags: [ "news","callout"]
---

When Roe v. Wade was decided in 1973, getting an abortion had very little to do
with communications technology.

In contrast, today, as we start experiencing the devastating impacts of the
decision's repeal, **getting an abortion is tightly bound with our digital
selves.**

We are already seeing reports of [Facebook and Instagram removing posts
mentioning abortion
pills](https://www.theguardian.com/technology/2022/jun/28/facebook-instagram-meta-abortion-pills-posts),
[high tech surveilance of those seeking
abortions](https://time.com/6184111/abortion-surveillance-tech-tracking/), and
[many other ways technology is
embedded](https://www.politico.com/news/2022/06/17/abortion-rights-advocates-digital-security-threats-00040654)
in the fight over autonomy and control of our bodies.

In response, we are seeing urgent calls for folks to delete their period apps,
close their Facebook accounts, de-Google their cell phones and, generally
speaking, turn their online lives upside down to avoid the techno-surveillance
dragnet unleashed by the recent Dobbs decision.

May First has been encouraging movements to give up our dependence on corporate
and surveillance technology for years and we will continue to do so. *Yet,
during a period of real and present crisis, we need to favor thoughtful,
intentional and well-planned strategies.* Rather then cycles of repression and
panic, we need to build on our history and strengths.

Now is the time to remind folx that over the last 20 years, **a growing movement
of organizers and technologists have been building user-driven,
privacy-respecting, consentful technology platforms as well as organizations
and communities to develop them.**

In the spirit of solidarity, we share this ecosystem of:

 * movement aligned Internet providers, many that pre-date the founding of
   Twitter and are still going strong ([May First](https://mayfirst.coop/),
   [Riseup](https://riseup.net/), [Autistici](https://autistici.org/),
   [Koumbit](https://koumbit.org/), [Greenhost](https://greenhost.com), [Green
   Net](https://greennet.org.uk/), [Electric
   Embers](https://electricembers.coop/), and [Web
   Architects](https://www.webarch.coop/)) just to name a few,

 * the [fediverse](https://fediverse.party/en/fediverse/), a well developed,
   de-centralized alternative to corporate social media (try
   [Social.coop](https://wiki.social.coop/home.html) if you want to get
   started),

 * powerful open source, privacy respecting software geared for organizing and
   movement providers that host it (see [Progressive Technology
   Project](https://progressivetech.org/) and [CiviCRM](https://civicrm.org/)),

 * multi-year campaigns targeting poor tech practices of corporate technology
   giants (see [Mijente](https://mijente.net/)’s [No Tech for
   ICE](https://notechforice.com/)),

 * libraries of resources to help us thoughtfully consider and change how we
   interact with technology (see [Our Data Bodies' Digital Defense
   Playbook](https://www.odbproject.org/tools/) or Allied Media Project's
   [Consentful Tech project](https://www.consentfultech.io/)), 

 * and many more examples, far too numerous to name here.

Like all collective endeavors, these projects need our love and support over
the long haul. Please help spread the word - rather then just deleting an app,
let’s encourage people to join an organization or try out technologies that
will serve us now and down the road when we may need it even more then today.
