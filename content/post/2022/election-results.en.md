---
date: 2022-11-22
title: "2022 Election results are in"
slug: election-results 
tags: [ "news","callout"]
---

Thank you to everyone who joined in building our democratic and participatory
movement technology project!

First, we are welcoming two new board members: [Brandon
Forester](https://vote.mayfirst.org/node/245) from [Media
Justice](https://mediajustice.org/) and [Santiago Navarro
Francisco](https://vote.mayfirst.org/node/249) from [Avispa
Midia](https://avispa.org/en/). We also welcome back 4 previous board members
Samantha Camacho, Jes Ciacci, Micky Metts and Melanie Bush, all of whom have
now been elected to new 3 year terms.

And second, here are the rankings for the member elected priorities:

 * 257: Continue project to update support documentation
 * 255: Prioritize relationship building, continuity and sustainability
 * 249: Spread message about importance of autonomous technology in both technology and movement spaces
 * 247: Increase member participation in May First and strengthen our community
 * 246: Collaborate with movement organizations to deepen our collective understanding of the current political moment
 * 210: Facilitate member collaboration on the open-source infrastructure projects
 * 207: Research, select, and test a suite of collaboration tools
 * 205: Host monthly member-led skillshare sessions and office hours

What's next? In 2023 our new board will be meeting to review the priorities and
begin plans to implement them.
