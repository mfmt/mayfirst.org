---
date: 2022-06-27
title: "Saturday, July 2nd: Allied Media Conference"
slug: amc2022 
tags: [ "news","callout"]
---

We are happy to announce our participation in the [2022 Allied Media
Conference](https://amc.alliedmedia.org) with the workshop **Collaborative
Journalism for Liberation.** This important conference brings together a
diversity of progressive left organizations and movements covering areas such
as technology, media, education, art, and others.

This year's Allied Media Conference will be held in Detroit, but will have a
hybrid format that will allow for both on-site and complementary virtual
activities. The Collaborative Journalism for Liberation workshop will be
conducted entirely online and bilingual Spanish-English.

We are presenting this workshop in conjunction with [La Coperacha](https://lacoperacha.org.mx/), an
alternative media outlet in our community. 

*The workshop will take place Saturday, July 2nd at 1:00 PM Mexico City time / 2:00 PM ET.*

This is a good opportunity to learn or experience a different way to
traditional journalism, in a way that strengthens our struggles and where the
Internet and our tools are critical to liberate us.

**To join the workshop, please [register for the Allied Media
Conference](https://www.eventbrite.com/e/allied-media-conference-2022-tickets-254718849937).**
