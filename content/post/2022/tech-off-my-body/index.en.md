---
date: 2022-05-26
title: "June 21st: Get the Tech Off My Body"
slug: tech-off-my-body 
tags: [ "news", "callout"]
---

*Thanks everyone who participated! Please see the [recording](/en/audio/tech.off.my.body/).*


----------

Please
[join](https://outreach.mayfirst.org/civicrm/event/register?id=79&reset=1) May
First and May First members [Progressive Technology
Project](https://progressivetech.org), [National Network of Abortion
Funds](https://abortionfunds.org), and [Detention Watch
Network](https://detentionwatch.org) for an online discussion  about the
implications of overturning Roe v. Wade on **Tuesday, June 21st:** 

 * 1:00 pm - 2:30 pm America/Los_Angeles time
 * 3:00 pm - 4:30 pm America/Chicgao and Mexico City
 * 4:00 pm - 5:30 pm America/New_York
 * 8:00 pm - 9:30 pm UTC

We'll hear from **Alejandra Pablos** (member of [Mijente](https://mijente.net),
[Detention Watch Network](https://detentionwatchnetwork.org) and [Immigrant
Justice Network](https://immigrantjusticenetwork.org)), **Riana Pfefferkorn**
([Stanford Internet Observatory](https://cyber.fsi.stanford.edu/io/io)), the
[National Network for Abortion Funds](https://abortionfunds.org), and **Alice Aguilar** ([Progressive
Technology Project](https://progressivetech.org)).

[*Register today to receive details on how to join the webinar*](https://outreach.mayfirst.org/civicrm/event/register?id=79&reset=1).

Overturning Roe v. Wade in the United States and restricting access to abortion
throughout the Americas is part of a strategy of repression that is focused on
our bodies. The strategy not only targets reproductive justice, but also
transgender rights, the mobility of poor people and people of color, and access
to health care and other critical services. In this webinar, we’ll hear from
May First members involved in the struggle and discuss the relationship between
the struggles for reproductive justice, privacy, autonomy, and freedom from
surveillance. **How can our campaign to re-envision technology based on consent
and liberation contribute to our movement's struggle for reproductive
justice?**


<table> 
<tr>
<td style="text-align: center">

![Alejandra Poblas head shot](alejandra.jpg) 

</td>
<td> 

**Alejandra Poblas** is a
reproductive justice organizer and storyteller at the intersections mass
incarceration and immigration. She shares her incarceration and abortion story
as an act of resistance to fight abortion stigma and bring awareness to
abolition and racial justice.

</td>
</tr>
<tr>
<td style="text-align: center">

![Alice Aguilar head shot](alice.png) 

</td>
<td> 

**Alice Aguilar's** (moderator) life work focuses in
supporting indigenous people's rights, environmental justice, and reproductive
justice issues. Alice's current work involves leading the fight against racism
and sexism in technology, bringing women, queer and Trans people of color into
movement technology, and winning respect for the people of color already doing
technology work within social justice movements. 

</td>
</tr>

<tr>
<td style="text-align: center">

![Riana Pfefferkorn head shot](riana.jpg) 

</td>
<td>

**Riana Pfefferkorn** is a Research
Scholar at the Stanford Internet Observatory. She investigates the U.S. and
other governments' policies and practices for forcing decryption and/or
influencing the security design of online platforms and services, devices, and
products, both via technical means and through the courts and legislatures.
Riana also studies novel forms of electronic surveillance and data access by
U.S. law enforcement and their impact on civil liberties.

</td>
</tr>

<tr>
<td style="text-align: center">

![May First logo](mayfirst.png) 

</td>
<td>

**May First Movement Technology**
engages in building movements by advancing the strategic use and collective
control of technology for local struggles, global transformation, and
emancipation without borders.

</td>
</tr>

<tr>
<td style="text-align: center">

![National Network of Abortion Funds logo](abortionfunds.png) 

</td>
<td>

**The National Network of Abortion Funds** The National Network of Abortion Funds builds power
with members to remove financial and logistical barriers to abortion access by
centering people who have abortions and organizing at the intersections of
racial, economic, and reproductive justice.

</td>
</tr>
</table>

[*Register today to receive details on how to join the webinar*](https://outreach.mayfirst.org/civicrm/event/register?id=79&reset=1).

