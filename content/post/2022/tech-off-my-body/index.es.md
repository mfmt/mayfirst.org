---
date: 2022-05-26
title: "21 de junio: Sacar la tecnología de mi cuerpo"
slug: sacar-de-mi-cuerpo
tags: [ "news","callout"]
---


*¡Gracias a todos los que han participado! Por favor, consulte la [grabación](/es/audio/tech.off.my.body/).*


--------

Por favor,
[unirse](https://outreach.mayfirst.org/civicrm/event/register?id=79&reset=1)
Mayo Los miembros de First y May First [Progressive Technology Progressive
Technology Project](https://progressivetech.org), [National Network of Abortion
Aborto](https://abortionfunds.org), [Detention Watch
Network](https://detentionwatchnetwork.org) para un debate en línea sobre las
implicaciones de la anulación de Roe vs. Wade el **martes 21 de junio:**


Escucharemos a **Alejandra Pablos** (miembro de [Mijente](https://mijente.net),
[Detention Watch Network](https://detentionwatchnetwork.org) y [Immigrant
Justice Network](https://immigrantjusticenetwork.org)), **Riana Pfefferkorn**
([Stanford Internet Observatory](https://cyber.fsi.stanford.edu/io/io)), la
[National Network for Abortion Funds](https://abortionfunds.org), y **Alice Aguilar** ([Progressive
Technology Project](https://progressivetech.org)).

 * 1:00 pm - 2:30 pm hora de América/Los_Angeles
 * 3:00 pm - 4:30 pm América/Chicgao y Ciudad de México
 * 4:00 pm - 5:30 pm América/Nueva_York
 * 8:00 pm - 9:30 pm UTC

[*Regístrese hoy para recibir detalles sobre cómo participar en el seminario web*](https://outreach.mayfirst.org/civicrm/event/register?id=79&reset=1).

Anular el caso Roe v. Wade en Estados Unidos y restringir el acceso al aborto
en todo el continente americano es parte de una estrategia de represión que se
centra en nuestros cuerpos. La estrategia no sólo afecta la justicia
reproductiva, sino también los derechos de las personas transgénero, la
movilidad de las personas pobres y de color, y el acceso a la atención
sanitaria y otros servicios críticos. En este seminario web, escucharemos a
personas afiliadas de May First en esta lucha y discutiremos la relación entre
las luchas por la justicia reproductiva, la privacidad, la autonomía y la
libertad de la vigilancia. **¿Cómo puede nuestra campaña para replantear la
tecnología basada en el consentimiento y la liberación contribuir a la lucha de
nuestro movimiento por la de nuestro movimiento por la justicia reproductiva.**

<table> <tr>
  <td style="text-align: center">
                                                                                                                                                                                                                     
  ![Alejandra Poblas head shot](alejandra.jpg)
                                                                                                                                                                                                                     
  </td>
  <td>
                                                                                                                                                                                                                     
  **Alejandra Poblas** es una
  organizadora de la justicia reproductiva y narradora en las intersecciones de la
  encarcelamiento masivo e inmigración. Ella comparte su historia de encarcelamiento y aborto
  como un acto de resistencia para luchar contra el estigma del aborto y concienciar sobre la
  la abolición y la justicia racial.                                                                                                                                                                                      
                                                                                                                                                                                                                     
  </td>
  </tr>

  <tr>
  <td style="text-align: center">
  
  ![Alice Aguilar foto de cabeza](alice.png)
  
  </td>
  <td>
  
  **El trabajo de Alice Aguilar** (moderadora) se centra en
  apoyar los derechos de los pueblos indígenas, la justicia ambiental y la justicia reproductiva
  reproductiva. El trabajo actual de Alice consiste en liderar la lucha contra el racismo
  racismo y el sexismo en la tecnología, incorporando a las mujeres, a las personas queer y trans de color al movimiento
  y ganarse el respeto de la gente de color que ya trabaja en tecnología dentro de los movimientos de justicia social.
  de color que ya están trabajando en tecnología dentro de los movimientos por la justicia social.
  
  </td>
  </tr>                                                                                                                                                                                                                    
  <tr>
  <td style="text-align: center">
                                                                                                                                                                                                                     
  ![Riana Pfefferkorn foto de cabeza](riana.jpg)
                                                                                                                                                                                                                     
  </td>
  <td>
  
  **Riana Pfefferkorn** es una investigadora
  Scholar en el Observatorio de Internet de Stanford. Investiga las políticas y prácticas de Estados Unidos y
  y otros gobiernos para forzar el descifrado o influir en el diseño
  influir en el diseño de la seguridad de las plataformas y los servicios, dispositivos y productos
  productos en línea, tanto por medios técnicos como a través de los tribunales y las legislaturas.
  Riana también estudia las nuevas formas de vigilancia electrónica y acceso a los datos por parte de
  de datos por parte de las fuerzas del orden de Estados Unidos y su impacto en las libertades civiles.
  
  </td>
  </tr>
  
  <tr>
  <td style="text-align: center">
  
  ![Logotipo de May First](mayfirst.png)
  
  </td>
  <td>
  
  **Tecnología del movimiento May First**
  se involucra en la construcción de movimientos mediante el avance del uso estratégico y el control colectivo
  de la tecnología para las luchas locales, la transformación global y la
  emancipación sin fronteras.
  
  </td>
  </tr>
  
  <tr>
  <td style="text-align: center">
  
  ![Logotipo de la Red Nacional de Fondos para el Aborto](abortionfunds.png)
  
  </td>
<td>
  
  **La Red Nacional de Fondos para el Aborto** La Red Nacional de Fondos para el Aborto construye el poder
  con los miembros para eliminar las barreras financieras y logísticas para el acceso al aborto mediante
  centrando a las personas que tienen abortos y organizando en las intersecciones de
  justicia racial, económica y reproductiva.
  
  </td>
  </tr>
  </table>

[*Regístrese hoy para recibir detalles sobre cómo participar en el seminario web*](https://outreach.mayfirst.org/civicrm/event/register?id=79&reset=1).
