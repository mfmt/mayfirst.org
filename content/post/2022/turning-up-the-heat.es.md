---
date: 2022-11-14
title: "Aumentando la presión sobre las grandes empresas tecnológicas: HOY"
slug: turning-up-the-heat
tags: [ "news","callout"]
---

No es demasiado tarde para unirse a nosotros hoy, Martes, 15 de noviembre a las: 

 * 2:30 pm - 3:45 pm EE UU (Pacífico)
 * 4:30 pm - 5:45 pm EEUU (Central)/Ciudad de México
 * 5:30 pm - 6:45 pm EE UU (Este)
 * 10:30 pm - 11:45 pm (UTC)

**[Regístrese aquí para obtener un enlace](https://outreach.mayfirst.org/civicrm/event/register?id=81&reset=1)**

La violencia del apartheid en los Territorios Ocupados Palestinos, las brutales
represalias contra los trabajadores de los almacenes, la criminalización y la
violencia contra las comunidades racializadas en todo el mundo dependen de la
infraestructura del capitalismo de vigilancia, en gran parte alimentada por los
contratos militares y policiales de Google y Amazon. Acompáñanos en un panel
que reunirá a las voces del movimiento que están denunciando el papel de
Google, Amazon, Zoom, Facebook y otros en el mantenimiento y la expansión de la
violencia estatal.

Los ponentes serán **Matyos Kidane** (Stop LAPD Spying), el **Dr. Rabab Abdulhadi** y
**Jonathan Bailey** (Amazon International).

*Se proporcionará interpretación del inglés al español. Por favor, regístrese
para recibir el enlace para asistir.*

