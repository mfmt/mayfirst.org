---
date: 2022-07-01
title: "Las luchas por la autonomía digital y corporal van de la mano"
slug: "autonomia-digital-corporal-van-de-la-mano"
tags: [ "news","callout"]
---

Cuando se decidió el caso Roe contra Wade en 1973, abortar tenía muy poco que
ver con las tecnologías de la información y las comunicaciones.

A día de hoy, cuando empezamos a experimentar los efectos devastadores de la
derogación del derecho al aborto en Estado Unidos, **esta decisión está
estrechamente ligada a nuestro ser digital.**

Ya estamos recibiendo informaciones de [Facebook e Instagram eliminando las
publicaciones que mencionan las píldoras
abortivas](https://www.theguardian.com/technology/2022/jun/28/facebook-instagram-meta-abortion-pills-posts),
[vigilancia de alta tecnología de quienes buscan
abortar](https://time.com/6184111/abortion-surveillance-tech-tracking/), y
[muchas otras formas en que la tecnología está
integrada](https://www.politico.com/news/2022/06/17/abortion-rights-advocates-digital-security-threats-00040654)
en la lucha contra la autonomía y por el control de nuestros cuerpos.

En respuesta, estamos viendo llamamientos urgentes para que las personas
desinstalen aplicaciones relacionadas con la menstruación, cierre sus cuentas
de Facebook, elimine Google de sus teléfonos móviles y, en general, ponga patas
arriba su vida digital para evitar la red de vigilancia tecnológica desatada
por la reciente decisión de Dobbs.

May First lleva años animando a los movimientos a abandonar nuestra dependencia
de la tecnología corporativa y de vigilancia, y seguiremos haciéndolo. Sin
embargo, *durante un periodo de crisis real y actual, necesitamos favorecer
estrategias reflexivas, bien intencionadas y bien planificadas.* En lugar de
ciclos de represión y pánico, tenemos que basarnos en nuestra historia y en
nuestros puntos fuertes.

Ahora es el momento de recordarnos que **durante los últimos 20 años, un
movimiento creciente de tecnólogues ha estado construyendo plataformas
tecnológicas consensuadas, impulsadas por las personas usuarias y respetuosas
con la privacidad, así como organizaciones y comunidades para desarrollarlas.**

Como parte de nuestras acciones de solidaridad, compartimos información sobre
estos ecosistemas de:

 *  proveedores de Internet alineados con el movimiento, muchos de los cuales
    son anteriores a la fundación de Twitter y siguen funcionando con fuerza
    ([May First](https://mayfirst.coop/), [Riseup](https://riseup.net/),
    [Autistici](https://autistici.org/), [Koumbit](https://koumbit.org/),
    [Greenhost](https://greenhost.com), [Green Net](https://greennet.org.uk/),
    [Electric Embers](https://electricembers.coop/), y [Web
    Architects](https://www.webarch.coop/)) por nombrar solo algunos),

 * el [fediverso](https://fediverse.party/en/fediverse/), una alternativa bien
   desarrollada y descentralizada a las redes sociales corporativas (para
   iniciar, prueba [Social.coop](https://wiki.social.coop/home.html)),
 
 * potentes software de código abierto, respetuosos de la privacidad y
   orientados a la organización y gestión de las relaciones entre los
   movimientos y sus comunidades (véase [Progressive Technology
   Project](https://progressivetech.org/) y [CiviCRM](https://civicrm.org/)),

 * campañas de varios años contra las malas prácticas de los gigantes
   tecnológicos corporativos (véase [Mijente](https://mijente.net/?lang=es),
   [No Tech for ICE](https://notechforice.com/?lang=es)),

 * bibliotecas de recursos que nos ayudan a reflexionar y a cambiar nuestra
   forma de interactuar con la tecnología (véase [Our Data Bodies' Digital
   Defense Playbook](https://www.odbproject.org/tools/) o Allied Media
   Project's [Consentful Tech
   Project](https://www.consentfultech.io/)),

 * y muchos otros ejemplos, demasiados para nombrarlos completos aquí.


Como todos los esfuerzos colectivos, estos proyectos necesitan nuestro amor y
apoyo a largo plazo. Puedes ayudar a correr la voz: en lugar de solamente
eliminar una aplicación, animemos a las personas  a unirse a una organización o
a probar un nuevo tipo de tecnología que nos servirán ahora y en el futuro,
cuando las necesitemos aún más que hoy.
