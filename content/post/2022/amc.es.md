---
date: 2022-06-27
title: "Periodismo Colaborativo para la Liberación: el sábado 2 de julio"
slug: amc2022 
tags: [ "news","callout"]
---

Estamos contentas de anunciar nuestra participación en [Allied Media
Conference](https://amc.alliedmedia.org/) 2022 con el taller **Periodismo
Colaborativo para la Liberación.** Esta conferencia es realmente importante,
confluyen una gran diversidad de organizaciones y luchas progresistas de
izquierda que trabajan en áreas como la tecnología, medios de comunicación,
educación, arte, entre otras.

Este año Allied Media Conference se desarrollará en Detroit, pero tendrá un
formato híbrido que permitirá tanto actividades presenciales como otras
complementamente virtuales. El taller Periodismo Colaborativo para la
Liberación estará llevándose a cabo completamente en línea y podrá escucharse
en inglés o español.

Este taller lo presentamos junto con [La
Coperacha](https://lacoperacha.org/mx/), medio de comunicación alternativo
parte de nuestra comunidad. 

*El taller tendrá lugar el sábado 2 de julio a las
13 Hrs. horario de México / 14:00 Hrs. horario este de América del Norte.*

Esta será una buena oportunidad para conocer o experimentar una forma distinta
al periodismo tradicional, en una manera que fortalece nuestras luchas y en
donde el internet y nuestras herramientas digitales son claves para liberarnos.

**Para participar en el taller, por favor [inscríbase en la Conferencia de Allied
Media](https://www.eventbrite.com/e/allied-media-conference-2022-tickets-254718849937).**
