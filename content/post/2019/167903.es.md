---
date: 2019-06-18
title: "Primero de Mayo Ahora es Una Cooperativa"
slug: node-167903
tags: [ "news","callout"]
---
Con 115 miembros votando, la membresía de May First / People Link decidió
convertir nuestra organización en una cooperativa sin fines de lucro llamada
May First Movement Technology.

Al finalizar un proceso que demoró aproximadamente seis meses, nuestra
organización ahora da otro paso hacia uno de nuestros objetivos estratégicos
centrales: democratizar la tecnología de información al colocarla en manos de
todo nuestro movimiento.

También aprobó nuestra "relación especial" con la cooperativa May First en
México y le otorgó al Comité de Liderazgo saliente la autoridad para continuar
coordinando nuestro trabajo hasta que elijamos la nueva Junta Directiva.

Tenemos mucho trabajo que hacer ahora: informar a nuestro movimiento de esta
decisión y explicar cómo afectará nuestro trabajo y el de todos los demás;
implementar el nuevo plan de trabajo que fue aprobado durante el proceso de
discusión; trabajar en la creación de las elecciones de la Junta para el
otoño; y, por supuesto, continuar todo el trabajo que hemos estado haciendo
(incluidas cinco sesiones nuevas de Tecnología y Revolución).

El CD se reunirá para comenzar la implementación de todo eso. Por ahora,
puedes visitar nuestro sitio web de transición.

<https://mayfirst.coop>

Vamos a construir uno nuevo con nuevos logotipos y un concepto de sitio
completamente diferente, pero eso llevará hasta el otoño porque tenemos que
diseñarlo y luego discutirlo a fondo con todos nuestros miembros. Por ahora,
este hará el trabajo por nosotros y tiene nuestra nueva información y un
logotipo de transición diseñado por Aarón Moysén, miembro del Comité de
Dirección. Jamie McClelland del CD hizo el trabajo en el sitio.

Aquí están los resultados (recuerde que la votación de una organización cuento
por dos votos):

Sí - No

Adopte los estatutos propuestos que transforman formalmente a la organización
en una organización cooperativa sin fines de lucro (haga clic para revisar los
estatutos completos).

174 - 3

Otorgue autoridad para que el Comité de Dirección existente continúe hasta que
la nueva junta directiva pueda ser elegida.

176 - 1

Establezca una relación especial con la Cooperativa Primero de Mayo en México
(como se define en nuestros nuevos estatutos)  
para continuar con nuestro liderazgo y estrategia binacionales.

172 - 5

Recordemos esto: aproximadamente 100 miembros participaron en el proceso que
acabamos de completar con muchas reuniones y conversaciones y varios
borradores de los estatutos y el nuevo plan de trabajo. Este es un desarrollo
importante y lo has hecho posible con tu arduo trabajo y compromiso.

Felicidades a nuestros miembros!

Tags:

[news callout](https://outreach.mayfirst.org/taxonomy/term/3)


