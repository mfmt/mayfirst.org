---
date: 2019-08-15
title: "Join us for the webinar of the DDOS attacks"
slug: content-join-us-webinar-ddos-attacks
tags: [ "news","callout"]
---
The webinar on the DDOS attacks is on Thursday, August 15 from 3:00 pm - 4:00
pm America/New_York time.

The webinar will take place entirely on mumble, our audio conferencing
software. To join, please [follow these instructions to install
mumble](https://support.mayfirst.org/wiki/mumble). If you are new to mumble,
we recommend using the iPhone or Android app, which is easier to setup and
configure.

Tags:

[callout](https://outreach.mayfirst.org/taxonomy/term/2)


