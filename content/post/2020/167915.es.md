---
date: 2020-03-19
title: "Alternativas a las reuniones en vivo"
slug: node-167915
tags: [ "news","callout"]
---
Si está buscando alternativas al encuentro cara a cara, tenemos varias para
que las use.

## El punto principal

La decisión que muchos de nuestros movimientos están tomando de cancelar o
posponer las convergencias en persona es una decisión responsable dadas las
circunstancias.

También es un revés potencialmente importante para nuestro trabajo y
organización. Nuestros movimientos alrededor del mundo usan la interacción
cara a cara para educar, organizar y movilizar. Sigue siendo nuestra
herramienta más potente y, por el momento, no podemos usarla.

Durante gran parte de su historia, la Tecnología del Movimiento Primero de
Mayo ha respondido a las peticiones de alternativas y apoyo durante las
crisis. Queremos hacerlo ahora. Pero también queremos hacer un punto que
nuestros miembros, compuestos en gran parte por cientos de organizaciones en
los Estados Unidos y México, han estado planteando:

Como movimiento, tenemos que empezar a repensar la forma en que convergen, se
reúnen y se comunican. Las grandes conferencias nacionales y regionales que no
son posibles momentáneamente resultarán cada vez menos factibles en el futuro.
A medida que nuestro movimiento se diversifica y crece, muchas de las personas
y organizaciones que queremos que asistan a estas conferencias no podrán
costearlas y una conferencia nacional a menudo consume la mayor parte de los
recursos de una organización, haciendo imposible otro trabajo importante.
Además, la enorme huella de carbono de los viajes a las conferencias hará que
este tipo de convergencia sea cada vez más insostenible a medida que la crisis
climática continúe.

El Primero de Mayo no está aquí para decirles cómo reunirse o converger. Sólo
informamos que muchos de nuestros miembros están repensando cómo encajan las
grandes reuniones e incluso las reuniones de tamaño medio en su trabajo de
organización... y están llegando a la conclusión de que tiene que haber otra
manera.  
Por ahora...

Estamos aquí para ayudarte a lidiar con la conferencia o reunión que acabas de
cancelar. Necesitas reunirte y no puedes hacerlo cara a cara. Tenemos
soluciones que hemos estado usando durante años porque, como una organización
cuyos miembros están en todos los Estados Unidos y México, el Primero de Mayo
mantiene constantes reuniones y convergencias en línea. Como una organización
líder en tecnología del movimiento de izquierda, compartimos lo que tenemos
con nuestro movimiento.

## Un punto sobre el Zoom

Sabemos que mucha gente está usando Zoom... incluyendo muchos de nuestros
miembros. Es un programa excelente pero tiene algunos inconvenientes muy
significativos. Cuesta dinero usarlo para grandes reuniones y muchas
organizaciones no tienen esa cantidad de dinero. Es un software propietario y
creemos que eso contradice la dirección que debemos tomar en el desarrollo de
software. Y, finalmente, es altamente intrusivo. La compañía que posee zoom
reclama el derecho de recolectar información de las personas en una reunión
que incluye:

Nombre, nombre de usuario, dirección física, dirección de correo electrónico,
números de teléfono, información de trabajo, información de tarjeta de
crédito, información de perfil de Facebook, información sobre el ordenador y
la conexión a Internet, y hábitos de compra y navegación.

Zoom dice que utiliza esta información para la comercialización y para servir
a sus usuarios. Pero hay otro "uso" que deja claro:

"Esto incluye responder a una demanda legalmente vinculante de información,
como una orden emitida por una entidad de aplicación de la ley de jurisdicción
competente, o como sea razonablemente necesario para preservar los derechos
legales de Zoom".

Esta es la declaración de privacidad de Zoom:

<https://zoom.us/privacy>

Puede que no tengas un problema con ello, pero deberías tenerlo en cuenta
cuando uses el zoom. Los participantes de su reunión serán entregados al
gobierno si éste lo exige. Punto.

Sólo para que quede claro. No entregamos ningún dato de los usuarios a ningún
gobierno. Punto.

## Nuestras soluciones

Estas son tres de las soluciones que usamos para las reuniones.

### Conferencias accesibles sólo con audio

Mumble es el programa de reunión y convergencia usado internamente para todas
las reuniones del Primero de Mayo. Por encima de todas las características,
campanas y silbatos, mumble prioriza el acceso para todos. Funciona en
entornos de bajo ancho de banda y se ejecuta sin problemas en todos los
equipos. Siempre ha sido y seguirá siendo gratuito, tanto el software libre
como el gratuito para que cualquiera lo ejecute. El programa puede acomodar la
interpretación simultánea (crítica para muchas organizaciones, incluyendo la
nuestra) de una manera que apoye la justicia del lenguaje: ningún idioma tiene
que ser el idioma dominante - siempre y cuando se tenga un intérprete en vivo,
cualquiera puede hablar en el idioma de su elección.

Mumble es un programa de sólo audio (sin video, sin compartir pantalla). Es
compatible con Windows, Macintosh y Linux, así como con android e iPhones.
Descargas una aplicación, vas a la sala específica que se utiliza para la
reunión y puedes reunirte con tantas personas como quieras. El programa
permite subcanales dentro del canal principal de la reunión que pueden ser
usados para sesiones de descanso.

Cualquiera puede unirse a la reunión y cualquiera puede iniciar una reunión,
sin necesidad de contraseñas ni de iniciar sesión. Los miembros de May First
pueden solicitar permiso para crear una sala permanente y la posibilidad de
silenciar o anular el silencio de los participantes.

Esta es nuestra página de apoyo para mumble con enlaces de descarga para
computadoras y teléfonos.

<https://support.mayfirst.org/wiki/mumble>

### Videoconferencia en la web para números pequeños

Jitsi-meet es un software de reuniones de video y audio para un número
limitado de personas. Es extremadamente fácil de usar y, si necesitas tener
una reunión con veinte o menos personas, este es absolutamente el programa a
usar. Es de código abierto, gratuito.

Jitsi-meet funciona mejor si todos los participantes usan la aplicación jitsi-
meet (ver <https://support.mayfirst.org/wiki/web-conference> para enlaces para
descargar la aplicación jitsi-meet para Windows, Macinstosh, Linux, Android o
iPhone).

En un apuro, se puede participar desde cualquier navegador web yendo
directamente a <https://meet.mayfirst.org>, sin embargo, el uso de la
aplicación resultará en un rendimiento mucho mejor.

La mejor manera de usar jitsi-meet es usar su propio navegador y nuestra
página de soporte le dice qué versión necesita y dónde descargarla.

### Transmisión unidireccional

El Primero de Mayo tiene una forma muy simple de transmitir su audio y video a
muchas personas: <https://live.mayfirst.org/>.

Usando tu navegador y la cámara de tu ordenador y el micrófono (funciona en tu
ordenador o en tu teléfono móvil) puedes publicar instantáneamente tu vídeo y
audio a través de un enlace con el resto del mundo.

Este servicio requiere un nombre de usuario y una contraseña del Primero de
Mayo para ser utilizado (cualquier cuenta de usuario existente del Primero de
Mayo funciona).

May First Live es un software beta - no ha sido probado completamente, en
particular para asegurar que funciona bien con todos los sistemas operativos y
navegadores (los usuarios de iPhone y iPad pueden tener problemas para ver el
vídeo en streaming - los comentarios son bienvenidos).

Es necesario registrarse para utilizar este servicio en nuestros servidores y
sólo los miembros pueden hacerlo sin preguntar. Así que si no eres miembro,
tienes dos opciones: puedes convertirte en uno (lo cual deberías hacer de
todas formas) o puedes enviarnos un correo electrónico para configurar una
credencial para ti y enviártela. Lo haremos en un par de horas por lo general.

Ten en cuenta: hay un retraso de 45 - 60 segundos en la transmisión de audio-
vídeo. Si dices algo, no se oirá en los ordenadores de la gente hasta un
minuto.

Tags:

[callout](https://outreach.mayfirst.org/taxonomy/term/2)


