---
date: 2015-12-02
title: "May First Elects New Leadership"
slug: content-may-first-elects-new-leadership
tags: [ "news","callout"]
---
The members of May First/People Link have elected Rob Robinson, Sandra
Contreras Martinez, Manisha Desai, Francia Gutierrez, Aarón Moysén and Jacobo
Nájera as new members of the organization's Leadership Committee. These
elected members fill five positions already open as well as the seat occupied
by Enrique Rosas who has stepped down from the LC. The full Leadership
Committee is listed at:

<https://mayfirst.org/en/who/>

The membership has also approved six official documents which can be found
here:

<https://mayfirst.org/en/official-documents/>

The vote breakdown:

Values/Valores 84 in favor, 2 against  
Goals/Objectivos 84 in favor, 2 against  
Mission/Misión 82 in favor, 4 against  
Structure/Estructura 80 in favor, 6 against  
Intentionality/V 76 in favor, 10 against  
Political Environment/Politico 73 in favor, 13 against

And the list of priorities set for our organization by its membership is:

<https://mayfirst.org/en/priorities/>

These votes were the final phase of the 2015 Membership Meeting which has now
been completed.

Tags:

[news callout](https://outreach.mayfirst.org/taxonomy/term/3)


