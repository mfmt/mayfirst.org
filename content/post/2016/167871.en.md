---
date: 2016-11-30
title: "21st Century COINTELPRO--FBI Surveillance Then and Now - How to Protect Ourselves"
slug: content-21st-century-cointelpro-fbi-surveillance-then-and-now-how-protect-ourselves
tags: [ "news","callout"]
---
Hold this date and event!

21st Century COINTELPRO--FBI Surveillance Then and Now - How to Protect
Ourselves

Monday, December 12th at 12pm PACIFIC / 1pm MOUNTAIN / 2pm CENTRAL / 3pm
EASTERN

You don't want to miss this online presentation. Let us explain why.

You're getting a lot of email describing the tough times we're facing. And
it's true...we're heading for some very difficult and dangerous times. We need
to be ready to resist and push back...paticulary in the area of communications
and our right to use communications technology. This new administration is not
only threatening to turn back our rights like destroying the Net Neutrality
protections we fought so hard for; it's encouraging right-wing forces to
actually attack us through things like Denial of Service attacks.

Our privacy, data protection and access to each other are all under very real
threat.

Sounds intimidating but one thing to keep in mind: we've actually been through
a very repressive period before and there are lots of people around who lived
through it and know all about it.

In the 1970s, the government sponsored a massive surveillance, infiltration
and disruption program called COINTELPRO. It destroyed movements,
relationships, and activists' lives. It destroyed the trust and hopefullness
needed to build and sustain a movement. It destroyed the movement for justice
in this country.

We haven't seen anything like it since then.

Some of us think we're about to. But this time, we have ways of fending it off
and protecting ourselves and we can start with our communications.

Join Progressive Technology Project and May First/People Link in a phone
"presentation/discussion" about COINTELPRO in the 21st Century: How to Protect
Yourself.

We'll talk about what COINTELPRO was and what we're facing now...and what you
can do to protect yourself, your data and your online access.

Here's the information page with the registration link:

[https://ptp.ourpowerbase.net/civicrm/event/info?reset=1&id=436](https://ptp.ourpowerbase.net/civicrm/event/info?reset=1&id=436)

Please register and participate. It's crucial to us all!

Tags:

[callout](https://outreach.mayfirst.org/taxonomy/term/2)


