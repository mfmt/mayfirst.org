---
date: 2024-12-24
title: "We present May First's new logo and visual design"
tags: [ "news","callout"]
---

As promised a few weeks ago, we want to present the new **May First logo!** We are
excited to have taken this small step that will soon be reflected in the
redesign of our website.

**Without further ado, here it is:**

![May First spelled out with the m appearing like two people holding hands with fist in air](logo.png)

We would like to briefly recount the steps we took to create the logo, which we
hope will help us **strengthen our cooperative in the coming years.**
 
You may remember (or maybe you didn't know?) that this project began in the
middle of last year when our **Board of Directors approved an allocation of
resources for the redesign of our logo and website.** In September 2023 we
formed a committee to follow up on this work. After organizing an open call
with two rounds ([first
round](https://outreach.mayfirst.org/civicrm/mailing/view?id=1274&reset=1),
[second
round](https://outreach.mayfirst.org/civicrm/mailing/view?id=1283&reset=1)) we
selected [Caro Cuberlo Design Studio](https://www.behance.net/carocurbelo)
based in Uruguay for the logo design and [DIA Design
Guild](https://www.diadesign.io/) for the website.
 
In May 2024, after gathering initial information, Caro sent us a diagnosis
analyzing our (then) current logo along with three proposals to start
discussing redesign possibilities. While those first proposals did not feel
entirely appropriate, we suggested changes to two of them with the intention of
making them more relevant.
 

> The process was carried out with an effort take care of both our anxieties
> and those of others. From the studio perspective, we think it is necessary to
> leave egos aside and work towards the common purpose.

 
In the following weeks they shared with us details of their experimentation
process, **taking into account the suggested changes** and adding two new
proposals. In June 2024, the redesign committee felt ready to **launch a survey
to the membership** seeking their [opinion on the two selected proposals](https://outreach.mayfirst.org/civicrm/mailing/view?id=1367&reset=1). At that
time we proposed a closed vote, i.e., to choose one of the two options, and
provide an opinion.
 
The work of collecting the opinions of the membership was very satisfying. It
was valuable to read both the enthusiasm and the resonances of these designs
according to the contexts and backgrounds of each person. The feedback was
enormous and **the "binary" vote yielded a close result: 51% for option 1, 49%
for option 2.** What do we do?
 
After several conversations within the committee we asked Caro and his team to
make **additional adjustments to both proposals** based on feedback from the
membership to **bring the results back to the membership.** In the words of Caro:
 

> We were left with a lot of learning about the ways May First operates. It's
> very different from what is usually done, where everything moves quickly.
> Here, collecting feedback is prioritized over speed. An organization that
> prioritizes the opinion of its members seems to be something to keep in
> mind.

 
In this **second voting moment** the intention was to broaden the view. We invited
members to vote for one or both proposals, in case both were acceptable to our
membership. We knew that eventually there would be only one logo that would be
chosen. Providing the option for our membership to choose both might give us a
vote of confidence that, if both proposals proved strong enough, **we could move
forward to a final decision knowing that the membership would be happy with
either one.**
 
We received more responses (and suggestions!) than expected. After a few more
adjustments we presented the entire process at a meeting of the Board
Coordinating Committee. The **end result** is what you have glimpsed over the past
few weeks and what we now share at length along with some applications we would
like you to use whenever you wish to speak or represent our cooperative. You
can [view and download full design and applications here](https://share.mayfirst.org/s/HZENDQ9BDfXxaiQ).

![image showing just the word first with purplish hue, suggesting documents regarding the design](design1.png)
![image showing just the word first with yellowish hue, suggesting documents regarding the design](design2.png)
![image showing just the word first, with blueish hue, suggesting documents regarding the design](design3.png)

We have **joy and gratitude** for each person who dedicated time and emotion during
this process. Whether it was completing the surveys, writing a detailed mailing
or contributing to the follow-up meetings.
 
Also we have gratitude for **having shared this journey together with Caro
Curbelo Design Studio:** *"We are glad that these organizations exist, it is our
way of adding to this world that is falling apart (...) that design makes a
letter or something have more impact. Or a dossier or a website works better.
And it is a way to contribute to these struggles, we like to be part of these
changes from where we know and with what we know."*
 
Thus, by bringing together our collective efforts from each individual's
contributions, we hope that this stage, which is metaphorically inaugurated
with our new logo, **will strengthen us in building digital technologies that
serve as allies to the groups and social movements we admire.**

