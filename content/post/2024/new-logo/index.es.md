---
date: 2024-12-24
title: "Presentamos nuevo logo e imagen visual de May First"
tags: [ "news","callout"]
---

Como prometimos hace algunas semanas queremos pasar por aquí para presentar **¡el
nuevo logo!** Imaginen la emoción que aún tenemos con este pequeño gran paso dado
y que pronto veremos reflejado en el rediseño de nuestra página web.
 
**Sin más preámbulos, he aquí el nuevo logo:**

![May First spelled out with the m appearing like two people holding hands with fist in air](logo.png)

Queremos contarles brevemente los pasos seguidos para alcanzar este resultado
que **esperamos nos acompañe** en los siguientes años de fortalecimiento de nuestra
cooperativa.
 
Quizás recuerden (¿o quizás no lo supieron?) que este camino inició a mediados
del año pasado cuando **nuestra Junta Directiva aprobó una asignación de
recursos destinada al rediseño de nuestro logo y página web.** En septiembre de
2023 conformamos un comité dedicado al seguimento de estos trabajos. Tras
organizar una convocatoria abierta que contó con dos rondas de llamados
([primera
ronda](https://outreach.mayfirst.org/civicrm/mailing/view?id=1272&reset=1),
[segunda
ronda](https://outreach.mayfirst.org/civicrm/mailing/view?id=1281&reset=1))
elegimos a [Caro Cuberlo Design Studio](https://www.behance.net/carocurbelo)
con sede en Uruguay para el diseño del logo y a [DIA Design
Guild](https://www.diadesign.io/) para la página web.
 
Tras recopilar información inicial, en mayo 2024 Caro nos envió un diagnóstico
analizando nuestra imagen (en ese entonces) actual y tres propuestas para
empezar a dialogar posibilidades de rediseño. Si bien esas primeras propuestas
no se sintieron del todo adecuadas, sugerimos cambios en dos de ellas con la
intención de hacerlas más pertinentes.
 

> El proceso se realizó intentando cuidar las ansiedades propias y ajenas.
> Desde el estudio, pensamos que es necesario dejar los egos de lado y trabajar
> en función del propósito", nos comparte Caro.

 
En las siguientes semanas nos compartieron detalles de su proceso de
experimentación tomando en cuenta los cambios sugeridos y sumaron dos nuevas
propuestas. Con todo este acervo en junio de 2024 desde el comité de rediseño
nos sentíamos listes para **lanzar una encuesta a la membresía buscando conocer
su opinión** sobre las [dos propuestas seleccionadas](https://outreach.mayfirst.org/civicrm/mailing/view?id=1366&reset=1). En ese momento propusimos
una votación cerrada, es decir, elegir y opinar entre una u otras de las
opciones.
 
El trabajo de recoger las opiniones de la membresía fue muy satisfactorio. Fue
valioso leer tanto el entusiasmo como las resonancias de estos diseños según
los contextos y recorridos de cada quien. La retroalimentación fue enorme y la
votación "binaria" arrojó un resultado cerrado: 51% por la opción 1, 49% por la
opción 2. Algo que no esperábamos. ¿Cómo seguir desde allí?
 
Tras varias conversaciones al interior del comité pedimos a Caro y su equipo
realizar **ajustes adicionales a ambas propuestas** basándonos en los comentarios
de la membresía para **presentar los nuevos resultados.** En palabras de Caro:
 
> Nos quedamos con mucho aprendizaje sobre las formas de consulta que lleva May
> First, muy diferente a lo que suele hacerse, donde todo es rápido. Aquí se
> prioriza la consulta antes que la velocidad. Una organización que prioriza la
> opinión de sus miembros me parece que algo para tener en cuenta.

 
En este **segundo momento de votación** la intención fue ampliar la mirada.
Invitamos a votar por una o ambas propuestas, en caso de no tener resistencia
con ninguna de ellas. Sabíamos que finalmente habría un solo logo que sería el
elegido. El hecho de tener la **posibilidad de elegir ambos nos daba un voto de
confianza** para que, si ambas propuestas resultaban ser suficientemente fuertes,
podríamos seguir avanzando hacia una decisión final sabiendo que la membresía
estaría conforme con ella.
 
Recibimos más respuestas (¡y sugerencias!) de las esperadas. Tras algunos
ajustes más presentamos el proceso completo en una reunión del Comité de
Coordinación de la Junta Directiva. El **resultado final** es el que vislumbraron
en estas semanas y que ahora compartimos en extenso junto a algunas
aplicaciones de uso que nos gustaría pudieran usar cada vez que deseen hablar o
representar a nuestra cooperativa. Pueden [revisar y descargar el paquete
completo desde aquí](https://share.mayfirst.org/s/HZENDQ9BDfXxaiQ).

![image showing just the word first with purplish hue, suggesting documents regarding the design](design1.png)
![image showing just the word first with yellowish hue, suggesting documents regarding the design](design2.png)
![image showing just the word first, with blueish hue, suggesting documents regarding the design](design3.png)

Tenemos **alegría y agradecimiento** por cada persona que
dedicó tiempo y emoción durante este proceso. Ya sea completando las encuestas,
escribiendo un correo detallado o aportando en las reuniones de seguimiento.
 
También por **haber compartido este recorrido junto a Caro Curbelo Design Studio:**
*"Nos alegra que existan estas organizaciones, es nuestra forma de sumar a est
mundo que se desarma (...) ese diseño hace que una carta o algo tenga más
impacto. O un dossier o una web funcione mejor. Y es una forma de aportar a
esas luchas, nos gusta ser parte de estos cambios desde donde sabemos y con lo
que sabemos."*
 
Así, sumando voluntades, desde los espacios de cada quien, esperamos que esta
etapa que se inaugura metafóricamente con nuestro nuevo logo, **nos fortalezca en
la construcción de tecnologías digitales aliadas de los grupos y movimientos
sociales a los que admiramos.**
