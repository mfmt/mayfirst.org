---
date: 2024-09-11
title: "Movement Technology post-US Election: report back"
tags: [ "news","callout"]
---

The US is approaching a pivotal presidential election this November and, like
many countries around the world, there is a growing fascist movement led by one
of the candidates. No electoral outcome will stop the fascist movement
completely, but the direction and strategy of those of us fighting fascism will
need to adapt depending on who wins.
 
We know from experience in 2016 that in times of crisis, organizers' interest
in and anxiety around digital security will spike. We will get urgent questions
and requests for support and advice. **How should we prepare?**
 
May First Movement Technology and the Progressive Technology Project gathered
an experienced and knowledgeable group of people together on September 26, 2024
to discuss movement technology needs -- including digital security -- after the
election.  

## Breakout Group Notes and Report backs

**What are the general messages and general approaches
technologists should take post-election to support movements seeking
better digital security?**

*Below are notes compiled from all break out groups.*

### Questions

 * How do we apporach security from an organized and focused standpoint,
   and not run scared from everyone?
 * How do we stay grounded on what are the most realistic attacks for
   our organizations?
 * Do we start with low hanging fruit or the most pressing security
   vulnerabilities?

### General Suggestions and things to keep in mind

 * Start these conversations now so folks who are not familiar with digital
   security can learn ahead of any crisis 
 * While things *are* constantly changing, new threats are typically built on
   the basics - understanding existing problems help us understand how they
   evolve. For example: phishing attacks (when someone sends you an email
   asking for information as if they are someone close to you).
 * When training, be sure to consider generational gaps in knowledge and
   ability to detect scams
 * Many groups find addressing these concerns overwhelming, keep
   in mind people's cognitive load, especially during stressful times.
 * You don't have to implement everything today, do it slowly, as a recurring
   task, like you are building your security muscles. 
 * Keep digital security in the frame of organizing 
 * Popularize security messaging in a way that is motivating not paralyizing
 * Balance convenience: more secure is generally less convenient
 * Consider everyone: security is a group project, not an individual project.
 * Messaging: let users know that they are not alone, especially in remote
   spaces
 * How do you stay current? Ensure one or more people are defined to manage the
   security of your organization. 
 * Try to reduce your digital movement footprint, while still being accessible
 * Both onboard *and* offboard users and volunteers (remove digital access when
   people leave)
 * With AI people's voices and appearances can be faked, and also you can more
   easily be recognized than before, even when masked/hooded

### Specific recommendations

 * Do not use biometrics on phones (law enforcement can force you to
   enter biomentrics, but not passwords), 
 * Use secure password managers that create a separate and secure
   password (that you don't have to memorize) for each site that you
   visit (see [Bitwarden](https://bitwarden.com/),
   [KeePassXC](https://keepassxc.org/), or
   [1Password](https://1password.com/)).
 * [Multi-factor
   authentication](https://en.wikipedia.org/wiki/Multi-factor_authentication)
   (password plus text message, etc) because passwords can be guessed. 
 * Use [full disk
   ecnryption](https://ssd.eff.org/module/how-encrypt-your-windows-device)
   option on phones and computers
 * Use [Jitsi Meet](https://jitsi.org/jitsi-meet/) or see May
   First [Jitsi Meet server](https://meet.mayfirst.org/) for video
   conferences
 * Use [Signal](https://signal.org/)
 * Use [Nextcloud](https://nextcloud.com/), or see May First [Nextcloud
   Server](https://share.mayfirst.org) instead of Google Drive
 * Use [Cryptpad](https://cryptpad.fr/), is end to end encrypted (but
   easier to keep track of files in Nextcloud)

### What we need

 * We should create slides on the why, concrete/clear/accessible info;
   why specific tools?
 * Need UI/interfaces on security tools that are more joyful, build in
   joy for this long-term project work

For more resources, see the excellent Electronic Frontier Foundation's
[Surveillance Self-Defense site](https://ssd.eff.org/).
