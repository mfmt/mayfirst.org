---
date: 2024-03-18
title: "2024 Election results are in"
tags: [ "news","callout"]
---

Our internal elections have concluded and votes have been tallied for electing
the new May First board members, proposed bylaws changes and priorities for the
next year.
 
We are very grateful to have received so many amazing candidates this year and
we are happy to announce that the following May First members have been elected
to our board of directors!

 * Charlotta Beavers - 94 votes
 * Yahaira Zapanta-Rosales - 88 votes
 * Ken Montenegro - 70 votes
 * Francisco Cerezo Contreras - 69 votes
 * Elijah Baucom - 67 votes
 * Carlos Eugenio Rodriguez - 63 votes
 * Kevin Ye - 63 votes

Technically, we had vacancies for only 6 seats on our board this year however
after analyzing the results we found there was a tie for 6th place. Since we
hadn't run into this issue previously we do not have a procedure in place for
resolving the tie. Rather than extend elections into a runoff period the board
coordination team has opted to accept this fortuitous result and temporarily
expand the board from 25 to 26 members for the next year. We will work on
proposing new language for our bylaws that will detail how to decide tied
elections in the future but for now we are very excited to begin working with
the 7 board members listed above!
 
We extend our sincere appreciation to all of the candidates who applied and we
hope to continue collaborating with you all through our program teams and other
activities.
 
Members also voted overwhelmingly in favor of approving the proposed bylaws
changes:
 
 * Make minor, editorial changes to the by-laws
 * Clarify procedures for expelling members in the by-laws
 
And finally, members voted to rank our priorities for the next year in the
following order:
 
 * Continue to promote the pertinence of autonomous technologies within movement spaces - 301 votes
 * Continue writing documentation at https://help.mayfirst.org - 280 votes
 * Strengthen relationships, facilitate collaboration and build community among May First Members - 278 votes
 * Explore creative options for enabling new digital services at May First - 256 votes 
 * Organize member workshops focusing on digital community care - 254 votes

We also thank each of our members who, by participating in this process, have
helped to strengthen the participatory culture of our Cooperative.


