---
date: 2024-08-14
title: "¿Hacia dónde estamos mirando? Un camino posible para la repolitización de la tecnología"
slug: hacia-donde-estamos-mirando
tags: [ "news","callout"]
---

Queremos compartir con ustedes el artículo de reciente publicación *[¿Hacia
dónde estamos mirando? Un camino posible para la repolitización de la
tecnología](https://centroculturadigital.mx/revista/hacia-donde-estamos-mirando)*
donde se recogen experiencias organizativas que recorren el camino hacia la
creación y uso de las tecnologías digitales desde perspectivas no hegemónicas.
Incluida la experiencia que construimos en el cotidiano desde May First:

> A estas alturas de la vida digital, ya se habrán encontrado más de una vez
> con alguna frase referente a que las tecnologías no son neutrales, sino que
> responden a posiciones sociales, políticas y económicas que no son
> explicitadas. Pero... no lo parecen, ¿cierto? O al menos, saberlo nos es
> insuficiente para tomar acción y encaminarnos hacia nuestras visiones de
> mundo cuando éstas no se asemejan a las visiones de las tecnologías que
> usamos. Reconocemos que un puñado de corporaciones codifican nuestras
> ilusiones, controlan lo que vemos e imponen las plataformas en las que
> trabajamos, pero no vemos opciones más allá de eso. ¿Hacia dónde estamos
> mirando? ¿Qué tanta disposición tenemos para reaprender y revincularnos con
> las tecnologías digitales que usamos en el cotidiano? ¿Para qué lo haríamos?

Les invitamos a
[leerlo](https://centroculturadigital.mx/revista/hacia-donde-estamos-mirando) y
seguir sumando reflexiones y acciones para fortalecer los procesos de
tecnología y justicia social con perspectivas amplias.


