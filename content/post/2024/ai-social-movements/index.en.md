---
date: 2024-05-12
title: "Webinar: The Impact of Artificial Intelligence on Social Movements"
tags: [ "news","callout"]
slug: "ai-social-movements"
---

Free - please [register here](https://outreach.mayfirst.org/civicrm/event/register?id=91&reset=1).

**Thursday, May 30th, at 1:00 pm Mexico City/2:00 pm Chicago/3:00 pm New York**

<img src="paola.png" style="float:left;max-width:300px" alt="Paola picture">

All technology, including the digital kind, is the product of a political
ideology that is usually hidden.

During 2023, **artificial intelligence** was everywhere. We heard about it in a
variety of spaces: artistic and creative, social research, medical, labor, and
government. We heard about its potential, scope, biases, problems, aspirations,
as well the "inevitability" of its presence.

*How and where do social movements position themselves in the face of these
issues? How close are the problems but also the impacts and opportunities?* In
this conversation we invite **Paola Ricaurte** who recovers the notion of the
coloniality of power to propose a vision of AI development that articulates the
macro and micro-political aspects of technological development as part of a
**decolonial and feminist project.**

Paola Ricaurte is an Associate Professor in the Department of Media and Digital
Culture at Tecnológico de Monterrey, Faculty Associate at the Berkman Klein
Center for Internet & Society at Harvard University, and co-founder of the
Tierra Común network. She leads the Latin American and Caribbean hub of the
Feminist AI Research Network.

Free - please [register here](https://outreach.mayfirst.org/civicrm/event/register?id=91&reset=1).
