---
date: 2024-05-12
title: "Webinar: El impacto de la inteligencia artificial en los movimientos sociales"
tags: [ "news","callout"]
slug: "ia-movimientos-sociales"
---

Gratuito - [inscríbase aquí](https://outreach.mayfirst.org/civicrm/event/register?id=91&reset=1).

**Jueves 30 de mayo, a las 13.00 horas Ciudad de México/14.00 horas Chicago/15.00 horas Nueva York**

<img src="paola.png" style="float:left;max-width:300px" alt="Paola picture">

Cada tecnología, incluidas las digitales, son producto de una ideología
política. La mayoría de las veces invisibilizada.

Durante 2023 las **inteligencias artificiales** estaban por todos lados. Escuchamos
hablar de ellas en diversidad de espacios: artísticos y de creación, de
investigación social, médicos, laborales, estatales. Escuchamos sobre sus
potencialidades, alcances, sesgos, problemáticas, aspiraciones, así como de la
"inevitabilidad" de su presencia.

*¿Cómo y dónde se posicionan los movimientos sociales frente a estos temas? ¿Qué
tan cercanos nos resultan sus problemáticas pero también sus impactos y
oportunidades?* En este conversatorio invitamos a **Paola Ricaurte** quien recupera
la noción de la colonialidad del poder para proponer una visión del desarrollo
de IA que articule los aspectos macro y micropolíticos del desarrollo
tecnológico como parte de un **proyecto descolonial y feminista.**

Paola Ricaurte es investigadora del Departamento de Medios y Cultura Digital
del Tecnológico de Monterrey. Además, es profesora asociada del Berkman Klein
Center for Internet & Society de la Universidad de Harvard y cofundadora de
Tierra Común, una red de académicos, profesionales y activistas interesados en
la descolonialidad y los datos. Coordina el nodo de América Latina y El Caribe
de la Red Feminista de Investigación en Inteligencia Artificial.

Gratuito - [inscríbase aquí](https://outreach.mayfirst.org/civicrm/event/register?id=91&reset=1).
