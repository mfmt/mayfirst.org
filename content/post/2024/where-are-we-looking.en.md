---
date: 2024-08-14
title: "Where are we looking? A possible Path for Repoliticizing Technology"
tags: [ "news","callout"]
---

We would like to share with you a recent article: *[Where are we looking? A
possible Path for Repoliticizing
Technology](https://mayfirst.coop/files/where-are-we-looking.pdf)*. The article
gathers organizational experiences on the path toward creating and using
digital technologies from non-hegemonic perspectives, including the experience
we build in our daily life at May First:

> At this point in digital life, you will have come across more than once a
> sentence referring to the fact that technologies are not neutral, but respond
> to social, political and economic factors that are not made explicit. But...
> they don't seem to be, do they? Or at least, knowing this is not enough for
> us to take action and move toward a vision of the world that does not
> resemble the vision of the technologies we use. We recognize that a handful
> of corporations codify our illusions, control what we see and impose the
> platforms on which we work, but we don't see options beyond that. Where are
> we looking? How willing are we to relearn and reconnect with the digital
> technologies we use on a daily basis? What would we do it for?

We invite you to [read
it](https://mayfirst.coop/files/where-are-we-looking.pdf) and continue adding
reflections and actions to strengthen the processes of technology and social
justice from broad perspectives.
