---
date: 2024-03-18
title: "2024 Elecciones han Concluído"
tags: [ "news","callout"]
---

Nuestras elecciones han concluido y tenemos los resultados respecto a las
nuevas personas que integrarán la Junta de May First, los cambios propuestos en
los estatutos y las prioridades para el próximo año.
 
¡Estamos muy agradecides de haber recibido tantos candidates impresionantes
desde nuestra membresía este año y estamos felices de anunciar que las
siguientes personas han sido elegidas para nuestra Junta Directiva!
 
 * Charlotta Beavers - 94 votos
 * Yahaira Zapanta-Rosales - 88 votos
 * Ken Montenegro - 70 votos
 * Francisco Cerezo Contreras - 69 votos
 * Elijah Baucom - 67 votos
 * Carlos Eugenio Rodriguez - 63 votos
 * Kevin Ye - 63 votos

 
En principio, este año sólo teníamos 6 puestos disponibles en nuestra Junta,
pero tras analizar los resultados encontramos un empate en el sexto lugar. Dado
que no habíamos enfrentado esta situación anteriormente, no disponemos de un
procedimiento para resolver el empate. En lugar de prolongar las elecciones a
una segunda vuelta, el equipo de coordinación de la Junta ha optado por aceptar
este resultado imprevisto y ampliar temporalmente la Junta de 25 a 26
integrantes para el siguiente año. En el futuro, trabajaremos en una propuesta
de nueva redacción de nuestros estatutos en la que se detallará cómo resolver
los empates en las elecciones. Por el momento aceptamos con gran emoción
integrar a la Junta las 7 personas listadas arriba.
 
Extendemos nuestro sincero agradecimiento a todas las candidaturas presentadas
y esperamos seguir colaborando con todes ustedes a través de nuestros equipos
de programa y otras actividades.
 
La membresía también votó mayoritariamente a favor de aprobar los cambios
propuestos en los estatutos:
    
 * Introducir cambios menores en la redacción de los estatutos.
 * Establecer claramente en los estatutos el procedimiento para la expulsión de membresías.

Por último, la membresía votó para clasificar nuestras prioridades para el
próximo año en el siguiente orden:
 
 * Continuar fomentando la pertinencia de las tecnologías autónomas dentro de los movimientos sociales - 301 votos
 * Continuar redactando la documentación en https://ayuda.mayfirst.org - 280 votos
 * Fortalecer las relaciones, facilitar la colaboración y hacer crecer la comunidad entre la membresía de May First - 278 votos
 * Explorar opciones creativas para habilitar nuevos servicios digitales en May First - 256 votos
 * Organizar talleres para la membresía centrados en los cuidados digitales comunitarios - 254 votos

Agradecemos también a cada membresía que, participando de este proceso, apoya
el fortalecimiento de los procesos participativos de ésta, su Cooperativa.
