---
date: 2024-11-26
title: "Cutting the Cord Webinar: Friday, December 6th"
tags: [ "news","callout"]
---

*We asked activists to help us make leaving Gmail and other Google services
easier and the reaction was unbelievable.* We received more than twice the
number of expected responses from our survey, and many people reached out to
express their excitement and support for this project.

**Clearly, as a movement, we want to leave Google behind. But how?**

We would like to share the survey results with you! Please [hear the recording](/en/audio/cutting-the-cord/) made on 
Friday, December 6th at 12:00 pm New York time (ET)/5:00 pm UTC - a one-hour
webinar where we presented our survey results and discussed our next steps
together. You'll hear about what we learned are the main obstacles to leaving
Google and some initial ideas about overcoming those obstacles.

![sometimes you have to cut the cord](cutting-the-cord.png)

The survey was conducted by [May First Movement
Technology](https://mayfirst.coop/) and the [Progressive Technology
Project](https://progressivetech.org/), in collaboration with the [Infrared
Network](https://in.fra.red/), and with funding from the [Association for
Progressive Communications](https://apc.org).

Interpretation will be provided between English and Spanish.

[Listen!](/en/audio/cutting-the-cord/)
