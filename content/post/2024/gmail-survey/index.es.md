---
date: 2024-11-26
title: "Webinar Cortar el hilo: Viernes, 6 de diciembre"
tags: [ "news","callout"]
---

*Preguntamos a diversos activistas si podían ayudarnos a hacer más fácil la
salida de Gmail y otros servicios de Google, y la reacción fue increíble.* Hemos
recibido más del doble de respuestas de las esperadas en nuestra encuesta, y
muchas personas se han puesto en contacto directo para expresar su entusiasmo y
apoyo a este proyecto.


**Está claro que, como movimiento, queremos dejar atrás Google. ¿Pero cómo?**

Nos gustaría compartir los resultados de la encuesta presentados durante el
webinar. Puedes [revisar aquí](/es/audio/cutting-the-cord) la conversación
mantenida durante el viernes 6 de diciembre a las 12:00 pm hora de Nueva York
(ET)/5:00 pm UTC. Durante una hora de seminario web presentamos hallazgos y
debatimos juntes nuestros próximos pasos. Presentamos los principales
obstáculos para dejar Google y algunas ideas preliminares para sortearlos.

![a veces solo hay que cortal el hilo](cortar-el-hilo.png)

Este investigación es realizada por [May First](https://mayfirst.coop/) y
[Progressive Technology Project](https://progressivetech.org/) en colaboración
con [Infrared](https://in.fra.red/). Cuenta con apoyo financiero de la
[Association for Progressive Communications (APC)](https://apc.org/).

Contaremos con interpretación inglés / español, español /inglés.

[Lea aqui](/es/audio/cutting-the-cord)
