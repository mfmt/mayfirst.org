---
date: 2024-09-11
title: "Tecnologías orientadas a los movimientos sociales tras las elecciones en EE.UU: Informe"
tags: [ "news","callout"]
---


EE.UU. se acerca a unas elecciones presidenciales cruciales en el mes  de
noviembre. Como muchos países del mundo, EEUU se enfrenta a un  creciente
movimiento fascista liderado por una de las dos candidaturas.  Ningún resultado
electoral detendrá el movimiento fascista por completo,  pero la dirección y la
estrategia de quienes luchamos contra el  fascismo tendrán que adaptarse
dependiendo de quién gane.

Sabemos por la experiencia vivida en 2016 que en tiempos de crisis, el  interés
y la ansiedad por la seguridad digital de parte de personas  activistas aumenta
significativamente. Recibiremos consultas urgentes y  peticiones de apoyo y
asesoramiento. ¿Cómo debemos prepararnos?

El 26 de septiembre de 2024, May First Movement Technology y Progressive
Technology Project, reunieron  a un grupo de personas conocedoras y con
experiencia organizativa para debatir las necesidades tecnológicas de los
movimientos sociales tras las elecciones en EEUU, incluidos los temas relativos
a la seguridad digital.

### Notas de los Grupos de Trabajo y Reportes

**¿Cuáles son los mensajes y enfoques principales que les tecnólogues
deberían adoptar tras las elecciones para apoyar a los movimientos que buscan
una mayor seguridad digital?**

*A continuación compartimos las notas compiladas de todos los grupos de trabajo.*

#### Preguntas

- ¿Cómo abordamos la seguridad de forma coordinada y a la vez focalizada, en lugar de huir despavorides de todo el mundo?
- ¿Cómo mantenemos los pies en la tierra en cuanto a cuáles son los ataques más realistas para nuestras organizaciones?
- ¿Empezamos por lo más fácil o por las vulnerabilidades de seguridad más acuciantes?

#### Sugerencias generales y aspectos a considerar

- Inicie estas conversaciones ahora para que la gente que no está familiarizada con la seguridad digital pueda aprender antes de cualquier crisis.
- Aunque las cosas cambian constantemente, las nuevas amenazas suelen estar relacionadas con lo básico: entender los problemas existentes nos ayuda a comprender cómo evolucionan. Por ejemplo: los ataques de phishing (cuando alguien te envía un correo electrónico pidiéndote información como si fuera alguien cercano a ti).
- A la hora de realizar actividades de formación, asegúrese de tener en cuenta las diferencias generacionales en cuanto a conocimientos y capacidad para detectar estafas.
- A muchos grupos les resulta abrumador abordar estas cuestiones; tenga en cuenta la carga cognitiva de las personas, especialmente en momentos de estrés.
- No es necesario aplicar todo inmediatamente, hágalo poco a poco, como una tarea recurrente, como si estuviera desarrollando músculos de seguridad.
- Mantener la seguridad digital en el contexto de la organización (jes dice: not sure what this suggestion refers to... can you explain more?)
- Divulgue los mensajes de seguridad de forma que motiven y que no paralicen.
- Equilibra la comodidad: más seguridad suele ser menos comodidad.
- Tenga en cuenta a todas las personas: la seguridad es un proyecto grupal, no individual.
- Mensajería: que las personas usuarias sepan que no están en soledad, sobre todo en espacios remotos.
- ¿Cómo mantenerse al día? Asegúrese de que hay una o varias personas encargadas de gestionar la seguridad en su organización.
- Intente reducir su huella digital (refiere al conjunto único de actividades, acciones y comunicaciones digitales rastreables en internet), sin perder disponibilidad.
- Retire el acceso a la información digital cuando las personas se marchen (sean parte del voluntariado o del grupo de trabajo).
- Con la IA se pueden falsear las voces y las apariencias de las personas, y también se te puede reconocer más fácilmente que antes, incluso cuando vas enmascarado/encapuchado.

#### Recomendaciones específicas

- No utilices datos biométricos en los teléfonos (las fuerzas de seguridad pueden obligarte a introducir biométricos, pero no contraseñas).
- Utiliza gestores de contraseñas seguros que creen una contraseña distinta y segura (que no tengas que memorizar) para cada sitio que visites (consulta [Bitwarden](https://bitwarden.com/),
   [KeePassXC](https://keepassxc.org/), o
   [1Password](https://1password.com/)).
- Autenticación [multifactor](https://en.wikipedia.org/wiki/Multi-factor_authentication)
multifactor (contraseña más mensaje de texto u otros) porque las contraseñas se pueden adivinar.
- Utiliza la opción de [cifrado de disco completo](https://ssd.eff.org/module/how-encrypt-your-windows-device) en teléfonos y computadoras.
- Utiliza [Jitsi Meet](https://jitsi.org/jitsi-meet/) o consulta el servidor [May First Jitsi Meet para videoconferencias](https://meet.mayfirst.org/).
- Utiliza [Signal](https://signal.org/).
- Utiliza [Nextcloud](https://nextcloud.com/), o consulta el [servidor Nextcloud de May First](https://share.mayfirst.org) en lugar de Google Drive.
- Utilice [Cryptpad](https://cryptpad.fr/), está cifrado de extremo a extremo (pero es más fácil hacer un seguimiento de los archivos en Nextcloud). (jes dice: maybe we need to explain what a pad is!)

#### Lo que necesitamos

- Deberíamos crear diapositivas sobre el por qué, información concreta/clara/accesible; ¿por qué usar tal o cuál herramienta específica?
- Necesitamos interfaces de usuario en las herramientas de seguridad que sean más alegres, crear alegría para este proyecto de trabajo a largo plazo.

Para más recursos, visita [Surveillance Self-Defense](https://ssd.eff.org/), un gran sitios desarrollado y mantenido por la Electronic Frontier Foundation.
