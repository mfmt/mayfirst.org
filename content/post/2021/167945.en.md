---
date: 2021-06-23
title: "Honoring Alfredo's Legacy: Watch the video"
slug: content-honoring-alfredos-legacy-watch-video
tags: [ "news","callout"]
---
Thanks to everyone who came to our event honoring Alfredo's Legacy at May
First Movement Technology.

And, thanks to Vienna, KJ Rhee and the Center for NuLeadership we have an
amazing send off video with all of your contributions:

<video width="800" controls><source type="video/mp4" src="https://share.mayfirst.org/s/f9GT39nHEj7AY3e/download/honoring-alfredo-edited-video-messages.m4v"></video>

Alfredo helped found May First Movement Technology 16 years ago and has been
central in the organization's development since then.

His vision of technology's fundamental role in movement work, starting with
his essay in the Organic Internet in 2007 and continuing through his analysis
guiding the Technology and Revolution campaign over a decade later, has
defined the organization and continues to have an enormous influence on the
movement.

How do we know Alfredo? Every member knows him from the remarkably personal
and thoughtful welcome message they received upon joining the organization.
Most movement activists know him from his leadership during the US Social
Forum, the Media Justice Network, or the Technology and Revolution campaign.
Every member of our Leadership Committee and Board knows him as a tireless
organizer with unwavering commitment to fight racism and build a multi-racial
leadership. Our Mexican comrades from Laneta know him as the central advocate
for joining forces to become a bi-national organization. Still others know him
from the Left Forum, Allied Media Conference, or any number of other network
events, conferences, coalitions and campaigns in which he has repped May First
with ucanny insight and vision.

Tags:

[callout](https://outreach.mayfirst.org/taxonomy/term/2)


