---
date: 2023-09-27
title: "¿Por qué la policía tiene tanto miedo de que sepamos quiénes son?"
tags: [ "news", "callout" ]
---

**Participe en un seminario web el jueves 5 de octubre: 16:00 h Los Ángeles / 17:00 h Ciudad de México / 18:00 h Chicago / 19:00 h Nueva York / 23:00 h UTC**

![Protesters walking by cop car](/img/stoplapd-protests.jpg)

**Image Credit: Stop LAPD Spying**

En la primavera de 2023, la coalición [Stop LAPD
Spying](https://stoplapdspying.org) lanzó un recurso educativo basado en
registros públicos, el sitio web [Watch the
Watchers](https://watchthewatchers.net). Este sitio web es un índice de
búsqueda de más de 9300 agentes de policía juramentados de Los Ángeles, que
muestra sus fotos departamentales, números de serie del LAPD y otra información
oficial sobre cada agente.

Al mismo tiempo, [MediaJustice](https://mediajustice.org) lanzó [Copaganda
Clapback](https://mediajustice.org/resource/copaganda-clapback/), un plan de
estudios y una serie de cursos de formación impartidos en ciudades de todo
Estados Unidos con el objetivo de comprender el control policial de los
ecosistemas mediáticos en todo el país.

Únase a Hamid Khan, de la Coalición Stop LAPD Spying, y a Rumsha Sajid, de
MediaJustice, ambas organizaciones miembras de May First, para comprender la
historia de lo que ocurre cuando invertimos la dinámica del poder de la
vigilancia y las lecciones aprendidas de las luchas contra la copaganda en todo
el país. La conversación ahondará en cuestiones relacionadas con el control
corporativo de las herramientas de Internet, las lucrativas asociaciones entre
las grandes corporaciones tecnológicas y la policía, el estado de los medios de
comunicación y cómo estamos contraatacando.

*Se facilitará interpretación entre inglés, español y subtítulos. Por favor,
regístrese para recibir un enlace para asistir.*

[Registrese ahora!](https://outreach.mayfirst.org/civicrm/event/register?id=86&reset=1)

![May First, Media Justice and Stop LAPD Spying logos](/img/mf-mj-stoplapd-logos.png)
