---
date: 2023-09-27
title: "Why are the police so afraid of us knowing who they are?"
tags: [ "news", "callout" ]
---

**Join us for a webinar on Thursday, October 5th: 4:00 pm Los Angeles / 5:00 pm Mexico City / 6:00 pm Chicago / 7:00 pm New York / 11:00 pm UTC**

![Protesters walking by cop car](/img/stoplapd-protests.jpg)

**Image Credit: Stop LAPD Spying**

In the Spring 2023, the [Stop LAPD Spying
Coalition](https://stoplapdspying.org) launched a public-records sourced
educational resource, the [Watch the Watchers](https://watchthewatchers.net)
website. This website is a searchable index of over 9,300 sworn Los Angeles
police officers, displaying their departmental photos, LAPD serial numbers, and
other official information about each officer.

At the same time, [MediaJustice](https://mediajustice.org) launched [Copaganda
Clapback](https://mediajustice.org/resource/copaganda-clapback/), a curriculum
and training series facilitated in cities across the United States aimed at
understanding police control of media ecosystems across the country.

May First Members Hamid Khan of the Stop LAPD Spying Coalition and Rumsha Sajid
of MediaJustice to understand the story behind what happens when we flip the
surveillance power dynamic and lessons learned from copaganda fights across the
country. The conversation will delve into questions related to the corporate
control of online tools, lucrative partnerships between Big Tech and police,
the state of media, and how we’re fighting back.

*Interpretation between english, spanish, and captions will be provided. Please
register to receive a link to attend.*

[Register Now!](https://outreach.mayfirst.org/civicrm/event/register?id=86&reset=1)

![May First, Media Justice and Stop LAPD Spying logos](/img/mf-mj-stoplapd-logos.png)
