---
date: 2023-06-20
title: "El rol de la tecnología en la red Rising Majority"
tags: [ "news", "callout" ]
---

*Una conversación con Alice Aguilar y Jerome Scott sobre el papel de la
tecnología en la estrategia de los movimientos.*

**Jueves, 29 Junio, 13:00 Mexico City/14:00 US Central/15:00 US Eastern/19:00 UTC**

[Registrate!](https://outreach.mayfirst.org/civicrm/event/register?id=83&reset=1)

![Black woman at edge of frame with title: The Rising Majority News Brief. A list of headlines includes: The US Struggles to combat Coronavirus, particisan battle over supreme court justice RBG's seat has begin, and others.](https://outreach.mayfirst.org/sites/default/files/civicrm/persist/contribute/images/rising-majority.jpg)

La red de [Rising Majority](https://therisingmajority.com), Mayoría Creciente,
se formó en 2017 como "una coalición que busca desarrollar una estrategia
colectiva y una práctica compartida que involucrará a fuerzas sindicales,
juveniles, abolicionistas, de derechos de los inmigrantes, de cambio climático,
feministas, antiguerra/antiimperialistas y de justicia económica con el fin de
amplificar nuestro poder colectivo y construir una alineación entre nuestros
movimientos."

En 2020 May First se unió a la red de Rising Majority para apoyar este esfuerzo
aportando nuestra experiencia en tecnología y construcción de movimientos. Por
favor, ¡ayúdanos a cumplir esta tarea! Acompaña a Alice Aguilar y Jerome Scott,
miembros de la Junta Directiva de May First, a discutir el papel de la
tecnología en la construcción de dicha estrategia. 
 
La sesión durará una hora e incluirá traducción simultánea entre inglés y
español.

[Registrate!](https://outreach.mayfirst.org/civicrm/event/register?id=83&reset=1)
