---
date: 2023-03-21
title: "Don’t [let your devices] talk to cops"
tags: [ "news","callout"]
---

**A joint statement from May First and the Progressive Technology Project**

As part of the mass mobilization to protest the creation of Cop City, a huge
police training facility in Atlanta, 23 more protesters have been charged with
domestic terrorism. This latest round brings the total number of Cop City
protesters facing this dangerous charge to 42.

Make no mistake: by charging political protesters with a crime that carries a
sentence of up to 35 years in prison, the police are resorting to desperate
measures to stop dissent. This tactic has been used in the past and will be
used in the future.

*What’s new, however, is the ever increasing role of technology in our
organizing work.*

We have all been trained never to talk to the cops - that’s a job for our
movement lawyers. But prosecutors can easily circumvent both us and our lawyers
by accessing our digital records. Most of these records are collected by
commercial technology services specifically designed to surveil our locations
and track our social networks. In the hands of the police, they will be taken
wildly out of context and used to harm us.

**What do we do now?**

The first step: keep organizing! While everyone should make individual
decisions about risks and personal safety, we cannot let the cops intimidate
us.

Second, keep using technology for organizing: We need to know the dangers of
corporate technology, but not let that derail our work. We can use a harm
reduction model for reducing our risks while balancing our need to communicate
with our comrades and the broader movement.

Third, avoid discussing tactics digitally

Fourth, use privacy respecting software and services whenever possible

**What is our plan?**

Our movement is largely dependent on corporate services for our communications,
databases and digital storage. These same corporations are designing the very
digital tools used by the cops to surveil us, providing the opposition access
to our data by request or by subpoena. But there are alternatives...

The Progressive Technology Project, May First and a broad, international
movement of organizations and activists are building alternatives. We are
providing movement-aligned technologies with our security and organizing needs
in mind. **If we want a viable long term strategy, we need to grow this part of
the movement—and quickly.**

This is where you come in: together we can build, use, and improve the
technologies we use to do movement work with an eye towards collective access,
safety, and security. Please join us on this journey; and, in the interim, keep
yourselves and your folks safe in the struggle for liberation and life.


