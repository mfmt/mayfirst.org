---
date: 2023-03-01
title: "Detengamos a Cop City"
tags: [ "news","callout"]
---

Chat GPT es una distracción. Cuando se trata de los peligros de la inteligencia
artificial y el aprendizaje automático, la verdadera historia estará en Atlanta
a partir de este fin de semana.

**May First anima a toda la membresía a apoyar la movilización masiva contra Cop
City que tendrá lugar entre el 4 y el 11 de marzo.**

## ¿Qué está ocurriendo?

La Fundación de la Policía de Atlanta está intentando construir unas enormes
instalaciones de entrenamiento policial en el bosque Weelaunee, "una cuenca
rodeada de residentes en su mayoría negros quienes en su mayoría se oponen al
proyecto" ([https://stopcop.city](https://stopcop.city)). La ciudad de Atlanta
ha ignorado las protestas locales en oposición a la instalación, ha desalojado
violentamente a los manifestantes (matando a tiros a un manifestante conocido
como Tortuguita con más de una docena de balas), y recientemente ha concedido
el permiso definitivo para que se construya la instalación.

La movilización masiva es para protestar por el asesinato y luchar contra el
permiso final.

## ¿Por qué está sucediendo esto?

La campaña para detener la instalación puede ser local, pero la campaña más
amplia para oponerse a la militarización y la vigilancia de la policía es una
cuestión internacional.

En concreto, se trata de una problemática que combina el lucro empresarial, la
tecnología y el racismo. La Fundación de la Policía de Atlanta, que financia
Cop City, forma parte de una tendencia estadounidense de policía financiada por
empresas identificada por Color of Change en un informe reciente.

Según el informe:

>    Las fundaciones policiales son organizaciones privadas que canalizan el
>    dinero de las empresas hacia el mantenimiento del orden, protegiendo los
>    intereses corporativos y permitiendo la violencia sancionada por el Estado
>    contra las comunidades negras y las comunidades de color. Puede que te
>    resulten conocidos los patrocinadores de la Fundación Policial de Atlanta:
>    Amazon, Bank of America, Chick-fil-A, Coca-Cola1, Delta Airlines, Home
>    Depot, Waffle House, Wells Fargo, Uber y UPS, por nombrar algunos. Estos
>    son los donantes que conocemos. En los últimos años, con el aumento de las
>    peticiones de rendición de cuentas, las fundaciones policiales han tomado
>    medidas adicionales para limpiar sus sitios web y ocultar la información
>    de los donantes.

¿Y qué financian las empresas a través de las fundaciones policiales?
Vigilancia. La Fundación de la Policía de Atlanta, por ejemplo, financia la
Operación Escudo, "una red urbana de 11,000 cámaras de vigilancia y lectores de
placas de matrícula que no ha hecho más que ampliar la vigilancia permanente de
los ciudadanos negros de Atlanta"
([https://stopcop.city/](https://stopcop.city/)).

Estas 11 mil cámaras están controladas por la misma tecnología corporativa de
inteligencia artificial cuyo desarrollo ha sido impulsado por el capitalismo de
vigilancia y entrenado a partir de los datos recogidos mediante el uso
generalizado de las redes sociales corporativas, las búsquedas, el correo
electrónico y otros servicios de Internet.

## ¿Qué podemos hacer?

Invitamos a toda la membresía a unirse a la movilización.

También animamos a la gente a unirse con Micky Metts, una integrante de la
junta directiva de May First, este viernes en una sesión de estrategia
organizada por Community Bridge: Viernes, 3 de marzo a las 6 pm ET a través de
[https://communitybridge.com/stopcopcity](https://communitybridge.com/stopcopcity)
(Código de acceso: solidaridad) o conectar con otro integrante de la junta
directiva de May First, Jerome Scott, a través de Community Movement Builders.

Por último y como siempre, les pedimos que apoyen a los proveedores autónomos
de tecnología en la construcción de una tecnología basada en el consentimiento,
centrada en la comunidad y guiada por las necesidades de comunicación y
organización de nuestros movimientos.
