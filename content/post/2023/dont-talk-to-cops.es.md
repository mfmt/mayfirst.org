---
date: 2023-03-21
title: "No [dejes a tus dispositivos] hablar con policías"
tags: [ "news","callout"]
---

**Declaración conjunta de May First y Progressive Technology Project**

Como parte de la movilización masiva para protestar por la creación de Cop
City, un enorme centro de entrenamiento policial en Atlanta, 23 manifestantes
más han sido acusados de terrorismo doméstico. Esta última ronda eleva a 42 el
número total de manifestantes de Cop City que enfrentan esta peligrosa
acusación.

No nos equivoquemos: al acusar a manifestantes políticos de un delito que
conlleva una pena de hasta 35 años de prisión, la policía está recurriendo a
medidas desesperadas para frenar la disidencia. Esta táctica se ha utilizado en
el pasado y se utilizará en el futuro.

*Lo nuevo, sin embargo, es el papel cada vez más importante de la tecnología en
nuestro trabajo de organización.*

A todes se nos ha enseñado a nunca hablar con la policía: esa es tarea de los
abogados del movimiento. Pero los fiscales pueden eludirnos fácilmente, tanto a
nosotres como a nuestros abogados, accediendo a nuestros registros digitales.
La mayoría de estos registros son recopilados por servicios tecnológicos
comerciales diseñados específicamente para vigilar nuestras ubicaciones y
rastrear nuestras redes sociales. En manos de la policía se sacarán de contexto
y se utilizarán para dañarnos.

**¿Y ahora qué hacemos?**

El primer paso: ¡seguir organizándonos! Aunque cada uno debe tomar decisiones
individuales sobre los riesgos y su seguridad personal, no podemos dejar que la
policía nos intimide. 

En segundo lugar, seguir utilizando la tecnología para organizarnos: Tenemos
que conocer los peligros de la tecnología corporativa, pero no dejar que eso
descarrile nuestro trabajo. Podemos utilizar un modelo de reducción de daños
para aminorar riesgos al tiempo que equilibramos la necesidad de comunicarnos
con nuestros compañeres y con el movimiento más amplio.

En tercer lugar, hay que evitar discutir las tácticas digitalmente.

Cuarto, utilizar software y servicios respetuosos de la privacidad siempre que
sea posible.

**¿Cuál es nuestro plan?**

Nuestro movimiento depende en gran medida de los servicios corporativos para
nuestras comunicaciones, bases de datos y almacenamiento digital. Estas mismas
corporaciones están diseñando las mismas herramientas digitales que utiliza la
policía para vigilarnos, facilitando a la oposición el acceso a nuestros datos
mediante solicitudes o comparecencias judiciales.... Pero hay alternativas...

May First y un amplio movimiento internacional de organizaciones y activistas
están construyendo tecnologías alternativas alineadas con el movimiento,
tomando en cuenta nuestras necesidades de seguridad y organización. **Si queremos
una estrategia viable a largo plazo, necesitamos que esta parte del movimiento
crezca- y rápido.**

Aquí entras tú: juntes podemos construir, utilizar y mejorar las tecnologías
que usamos para hacer el trabajo del movimiento con la vista puesta en el
acceso colectivo, la seguridad y la protección. Por favor, únete a nosotres en
este viaje y, mientras tanto, mantente a ti y a tu gente a salvo en la lucha
por la liberación y la vida.
