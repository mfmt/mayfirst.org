---
date: 2023-03-29
title: "Watch The Watchers: Turning the tables on surveillance"
tags: [ "news", "callout" ]
---

**A joint statement from May First and Stop LAPD Spying Coalition**

!["Stop LAPD Spying logo of helicopter with ears and May First logo with earth and dandelions"](/img/mayfirst-stoplapdspying-logos.png)

Last week, the Stop LAPD Spying Coalition launched a public-records sourced
educational resource, the [Watch the Watchers website](https://watchthewatchers.net/). This website is a searchable index of
over 9,300 sworn Los Angeles police officers, displaying their departmental
photos, LAPD serial numbers, and other official information about each officer.

The website is not only a practical tool for researching abusive police
officers but also a public education resource. What happens when we flip the
surveillance power dynamic?

We quickly learned the answer to that question. 

Stop LAPD Spying is a member of May First Movement Technology, a cooperative
that provides hosting services to its members, including the Watch the Watchers
site. Soon after the site's launch, May First began receiving pressure to take
the site down, supposedly because it was in violation of California law
prohibiting "misappropration" of personal information.

Yet, the misappropriation law - which prevents your name, image or personal
information from being used against your wishes in *products, merchandise, or
goods* - is intended to protect your likeness from being commercialized for
profit. It does *not* protect public officials from having already publicly
available data about them shared in a new online format.

Stop LAPD Spying's lawyer [responded to the take down
request](/files/watchthewatchers-take-down-response.pdf), clearly outlining how
100% of the information published on Watch the Watchers is data made public by
government agencies based on the California Public Records Act, and none of it
us being used to sell or promote any products. 

The story soon took another turn. Earlier this week, the Los Angeles Police
Protective League, a private association of police officers, filed a lawsuit
demanding that city officials take legal action against anyone who publishes
the records. The lawsuit argues that the publicly released data includes
"undercover" officers, which they've claimed means all "officers that surveil"
and officers who might take an undercover assignment in the future
([citation](https://www.latimes.com/california/story/2023-03-28/lapd-police-union-sues-police-chief-to-force-him-to-claw-back-photos-of-undercover-officers)).
The police association behind the lawsuit has vowed that they will "ask the
judge to temporarily take down the Watch the Watchers website" until their
dispute with the city is resolved ([citation](https://www.latimes.com/california/story/2023-03-28/lapd-police-union-sues-police-chief-to-force-him-to-claw-back-photos-of-undercover-officers)). 

Threats like this are textbook tactics used widely by corporations and
government agencies to intimidate hosting providers and pressure them to remove
perfectly lawful content. These bogus legal claims are used to bully people who
do not have the energy, legal resources, or political will to fight.

Unfortunately, the corporations controlling our online tools are growing larger
while also forming lucrative partnerships with police agencies as they develop
new surveillance technologies. This combination is a potential disaster for
movement activists who depend on the Internet to fight police abuse or any
issue critical of capitalism or the corporate classes.

May First Movement Technology and Stop LAPD Spying Coalition stand together
against both police surveillance and the corporate technology that fuels this
dangerous trend. We encourage all movement activists to fight the stalker state
while supporting autonomous movement technology providers. You can learn how to
join and support this movement at
[https://stoplapdspying.org/](https://stoplapdspying.org/) and
[https://mayfirst.coop/](https://mayfirst.coop/).
