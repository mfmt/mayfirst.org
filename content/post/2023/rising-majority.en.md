---
date: 2023-06-20
title: "Technology's Role in the Rising Majority"
tags: [ "news", "callout" ]
---

*A conversation with Alice Aguilar and Jerome Scott on the role of technology
in movement strategy.*

**Thursday, June 29th, 1:00 pm Mexico City/2:00 pm US Central/3:00 pm US Eastern/7:00 pm UTC**

[Register Now!](https://outreach.mayfirst.org/civicrm/event/register?id=83&reset=1)

![Black woman at edge of frame with title: The Rising Majority News Brief. A list of headlines includes: The US Struggles to combat Coronavirus, particisan battle over supreme court justice RBG's seat has begin, and others.](https://outreach.mayfirst.org/sites/default/files/civicrm/persist/contribute/images/rising-majority.jpg)

What if we could collectively develop a strategy that recognized the autonomy
and differences of our diverse movements, while unifying our resources and
energy toward a common goal? **What role would technology play in such a
strategy?**
 
The [Rising Majority](https://therisingmajority.com) formed in 2017 as "a
coalition that seeks to develop a collective strategy and shared practice that
will involve labor, youth, abolition, immigrant rights, climate change,
feminist, anti-war/anti-imperialist, and economic justice forces in order to
amplify our collective power and to build alignment across our movements." 
 
In 2020 May First joined The Rising Majority to support this effort by
contributing our experience with technology and movement building. Please help
us fullfil this task! Join May First Board members Alice Aguilar and Jerome
Scott in discussing the role of technology in building such a strategy. 
 
The session will last one hour and include simultaneous translation between
english and spanish.

[Register Now!](https://outreach.mayfirst.org/civicrm/event/register?id=83&reset=1)
