---
date: 2023-03-29
title: "Vigilar a los vigilantes: Invertiendo la dinámica de poder"
tags: [ "news", "callout" ]
---

**Declaración conjunta de May First y Stop LAPD Spying Coalition**.

!["Stop LAPD Spying logo of helicopter with ears and May First logo with earth and dandelions"](/img/mayfirst-stoplapdspying-logos.png)

La semana pasada, la Coalición Stop LAPD Spying lanzó un recurso educativo basado en registros públicos, el [sitio web Watch the Watchers](https://watchthewatchers.net/). Este sitio web es un directorio buscable de más de 9.300 agentes de policía jurados de Los Ángeles, que muestra sus fotos departamentales, números de serie del LAPD y otra información oficial sobre cada agente.

El sitio web no es sólo una herramienta práctica para investigar a los policías que cometen abusos, sino también un recurso de educación pública. ¿Qué sucede cuando invertimos la dinámica de poder de la vigilancia?

Enseguida descubrimos la respuesta a esa pregunta. 

Stop LAPD Spying es miembro de May First Movement Technology, una cooperativa que proporciona servicios de alojamiento a su membresía, incluyendo el sitio Watch the Watchers. Poco después del lanzamiento del sitio, May First empezó a recibir presiones para que retirara el sitio, supuestamente porque violaba la ley de California que prohíbe la "apropiación indebida" de información personal.

Sin embargo, la ley de apropiación indebida -que impide que tu nombre, imagen o información personal se utilicen en contra de tus deseos en *productos, mercancías o bienes*- tiene por objeto proteger tu imagen para que no se comercialice con fines lucrativos. No *protege* a los funcionarios públicos de la divulgación de datos sobre ellos que ya están a disposición del público aunque sea mediante un nuevo formato en línea.

El abogado de Stop LAPD Spying [respondió a la solicitud de retirar el sitio](/files/watchthewatchers-take-down-response.pdf), explicando claramente que el 100% de la información publicada en Watch the Watchers son datos hechos públicos por organismos gubernamentales en virtud de la Ley de Registros Públicos de California, y que ninguno de ellos se utiliza para vender o promocionar producto alguno. 

La historia pronto dio otro giro. A principios de esta semana, la Liga Protectora de la Policía de Los Ángeles (Los Angeles Police Protective League), una asociación privada de agentes de policía, presentó una demanda exigiendo que los funcionarios municipales tomaran medidas legales contra cualquiera que publicara los registros. La demanda argumenta que los datos hechos públicos incluyen a los agentes "encubiertos", lo que, según ellos, incluye a todos los "agentes que vigilan" y a los agentes que podrían aceptar una misión encubierta en el futuro ([cita](https://www.latimes.com/california/story/2023-03-28/lapd-police-union-sues-police-chief-to-force-him-to-claw-back-photos-of-undercover-officers)).  La asociación policial que está detrás de la demanda ha prometido que "pedirá al juez que retire temporalmente el sitio web Watch the Watchers" hasta que se resuelva su disputa con la ciudad ([cita](https://www.latimes.com/california/story/2023-03-28/lapd-police-union-sues-police-chief-to-force-him-to-claw-back-photos-of-undercover-officers)). 

Este tipo de amenazas son una táctica clásica muy utilizada por empresas y organismos gubernamentales para intimidar a los proveedores de alojamiento y presionarlos para que retiren contenidos perfectamente legales. Estas falsas acusaciones legales se utilizan para amedrentar a las personas que no tienen la energía, los recursos legales o la voluntad política para luchar.

Por desgracia, las empresas que controlan nuestras herramientas en línea no solo son cada vez más grandes, van formando alianzas muy lucrativas con agencias policiales a medida que desarrollan nuevas tecnologías de vigilancia. Esta combinación es un desastre en potencia para las personas activistas del movimiento que dependen de Internet para luchar contra los abusos policiales o contra cualquier tema crítico del capitalismo o de las clases corporativas.

May First Movement Technology y Stop LAPD Spying Coalition se unen contra la vigilancia policial y la tecnología corporativa que alimenta esta peligrosa tendencia. Animamos a todos las personas activistas del movimiento a luchar contra el estado acosador y a apoyar a los proveedores autónomos de tecnología para el movimiento. Para saber cómo unirse a este movimiento y apoyarlo, visita [https://stoplapdspying.org/](https://stoplapdspying.org/) y [https://mayfirst.coop/](https://mayfirst.coop/).

