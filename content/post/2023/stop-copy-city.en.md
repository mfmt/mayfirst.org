---
date: 2023-03-01
title: "Stop Cop City!"
tags: [ "news","callout"]
---

Chat GPT is a distraction. When it comes to the dangers of artificial
intelligence and machine learning the real story will be in Atlanta starting
this weekend.

**May First encourages all members to support the mass mobilization against Cop
City taking place between March 4th and 11th.**

## What is happening?

The Atlanta Police Foundation is trying to build a huge police training
facility in Weelaunee Forest, “a watershed surrounded by primarily Black
residents who overwhelmingly oppose the project”
([https://stopcop.city](https://stopcop.city)). The city of Atlanta has ignored
the local protests in opposition to the facility, violently evicted protesters
(shooting a protester known as Tortuguita to death with over a dozen bullets),
and recently granted a final permit for the facility to be built.

The mass mobilization is to protest the killing and fight the final permit.

## Why is it happening?

The campaign to stop the facility may be local, but the broader campaign to
oppose the militarization of and surveillance by the police is an international
issue.

Specifically it is an issue that combines corporate profit, technology, and
racism.  The Atlanta Police Foundation, which is funding Cop City, is part of a
US trend of corporate funded policing identified by Color of Change in a recent
report.

According to the report:

>    Police foundations are private organizations that funnel corporate money
>    into policing, protecting corporate interests and enabling
>    state-sanctioned violence against Black communities and communities of
>    color. You might be more familiar with the Atlanta Police Foundation’s
>    sponsors: Amazon, Bank of America, Chick-fil-A, Coca-Cola1, Delta
>    Airlines, Home Depot, Waffle House, Wells Fargo, Uber and UPS, to name a
>    few. These are the donors we know about. As calls for accountability
>    increased in recent years, police foundations have taken additional steps
>    to scrub their websites and hide donor information.

And what are corporations funding via police foundations? Surveillance. The
Atlanta Police Foundation, for example, funds Operation Shield - "a citywide
network of 11,000 surveillance cameras and license plate readers that has only
expanded the round-the-clock monitoring of black Atlantans"
([https://stopcop.city/](https://stopcop.city/)).

These 11,000 cameras are monitored by the same corporate machine learning
technology whose development has been driven by surveillance capitalism and
trained on data collected through the widespread use of corporate social media,
search, email and other internet services.

## What can we do?

We encourage all members to join the mobilization.

We also encourage folks to join May First board member Micky Metts this Friday
for a strategy session sponsored by Community Bridge: Friday, March 3rd at 6pm
ET via
[https://communitybridge.com/stopcopcity](https://communitybridge.com/stopcopcity)
(Acess Code: solidarity) or connect with May First board member Jerome Scott
via Community Movement Builders.

And, as always, please support autonomous technology providers in building
consentful, community centered technology guided by the communication and
organizing needs of our movements.


