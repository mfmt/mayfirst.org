---
date: 2023-04-27
title: "Why do we support autonomous technology?"
tags: [ "news", "callout" ]
---


Over the summer, four May First board members interviewed over a dozen May
First member organizations to learn more about how our movement interacts with
technology, how our use of technology aligns (or mis-aligns) with our values,
and to ask: **How could we design technologies in a consensual way, in order to
support both people and our planet?** The result is a thoughtful and insightful
article that explores our relationship with technology and the role that May
First plays in that relationship.

### [The politics and practices of an autonomous technology: Voices of the members of May First](/files/politics-practices-autonomous-technology.pdf)

[<img alt="Post it notes around question - what technology does your org depend on?" src="/img/what-tech-do-you-depend-on.png" class="img-responsive">](/files/politics-practices-autonomous-technology.pdf)

We invite all of you to [read it](/files/politics-practices-autonomous-technology.pdf) or [listen to an interview about it (in Spanish)](https://soundcloud.com/ccd-radio/mayfirstmovementtechnology)!

