---
date: 2023-04-27
title: "¿Por qué defendemos una tecnología autónoma?"
tags: [ "news", "callout" ]
---

Durante el verano, cuatro integrantes de la junta de May First entrevistaron a
más de una docena de organizaciones que forman parte de la Cooperativa para
saber más sobre cómo nuestro movimiento interactúa con la tecnología, cómo su
uso se alinea (o desalinea) con nuestros valores, y preguntarse: **¿Cómo diseñar
tecnologías de forma consensuada, en apoyo a las personas y el planeta?** El
resultado es un artículo reflexivo y enriquecedor que explora nuestra relación
con la tecnología y el papel que May First desempeña en esa relación.

### [Política y prácticas de una tecnología autónoma: voces de la membresía de May First](/files/politica-practicas-tecnologia-autonoma.pdf)

[<img alt="Post it notes around question - what technology does your org depend on?" src="/img/what-tech-do-you-depend-on.png" class="img-responsive">](/files/politica-practicas-tecnologia-autonoma.pdf)

Les invitamos a [leerlo](/files/politica-practicas-tecnologia-autonoma.pdf) o [escuche una entrevista al respecto](https://soundcloud.com/ccd-radio/mayfirstmovementtechnology)!

