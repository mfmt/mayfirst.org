+++
date = 2015-10-01T13:52:02Z
title = "Declaración de Unidad"
slug = 'declaración-de-unidad'
+++

*Este documento fue escrito en colaboración por un grupo de 20 miebmros de Primero de Mayo/Enlace Popular en 2006. Ha sido reemplazado por [los documentos oficiales](/es/documentos-oficiales) que fueron aprobados por los miembros en noviembre 2015. queda aquí como un documento histórico.*

 1. Nosotros entendemos a la tecnología como las herramientas que los seres humanos han desarrollado a lo largo de nuestra historia para sobrevivir y seguir adelante.
 1. Creemos que las personas tenemos derecho a un mundo pacífico y con justicia en el que podamos tener una vida digna, sentirnos seguros y ser productivos sin ser objeto de explotación.
 1. Creemos que se nos niega ese derecho en el terreno social, político y económico debido al subdesarrollo, la explotación, la represión, la destrucción del medio ambiente y la guerra.
 1. Creemos que hoy existen las condiciones para revertir esta situación y construir el mundo que nos merecemos.
 1. Creemos que debido a que la tecnología es un producto de la colaboración de las personas de todo el mundo a lo largo de la historia, ésta -la tecnología- nos pertenece todos y debemos tener el derecho de ejercer control sobre ella.
 1. Creemos que Internet es parte de esa tecnología y, como tal, debe ser accesible, controlada y utilizada en beneficio de todas las personas.
 1. Creemos que los intentos de unos pocos por comerciar con el Internet, desviar su desarrollo y restringir su uso, van en contra del espíritu de colaboración del Internet y en contradicción con los intereses de la humanidad.
 1. Creemos que es una prioridad importante de la humanidad asumir el control de la Internet, ampliar sus recursos y usarlo productivamente.
 1. Debido a que nuestras creencias y actividades reflejan un compromiso con esta idea del mundo y del Internet, creemos que los activistas que trabajan por la justicia social deben estar a la vanguardia de la lucha por tomar el control de Internet, para utilizarlo plenamente y de manera creativa para alcanzar nuestros objetivos y para ampliar sus capacidades con miras a satisfacer las cada vez mayores y más complejas necesidades de comunicación y organización.
 1. Para que esto suceda, necesitamos un movimiento que refleje este pensamiento y este enfoque sobre Internet y que una a tod@s l@s activistas por la justicia social a través de Internet.
 1. Con MayFirst/PeopleLink queremos empezar y continuar con la creación de este movimiento.


