+++
date = 2020-10-04T14:47:48Z
description = "Prioridades"
title = "Prioridades"
slug = 'prioridades'

+++

En la reunión de miembros de 2020 se aprobaron y clasificaron las siguientes prioridades para 2021:

 * 520 Votas: Mejorar la sostenibilidad económica de la organización 
 * 506 Votas: Completar el desarrollo y la aplicación de la infraestructura de nuevas tecnologías
 * 432 Votas: Expandir el trabajo de traer organizaciones y activistas al Primero de Mayo mientras difundimos nuestro pensamiento en el resto de nuestros movimientos
 * 423 Votas: Facilitar la participación de los miembros del Primero de Mayo en el desarrollo y mejora de nuestra tecnología e infraestructura
 * 390 Votas: Desarrollar un programa de trabajo colaborativo entre nuestros miembros en los EE.UU. y México enfocado en temas que afectan a ambos países
 * 382 Votas: Explorar las opciones para hacer que las operaciones de nuestro centro de datos sean más ambientalmente sostenibles
 * 366 Votas: Desarrollar una política de medios sociales que aborde el trabajo y la educación sobre las formas actuales de los medios sociales y comience a organizar el desarrollo de prácticas y programas alternativos de medios sociales
 * 356 Votas: Facilitar la comunicación de miembro a miembro
 * 298 Votas: Explorar alternativas a la Internet

Prioridades del año anterior

 * [2016](/priorities-2016)
 * [2017](/priorities-2017)
 * [2018](/priorities-2018)
 * [2019](/priorities-2019)
 * [2020](/priorities-2020)
