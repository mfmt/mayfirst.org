+++
date = 2019-06-14T13:52:02Z
title = "Los Estatutos"
slug = 'estatutos'
[menu]
  [menu.more]
    parent = "Acerca de Nosotros"
    weight = 20
+++

En junio de 2019, la membresía de May First aprobó [los estatutos](/files/bylaws.es.pdf) convirtiendo oficialmente a la organización en una organización de membresía sin fines de lucro y convirtiéndose efectivamente en una cooperativa.
