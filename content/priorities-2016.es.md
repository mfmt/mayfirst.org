+++
date = 2015-11-24T14:47:48Z
description = "Prioridades de 2016"
title = "Prioridades de 2016"
slug = 'prioridades-2016'
+++

Estas prioridades son para fines de archivo. Por favor vea nuestras [prioridades del año actual](/es/prioridades).

El Reunión de Membresía aprobó las siguientes prioridades para el 2016.

Si se ha perdido el encuentro, los [notas completas están disponibles en línea (en ingles)](https://support.mayfirst.org/raw-attachment/wiki/projects/membership-meeting/2015/2015-annual-membership-meeting-notes.odt).

## Mesa sobre tecnologías humanas de comunicación y arte

* Dada la situación de urgencia de la soberanía de nuestros datos en la que nos encontramos en este momento de ataques continuos a nuestro movimiento, el diálogo entre activistas y técnicos es imprescindible. PMEP llevará a cabo una campaña para promocionar este tipo de diálogos para que juntos puedan definir las políticas de tecnología y comunicación del movimiento.
* Encuesta para identificar qué tipo de tecnología están usando los miembros de PMEP para la comunicación más allá de lo que ofrece nuestra organización (específicamente, cómo usan las redes sociales y cuáles son sus preocupaciones en cuanto a la seguridad de sus datos).
* Educación popular para miembros y el movimiento sobre raza, clase y temas culturales que interfieren en las diferentes conversaciones. 
* Continuación y expansión del programa de entrenamiento de técnicos/as de color
* Desarrollo y lanzamiento de una campaña de videos de educación e información sobre PMEP y sus servicios. 


## Mesa internacional/cuidado de la tierra

* Uso de Jitsi u otras herramientas de fuente abierta, implementación de sesiones donde miembros de diferentes países puedan compartir información acerca de cómo se están manifestando amenazas a la privacidad en internet en sus respectivas comunidades. Uso del contenido de estas sesiones para crear recursos de educación política, incluyendo paneles internacionales en varios eventos como el Foro de Izquierda.  
* Fortalecer las relaciones con los principales movimientos internacionales como el Foro Social de Internet, la Marcha Mundial de Mujeres, el FSM en Montreal y la Convergencia Climática. 
* Coordinar la comunicación con cada miembro de PMEP para saber qué tipo de trabajo están haciendo, cómo usan la tecnología y qué necesidades tienen con el objetivo de identificar y fortalecer conexiones entre miembros, especialmente de diferentes países.

## Mesa organizada con participantes en línea

* Conectar una “Campaña de privacidad en internet” con la vigilancia local y cotidiana que todos vivimos (cámaras en calles y edificios, antecedentes penales, vigilancia policial).

## Mesa sobre derechos humanos, derechos de los pueblos y democracia

* Aumentar la cuota de membresía de acuerdo con criterios equitativos que no excluyan a nadie. Esto debe hacerse para que crezca la membresía, infraestructura y alcance de PMEP, y para ofrecer recursos para temas prioritarios. 
* Involucrar a miembros de comunidades marginalizadas en actividades de fortalecimiento de capacidades y educación, para aumentar y apoyar el liderazgo dentro del colectivo. 
* Fortalecer nuestra voz para pelear en contra del uso opresivo de la tecnología. Esto incluye el desarrollo de un análisis de cómo la tecnología se usa en forma opresiva, y usar este análisis para educar nuestra membresía. 

## Mesa on Technology, Media, Gender, and Culture

 * May First must have as a political strategy for the collaboration and alliance and the strengthening of the alternative media movement and free media.
 * May First should promote the work of and messages from the organizations within it at the base, the grassroots, which will serve to strengthen the May First organization.
 * Make communications more clear with a deliberate campaign to communicate joint common interests and how these interests can be advanced/implemented through common tools.
 * May First should have as a priority to break the language barrier in its work.
 * May First should offer clarity about the way and logistics of documentation, and promote member participation.  Especially technical documentation and also organizational documentation.
 * Think of strategies of popular education that can generate resources to sustain the infrastructure.  It could be community parties to host workshops to share knowledge and promote linkages between our organizations that are members.
 * Due to the importance of MFPL, our organization should help shape main agendas of internet /related organizations and find common positions.

## Mesa on Countryside, Social development, social economy, and human rights.

 * We propose to design methodologies and diagnostics about information risk and security.  Should include analysis and case studies.
 * Grow MFPL infrastructure with the organization, assess server needs/operations in Mexico and in other countries.
 *  We believe it is necessary to promote Free Software tools and include training. To present to the members a table of the tools that MayFirst has as Free Software compared with those available in the commercial market, so we can see the differences.
 *  It is necessary to research to minimize the problems we've been having with communication systems, in particular Mezcla software.




