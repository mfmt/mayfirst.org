+++
date = 2015-10-01T13:52:02Z
title = "Official Documents"
+++

As of June 2019, the organization is governed by our member-approved [bylaws](/en/bylaws).

The following set of documents were in place from 2015 - 2019 and remain for historical purposes.

 * [Political Environment](/en/political-environment)
 * [Mission](/en/mission)
 * [Values](/en/values)
 * [Goals](/en/goals)
 * [Structure](/en/structure)
 * [Combatting Racism and Sexism](/en/intentionality)

*Approved by the membership in November 2015.*

From 2006 - 2015, the organization used a [statement of unity](/en/unity) which is provided for historical purposes. 
