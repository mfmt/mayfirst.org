+++
date = 2015-10-01T13:52:02Z
title = "Política de privacidad"
slug = 'politica-de-privacidad'
[menu]
  [menu.more]
    parent = "Hosting"
    weight = 200
+++

No venderemos tu información. Bajo ninguna circunstancia venderemos información de las membresías a ningún otro grupo o individuo.

Nos opondremos activamente a cualquier intento de obligar a May First Movement Technology a revelar información o registros de sus membresías.

No leeremos, buscaremos ni procesaremos ninguno de tus datos a menos que sea necesario para protegerte de virus y spam, depurar problemas que afecten a nuestra infraestructura compartida o cuando nos lo indiques al solucionar problemas.

Tus datos están encriptados en reposo. Todos los datos guardados en nuestros servidores y todas las copias de seguridad se almacenan en un formato cifrado.

Es nuestra política notificarle inmediatamente a la membresía en cuestión si conocemos que está siendo investigada.

Para proteger la privacidad de nuestras membresías, eliminamos periódicamente los archivos de registro.

Datos de registro: Al darse de baja una membresía, los datos de la misma serán completamente purgados.

Datos personales: Información como su nombre, datos de contacto o cuándo se afilió a la organización y su historial de pagos pueden conservarse indefinidamente para que podamos realizar un seguimiento y evaluar el compromiso de nuestras membresías a lo largo del tiempo. Sin embargo, cualquier membresía puede solicitar el acceso, la corrección o la eliminación de todos o parte de los datos personales que conservamos. Puede realizar dicha solicitud al correo electrónico info@mayfirst.org. Procuraremos satisfacer estas peticiones, a excepción de que la ley nos lo prohíba.
