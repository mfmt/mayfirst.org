+++
date = 2019-01-22T14:47:48Z
title = "Equipo de Estrategia ampliado de 2019"
tags = [ "callout" ]
+++

Gracias a todos los miembros que participaron en nuestra estrategia ampliada de
nueve semanas Proceso de equipo para elaborar planes de trabajo para
convertirse en un cooperativo y ampliar nuestra ¡organización!

Los planes para convertirse en una cooperativa señalan un cambio importante e
histórico en cómo funciona nuestra organización y por lo tanto requieren la
aprobación de nuestros miembros existentes * - así que continúe leyendo para
aprender sobre lo que el comité tiene Recomendar y leer nuestros nuevos
estatutos propuestos.

El plan de trabajo para [ampliar la organización](/files/expanded-strategy-2019/expansion.es.pdf) ya ha sido Aprobado por el Comité de
Liderazgo y se está implementando de inmediato.

## Resumen de las principales decisiones

* Anteriormente, Media Jumpstart, Inc operaba como una organización sin fines
  de lucro 501c3 en el que la Junta controlaba legalmente la organización y
  nombraba nuevos miembros de la Junta. Los miembros de la Junta
  *voluntariamente* permitieron el liderazgo electo Comité para tomar todas las
  decisiones para la organización. **Proponemos el torneado.  sobre el control
  legal de la organización a los Miembros y al Personal retirándose El Comité
  de Liderazgo y el empoderamiento de los miembros y el personal para elegir a
  la Junta como una organización de múltiples partes interesadas.** Esta
  decisión significa que hemos decidido no incorporar bajo la ley Coop del
  estado de Nueva York porque la ley excluye una operación de tenedor de
  múltiples estacas. También hemos optado por continuar como sin fines de
  lucro, aunque esto signifique que nuestros miembros no serán técnicamente
  "propietarios". Al permanecer sin fines de lucro, nos aseguramos de que
  nuestra organización no pueda comience a emitir dividendos, desconfíe de las
  ganancias o trate de obtener ganancias de su funcionamiento.

* Nuestro nombre legal siempre ha sido Media Jumpstart, Inc. y hemos sido
  operando bajo el nombre May First/People Link. Continuaremos como medios
  Jumpstart, Inc., pero para ayudar a expresar el significado de nuestros
  nuevos estatutos, **Proponemos operar bajo el nombre "May First Movement
  Technology".**

* Anteriormente, May First / People Link operaba extra-legalmente como una
  colaboración entre Media Jumpstart, Inc y Primero de Mayo (con sede en
  México) en el que Ambas organizaciones operaron bajo el poder del liderazgo
  elegido.  Comité. **Proponemos que cada organización opere de manera
  autónoma,** con Miembros de Primero de Mayo ganan membresía automática en
  May First Movement Tecnología, y un número reservado de asientos en el
  Consejo de Administración. Primero de Mayo se compromete a realizar pagos en
  especie o en efectivo para compensar los costos de Actividad de membresía y
  programa para apoyar la misión de construcción de movimiento de la
  organización.


#### Consulte los [estatutos propuestos](/files/expanded-strategy-2019/bylaws-draft-legally-reviewed-lc-approved.es.pdf).

## Proceso

Basado en nueve semanas de discusión y trabajo duro, el equipo de estrategia expandida
escribió un [reglamento de lenguaje claro
borrador](/files/expanded-strategy-2019/bylaws-expanded-strategy-team-approved.en.pdf).

A continuación, nuestro generoso abogado voluntario de la [Justicia Urbana
Centro](https://urbanjustice.org/) nos ayudó a convertir ese borrador en un
documento sonoro. Si bien el documento resultante tiene una gran cantidad de verbage extra,
Todavía conserva los principios y objetivos del borrador original.

#### Nuestro 22 de abril de 2019, el Comité de Liderazgo aprobó el borrador y ahora lo presentamos a nuestros miembros para su revisión: https://mayfirst.org/files/expanded-strategy-2019/bylaws-draft-legally-reviewed-lc-approved.es.pdf.

Entre el 24 de abril y el 10 de mayo, se recomienda encarecidamente a los miembros que revisen el borrador
y publique comentarios y preguntas a través de nuestro [panel de discusión](https://comment.mayfirst.org/t/discuss-the-proposed-coop-bylaws/1245).

El 13 de mayo, el Comité de Liderazgo revisará todos los comentarios, incorporará
cambia según sea necesario y luego aprueba la versión final.

Después de la traducción, la versión final se enviará a todos los miembros para una votación.

Los miembros votarán en tres ítems:

 * ¿Usted aprueba los nuevos estatutos?
 * Si se aprueban los estatutos, ¿aprueba el otorgamiento de una relación especial?
   a los miembros de Primero de Mayo en México para permitirles continuar como miembros
   ¿de la organización?
 * Si se aprueban los estatutos, ¿aprueba el permiso de salida?
   Comité de Liderazgo para continuar gobernando la organización a través del
   elección del nuevo Consejo de Administración, que debe ocurrir antes de finales de
   Junio, 2019?

## Agendas

Las agendas completas de las reuniones ampliadas se documentan a continuación con fines históricos.

 * 30 de enero: Introducciones, Objetivos y proceso.
 * 6 de febrero: Unificación de los polos de prestación de servicios y construcción de movimientos ([leer primer](/files/expanded-strategy-2019/poles.es.pdf))
 * 13 de febrero: Evaluando la estrategia de Estados Unidos / México([leer primer](/files/expanded-strategy-2019/us-mexico-strategy.es.pdf))
 * 20 de febrero: Discusión sobre coop ([leer resumen legal](/files/expanded-strategy-2019/legal-entity-options.pdf))
 * 27 de febrero: Discusión sobre creyendo ([leer propuesta](/files/expanded-strategy-2019/expansion.es.pdf))
 * 6 de marzo: Revisión de borradores de planes de coop y crecimiento de organización.
 * 13 de marzo: Revisión de borradores de planes de coop y crecimiento de organización.
 * 20 de marzo: Finalizar borradores de planes de coop y crecimiento de organización.

## Background


Como saben, nuestra membresía ha asignado al Comité de Dirección el desarrollo
de planes para implentar dos tareas: [transformar nuestra organización en una
"cooperativa"](https://comment.mayfirst.org/t/mm-2018-form-a-committee-to-explore-converting-our-organization-to-a-multi-stake-holder-cooperative/910)
y [desarrollar un programa para la "expansión de nuestra
organización"](https://comment.mayfirst.org/t/mm-2018-form-a-committee-to-explore-ways-to-significantly-grow-the-organization-to-a-more-sustainable-size/911).
El CD asignó a su Equipo de Estrategia (un subcomité del CD que se reúne cada
semana) la elaboración de un plan para implementar ambas decisiones y el Equipo
de Estrategia ha decidido expandirse con nuestros miembros para realizar esas
discusiones.
