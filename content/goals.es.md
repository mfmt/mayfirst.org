+++
date = 2015-10-01T13:52:02Z
title = "Objetivos"
slug = 'objetivos'
+++

## Fortalecimiento de capacidades en tecnología

 * Fortalecer las comunicaciones y la infraestructura de tecnología a partir de prioridades determinadas por los movimientos sociales
 * Hacer que el acceso a la tecnología sea de bajo costo, especialmente para la mayoría global y las mujeres
 * Fortalecer las capacidades en el uso de tecnología para organizaciones sociales y activistas 
 * Apoyar las necesidades de comunicación y tecnología de los miembros
 * Fortalecer la comunicación más allá de las fronteras nacionales
 * Promover el uso y desarrollo del software libre
 * Contribuir al desarrollo de tecnologías para promocionar economías solidarias y otras alternativas al capitalismo y construir relaciones justas en los lugares de trabajo 

## Fortalecimiento de los movimientos sociales

 * Sensibilizar, particularmente entre nuestros miembros, sobre los temas políticos indisociables de la tecnología 
 * Involucrar a los miembros en el trabajo de los movimientos sociales 
 * Promover y defender los derechos en internet
 * Defender la privacidad, la soberanía de datos y la seguridad
 * Promover el respeto por los derechos culturales y la diversidad lingüística

*Aprobado por la membresía en noviembre 2015, como parte de los [documentos oficiales](/es/documentos-oficiales).*
