+++
date = 2015-10-01T13:52:02Z
title = "Estructura"
slug = 'estructura'
+++

Primero de Mayo/Enlace Popular es una organización democrática de membresía con los miembros de todas partes del mundo. Hay dos centros con funciones exclusivamente operativos y administrativos: Media Jumpstart (con sede en Nueva York y que recauda las cuotas de los miembros que no están en México) y La Cooperativa Primero de Mayo/Enlace Popular (con sede en México DF y que recauda las cuotas de los miembros que están en el país) que responden a los lineamientos del Comité de Dirección.

Los documentos fundamentales de la organización son: Ambiente político, Declaración de misión, Valores, Declaración de intencionalidad, y este documento sobre la estructura.

Los miembros tienen la responsabilidad y autoridad para: 
 
 * Elegir a la totalidad de los 15 miembros del Comité de dirección.
 * Definir las prioridades de la organización para el año siguiente, dentro de la órbita de los documentos fundamentales. 
 * Aprobar cambios a los documentos fundamentales. 
 * Presentar propuestas políticas, organizativas, o de cualquier otra naturaleza al Comité de Dirección y recibir una respuesta oportuna.

Cada centro tiene la responsabilidad y la autoridad para:

 * Cobrar las cuotas de los miembros de su área respectiva.
 * Organizar la elección de candidatos para el Comité de dirección entre los miembros de cada región de acuerdo con los principios esbozados en la Declaración de intencionalidad, asegurando una representación diversa en cuanto a género y orígenes. Los miembros del Comité de Dirección son elegidos en proporción al número de miembros que pagan cuotas en su centro, siempre que ninguno de ellos supere los dos tercios de los puestos. La totalidad de la membresía elige a los integrantes nominados o reelige personas cuyo periodo haya expirado ese año. 
 * Proponer un presupuesto para adjudicar los recursos recaudados por las cuotas de los miembros o apoyos externos recibidos para ser aprobado por el Comité de dirección, con una renovación anual de cinco miembros. 
 * Informar a los miembros acerca de la utilización de los recursos.
 * Promover el desarrollo político y organizativo en su entorno para consolidar condiciones de sostenibilidad, así como contribuir a la formación de nuevos centros para la proyección futura de la asociación, de acuerdo con las estrategias diseñadas por el Comité de Dirección.
 * Presentar propuestas como instancia local al Comité de Dirección y recibir una respuesta oportuna.

El Comité de dirección tiene la responsabilidad y la autoridad para:

 * Organizar y facilitar la reunión de los miembros. 
 * Ofrecer liderazgo político y aprobar la adjudicación de recursos de acuerdo con los documentos fundamentales, así como las prioridades definidas por los miembros en la reunión de membresía previa. 
 * Convertir las prioridades en proyectos una vez evaluada su factibilidad tecnica, economica y politica asi como los centros y las regiones donde se aplicarán.
 * Auxiliarse con los trabajos de las comisiones de administracion y programacion para el funcionamiento de la organización.

*Aprobado por la membresía en noviembre 2015, como parte de los [documentos oficiales](/es/documentos-oficiales).*
