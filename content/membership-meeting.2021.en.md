+++ 
date = 2021-09-23T14:47:48Z
description = "Welcome to the May First Movement Technology Members' Meeting" 
title = "2021 Members' Meeting and voting process" 
tags = [ "callout" ]
+++

# Now in Session!

Please join us for our membership meeting: https://i.meet.mayfirst.org/MembershipMeeting

3:00 pm - 4:00 pm America/New_York time on Friday, December 3rd.

## Calendar of Dates and Meetings

### Summary

**Welcome to the official 2021 May First Membership meeting page!**

The United States may have a new president, but the global problems of hopelessly inadequate health care, life-threatening climate change, growing fascist movements and precarious labor continue.

At the same time, *movements around the world have responded,* producing awe inspiring mutual aid networks, creative and powerful campaigns for justice, and new ways to support and defend ourselves.

One theme threading both the threats and our responses is technology: who owns it and to what end?

Please join May First for our annual membership meeting as we continue to build our collectively owned technology resources and plan our strategies for making fundamental changes to reverse the current trends and build a more just, healthy world.

Our membership meetings will take place from late October through early December and will include online meetings, forum discussions and a one week voting period.

You can register for all events on our [event registration page](https://outreach.mayfirst.org/civicrm/event/register?id=76&reset=1).

Please take a look at our schedule and **commit to attending at least one discussion meeting and the main membership meeting** in December.

<a href="/img/2021-membership-meeting-en.png"><img class="img-responsive" alt="2021 Membership Meeting schedule info graphic" src="/img/2021-membership-meeting-en-thumb.png"></a>

### Discussion Meetings

*All meetings are scheduled for either 1 hour or 1.5 hours.*

We will hold the following five discussion meetings. Members are welcome to attend all meetings, or you can attend just the meetings most relevant to your organizing work.

Each meeting will take place online via our video conference system (Jitsi Meet) via <https://i.meet.mayfirst.org/MembershipMeeting>. Please note: You must connect via a desktop computer using either the Chrome web browser or the Firefox web browser. Our interpretation software does not work properly when connecting via most mobile phones.

We will also have a corresponding forum discussion via [discourse](https://comment.mayfirst.org/) for people unable to attend the meeting or who wish to continue the discussion (TBD).

Additionally, written reports evaluating our work will be available soon.

Please [let us know which meetings you plan to attend](https://outreach.mayfirst.org/civicrm/event/register?id=76&reset=1).

#### Schedule of Meetings

* **Thursday, October 28, at 3:00 - 4:00 pm** (America/New_York, 2:00 - 3:00 pm Mexico City, 7:00 - 8:00 pm UTC) \
  *Are you interested in running for the Board of Directors?* \
  If you are interested in helping direct May First, this workshop is for you. We will share information for all prospective board members about how May First's leadership operates, expectations of board members, the history of the organization and the organization's commitment to combating racism and sexism. Please [review our board orientation materials](https://mayfirst.coop/en/orientation/).
* **Wednesday, November 3, at 1:00 - 2:30 pm** (America/New_York, 11:00 am - 12:30 pm Mexico City, 6:00 - 7:30 pm UTC) \
  *Assessing the current political environment* \
  Join us in a collaborative workshop to discuss the most important aspects of the current political moment and help us plan for what's to come.
* **Tuesday, November 9, at 3:00 - 4:30 pm** (America/New_York, 2:00 - 3:30 pm Mexico City, 8:00 - 9:30 pm UTC ) \
  *Technology Infrastructure and Services* \
  If you want to learn more about our current technology and services, help plan how to engage the movement in their development, and plan our future development, join us and meet the TIAS team. This meeting will produce about 6 - 8 priorities for the coming year.
* **Thursday, November 18, at 1:00 - 2:30 pm** (America/New_York, 12:00 - 1:30 pm Mexico City, 6:00 - 7:30 pm UTC) \
  *Engagement and Communications* \
  How do we engage the movement and communicate the importance of movement-owned technology? How do we align our projects with the current political moment? Help us strategize for next year. This meeting will produce about 6 - 8 priorities for the coming year.
* **Tuesday, November 23, at 3:00 - 4:00 pm** (America/New_York, 2:00 - 3:00 pm Mexico City, 8:00 - 9:00 pm UTC) \
  *New dues structure* \
  The new dues structure is coming! If you have question about how it will impact your dues join us for this informational session. Curious? You can [review it here](https://mayfirst.coop/en/member-benefits/#new-dues-structure).

Please [register](https://outreach.mayfirst.org/civicrm/event/register?id=76&reset=1)!

### Board Elections

According to our bylaws, 5 board seats are chosen by workers and 20 are elected by the membership. Of the 20, we elect approximately 1/3 each year for 3 year terms.

In addition, we reserve a proportionate number of seats for members who pay dues in Mexico (about 20 percent).

This year, given the current ratio of Mexican members, two board seats for a member paying dues in Mexico and seven board seats for all other members are up for election.

The board nomination period will run from November 1 through ~~November 23rd~~ (now extended to December 1st!). And the election will run From December 3 - December 10th.

If you would like to run for the board, please attend the board session (see above). You may also be interested in reviewing our [board orientation materials](/orientation).

Starting November 1, you can [nominate yourself or another May First member who has agreed to run via this form](https://vote.mayfirst.org/).

## The Main Meeting

By law, we have to hold an "official" membership meeting with attendance by at least 10% of our members (about 65 members).

**The official membership meeting will take place on Friday, December 3, from 3:00 - 4:00 pm America/New_York (2:00 - 3:00 pm Mexico City, 8:00 - 9:00 pm UTC)**

Once attendance is taken and we have determined that we have quorum, we will be able to make proposals for member approval. When voting, individual members get one vote and organizational members get two votes. Even if your organization sends 10 people to the meeting, you still only get two votes.

The agenda is:

1. **Proposal on Board elections.** This proposal will ask the members to approve electing the board through a one week online ballot system to ensure as many members as possible can participate (rather than only allowing the members present on the call to vote). See below for more details.
2. **Reports.** There will be three reports, all based on the conversations at the member discussion meetings that precede the Members' meeting. The reports will emphasize the 5 - 7 priorities each meeting agreed consensed on or the general outline of the proposal being put forward.
   * Financial Report
   * Engagement and Communications
   * Technology Infrastructure and Services

## The Voting Period

May First has always emphasized the importance of internal democracy and we don't feel a single online meeting is the most inclusive way to elect Board members since not all members will be able to attend.

So the Board will propose to the membership meeting that we conduct the elections via an online ballot (the exact same [voting site](https://vote.mayfirst.org/) we have conducted our elections for the last 6 years) for a week following the meeting: December 3 through December 10th.

In that Board elections ballot, members will also be asked to rank the priorities that were generated during discussion meetings.

## Legal Details

Our new bylaws require a yearly meeting of members at which 10 percent of our active membership makes up a quorom. This means we need attendance by at least 65 members (the exact number will be determined on October 31st, the cut-off date for voter eligibility). To evaluate this requirement, we will be counting memberships, not people on the call. We invite members to bring as many people from your organization as you want, however, we can only count each membership once when taking attendance.

This quorum is a requirement of New York State law under which our cooperative is incorporated.
