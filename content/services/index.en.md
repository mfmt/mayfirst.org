+++ 
title = "May First Services" 
date = 2024-05-25T13:53:34Z
+++

## Common Hosting Services

### Websites

May First members can create any kind of web site, including common PHP web
applications, like Backdrop, WordPress, Drupal, or CiviCRM, or Django, Rails or
Node apps. Both MySQL and Postgres databases are available, as well as the
Memcache key/value database. Members can create scheduled jobs or ongoing
services using systemd services and timers. All web traffic travels through
a proxy layer allowing us to block bad bots or other malicious traffic.

### Email

Despite claims that email is dead, our members use it massively. You can add as
many email accounts (within disk limits), and then check your email using your
own email client on your phone or desktop computer, or you can use one of our
two  webmail programs. You can also have as many forwarding accounts as you
need.

### Email lists

You can create as many email lists as you want and make them public or private.

### Virtual Private Server

Most members web sites and email are hosted on servers securely shared with
other members. We do offer a virtual private servers for members who need a
server dedicated just to their use (and who contribute higher membership dues).

## Extra Services

The following services are all available to all members (and some are open to
the general public).

### Nextcloud

![](nextcloud.png)

Share documents with NextCloud. May First members can share and synchronize
documents with other users through our NextCloud service. You can share
documents with your friends, synchronize them to your computer or cell phone,
make the publicly available via the web, and you can even edit them on the web.

In addition, you can use this program to synchronize your contacts and calendar
on your cell phone, allowing you to backup your sensitive personal data via May
First rather than Google or Apple. No need to synchronize your data to the
corporate cloud!

The NextCloud services comes with programs you can install on Linux, Macintosh
or Windows computers to synchronize the files. There are also apps for Android
and iPhone.

### Jitsi Meet

![](jitsi-front.png)

Web conference with Jitsi Meet. Do you want to have a video conference call? No need to install special software or pay for a skype account. You can do it with free software and just a web browser. Send all of your meeting participants a simple link that they click on and you can all talk with each other and see each other.

Perfect for collaborating with people in different locations or when you can't all be in the same room for a meeting. For full directions, see our web conference page.

### Gajim

![](groupchat-window.png)

Instant Messaging/Chat (xmpp). We run our own federated chat service. Any
member with a valid May First user account can start using it immediately. Your
handle is your username followed by @mayfirst.org. By using an open protocol,
May First users can communicate with other services that also use the XMPP
protocol. Our server supports text, video and audio!

In addition, our service supports OMEMO encryption for end-to-end encryption.

You can connect via a desktop chat client or an Android or iPhone application.
And the best part is that most chat clients can connect to our service,
allowing you to continue chatting with your friends while also providing the
opportunity to securely chat with people using XMPP.

### Mumble

Audio Conference with Mumble. Need a simple, audio-only conference system that
runs over low-bandwidth connection on Windows, Linux, Mac and even your android
or iPhone? Our mumble server is the way to go.

### Ethercalc

Share spreadsheets with EtherCalc: https://calc.mayfirst.org/. Anyone can use
it to collaboratively and simultaneously work on spreadsheet documents.

### Discourse

Engage in a nicely formatted online discussions with Discourse. We operate a
Discourse server, a platform for discussions.
