+++
date = 2019-11-14T13:52:02Z
title = "About May First"
homepage = "about"
+++ 

May First Movement Technology is a non-profit membership organization that
engages in building movements by advancing the strategic use and collective
control of technology for local struggles, global transformation, and
emancipation without borders.

<div class="faq-question"><a href="#faq-answer-who" data-toggle="collapse">Who are you?</a></div>
<div class="faq-answer collapse" id="faq-answer-who">
<p>We are a <strong>democratically-run, not-for-profit cooperative of movement
organizations and activists</strong> in the United States and Mexico. We've been around
since 2005 and our 850 members (mostly organizations and mainly in the U.S. and
Mexico) host over 10,000 email address and over 2,000 web sites on our
collectively owned and secured hardware that run exclusively on encrypted
disks.</p>

<p>Our members run our organization via a <strong>race and gender diverse</strong> <a href="who">elected body of
movement activists, organizers and technologists</a></p>

<p>Besides hosting that data, our organization participates in networks,
coalitions and campaigns organizing around many technology-related issues and
has distinguished itself in confronting many legal threats and standing up to
subpoenas.</p>

<p>That's what we are. Here's what we're not: we are not a company or a service
provider or a collective or a small non-profit. All those things are fine and
many of those types of organizations do outstanding work. We just do something
else.</p>
</div>

<div class="faq-question"><a href="/who">Who is on your board?</a></div>
<div class="faq-question"><a href="#faq-answer-what" data-toggle="collapse">What do you do?</a></div>
<div class="faq-answer collapse" id="faq-answer-what">
<p>We believe that movements for fundamental change must prioritize the use,
protection and democratization of technology. We want our movements' leadership
to think about those goals and develop a unified plan to make them happen.
That's the basis of our work and it makes May First one of the most active and
prominent technology organizations in the Left of this country.</p>

<p>We are a leading organization of the <a
href="https://mediajustice.org/network/">Media Justice Network</a>; a founder
and leader of the <a href="https://radicalconnections.net">Radical Connections
Network</a>; a long-time member of the <a href="https://apc.org/">Association
for Progressive Communications</a>; a member of several new economy coalitions
and networks and we have been involved in many coalitions and networks set up
to deal with specific issues and struggles. We participate in many conferences
and major convergences in the course of the year in the U.S., in Mexico and
internationally. Our representatives will participate in over 20 such events
during any year.</p>

<p>Our unique and unprecedented <a href="https://techandrev.org">Technology and
Revolution</a> series has brought together over 1500 movement leaders and
activists in the U.S. and Mexico in 24 convergences since 2017 to think and
talk about the intersection of technology and revolution and to forge a program
of work we can do around technology.</p>

<p>That commitment to movement work is also seen in our commitment to the
protection of our data. Our members are <a
href="https://support.mayfirst.org/wiki/legal">frequently hit</a> with demands
for data from governments, take-down notices from corporations or Denial of
Service attacks from political enemies. We resist them all. We have
successfully resisted denial of service attacks on members working on
reproductive justice, Palestinian rights, and the Black Lives Matter movement.
We refused to give the FBI details when it demanded payment information that
would have identified the members of an Independent Media Center site in
Greece. We have consistently refused to take down satire websites by members
such as the Yes Men, which corporations consider trademark violations.</p>

<p>But those are only the most dramatic instances because, day to day, we fiercely
protect data through encryption, back-up and monitoring. Our members' data is
our movements' tool and our movements are our priority.</p>

<p>All of this - our movement work and our data protection - has a purpose: to
help our movements build a new relationship with technology. We want our
movement to think about technology, develop a program to protect and expand it
and actually work on that program. We want to be a platform for that kind of
collaboration: the democratic control and expansion of technology as a tool for
human survival and the struggle to make that happen. We want to build that and
we want to model it.</p>
</div>

<div class="faq-question"><a href="#faq-answer-cost" data-toggle="collapse">How much does it cost?</a></div>
<div class="faq-answer collapse" id="faq-answer-cost">

<p>Members pay dues based on their ability to pay and the resources required. Please see our <a href="/member-benefits">member benefits page</a> for more information.</p>

</div>

<div class="faq-question"><a href="#faq-answer-democracy" data-toggle="collapse">How does democracy work?</a></div>
<div class="faq-answer collapse" id="faq-answer-democracy">

<p>Our members make our key decisions through meetings.</p>

<p>The <strong>yearly Members Meeting</strong> usually takes place in the Fall and takes at least three actions:</p>

<ul>
  <li>develops and then ranks the <a href="priorities">priorities</a> which describes the general thrust of our work for the coming year</li>
  <li>the approval of the organization's yearly financial report</li>
  <li>the election of members to our 21 - 25 person <a href="who">Board of Directors</a></li>
</ul>

<p>The <strong>Board meets quarterly</strong>. The Board leads May First based on the priorities document and budget by approving work plans, appointing work groups and hiring staff. Those work groups and staff carry on the day to day work plan implementation.

<p>By the way, any May First member can attend a Board meeting or a meeting of any of its work groups. You can't vote at the Board meeting unless you're a Board member but you can participate.</p>
</div>

<div class="faq-question"><a href="/get-involved">How do I get involved?</a></div>
<div class="faq-question"><a href="#faq-answer-racism-sexism" data-toggle="collapse">Can you explain your policy on combatting racism and sexism?</a></div>
<div class="faq-answer collapse" id="faq-answer-racism-sexism">

<p>Those social diseases have long plagued technology work and are particularly
prevalent in the culture of movement technology. For our entire existence, we
have worked to combat both racism and sexism through conscious recruitment,
representation, and a <a href="intentionality">member approved
statement</a>.</p>

<p>Most Board members are people of color and half are women. All major
representation of May First Movement Technology to the U.S. and world movements
must include at least one person of color and one woman. Exceptions to this
rule should be approved by the appropriate Board working group. The
coordinators of each workgroup and standing committee of the Board should
include at least one person of color or woman.</p>

<p>We concentrate our outreach and recruitment efforts on organizations of the
Global Majority and conduct meetings using popular education methods as a
foundation to reflect the diversity of our members. All meetings and public
events are conducted in English and Spanish with qualified, professional
interpretation.</p>
</div>

<div class="faq-question"><a href="#faq-answer-online-meetings" data-toggle="collapse">How should we meet online?</a></div>
<div class="faq-answer collapse" id="faq-answer-online-meetings">
We recommend using:
<ul>
  <li><a href="https://support.mayfirst.org/wiki/mumble">mumble</a> for audio-only conferences of up to hundreds of participants with tools to support human-assisted simultaneous interpretation.</li>
  <li><a href="https://support.mayfirst.org/wiki/web-conference">Jitsi Meet</a> for video-based meetings of 20 or fewer participants</li>
  <li><a href="https://comment.mayfirst.org">Discourse</a> for an email and web based text discussions</li>
</ul>

  For a full explanation of our politics and the options for online meetings, please see our <a href="post/2020/content-alternatives-live-meetings/">full article on online meetings</a>.
</div>
<div class="faq-question"><a href="joinus" >How do I join?</a></div>
