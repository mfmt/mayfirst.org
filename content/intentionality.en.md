+++
date = 2015-10-01T13:52:02Z
title = "Combatting Racism and Sexism"
[menu]
  [menu.more]
    parent = "About"
    weight = 60
+++

May First Movement Technology is an organization of activists & organizers that make use of the Internet to connect communities on the ground with members predominantly in the US and Mexico.

As a movement organization we respond to the activities and developments within our movements' for change and seek to respond to those activities by developmenting and reflecting in our membership and
leadership those movements' demographics and culture.

 * At this point in the United States, major movements for social change are predominantly led by people of color = the Global Majority. To be successful, our organization must reflect this reality.
 * The fierce attack on women's rights, coupled with the persistent and deep-seated sexism within the technology culture, and the fundamental importance of feminism and women's movements to all movements for social change makes urgent the same reflection of women and women's movements in our membership, leadership and activities.
 * The all-too-frequent neglect of the political and historical importance and success of movements in Mexican agricultural regions makes urgent that connections be made with those movements and that they be reflected in our membership and leadership.

The role of technology in all these movements is particularly important. Because communications technology represents the most eloquent expression of democratic spirit and human collaboration, it is vital to the success of our movements. At the same time, we must be conscious that particular effort has been made to lure people and movements of color into the use of corporate-owned social media and other proprietary tools and that the Internet's Left has done little to prevent that and that, in many of these movements, Internet activism isn't a priority.

These are the challenges we face and we cannot face them unless we are predominantly comprised of and led by people of the Global Majority.

Finally, we believe it is essential to bring together politically progressive technologists of all backgrounds, particularly people of color, to begin to coordinate a strategy for response to these repressive measures and to advance the protection of the Internet's neutrality and free software development.

For these reasons, we will conduct our work and guide our organization's work with this intentionality. Specifically:

 * We will vigorously circulate this statement to everyone with whom we have a relationship and rigorously apply it to all our work.
 * All major representation of May First Movement Technology to the U.S. and world movements must include at least one person of color and one woman. Exceptions to this rule should be approved by the LC itself.
 * The coordinators of each workgroup should include at least one person of color or woman.
 * We will concentrate our outreach and recruitment efforts on organizations of the Global Majority.
 * We will conduct our meetings using popular education methods as a foundation to reflect the diversity of our members.

We ask all who work with us to support us by challenging our activities when they don't reflect these principles and noticing them when they do.

*Approved by the membership in November 2015 as part of our [official documents](/en/official-documents).*

