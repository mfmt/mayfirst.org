+++
date = 2020-10-04T14:47:48Z
description = "Prioridades"
title = "Prioridades"
[menu]
  [menu.more]
    parent = "Acerca de Nosotros"
    weight = 30

+++

En la reunión de miembros de 2024 se aprobaron y clasificaron las siguientes prioridades para 2024:

 * 301 Continuar fomentando la pertinencia de las tecnologías autónomas dentro de los movimientos sociales
 * 280 Continuar redactando la documentación en https://ayuda.mayfirst.org
 * 278 Fortalecer las relaciones, facilitar la colaboración y hacer crecer la comunidad entre la membresía de May First
 * 256 Explorar opciones creativas para habilitar nuevos servicios digitales en May First
 * 254 Organizar talleres para la membresía centrados en los cuidados digitales comunitarios

Los trabajadores identificaron las siguientes prioridades:

 * Continuar la renovación de la infraestructura principal, incluidos los cambios relativos al correo electrónico y el alojamiento web y la propuesta de rediseño del panel de control.
 * Investigar opciones para que las operaciones de nuestro centro de datos sean ambientalmente más sostenibles.
 * Impulsar el proceso de rediseño de la identidad visual y el sitio web de May First.

Prioridades del año anterior

 * [2016](/priorities-2016)
 * [2017](/priorities-2017)
 * [2018](/priorities-2018)
 * [2019](/priorities-2019)
 * [2020](/priorities-2020)
 * [2021](/priorities-2021)
 * [2022](/priorities-2022)
 * [2023](/priorities-2023)
