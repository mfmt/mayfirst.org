+++
date = 2020-10-03T14:47:48Z
description = "Priorities"
title = "Priorities"
[menu]
  [menu.more]
    parent = "About"
    weight = 30

+++

The 2024 membership meeting approved and ranked the following member priorities for 2024:

 * 301 Continue to promote the pertinence of autonomous technologies within movement spaces
 * 280 Continue writing documentation at https://help.mayfirst.org
 * 278 Strengthen relationships, facilitate collaboration and build community among May First Members
 * 256 Explore creative options for enabling new digital services at May First
 * 254 Organize member workshops focusing on digital community care

The workers identified the following priorities:

 * Continue renovation of core infrastructure, including underlying e-mail and web hosting technology changes and control panel redesign proposal.
 * Research options to make our data center operations more environmentally sustainable.
 * Lead May First's visual identity and website redesign process.

Previous year priorities:

 * [2016](/priorities-2016)
 * [2017](/priorities-2017)
 * [2018](/priorities-2018)
 * [2019](/priorities-2019)
 * [2020](/priorities-2020)
 * [2021](/priorities-2021)
 * [2022](/priorities-2022)
 * [2023](/priorities-2023)
