+++ 
date = "2021-09-28T14:47:48Z"
description = "Bienvenido a la reunión de la membresía de May First" 
title = "Reunión de miembros 2021" 
tags = [ "callout" ]
+++

# ¡Ahora en sesión!

Acompáñenos en nuestra reunión de miembros: https://i.meet.mayfirst.org/MembershipMeeting

3:00 pm - 4:00 pm hora de América/Nueva_York el viernes 3 de diciembre.

## Calendario de fechas y reuniones

### Resumen

**Bienvenidos a la página oficial de la primera reunión de la membresía de May First 2021**.

Los Estados Unidos puede tener un nuevo presidente, pero los problemas globales de  servicios de salud médica desesperadamente inadecuados, el cambio climático que amenaza la vida, los crecientes movimientos fascistas y la precariedad laboral continúan.

Al mismo tiempo, *los movimientos de todo el mundo han respondido*, produciendo asombrosas y inspiradoras redes de ayuda mutua, creativas y poderosas campañas por la justicia, y nuevas formas de apoyarnos y defendernos.

Un tema que hila tanto las amenazas como nuestras respuestas es la tecnología: ¿a quién pertenece y con qué fin? ¿Quién la posee y con qué fin?

Acompaña a May First en nuestra reunión anual de la membresía mientras seguimos construyendo nuestros recursos tecnológicos de propiedad colectiva y planificando nuestras estrategias para realizar cambios fundamentales que inviertan las tendencias actuales y construyan un mundo más justo y saludable.

Nuestras reuniones de la membresía tendrán lugar desde finales de octubre hasta principios de diciembre e incluirán reuniones en línea, discusiones en foros digitales y un período de votación de una semana.

Puede inscribirse en todos los eventos en nuestra [página de registro](https://outreach.mayfirst.org/civicrm/event/register?id=76&reset=1).

Por favor, eche un vistazo a nuestro programa y **comprométase a asistir al menos a una reunión de discusión y a la reunión principal de la membresía** en diciembre.

<a href="/img/2021-membership-meeting-es.png"><img class="img-responsive" alt="Gráfico informativo del calendario de las reuniones de socios de 2021" src="/img/2021-membership-meeting-es-thumb.png"></a>

### Reuniones de discusión

*Todas las reuniones tienen una duración de 1 hora o 1.5 horas.*

Celebraremos las siguientes cinco reuniones de discusión. Toda las membresía es bienvenida a asistir a todas las reuniones, o pueden asistir solo a las reuniones más relevantes para su trabajo de organización.

Cada reunión tendrá lugar en línea a través de nuestro sistema de videoconferencia (Jitsi Meet) a través de <https://i.meet.mayfirst.org/MembershipMeeting>. Nota: Debe conectarse a través de una computadora d escritorio o laptop utilizando el navegador web Chrome o el navegador Firefox. **¡ojo!** Nuestro software de interpretación no funciona correctamente cuando se conecta a través de la mayoría de los teléfonos móviles.

Para cada tema también dispondremos de un hilo correspondiente en nuestro foro de discusión a través de (https://comment.mayfirst.org/) para las personas que no puedan asistir a la reunión o que deseen continuar la conversación. (urls por determinar).

Además, pronto estarán disponibles los informes escritos de evaluación de nuestro trabajo.

Por favor, [infórmenos de las reuniones a las que tiene previsto asistir](https://outreach.mayfirst.org/civicrm/event/register?id=76&reset=1).

#### Calendario de reuniones

* **Jueves, 28 de octubre, 15:00 - 16:00** (América/Nueva_York, 14:00 - 15:00 Ciudad de México, 19:00 - 20:00 UTC) \
  *¿Estás interesado en presentarte como candidato a la Junta Directiva?*\
  Si estás interesado en ayudar a dirigir May First, este taller es para ti. Compartiremos información para todas las personas de la membresía interesadas en participar en la junta directiva sobre cómo funciona el liderazgo de May First, las expectativas de la membresía de la junta directiva, la historia de la organización  y el compromiso de la organización para combatir el racismo y el sexismo. Por favor, [revise nuestrxs materiales de orientación de la junta directiva](https://mayfirst.coop/en/orientation/).
* **Miércoles, 3 de noviembre, 13:00 - 14:30** (América/Nueva_York, 11:00 - 12:30 Ciudad de México, 18:00 - 19:30 UTC) \
  *Evaluación del entorno político actual* \
  Únase a nosotrxs en un taller de colaborativo para para hablar de los aspectos más importantes del momento político actual y ayudarnos a planificar por lo que está por venir.
* **Martes, 9 de noviembre, 15:00 - 16:30** (América/Nueva_York, 14:00 - 15:00 Ciudad de México, 20:00 - 21:30 UTC) \
  *Infraestructura y servicios tecnológicos* \
  Si quiere saber más sobre nuestra tecnología y servicios actuales, ayudar a planificar cómo involucrar al movimiento en su desarrollo, y planificar nuestro desarrollo futuro, únete a nosotrxs y conoce al equipo de TIAS. De esta reunión saldrán entre 6 y 8 prioridades para el próximo año.
* **Jueves, 18 de noviembre, 13:00 - 14:30 horas** (América/Nueva_York, 12:00 - 01:30 Ciudad de México, 18:00 - 19:30 UTC) \
  *Compromiso y Comunicaciones* \
  ¿Cómo involucramos al movimiento y comunicamos la importancia de la tecnología propia del movimiento? ¿Cómo alineamos nuestrxs proyectos con el momento político actual? Ayúdanos a elaborar estrategias para el próximo año. Esta reunión producirá entre 6 y 8 prioridades para el próximo año.
* **Martes, 23 de noviembre, 15:00 - 16:00** (América/Nueva_York, 14:00 - 15:00 Ciudad de México, 20:00 - 21:00 UTC) \
  *Nueva estructura de cuotas* \
  La nueva estructura de cuotas ya está en marcha.  Si tienes preguntas sobre cómo afectará a tus cuotas, únete a nosotros en esta sesión informativa. ¿Tienes curiosidad? Puedes [revisarla aquí](https://mayfirst.coop/en/member-benefits/#new-dues-structure).

¡Por favor, [regístrese](https://outreach.mayfirst.org/civicrm/event/register?id=76&reset=1)!

### Elecciones de la Junta Directiva

Según nuestros estatutos, 5 puestos de la junta directiva son elegidos por lxs trabajadores y 20 son elegidos por la membresía. De los 20, elegimos aproximadamente un tercio cada año a términos de 3 años.

Además, reservamos un número proporcional de puestos para la membreśia que pagan cuotas en México (alrededor del 20%).

Este año, dada la proporción actual de la membresía pagando sus cuotas en México, se reservan dos puestos de la junta para las personas que pagan cuotas en México y siete puestos de la junta para el resto de la membresía.

El periodo de presentación de candidaturas a la junta directiva será del 1 al ~~23 de noviembre~~ ¡ahora ampliado hasta el 1 de diciembre! Y la elección se llevará a cabo del 3 al 10 de diciembre.

Si quieres presentar tu candidatura a la junta, asiste a la sesión de información sobre la junta (ver arriba). También puede estar interesado en revisar nuestra [materiales de rientación para la junta](/orientaci%C3%B3n).

A partir del 1 de noviembre, puedes [nominarte a ti misma o a otra persona de la membreśia de May First que haya aceptado presentarse a través de este formulario](https://vote.mayfirst.org/).

## La Reunión Principal

Por ley, tenemos que celebrar una reunión "oficial" de la membresía con la asistencia de al menos el 10% de nuestra membreśia total (unxs 65 miembros).  

**La reunión oficial de socios tendrá lugar el viernes 3 de diciembre, 15:00 - 16:00 América/Nueva_York (14:00 - 15:00 Ciudad de México, 20:00 - 21:00 UTC)**

Una vez que se haya tomado la asistencia y determinemos que tenemos quorum, podremos  hacer propuestas para la aprobación de la membresía. En la votación, los miembros individuales tienen un voto y los miembros de la organización tienen dos votos. Aunque su organización envíe a 10 personas a la reunión, sólo tendrá dos votos.

El orden del día es:

1. **Propuesta de la elección del la Junta Directiva** Esta propuesta pedirá a la membresía que aprueben la elección de la junta a través de un sistema de votación en línea de una semana para garantizar la participación del mayor número posible de personas puede participar (en lugar de solo permitir votar las personas presentes en la videollamada ). Para más detalles, véase más abajo.
2. **Informes** Habrá tres informes, todos ellos basados en las conversaciones de las reuniones de discusión entre la membresía que sucedieron previa a la reunión principal. Los informes harán hincapié en las 5 - 7 prioridades que fueron consensuadas  en cada reunión o las líneas generales de la propuesta que se presenta.
   * Informe financiero
   * Compromiso y comunicaciones
   * Infraestructura y servicios tecnológicos

## El periodo de votación

May First siempre ha hecho hincapié en la importancia de la democracia interna y no creemos que una sola reunión en línea sea la forma más inclusiva de elegir a las personas integrantes de la Junta directiva, ya que no toda la membresía podrá asistir.

Por lo tanto, la Junta directiva actual propondrá a la reunión de la membresía que realicemos las elecciones a través de una votación en línea (exactamente el mismo [sitio de votación (https://vote.mayfirst.org/) que hemos llevado a cabo las elecciones durante los últimos 6 años) durante una semana después de la reunión: Del 3 al 10 de diciembre.

En esa votación para las elecciones de la Junta, también se pedirá a la membresía que clasifiquen las prioridades que se generaron durante las reuniones de discusión.

## Detalles legales

Nuestros nuevos estatutos exigen una reunión anual de la membreśia en la que el 10% de la membresía activa constituya un quorum. Esto significa que necesitamos la asistencia de al menos 65 representantes de la membresía (el número exacto se determinará el 31 de octubre, fecha límite para la elegibilidad de lxs votantes). Para evaluar este requisito, contaremos miembros, no las personas que acudan a la convocatoria. Invitamos a la membresía a traer a tantas personas de su organización que deseen, sin embargo, sólo podemos contar cada membresía una vez al tomar la asistencia.

Este quorum es un requisito de la ley del Estado de Nueva York bajo la cual nuestra cooperativa está constituida.
