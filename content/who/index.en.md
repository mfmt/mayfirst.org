+++
date = 2015-10-01T13:52:02Z
title = "Who we are"
[menu]
  [menu.more]
    parent = "About"
    weight = 300 
+++

## Leadership 

May First Movement Technology is a democratic cooperative. Based on our [bylaws](/bylaws), the members elect 16 - 20 people to the board and the workers select five.

Representatives from Mexico are allocated based on the total percentage of their membership (approximately 20% or five members).

Each member is elected for 3 year terms. The year each member's term expires is listed below their name.

<div id="who">

  <div class="row">
  <div class="col-md-4">
    <img src="images/aaron.png">
    <div class="who-name">Aarón Moysén</div>
    <div class="who-source">Mexico, member elected, 2025</div>
  </div>
  <div class="col-md-4">
    <img src="images/alice.png">
    <div class="who-name">Alice Aguilar</div>
    <div class="who-source">US, worker selected, 2025</div>
  </div>
  <div class="col-md-4">
    <img src="images/beau.jpg">
    <div class="who-name">Beau Cromartie</div>
    <div class="who-source">US, member elected, 2025</div>
  </div>
  </div>

  <div class="row">
  <div class="col-md-4">
    <img src="images/brandon.jpg">
    <div class="who-name">Brandon Forester</div>
    <div class="who-source">US, member elected, 2026</div>
  </div>
  <div class="col-md-4">
    <img src="images/carlos.png">
    <div class="who-name">Carlos Eugenio Rodriguez</div>
    <div class="who-source">Mexico, member elected, 2027</div>
  </div>
  <div class="col-md-4">
    <img src="images/charlotte.jpeg">
    <div class="who-name">Charlotta Beavers</div>
    <div class="who-source">US, member elected, 2027</div>
  </div>
  </div>

  <div class="row">
  <div class="col-md-4">
    <img src="images/elijah.jpg">
    <div class="who-name">Elijah Baucom</div>
    <div class="who-source">US, member elected, 2027</div>
  </div>
  <div class="col-md-4">
    <img src="images/estrella.jpg">
    <div class="who-name">Estrella Soria</div>
    <div class="who-source">Mexico, member elected, 2025</div>
  </div>
  <div class="col-md-4">
    <img src="images/francisco.jpg">
    <div class="who-name">Francisco Cerezo Contreras</div>
    <div class="who-source">Mexico, member elected, 2027</div>
  </div>
  </div>

  <div class="row">
  <div class="col-md-4">
    <img src="images/hamid.png">
    <div class="who-name">Hamid Khan</div>
    <div class="who-source">US, member elected, 2025</div>
  </div>
  <div class="col-md-4">
    <img src="images/jaime.jpg">
    <div class="who-name">Jaime Villarreal</div>
    <div class="who-source">Mexico, worker selected, 2025</div>
  </div>
  <div class="col-md-4">
    <img src="images/jamie.jpg">
    <div class="who-name">Jamie McClelland</div>
    <div class="who-source">US, worker selected, 2025</div>
  </div>
  </div>

  <div class="row">
  <div class="col-md-4">
    <img src="images/jes.png">
    <div class="who-name">Jes Ciacci</div>
    <div class="who-source">Mexico, member elected, 2026</div>
  </div>
  <div class="col-md-4">
    <img src="images/jerome.png">
    <div class="who-name">Jerome Scott</div>
    <div class="who-source">US, member elected, 2025</div>
  </div>
  <div class="col-md-4">
    <img src="images/ken.jpg">
    <div class="who-name">Ken Montenegro</div>
    <div class="who-source">US, member elected, 2027</div>
  </div>
  </div>

  <div class="row">
  <div class="col-md-4">
    <img src="images/kevin.jpeg">
    <div class="who-name">Kevin Ye</div>
    <div class="who-source">US, member elected, 2027</div>
  </div>
  <div class="col-md-4">
    <img src="images/lyre.png">
    <div class="who-name">Lyre Calliope</div>
    <div class="who-source">US, member elected, 2025</div>
  </div>
  <div class="col-md-4">
    <img src="images/melanie.png">
    <div class="who-name">Melanie Bush</div>
    <div class="who-source">US, member elected, 2026</div>
  </div>
  </div>

  <div class="row">
  <div class="col-md-4">
    <img src="images/micky.jpg">
    <div class="who-name">Micky Metts</div>
    <div class="who-source">US, member elected, 2026</div>
  </div>
  <div class="col-md-4">
    <img src="images/natalie.jpg">
    <div class="who-name">Natalie Brenner</div>
    <div class="who-source">US, worker selected, 2025</div>
  </div>
  <div class="col-md-4">
    <img src="images/omar.jpg">
    <div class="who-name">Omar Olivera Espinosa </div>
    <div class="who-source">Mexico, member elected, 2025</div>
  </div>
  </div>

  <div class="row">
  <div class="col-md-4">
    <img src="images/pablo.jpg">
    <div class="who-name">Pablo Correa</div>
    <div class="who-source">Mexico, worker selected, 2025</div>
  </div>
  <div class="col-md-4">
    <img src="images/samantha.png">
    <div class="who-name">Samantha Camacho Guadarrama</div>
    <div class="who-source">Mexico, member elected, 2026</div>
  </div>
  <div class="col-md-4">
    <img src="images/santiago.jpg">
    <div class="who-name">Santiago Navarro Francisco</div>
    <div class="who-source">Mexico, member elected, 2026</div>
  </div>
  </div>

  <div class="row">
  <div class="col-md-4">
    <img src="images/shawna.jpg">
    <div class="who-name">shawna finnegan</div>
    <div class="who-source">Canada, member elected, 2025</div>
  </div>
  <div class="col-md-4">
    <img src="images/yahaira.png">
    <div class="who-name">Yahaira Zapanta-Rosales</div>
    <div class="who-source">US, member elected, 2027</div>
  </div>
  <div>&nbsp;</div>
  </div>

</div>
