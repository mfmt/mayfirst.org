+++
date = 2015-10-01T13:52:02Z
title = "Statement of Unity"
+++

*This document was written collaboratively by a group of 20 May First/People Link Members in 2006. It has been superceded by the [official documents](/en/official-documents) that were approved by the membership in November 2015. It remains here as a historical document.*

 1. We understand technology as the tools human beings have developed throughout our history to survive and move forward.
 1. We believe that people have the right to a peaceful and just world in which we have a decent life, can be productive, are free of exploitation and feel secure.
 1. We believe that we are denied that right in a world of social, political, and economic under-development, exploitation, repression, environmental destruction and war.
 1. We believe that today the conditions exist where we can finally reverse this situation and build the world we deserve.
 1. We believe that because technology is the product of collaboration by people all over the world throughout history, it rightfully belongs to all people and all people should control it.
 1. We believe the Internet is part of that technology and, as such, it should be accessible to everyone, used for everyone's benefit and controlled by all people.
 1. We believe the attempts by a few to commercialize the Internet, divert its development and restrict its use run counter to the collaborative spirit of the Internet and contradict the best interests of humanity.
 1. We believe that it is an important priority of humanity to assume control of the Internet, expand on it and use it productively.
 1. Because our beliefs and activities reflect a commitment to this idea of the world and the Internet, we believe that progressive and social justice activists must be at the forefront of the struggle to take control of the Internet, to use it fully and creatively to pursue our goals, and to expand its capabilities to enable our constantly growing and even more complex organizing and communications needs.
 1. To make this happen, we need a movement that reflects this thinking and approach to the Internet and unites all social justice activists using the Internet.
 1. With May First/People Link we want to begin and continue the creation of this movement.
