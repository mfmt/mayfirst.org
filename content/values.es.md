+++
date = 2015-10-01T13:52:02Z
title = "Valores"
slug = 'valores'
+++

Los valores fundamentales de Primero de Mayo/Enlace del Pueblo nos guían en la implementación de nuestra visión y aseguran su integridad. 

Estos son los valores que identificamos cómo críticos para nuestro trabajo: 

 * La cooperación, la colaboración y el intercambio nos ayudan a contribuir a un movimiento mucho más amplio que nuestra organización. 
 * La transparencia, apertura y honestidad nos aseguran la construcción de una organización democrática.
 * La equidad y el respeto nos guían a la hora de trabajar con la diversidad de gente necesaria para generar cambios. 
 * La convicción y la disciplina nos dan la fortaleza necesaria para seguir a pesar de las circunstancias adversas. 
 * La solidaridad, la ayuda mutua y la sustentabilidad nos aseguran que estamos construyendo nuevas maneras de trabajar juntas. 
 * La humildad, la disposición a la crítica yel  compromiso con la resolución de conflictos crean una cultura de resiliencia para nuestra organización y los movimientos en los que participamos. 

*Aprobado por la membresía en noviembre 2015, como parte de los [documentos oficiales](/es/documentos-oficiales).*
