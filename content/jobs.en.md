+++
date = 2023-03-21T12:00:00Z
title = "Job Openings at May First"
+++

## ~~We are hiring!~~

**This position has been filled.**

* **Job Title:** Membership Coordinator
* **Job Type:** Part-Time Position: 20 hours per week
* **Experience:** Entry-level
* **Location:** Remote - North, Central or South America timezones
* **Salary:** $24,000 USD per year

See [full job description below](#job-description-membership-coordinator).

Please send a cover letter and resume by **Friday, April 14th, 2023**.

Send cover letter and resume in both **Spanish and English** to: **job@mayfirst.org**

## About May First

**Mission:** May First engages in building movements by advancing the strategic use and collective control of technology for local struggles, global transformation, and emancipation without borders.

We are a non-profit, democratic, multi-stakeholder cooperative whose membership includes a diverse range of social justice and liberation oriented movement organizations and individuals including international NGO's, small local orgs, unions, independent collectives, organizers and activists. Membership is mostly based in the US with roughly 20% of membership based in Mexico and another 5% spread throughout the world. May First's work focuses on two interrelated program areas:

* Engaging the broader left movement in conversation about the critical role of technology in our fight for a more just world.
* Collectively running our own Internet-based technology infrastructure and services for our members to provide privacy and stability for critical movement activities and communications. These services include website and e-mail hosting, "cloud" style online file sharing and collaborative office suite, videoconferencing, and more.

### May First Values

* cooperation, collaboration and sharing
* transparency, openness and honesty
* equality and respect
* conviction and discipline
* commitment to fighting racism, sexism and exploitation
* solidarity, mutual aid and sustainability
* humility, openness to criticism and commitment to conflict resolution

## Job Description: Membership Coordinator

### Overview

The Membership Coordinator will serve as a primary point of contact for all May First members, help plan and coordinate both internal and public events and communications, and actively participate as a contributing member of the staff, program teams, and coordination committee in support of the organization's overall mission. Our ideal candidate combines strong communication skills with a passion for leveraging technology for social justice and liberation.

### Responsibilities

* Serve as primary contact for members via email correspondence and member web forum
* Author and edit membership communications and oversee membership outreach
* Answer questions about membership subscription and benefits
* Process new member registrations and assist with new member welcome and onboarding via the welcome committee
* Manage membership payments and other day to day administrative tasks
* Follow up with members who are behind in their dues
* Participate in coordination of annual membership meeting and related communications
* Participate in recruitment and engagement campaigns with prospective members
* Manage administrative tasks of the organization
* Work closely with staff and program teams to help plan both internal and public events and communications
* Manage social media accounts (e.g. Mastodon)
* Author and edit public e-mail blasts, event invitations, calls to action

### Critical Qualifications (Skills and Attributes)

* Movement experience required - applicants will be asked to describe their experience as an organizer or activist and the role technology has played in their previous work
* Basic technology experience or knowledge is required (e.g. comfort using online meeting and collaboration platforms and learning new technology tools).
* Specialized or advanced technology skills are not required, however, a willingness to learn and use free software alternatives for day to day tasks and to learn some advanced technology skills in order to better collaborate with members is an important part of the job.
* Experience working in racial, cultural, and gender diverse environments is a plus and a critical skill for being successful in this position.
* Excellent written and verbal communication skills in both English and Spanish.

### Additional Qualifications

* Already being an individual or organizational member of May First is not required but a plus
* Excellent interpersonal and communication skills
* Exceptional writing and editing skills; ability to translate complex ideas and strategies into clear, concise, and compelling narratives
* Social media management experience
* Experience with CiviCRM and Nextcloud software platforms is a plus
* A collaborative spirit paired with ability to work proactively and independently
* Strong organizational, analytical, problem solving, and time management skills demonstrated through previous employment, academic or practical experience
