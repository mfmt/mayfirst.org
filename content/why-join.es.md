+++
date = 2015-10-01T13:52:02Z
title = "¿Por qué deben unirse a PMEP?"
slug = 'porque-unirse'
[menu]
  [menu.more]
    parent = "Membresía" 
    weight = 20
+++

Hoy en día, el trabajo en Internet es de vital importancia para personas y organizaciones por igual. Cualquier persona u organización responsable debe pensar bien antes de elegir un Proveedor de Servicios de Internet (ISP, por sus siglas en inglés) o bien antes de trasladar sus servicios a un proveedor distinto. Es por eso que aquí ofrecemos algunas razones por las que creemos que es muy importante que te unas a Primero de Mayo/Enlace del Pueblo.

1. **El Internet somos tod@s nosotr@s**

    Más que sólo tecnología, el Internet es una red masiva de personas que utilizan las computadoras para comunicarse entre sí. Internet sufre cambios cada segundo del día, en tamano y en el caracter de las comunicaciones que por él se transmiten. Y tiene una cultura, una historia y un futuro de gran vitalidad:

    Su cultura se basa en la libertad de comunicación: es un lugar donde todo el mundo con una opinión o una historia, puede expresarla.

    Su historia es de colaboración donde todas las tareas principales para su funcionamiento, se conducen mediante software y estándares libres, desarrollados por grandes grupos de voluntari@s que trabajan junt@s y que de la misma manera colaboran, dich@s voluntari@s en su mayoria nunca se han conocido personalmente.

    Y su futuro es contribuir a los cambios sociales. El Internet ha desempeñado un papel fundamental en la lucha contra la desinformación, en la expresión de ideas nuevas, en el conocimiento de la verdad histórica y en reunir a la gente que tiene objetivos en común. Es una de las herramientas más potentes de comunicación con las que cuenta el movimiento que lucha por la justicia social.

    En pocas palabras, es nuestro y debemos estar usándolo y desarrollando sus recursos al máximo. Es por eso que estamos construyendo Primero de Mayo/Enlace del Pueblo.

2. **Tenemos que proteger el internet**

    Muchos proveedores comerciales y los intereses corporativos no quieren que Internet pertenezca a tod@s. Para ellos, Internet es una forma de vender sus productos y servicios y con ese enfoque tratan de imponer condiciones y restricciones al uso de Internet y al desarrollo de las tecnologías relacionadas con la red global.
Nuestro movimiento tiene que resistir los intentos de dichas empresas a bloquear el correo electrónico con contenido político en el nombre de "la lucha contra el spam", tambien oponerse a sus intentos por priorizar la transmisión de información comercial en detremento de la transmisión de contenidos no-comerciales y a cualquier otra forma de limitar el potencial de Internet para distribuir información vital para nuestros movimientos y organizaciones.

    Pero más que eso, tenemos que mejorar nuestras capacidades y habilidades para desarrollar las tecnologías de Internet y ampliar y profundizar el uso de ellas: herramientas que mejoren las comunicaciones de nuestros movimientos y nuestra organización y nos ofrezcan mejores métodos de servicio.

    Es momento de unirnos para proteger al Internet de aquellos que quieren desviarlo, restringirlo y controlar su uso.

    Creemos que una manera de hacerlo es a través de Primero de Mayo/Enlace del Pueblo.

3. **Somos parte del mismo movimiento.**

    Aunque pensamos que la calidad de nuestros servicios y personal pueden competir con la de cualquier proveedor comercial, Primero de Mayo/Enlace del Pueblo no es una empresa o un Proveedor de Servicios de Internet tradicional, por lo que ustedes no son vist@s como "clientes potenciales".

    Somos una organización que busca la justicia social, formada por activistas y organizaciones que utilizan Internet. Estamos estructurados de esa manera, se trabaja de esa manera y se planifica nuestro trabajo de esa manera.

    Tambien somos conscientes de que su trabajo en Internet es un trabajo político. El uso de sitios web, correo electrónico y otras herramientas es, en efecto, tan necesario como todos los otros trabajos que realizan.

    Y es por eso que defendemos activamente el uso de Internet por parte del movimiento por la justicia social y nos comprometemos a crear un importante sistema de comunicaciones para dicho movimiento.

    Como la derecha política aprendió hace años ya: nunca tendremos éxito sino creamos nuestra propia infraestructura de comunicaciones.

    Primero de Mayo/Enlace del Pueblo y ustedes somos parte del mismo movimiento.

4. **Ustedes toman el control**

    Todo lo que hacemos enpodera a l@s usuari@s miembros: ustedes estan en control de sus sitios web, sus cuentas y listas de correo electronico y de cualquiera de las herramientas en linea que ustedes utilizan. Nuestro papel es desarrollar estas herramientas, facilitar su uso y proporcionar apoyo y asistencia cuando nos necesiten.

    Por otro lado, trabajamos con coaliciones, organizaciones y activistas en todos los EU, Guatemala, Bolivia y proximamente en Mexico para planificar el diseño de los nuevos tipos de herramientas de Internet que el movimiento necesita para avanzar y encontrar la forma de desarrollarlas e implementarlas.

    ¿Cómo lo estamos haciendo? Echa un vistazo a nuestros estudios de caso para ver algunos ejemplos.

5. **Ofrecemos un excelente servicio**

    Quizas la mejor razón que podemos ofrecer para unirse a Primero de Mayo/Enlace del Pueblo es el proveedor que están utilizando actualmente.

    Tome un momento para comparar aquello con lo que actualmente cuenta y lo que ofrecemos. Creemos que usted encontrara que:

    Nuestros paquetes de membresía ofrecen a l@s miembros, mayores márgenes de acción en el terreno tecnológico, más opciones y más herramientas que nadie por el mismo dinero en la industria ISP.

    Nuestro soporte técnico es superior: disponible cuando lo necesiten, con personal que realiza su trabajo directamente en nuestra red, que cuenta con sitios web alojados aquí y que usa nuestros sistemas de listas y correo electrónico.

    Nuestro equipo y la tecnologia que usamos son tan buenos o mejores, que las opciones comerciales: estables, potentes y fiables, con conexiones y datos respaldados y trasladados a servidores remotos todos los dias.

    Nuestro servicio es radicalmente diferente porque somos activistas del movimiento social y nos relacionamos con ustedes de esa manera.

    Nuestros estudios de caso describen la forma en la que trabajamos con la gente. Pónte en contacto con algunos de nuestros miembros y pregúntales sobre sus experiencias con nosotros.

    !Piensa en ello! ¿Es eso lo que están recibiendo ahora con su actual proveedor?

6. **Somos conscientes de la importancia de la seguridad informática**

    Para los activistas del movimiento por la justicia social, la seguridad es una necesidad muy real y a menudo de caracter crítico. Como activistas, sabemos lo importante que es la seguridad informática, le damos prioridad a ello en la planeación de nuestras actividades y en la aplicación de medidas de seguridad que protegen sus datos, su identidad y acceso. Creemos que nuestro enfoque sobre la seguridad y acceso se encuentra entre lo más completos en Internet y algunas de las medidas que tomamos son unicas.

7. **Tratamos de practicar lo que predicamos**

    Primero de Mayo/Enlace del Pueblo está comprometida con un entorno respetuoso con el trabajo y la dignidad de las personas que aqui laboran. En la forma de tomar decisiones, en el hecho de que nos encontramos en un local sindical, en los entornos físicos en los que trabajamos y en el trato entre nosotr@s, en todo ello intentamos llevar a la practica, los principios de la convivencia humana por los que luchamos para todo el mundo. Ese enfoque no-alienado del trabajo que realizamos se aplica a la manera de tratar a l@s miembros, ya sea cuando brindamos soporte técnico, ayuda en los problemas propios del servicio o simplemente cuando damos asesoría. Te tratamos con el respeto debido como a cualquier persona y especialmente por ser un/a companer@ activista, y esperamos -y por lo general obtenemos- ese trato de nuestr@s miembros.

    Este enfoque tambien conduce nuestra seleccion de personal y de "asociados" proveedores de servicios de conexión. Estamos trabajando para crear un grupo diverso de trabajadores/as de Internet que reflejen la trayectoria y experiencia de tod@s nuestr@s miembros. Esa es una de las metas que siempre perseguimos cumplir.

8. **Que el dinero del movimiento se invierta en el movimiento**

    Ustedes probablemente ya estan en Internet con un proveedor de servicios. Así que aquí está una pregunta para tí:

    Supón que hay otro "proveedor" que les puede dar servicio, precio, y el mismo apoyo -si no es que con mejor calidad- que el que ustedes reciben de su proveedor actual.

    Y supón que este otro "proveedor" no sólo comparte su compromiso y sus objetivos, sino que además trabaja con ustedes para promover su trabajo.

    Por último, decir que este otro "proveedor" esta construyendo el tipo de organizacion de Internet que ustedes desean ver y que necesita su apoyo para seguir haciéndolo.

    ¿Cuál de los dos elegirías? Primero de Mayo/Enlace del Pueblo es la mejor opción.

9. **Los necesitamos**

    Nada de esto puede funcionar sin ustedes. Ninguna organizacion puede crecer sin atraer nuevos miembros y la nuestra requiere apoyo para sostener al personal remunerado, los equipos, líneas de Internet y las instalaciones que utilizamos.

    Todo ello proviene de la membresía. No buscamos financiamiento por parte de fundaciones u otras donaciones. Lo que hacemos es posible sólo a traves de las cuotas anuales de los miembros. Para que podamos seguir adelante y seguir creciendo, te necesitamos ahora.

    Las posibilidades de crecer y desarrollar nuestras capacidades se dan en la medida en que mas organizaciones se adhieren. Con cada nueva membresia se incrementa aquello que podemos hacer para cada un@.

    Esas son nueve razones por las que creemos que ustedes deben ser parte de Primero de Mayo/Enlace del Pueblo. Si no les parecen convincentes, utiliza esta vía de comunicación para decirnos por qué y vamos a tratar de responder a tus inquietudes o preguntas.

    Si simpatizan con estas ideas y este trabajo, por favor vayan a la sección ¡Unete! ahora! Usted no tiene que cambiar de proveedor en este momento para ser miembro de Primero de Mayo/Enlace del Pueblo. Sólo queremos hacerles parte de nuestra organización y les asistiremos cuando estén list@s para ese cambiar de proveedor.

