+++
date = 2020-10-03T14:47:48Z
description = "Priorities"
title = "Priorities"

+++

The 2020 membership meeting approved and ranked the following priorities for 2021:


 * 520 Votes: Improve the economic sustainability of the organization
 * 506 Votes: Complete development and implementation of new technology infrastructure
 * 432 Votes: Expand work of bringing organizations and activists into May First while spreading our thinking within the rest of our movements
 * 423 Votes: Facilitate participation of May First members in developing and improving our technology and infrastructure
 * 390 Votes: Develop a program of collaborative work between our members in the U.S. and Mexico focusing on issues affecting both countries
 * 382 Votes: Explore options to make our data center operations more environmentally sustainable
 * 366 Votes: Develop a social media policy that addresses work on and education about current social media forms and begins to organize the development of alternative social media practices and programs
 * 356 Votes: Facilitate member to member communication
 * 298 Votes: Explore alternatives to the Internet - 298

Previous year priorities:

 * [2016](/priorities-2016)
 * [2017](/priorities-2017)
 * [2018](/priorities-2018)
 * [2019](/priorities-2019)
 * [2020](/priorities-2020)
