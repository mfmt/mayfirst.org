+++
date = 2020-10-02T14:47:48Z
description = "Welcome to the May First Movement Technology Members' Meeting"
title = "2020 Members' Meeting and voting process"
tags = [ "callout" ]
+++

## Calendar of Dates and Meetings

### Summary

**Welcome the official 2020 May First Membership meeting page!**

As we witness the implosion of the presidency, dangerous climate change and the
rise of fascism, the role of the corporate technology titans becomes clearer
and more complicit every day. 

So, what makes May First different?

It's not just the politics we espouse, or the movement building we are
committed to, or even the free and open source software we tireless maintain:
May First is unique in our commitment to democratizing ownership of our
technology. **We are here to fundamentally change the movement's relationship
to technology.**

And that's where you, a May First member, have an important role to play!

We need every May First member to participate in our annual membership
meetings, even if it's the only thing you do for May First all year. We need
you to hear our reports and evaluations on how well well we met our goals this
year and to help shape our plans for next year based on the constantly changing
political environment. These are unprecedented times - so we need all the input
we can muster to make the right decisions moving forward.

Our membership meetings will take place from mid October through mid December
and will include online meetings, forum discussions and a one week voting
period.

You can register for all events on our [event registration
page](https://outreach.mayfirst.org/civicrm/event/register?id=67&reset=1).  

Please take a look at our schedule and *commit to attending at least one
discussion meeting and the main membership meeting* in December.

See our [infographic](/img/2020-membership-meeting-en.jpg) for the full details or read on below.

[![Info graphic](/img/2020-membership-meeting-en-thumb.jpg)](/img/2020-membership-meeting-en.jpg)

### Discussion Meetings

*All meetings are scheduled for 1 hour.*

We will hold the following six discussion meetings. Members are welcome to
attend all meetings, or you can attend just the meetings most relevant to your
organizing work. 

Each meeting will take place online via the audio app
[mumble](https://support.mayfirst.org/wiki/mumble) and also have a
corresponding forum discussion via [discourse](https://comment.mayfirst.org/)
for people unable to attend the meeting or who wish to continue the discussion.

Additionally, written reports evaluating our work this year have been prepared.

Please [review all our reports](https://share.mayfirst.org/s/ZrsjayExTpAMpdo).
NOTE: All reports with "en" are in english and the ones with "es" are the
spanish versions.

Please [let us know which meetings you plan to attend](https://outreach.mayfirst.org/civicrm/event/register?id=67&reset=1).

#### Schedule of Meetings

*All meetings are scheduled for 1 hour.*

 * *Friday, October 23, at 1:00 pm America/New_York:* **Assessing the current
   political environment** The intended outcome will be 5 - 7 bullet points
   that describe the most important aspects of the current political situation
   for us to consider while planning our future work. Please [review the
   political environment report](https://share.mayfirst.org/s/ZrsjayExTpAMpdo)
   and participant in the [forum
   discussion](https://comment.mayfirst.org/t/political-environment/1848).

 * *Tuesday, October 27, at 3:00 pm America/New_York:* **Technology
   Infrastructure and Services** The intended outcome will be 5 - 7 bullet
   points describing our organization's priorities in building our
   infrastructure. Please [review the infrastructure and services
   report](https://share.mayfirst.org/s/ZrsjayExTpAMpdo) and participate in the
   [forum
   discussion](https://comment.mayfirst.org/t/technology-infrastructure-and-services/1849).

 * *Friday, November 6, at 4:00 pm America/New_York time:* **Movement
   priorities** The intended outcome will be 5 - 7 bullet points describing our
   organization's movement priorities for 2021. Please [review the movement
   report](https://share.mayfirst.org/s/ZrsjayExTpAMpdo) and participate in the
   [forum discussion](https://comment.mayfirst.org/t/movement-priorities/1850). 

 * *Tuesday, November 10, at 1:00 pm America/New_York* **Proposal on new dues
   structure** The intended outcome will be a discussion to ensure everyone
   understands the proposal and to consider proposed changes before a final
   proposal is put to a vote by our membership. [Forum
   Discussion](https://comment.mayfirst.org/t/new-dues-structure-proposal/1851).

 * *Friday, November 13, at 2:00 pm America/New_York* **Board Member
   Orientation for anyone who is considering a run for the Board of Directors**
   If you are interested in helping direct May First, this workshop is for you.
   We will share information for all prospective board members about how May
   First's leadership operates, expectations of board members, the history of
   the organization and the organization's commitment to combatting racism and
   sexism. Please [review our board orientation
   materials](https://mayfirst.coop/en/orientation/) and participate in the
   [forum
   discussion](https://comment.mayfirst.org/t/new-board-member-orientation/1852).

 * *Wednesday, November 18, at 1:00 pm America/New_York.* **The coop structure
   and membership involvement** The intended outcome will be 5 - 7 bullet
   points describing our organization's priorities related to membership
   involvement. Please [review our coop and engagement
   report](https://share.mayfirst.org/s/ZrsjayExTpAMpdo) and participate in our
   [forum
   discussion](https://comment.mayfirst.org/t/coop-and-membership-engagement/1853).


Please [register](https://outreach.mayfirst.org/civicrm/event/register?id=67&reset=1)!

### Board Elections

According to our bylaws, 5 board seats are chosen by workers and 20 are
elected by the membership. Of the 20, we elect approximately 1/3 each year for
3 year terms. 

In addition, we reserve a proportionate number of seats for members who pay
dues in Mexico (about 20 percent).

This year, given the current ratio of Mexican members, one board seat for a
member paying dues in Mexico and six board seats for all other members are up
for election. 

The board nomination period will run from November 1 through November 30th. And
the election will run From December 1 - December 8th.

If you would like to run for the board, please attend the board orientation
(see above). You may also be interested in reviewing our [board orientation
materials](/orientation).  

When you are ready, you can [nominate yourself or another May First member who
has agreed to run via this form](https://vote.mayfirst.org/).

## The Main Meeting

By law, we have to hold an "official" membership meeting with attendance by at
least 10% of our members (about 65 members).

**The official membership meeting will take place on Tuesday, December 1, at
3:00 pm America/New_York**

Once attendance is taken and we have determined that we have quorum, we will be
able to make proposals for member approval. When voting, individual members get
one vote and organizational members get two votes. Even if your organization
sends 10 people to the meeting, you still only get two votes.

The agenda is:

1. **Proposal on Board elections.** This proposal will ask the members to approve
   electing the board through a one week online ballot system to ensure as many
   members as possible can participate (rather than only allowing the members
   present on the call to vote). See below for more details.

2. **Reports.** There will be four reports, all based on the conversations at
   the member consultation meetings that precede the Members' meeting. The
   reports will emphasize the 5 - 7 priorities each meeting agreed consensed
   on or the general outline of the proposal being put forward.

   * Movement Work
   * Technology Infrastructure
   * Coop status and membership engagement
   * New dues proposal (this proposal will include changes to our bylaws)


## The Voting Period 

May First has always emphasized the importance of internal democracy and we
don't feel a single online meeting is the most inclusive way to elect Board
members since not all members will be able to attend.

So the Board will propose to the membership meeting that we conduct the
elections via an online ballot (the exact same [voting
site](https://vote.mayfirst.org/) we have conducted our elections for the last
6 years) for a week following the meeting: December 1 through December 8th. 

In that Board elections ballot, members will also be asked to rank the
priorities that were generated during discussion meetings.

Additionally, the members will be asked to approve or reject a proposed change
to the bylaws regarding membership dues.

## Legal Details 

Our new bylaws require a yearly meeting of members at which 10 percent of our
active membership makes up a quorom. This means we need attendance by at least
65 members (the exact number will be determined on October 31st, the cut-off
date for voter eligibility). To evaluate this requirement, we will be counting
memberships, not people on the call. We invite members to bring as many people
from your organization as you want, however, we can only count each membership
once when taking attendance.

This quorum is a requirement of New York State law under which our cooperative
is incorporated.

