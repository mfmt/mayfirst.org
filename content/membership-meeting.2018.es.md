+++
date = 2018-09-13T14:47:48Z
title = "Reunión de Miembros 2018"
tags = [ "callout" ]
+++

¡La reunión de membresía de 2018 ha terminado!

Gracias a todas las personas que participaron, en particular al número récord de miembros que votaron en nuestras elecciones.

### propuestas

Las tres propuestas pasaron por amplios márgenes:

 * [Convertirnos legalmente a una cooperativa](https://comment.mayfirst.org/t/mm-2018-form-a-committee-to-explore-converting-our-organization-to-a-multi-stake-holder-cooperative/910) (Sí: 160, No: 11)
 * [Ampliar significativamente la organización](https://comment.mayfirst.org/t/mm-2018-form-a-committee-to-explore-ways-to-significantly-grow-the-organization-to-a-more-sustainable-size/911) (Sí: 162, No: 9)
 * [Posponer las elecciones del Comité de Dirección](https://comment.mayfirst.org/t/mm-2018-postpone-leadership-committee-elections-until-we-vote-on-coop/912) (Sí: 136, No : 35)


