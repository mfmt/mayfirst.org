+++ 
title = "The Organic Internet" 
date = 2021-05-12T13:53:34Z
[menu]
  [menu.more]
    parent = "About"
    weight = 900

+++

The *Organic Internet* was published in 2007, two years after May First
Movement Technology (then known as May First/People Link) was born. It provided
our initial analysis of the relationship between the movement and the Internet
and guided our strategy for the years that followed. 

[Download the PDF version](/files/organicinternet.1.5.pdf)
