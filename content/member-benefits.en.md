+++
date = 2015-10-01T13:52:02Z
title = "Membership Benefits"
[menu]
  [menu.more]
    parent = "Membership"
    weight = 20
+++

<a id="hosting-services"></a>

### Access to Technology Infrastructure and Services

Our autonomous technology infrastructure is as a shared community resource for use by our members
and is supported entirely by membership dues.

Click on any of the following sections to see more details:

<details>
  <summary><h4>List of Available Services</h4></summary>
  <p>May First members who take advantage of our <strong>hosting plans</strong>
 receive access and support for the following list of online hosting services
 maintained by our technical staff.</p>
  <ul class="accordion">
  <li>
    <details>
      <summary>Web Site Hosting:</summary>
      <p>We provide automated installations of website content management systems like
      Wordpress, Drupal, and Backdrop. Other popular website software can be manually
      installed or migrated to our shared servers.</p>
    </details>
  <li>
    <details>
      <summary>Databases:</summary>
      <p>MySQL and Postgres database access.</p>
    </details>
  <li>
    <details>
      <summary>E-mail:</summary>
      <p>E-mail accounts for you own domain accessible through our webmail interface or
  by most e-mail software apps and devices using standard protocols (pop, imap,
  smtp).</p>
    </details>
  <li>
    <details>
      <summary>File Sharing and Collaboration:</summary>
      <p>Our Nextcloud instance facilitates file sharing, collaborative document
      editing, shared calendars, contacts, and tasks between groups as well as
      syncing data between your own devices.</p>
    </details>
  <li>
    <details>
      <summary>Virtual Events and Meetings:</summary>
      <p>Our Jitsi Meet, Mumble and Icecast servers provide solutions for virtual web
      meetings and live event streaming, All of our live meeting platforms include
      tools for live language interpretation.</p>
    </details>
  <li>
    <details>
      <summary>Mailing Lists:</summary>
      <p>We provide automatic setup of discussion or broadcast style
      mailing lists through the highly configurable Mailman listserv.</p>
    </details>
  <li>
    <details>
      <summary>Secure Chat:</summary>
      <p>Our XMPP/Jabber server can be used for provide real time encrypted chat with
      accounts at any compatible XMPP server and is compatible with multiple FOSS
      chat programs available for your computer and smartphone.</p>
    </details>
  </ul>

  For a more complete description of these and additional services, please see our [services](/en/services).
</details>

<details>
  <summary><h4>Security and Privacy</h4></summary>
  <p>Built entirely on free and open source software and maintained
  with the security concerns and features needed by activists groups and organizers,
  our services are designed to ensure that essential movement work and communications
  remain free of surveillance and censorship.</p>
</details>
<details>
  <summary><h4>Usage Limits</h4></summary>
  <p>Technology resources derived from the physical material of the earth should
  never be treated as unlimited and we prefer not use that word to describe our
  services. We place limits on total disk space usage but otherwise lean towards
  abundance and believe we have plenty of resources to go around. Members are
  allowed to create as many accounts, websites, databases, and lists as needed
  for their own projects. Reselling of May First infrastructure and services to
  third parties is prohibited.</p>
</details>

<details>
  <summary><h4>Technical Details</h4></summary>
  <p>For the technically curious - our shared hosting infrastructure supports all
  common programming languages (PHP, perl, python, ruby, etc), the most common
  databases (MySQL, Postgres, SQLite), un-restricted ssh and Secure FTP access,
  webmail via the Roundcube web mail server as well as full, encrypted access via
  IMAP or POP, the Mailman email list software, access to our bulk mail relay
  system (which sends hundreds of thousands of email messages daily), logins for
  our shared ownCloud server, accounts on our shared XMPP (Jabber) service, and
  many more services. Our members run Drupal, WordPress, Joomla, Wikimedia,
  CiviCRM and many other content management systems and databases. All servers
  run the Debian stable operating system.</p>
</details>

<hr>

<a id="new-dues-structure"></a>
<a id="dues-structure"></a>

### Hosting Plans and Membership Dues

 Our cooperative has adopted a unique approach to calculating hosting "costs"
 compared to traditional hosting providers. We use a sliding scale system based
 on each members' self reported income range and total resource usage to
 determine total membership dues. Try our online [Membership Dues
 Calculator](https://outreach.mayfirst.org/civicrm/public/calculate#/mayfirst/calculate)
 to explore interactively how much you would be expected to pay using different
 hosting plans and income range levels.

Click on any of the following sections to see more details:

<details>
<summary><h4>Basic Membership</h4></summary>

 A $50 US dollar annual **basic membership** fee is the minimum contribution
 necessary to join our cooperative with full member voting rights. Basic
 membership includes the benefits of one mail.mayfirst.org email address, one
 Nextcloud account and XMPP/Jabber chat account with 10GB of storage between
 them all. Members can upgrade from basic membership to one of our <strong>full
 hosting plans</strong> at any time.

</details>

<details>
<summary><h4>Full Hosting Benefits and Costs</h4></summary>

 Members who take advantage of our full hosting plans receive access to all of
 our services, multiple accounts, and larger amounts of storage space.

<table class="table">
  <tr>
    <th>Plan</th><th>Total annual dues (US dollars)</th><th>Storage limit</th>
  </tr>
  <tr>
    <td>Basic</td><td>$50.00</td><td>10G</td>
  </tr>
  <tr>
    <td>Plan 1</td><td>$62.50</td><td>40G</td>
  </tr>
  <tr>
    <td>Plan 2</td><td>$125.00</td><td>100G</td>
  </tr>
  <tr>
    <td>Plan 3</td><td>$250.00</td><td>232G</td>
  </tr>
  <tr>
    <td>Plan 4</td><td>$500.00</td><td>500G</td>
  </tr>
</table>

Access to the above plans is conditioned by an income based sliding scale criteria.<br>
Click on any of the following questions to see more details:

  <details>
    <summary><strong>How does the income based sliding scale work?</strong></summary>

 Under our system access to a **hosting plan levels** is dependent on each
 member's self reported **income range level**. Individuals can use their
 personal annual income and organizations can use their annual operating budget
 to determine their income range level. Smaller hosting plans are available to
 members with lower income but members with higher income must choose a larger
 hosting plan.

 In the graphic below, income range levels are represented by different color
 horizontal lines. The "entry points" indicate the first hosting plan available
 for that income range level.

<a href="/img/dues-subway-map-en.png"><img class="img-responsive" alt="New
 membership dues map infographic" src="/img/dues-subway-map-thumb-en.png"></a>

  </details>

  <details>
    <summary><strong>How do I calculate my Income Range Level?</strong></summary>

 The following table details our established member income range levels based on
 annual income or organizational budget in USD. Individuals should use their
 personal annual income and organizations will use their annual operating budget
 to determine their **income range level**.

  <table class="table">
    <tr>
      <th>Income Range</th><th>Annual Income or Organizational Budget in (US dollars)</th>
    </tr>
    <tr>
      <td>Income Range 1</td><td>less than $25,000</td>
    </tr>
    <tr>
      <td>Income Range 2</td><td>between $25,000 and $150,000</td>
    </tr>
    <tr>
      <td>Income Range 3</td><td>between $150,000 and $500,000</td>
    </tr>
    <tr>
      <td>Income Range 4</td><td>over $500,000</td>
    </tr>
  </table>

  </details>
  <details>
    <summary><strong>What if I need more storage space?</strong></summary>
    <p>Your <strong>hosting plan level</strong> begins at your equivalent
    <strong>income range level</strong> but is not limited to it. All members can
    incrementally add additional storage space or select a higher hosting plan
    level and contribute more dues accordingly but members cannot scale down to a
    hosting plan level below their income range level.</p>
  </details>
  <p>
</details>

<details>
<summary><h4>Dedicated Virtual Private Servers</h4></summary>

 For members who wish to administrate their own virtual private servers and
 software we can provide two options:

* Semi-managed VPS - we can provide some basic monitoring and help with system upgrades
* Unmanaged VPS - completely private

A VPS can only be added in combination with a hosting plan.

Each virtual private server includes:
* 25 GB system disk
* 1 CPU core
* 1 GB of RAM

The base monthly price for each virtual private server is also dependent on member
income range level.

<table class="table">
  <tr>
    <th>Base Income Range</th><th>Base Monthly dues for VPS</th>
  </tr>
  <tr>
    <td>Income Range 1</td><td>$45.00</td>
  </tr>
  <tr>
    <td>Income Range 2</td><td>$70.00</td>
  </tr>
  <tr>
    <td>Income Range 3</td><td>$120.00</td>
  </tr>
  <tr>
    <td>Income Range 4</td><td>$220.00</td>
  </tr>
</table>

Members can add additional CPU cores, RAM and/or SSD storage at additional cost.

* **CPUs**: $10/per month/per CPU (Max per VPS: 4)
* **RAM**: $10/per month/per 1GB (Max per VPS: 8)
* **Virtual Solid State Drives**: $20/per month/per 10GB (Max per VPS: 20GB)

Extra data storage space can be provisioned for your VPS from your selected
hosting plan's total disk space allotment at **no extra cost**. Please indicate
how much of that storage you would like dedicated to your vps in your instructions.

If you have more then one virtual private server, you can mix and match your
allocation of all of the above resources as you wish.
</details>

<details>
  <summary><h4>Payment Methods</h4></summary>
  <p>All membership dues are calculated in US dollars and can be paid via PayPal or
  Credit Card. <strong>Members based in Mexico</strong> who require a Mexican
  fiscal receipt can choose to pay in Mexican pesos via bank deposit. An exchange
  rate of Mexican pesos to the dollar will be calculated based on current market
  rates.</p>
</details>

<hr>

<a id="movement-work"></a>

### Benefits of Our Movement Engagement Work

We affirm that our political work engaging global movements in conversation about
the role of technology in our fight for a more just world is of significant benefit
to our members and the broader left movement.

<details>
<summary><h4>The Basis of our Movement Work</h4></summary>

We believe that movements for fundamental change must prioritize the use,
protection and democratization of technology. We want our movements' leadership
to think about those goals and develop a unified plan to make them happen.
That's the basis of our work and it makes May First one of the most active and
prominent technology organizations in the Left of this country.

</details>

<details>
<summary><h4>Coalition Work and Gatherings</h4></summary>

As active supporters of the social forum and free and open media and technology
coalitions, May First Movement Technology contributes our political thinking and
technology skills to these important areas.

We are a leading organization of the <a
href="https://mediajustice.org/network/">Media Justice Network</a>; a founder
and of the <a href="https://radicalconnections.net">Radical Connections
Network</a>; a long-time member of the <a href="https://apc.org/">Association
for Progressive Communications</a>; a member of several new economy coalitions
and networks and we have been involved in many coalitions and networks set up
to deal with specific issues and struggles. We participate in many conferences
and major convergences in the course of the year in the U.S., in Mexico and
internationally. Our representatives will participate in over 20 such events
during any year.

Our unique and unprecedented <a href="https://techandrev.org">Technology and
Revolution</a> series has brought together over 1500 movement leaders and
activists in the U.S. and Mexico in 24 convergences since 2017 to think and
talk about the intersection of technology and revolution and to forge a program
of work we can do around technology.
</details>

<details>
<summary><h4>Defending Our Members and their Data</h4></summary>

That commitment to movement work is also seen in our commitment to the
protection of our member's data. Our members are <a
href="https://support.mayfirst.org/wiki/legal">frequently hit</a> with demands
for data from governments, take-down notices from corporations or Denial of
Service attacks from political enemies. We resist them all. We have
successfully resisted denial of service attacks on members working on
reproductive justice, Palestinian rights, and the Black Lives Matter movement.
We refused to give the FBI details when it demanded payment information that
would have identified the members of an Independent Media Center site in
Greece. We have consistently refused to take down satire websites by members
such as the Yes Men, which corporations consider trademark violations.</p>

But those are only the most dramatic instances because, day to day, we fiercely
protect data through encryption, back-up and monitoring. Our members' data is
our movements' tool and our movements are our priority.

</details>

<details>
<summary><h4>Giving Back to the Free Technology Movement</h4></summary>

 We also contribute to the development of new technologies that are consistent
 with our political vision of a free and open Internet, including support for
 free and open source software and open network and data standards.

</details>

<details>
<summary><h4>Our Purpose</h4></summary>

The purpose of this work is to help our movements build a new relationship with technology.
We want our movements to think about technology, develop a program to protect and expand it
and actually work on that program. We want to be a platform for that kind of
collaboration: the democratic control and expansion of technology as a tool for
human survival and the struggle to make that happen. We want to build that and
we want to model it.

</details>

<hr>
