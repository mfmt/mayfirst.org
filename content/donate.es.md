+++
date = 2015-10-26T13:52:02Z
title = "Donar"
slug = 'donar'
[menu]
  [menu.more]
    parent = "Acerca de Nosotros"
    weight = 999 
+++

May First Movement Technology is a tax-exempt 501(c)(3) nonprofit organization. Contributions to May First are tax-deductible to the fullest extent allowed by the law.
## Make a donation online

[Donate!](https://outreach.mayfirst.org/civicrm/contribute/transact?reset=1&id=1)

## Make a donation by mail

Please make your check payable to May First and mail it to:

    May First Movement Technology
    440 N BARRANCA AVE #4402
    COVINA, CA 91723
