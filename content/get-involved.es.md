+++
date = 2022-01-27T13:53:34Z
title = "Involúcrate"
[menu]
  [menu.more]
    parent = "Membresía"
    peso = 50
+++

## ¿Todavía no eres miembro?

Si todavía no eres socio, ¿por qué no [te unes ahora](/joinus)?

Si aún no estás preparado para unirte, no dudes en [inscribirte en nuestra lista de correo](https://outreach.mayfirst.org/civicrm/profile/create?gid=2&reset=1).

## ¿Ya eres miembro?

¡Genial! Si quieres participar más, tenemos dos equipos de trabajo que se reúnen regularmente.

Ambos equipos apoyan nuestra misión de involucrar a nuestros miembros y al movimiento en general
en la construcción y el mantenimiento de la tecnología autónoma para la organización para que podamos ganar
nuestras luchas por la liberación.

 * **El Equipo de Tecnología, Infraestructura y Servicios (TIAS)** se centra en las
   herramientas, incluyendo el desarrollo técnico así como la documentación, la formación
   y la planificación. Consulte la [invitación completa a
   unirse](https://comment.mayfirst.org/t/invitation-to-participate-in-the-technology-infrastructure-and-services-team-invitacion-a-participar-en-el-equipo-de-infraestructura-y-servicios-tecnologicos/1954).

 * **El Equipo de Compromiso y Comunicación** se centra en otras formas de transmitir la
   importancia de la tecnología autónoma para el movimiento, incluyendo
   presentaciones, conferencias, talleres y otros. Para recibir notificaciones
   de reuniones, rellene nuestro
   [formulario](https://outreach.mayfirst.org/civicrm/profile/create?gid=35&reset=1).

Ambos equipos se reúnen con interpretación simultánea para apoyar a los hablantes de
inglés o español.


