+++ 
date = 2020-09-21T14:47:48Z 
description = "Bienvenidos a la página de la Orientación del May First" 
title = "Orientación" 
slug = "orientación" 
tags = [ "" ] 
[menu] 
  [menu.more] 
    parent = "Membresía" 
    weight = 40
+++

## Orientación

May First es una organización con una larga historia y muchas partes en movimiento. Esta página es una colección de enlaces que le ayudarán a conocer nuestra historia y como operamos.

## Quiénes somos: Nuestra membresía, misión y programa

May First es una organización de membresía dedicada la construcción de movimientos y tiene un enfoque en la tecnología.

A partir de febrero de 2025, tenemos 759 membresías activas, la mayoría se encuentran en Estados Unidos, un 20% en México, y el resto repartidos por todo el mundo. 

Alrededor de 60% de nuestra membresía son de organizaciones y el resto son membresías individuales.

Nuestras membresías incluyen grandes organizaciones como Media Justice, CWA Local 1180, National Network of Abortion Funds y otras, hasta pequeños colectivos de médicos de calle, grupos de producción de medios de comunicación y organizaciones comunitarias.

Las áreas temáticas van desde la atención médica universal hasta los derechos de los palestinos, la libertad de los inmigrantes y muchas otras.

Nuestra misión, aprobada por nuestros miembros en 2015, es:

 *May First participa en la construcción de movimientos promoviendo el uso estratégico y el control colectivo de la tecnología para las luchas locales, la transformación global y la emancipación sin fronteras.*

Nuestro programa, aunque complejo, variado y cambiante, fluye fundamentalmente de dos áreas interrelacionadas:

1. **1. Participación y comunicación:** involucrar al movimiento de la izquierda amplia en una conversación sobre el papel fundamental de la tecnología en nuestra lucha por un mundo más justo y ayudar a desarrollar una estrategia de cambio que centra la tecnología.  

2. **2. Infraestructura y servicios tecnológicos:**  administramos colectivamente nuestros propios servicios basados en Internet para la membresía con el fin de proveer privacidad y estabilidad a las actividades críticas del movimiento mientras demostramos y experimentamos con la tecnología basada en nuestros valores. 

## Historia y cultura

¿Cuándo comenzó el May First ? ¿Fue en 1994? 2005? Lea más sobre los [puntos clave de la historia de nuestra organización](https://share.mayfirst.org/s/4Fnb8MBX6ffPK2K).

A lo largo de nuestra historia, nos hemos comprometido con el liderazgo de las personas tradicionalmente excluidas de la tecnología, que se formalizó en [una declaración de intencionalidad de lucha contra el racismo y el sexismo](https://mayfirst.coop/es/intencionalidad/).

Nuestra cultura también incluye un [acuerdo sobre cómo interactuamos](https://share.mayfirst.org/s/fdAEDg3xRGpXmqG) que identifica los comportamientos y prácticas que buscamos y los que intentamos evitar y además de [consejos sobre la programación y el desarrollo de reuniones](https://share.mayfirst.org/s/X7yPJGFPEMWdsoP).

## México

Como puedes leer en nuestra historia, tenemos una relación especial con activistas y organizaciones de los movimiento sociales en México. Los detalles están en los [estatutos](https://mayfirst.coop/es/estatutos/) y el [documento de historia](https://share.mayfirst.org/s/LFmQx74wK3SQpEy), pero aquí hay un breve resumen.

May First es una organización *internacional* - nuestra membresía proviene de todo el mundo y damos la bienvenida a cualquier persona sin importar donde vive.

Eso sí, May First empezó en Estados Unidos y la mayoría de sus membresía radican en ese país. En 2011, el histórico proveedor mexicano de Internet alternativo LaNeta decidió dejar de proporcionar servicios de alojamiento y preguntó si May First consideraría integrar a su membresía. Aceptamos, y la membresía de LaNeta lo aprobaron, lo que dio lugar a nuestro primer número sustancial de membresía desde otra un país que  los Estados Unidos (alrededor del 20%).

Este acuerdo se alineaba con la visión política de nuestra organización: no sólo la idea de que las fronteras no impidan la construcción de nuestros movimientos, sino específicamente reconocer la importancia de la relación entre los movimientos sociales de EE.UU. y México, dada la frontera, las presiones económicas, la xenofobia y el racismo que han distorsionado esta relación.

Aunque la membresía mexicana es igual que cualquier otra de May First, hemos incluido en nuestros estatutos una estipulación para reservar en la junta directiva un número de integrantes de México proporcional a la representación de la membresía mexicana en nuestra organización.

## Estructura y toma de decisiones

Tenemos uno de los liderazgos más diversos de cualquier organización tecnológica. Para más información, consulte nuestra [página de integrantes de la junta directiva](https://mayfirst.coop/en/who/).

Nuestros [estatutos](https://mayfirst.coop/es/estatutos/) definen los requisitos mínimos de nuestra estructura y toma de decisiones.

Los estatutos establecen lo siguiente

 *La membresía elige el 80% de la junta, y el personal elige el 20% restante.*
   Actualmente tenemos 23 [integrantes del consejo](https://mayfirst.coop/en/who).

 *El Comité de la Junta de Revisión de Trabajadores contrata y despide a lxs trabajadores* (lxs trabajadores incluyen a personas remuneradas y no remuneradas). Actualmente, tenemos tres trabajadores: Jaime Villarreal (remunerados), Jes Ciaci (remunerados) y Jamie McClelland (voluntario).

Sin embargo, hay mucho más en nuestra estructura y toma de decisiones de lo que está definido en los estatutos. 

Por ejemplo, la junta directiva suele pedir a la membresía que ayuden a elaborar un conjunto de **prioridades para el próximo año** y luego que las clasifiquen en términos de importancia en nuestra reunion anual de la membresía (ver nuestras [prioridades actuales](/es/prioridades/)).

### Equipos y Comités

#### Equipos de programa 

May First involucra a su membresía en su trabajo a través de dos **equipos de programa**:

 * **El Equipo de Participación y Comunicaciones:** Cubre nuestro compromiso y participación con el movimiento más amplio, como la participación en conferencias/webinars/presentaciones participación en campañas y coaliciones, educación política/popular.

 * El equipo de Tecnología Infraestructura y Servicios: abarca la gestión, el desarrollo y el soporte para toda la infraestructura y los servicios tecnológicos.

Se invitan a toda la membresía de May First (no sólo a lxs integrantes de la Junta Directiva) a formar parte de estos equipos, que se reúnen periódicamente (al menos una vez al mes) en horarios claramente establecidos, con on al menos una persona coordinadora designada por la Junta, o de preferencia dos.

Las personas coordinadoras de los programas se encargan de redactar un plan de trabajo, basado en los comentarios del equipo del programa, para su discusión por el y la aprobación de la Junta Directiva. Una vez aprobado el plan de trabajo por la Junta, las personas coordinadores del programa están autorizadas a tomar cualquier decisión operativo de acuerdo con el plan de trabajo aprobado.

Los equipos de programa no son órganos formales para la toma de decisiónes: las personas coordinadoras conservan el derecho a tomar las decisiones finales en el ámbito del plan de trabajo. En cambio, están concebidos para involucrar a la membreśia en nuestro trabajo, crear y discutir ideas para  el programa, proporcionar información inicial sobre los planes de trabajo y los informes de evaluación, y coordinar el trabajo. Para ello los equipos deben ser abiertos, abiertos, acogedores y flexibles.

Las personas coordinadoras del programa también son responsables de presentar los informes de evaluación al final del año (basados en los comentarios del equipo del programa). Los informes deben someterse a la aprobación de la Junta Directiva y luego presentarse a la membresía durante la reunión anual para su discusión antes de la selección de las prioridades del año siguiente.

#### Comité de Coordinación (CC)

* **Finalidad** - La finalidad del Comité de Coordinación es conectar y coordinar las actividades de todos los equipos y procesos de MFMT así como garantizar que las decisiones y los compromisos asumidos en todas las partes de la organización sean coherentes con la dirección de la Junta Directiva y las prioridades votadas por la membresía.

* **Composición** - El Comité de Coordinación se limitará a los actuales miembros de la Junta. 

* **Responsabilidades** - El Comité de Coordinación se reúne semanalmente con las siguientes responsabilidades:

  1. **Intercambio de información y coordinación** - El CC recibe informes sobre todas las actividades de la organización y ayuda a coordinar el trabajo entre los equipos. El CC también es responsable de garantizar que las decisiones y compromisos asumidos en todas las partes de la organización sean coherentes con la labor de la Junta Directiva y las prioridades de las membresías.

  2. **Comentarios y toma de decisiones** - El CC examina las propuestas y preguntas de: trabajadores, otros equipos y sus propios integrantes. Dependiendo de la naturaleza de la cuestión, el comité de coordinación puede:
     - *Ofrecer* retroalimentación: cuando la decisión final deba recaer en trabajadores, el equipo de trabajo o la persona que planteó la cuestión
     - *Tomar decisiones*: en cuestiones operativas o administrativas demasiado importantes para que el equipo que las planteó las tome por sí solo, pero no lo suficientemente importantes (o demasiado urgentes) para que las tome la Junta Directiva.
     - *Elevar la* cuestión a la Junta Directiva: precisar y remitir la cuestión a la Junta Directiva para que adopte decisiones de mayor trascendencia.

   3. **Apoyo a la Junta Directiva** - El CC ayuda a la Junta Directiva para
      - Elaborar agendas de reuniones de la Junta Directiva
      - Debatir y preparar decisiones estratégicas de mayor trascendencia para la organización.
      - Aplicar o asignar decisiones tomadas por la Junta Directiva.

#### Comités adicionales 

Además de los equipos de programa y el comité de coordinación, nuestros estatutos especifican un Comité de Revisión de Trabajadores que se reúne cuando es necesario (véase más arriba). 

También pueden formarse otros comités como sean necesarios para encargarse de tareas concretas, como el la planificación de las reuniones de la membresía, . Todos los comités adicionales están abiertos a cualquier integrante de la junta directiva y deben ser anunciados, junto con un mandato claro, a toda la junta directiva antes de su formación. 

### ¿Cómo se hacen las propuestas? ¿Cómo puedo participar? ¿Qué proceso de toma de decisiones se utiliza?

Normalmente, la mayoría de las decisiones importantes comienzan con el equipo del programa al que corresponden. 

A continuación, pasan al comité de coordinación para su discusión.

Si la decisión es menor, o entra claramente en el ámbito del plan de trabajo ya definido, puede enviarse a un comité de programa para que la aplique o a lxs trabajadorxs para que la apliquen. 

En caso contrario, se envía a la junta directiva para su votación.

Si la decisión requiere una modificación de los estatutos, entonces debe ser sometida a la reunión de membresía  para su aprobación final.

En última instancia, las decisiones finales pueden ser tomadas por la junta de May First a través de una simple mayoría de votos. Sin embargo, la cultura es hacer todo lo posible para lograr el consenso antes de recurrir a una votación.

### Responsabilidades de los miembros de la Junta Directiva

Lxs integrantes de la Junta Directiva son elegidxs por períodos de 3 años. Los requisitos mínimos para ser integrante de la junta son:

 * Asistir a ocho reuniones de la junta en línea de una hora y media al año.
 * Dedicar hasta 1 hora a la semana a leer y responder al correo electrónico
 * Asistir a la reunión anual de la membresía.

Además, aunque no es obligatorio, se anima a lxs integrantes de la junta a participar en al menos en uno de los comités del programa.

## Finanzas

Lxs trabajadorxs de May First son responsables de presentar un informe financiero a la junta directiva cada año. Consulte nuestro [Informe financiero de 2021 Informe](https://share.mayfirst.org/s/gBDCCQKgdKQaa2w).

La junta es responsable de aprobar el presupuesto, basado en el plan de trabajo, cada año.

## Herramientas

Las principales herramientas de comunicación que utilizamos:

 * Listas de correo electrónico: todos lxs integrantes de la junta directiva están suscritxs a una lista de discusión por correo electrónico.  El equipo de coordinación también tiene una lista. Otros equipos de trabajo emplean sus propias herramientas de comunicación.

 * Nextcloud: La junta y todos los grupos de trabajo dependen en gran medida de nuestro sistema de almacenamiento de documentos [Nextcloud](https://support.mayfirst.org/wiki/nextcloud). Tenemos carpetas para todos los comités y tomamos notas regularmente para todas las reuniones de los comités. La mayoría de las propuestas se escriben y se guardan en Nextcloud y los informes finales también se guardan aquí.

 * Discurso: Nuestro [servidor Discourse](https://comment.mayfirst.org/) es un es un foro de discusión basado en la web para que la membresía comenten sobre los temas y las propuestas.  Utilizamos mucho a Discourse durante nuestras reuniones de la membresía, para que puedan elegir entre asistir a una reunión en directo, si su calendario lo permite, o participar asincrónicamente a través del sistema Discourse.

* Jitsi Meet: Cada vez utilizamos más el [sistema de videoconferencia Jisti Meet](https://support.mayfirst.org/wiki/web-conference) para las reuniones en vivo.  
