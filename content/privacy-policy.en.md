+++
date = 2015-10-01T13:52:02Z
title = "Privacy Policy"
[menu]
  [menu.more]
    parent = "Hosting"
    weight = 200
+++

We will not sell your data. We will never sell any user information to any other groups or individuals.

We will actively fight any attempt to force May First Movement Technology to disclose user information or logs.

We will not read, search, or process any of your data other than to protect you from viruses and spam, debug problems affecting our shared infrastructure, or when directed to do so by you when troubleshooting.

Your data is encrypted at rest. All data saved on our servers and all backups are stored in an encrypted format.

Our policy is to immediately notify you if we learn that you are under investigation.

To protect the privacy of our members, we regularly purge log files and backup files.

User data: If you cancel your membership, all your user data will be completely purged.

Personal data: Information such as your name, contact information or when you joined the organization and your payment history may be kept indefinitely so we can track and evaluate our membership engagement over time. However, any member may request access to, correction of, or deletion of any or all of your personal data retained by us. You may make this request by emailing info@mayfirst.org. We will attempt to fulfill these requests, unless legally prohibited from doing so.
