#!/bin/bash

template='+++
date = "DATE" 
title = "TITLE"
mp3 = "MP3"
webm = "WEBM"
type = "audio"
podcast_duration = "DURATION"
bytes = "BYTES"
+++

TBD
'

site="$1"

if [ -z "$site" ]; then
  printf "Please pass path to site as first argument.\n"
  exit 1
fi

if [ ! -d "$site" ]; then
  printf "Can't find site path.\n"
  exit 1
fi

# Hugo wants an ISO8601 date with Z at the end.
date=$(date -u --iso-8601=seconds | sed 's/+[0:]*$/Z/')
for lang in en es; do 
  audio_pages_path="${site}/content/audio"
  audio_media_path="${site}/media/${lang}/audio" 
  for file in $(ls "$audio_media_path"/*.webm 2>/dev/null); do
    name=$(basename "$file" .webm)
    page_path="${audio_pages_path}/${name}.${lang}.md"

    # Generate markdown page if it doesn't exist.
    if [ ! -f "$page_path" ]; then
      # The page has not yet been created.
      # Create the title using title case. First convert
      # dashes, underscores and dots to spaces.
      title_words=$(echo "$name" | tr "_\-." " ")
      title=
      for word in $title_words; do
        firstletter=$(echo "$word" | cut -c1 | tr '[[:lower:]]' '[[:upper:]]')
        otherletters=$(echo "$word" | cut -c2-)
        title="$title ${firstletter}${otherletters}"
      done
      # Delete trailing or beginning spaces from name.
      title=$(echo "$title" | sed 's/^ //' | sed 's/ $//')
      duration=$(ffprobe -v error -show_entries format=duration -sexagesimal -of default=noprint_wrappers=1:nokey=1 "$file" | sed 's/\.[0-9]*//g')
      bytes=$(stat -c %s "$file")
      echo "$template" | sed "s#DATE#$date#" \
        | sed "s#TITLE#$title#" \
        | sed "s#WEBM#media/${lang}/audio/${name}.webm#" \
        | sed "s#MP3#media/${lang}/audio/${name}.mp3#" \
        | sed "s#DURATION#$duration#" \
        | sed "s#BYTES#$bytes#" \
        > "$page_path"
    fi
  done
done
