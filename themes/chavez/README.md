# Org Theme

The nonprofit theme is based on the [Agency Theme](https://github.com/digitalcraftsman/hugo-agency-theme), but it is multi-page and the sections are renamed to better reflect the needs of a non-profit organization.

## Contents

- [Installation](#installation)
- [License](#license)
- [Annotations](#annotations)


## Installation

Inside the folder of your Hugo site run:

    $ mkdir themes
    $ cd themes
    $ git clone https://github.com/digitalcraftsman/hugo-agency-theme

For more information read the official [setup guide](//gohugo.io/overview/installing/) of Hugo.

## License

This theme is released under the Apache License 2.0 For more information read the [License](//github.com/jmcclelland/hugo-nonprofit-theme/blob/master/LICENSE).

## Annotations

Thanks to [Steve Francia](//github.com/spf13) for creating Hugo and the awesome community around the project.
