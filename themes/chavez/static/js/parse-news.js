
// Only operate on home page (or any page with member-news id.)
if ($("#member-news").length) {

  function formatDate(date) {
   var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ]

    var startMonth = monthNames[date.getMonth()];

    // Omit the ending month and date
    return startMonth + " " + date.getDate();
  }

  console.log("Parsing News");
  // Check for english or spanish.
  var file = null;
  console.log(window.location.pathname.substring(1,3));
  if (window.location.pathname.substring(1,3) == 'en') {
    file = '/news.en.json'; 
    console.log(file);
    console.log(window.location.pathname);
  }
  else if (window.location.pathname.substring(1,3) == 'es') {
    file = '/news.es.json'; 
    console.log(file);
    console.log(window.location.pathname);
  }
  if (file) {
    $.ajax({
      url: file,
      dataType: 'json',
      success: function (data) {
        console.log(data);
        var length = data.length;
        var content = '';
        for (var i = 0; i < length; i++) {
          var title = data[i].title;
          var link = data[i].link;
          var source = data[i].source;
          var date = formatDate(new Date(data[i].date));
          content += '<div class="news-title"><a href="' + link + '">' + title + '</a> (from: ' + source + ')</div>';
          content += '<div class="news-date">' + date + '</div>';
        }

        if (content) {
          $("#member-news").append(content);
        }
      }
    });
  }
}
