Statement on Federal Gag Order Against MF/PL
Dec 18 2014 - 6:07pm

May First/People Link's leadership has been under a Federal gag order since
early September. The gag order prevented us from discussing a subpoena for
member information and the existence of the gag order itself. As of today, that
gag order has expired.

The gag order protected a subpoena that was issued by the Department of Justice
on September 5, 2014, which demanded account information about the Athens
(Greece) Indymedia Center membership. Apparently, the target of the
investigation was a member of an organization wanted by the Greek law
enforcement who is believed to have used the Indymedia website at one point.

On the request of the Athens IMC, we refused to release personally identifying
information to the department of justice to shield members of the Athens IMC
from political persecution in Greece. We then publicly announced the existence
of the subpoena to our membership and posted that information on our website.

Two weeks later, we were served with the gag order forbidding us from talking
about the subpeona and forbidding us from even acknowledging to anyone outside
our Leadership Committee that the order existed. We were to act as if nothing
had happened.

We were informed that any violation of this order could result in fines and
imprisonment, which could have destroyed the organization.

Under the advice of our lawyers from the Electronic Frontier Foundation we
obeyed the order.

Because the order has now expired, we want to comment on two issues arising
from this episode that we consider important.

First, May First/People Link has always espoused the principle that the release
of member data is a violation of our members' right to privacy: a fundamental
human right and one that protects the ability of our members to engage in
activism and political organizing. It is at the crux of our responsibility as
an organization. We do not cooperate with government intrusions unless the
specific affected members agree to such cooperation. It is this policy that
guides our actions.

In the case of the Athens IMC, we had no data of any real value to this
investigation and that fact makes our second concern even deeper.

Hitting us with an order that forbids us from speaking about a government
demand for our members' information and then forbids us from publicly
acknowledging that there is such a demand is grotesquely repressive and
damaging to our organization's members and their right to information about
their data and their organization.

We believe you have the right to privacy and to withhold from any investigation
information you may have that is legally obtained and legal in and of itself.
We believe you have the right tell others that the government is investigating
you or demanding that kind of information from you. Moreover, we believe you
have the right to tell others when the government forbids you from revealing
information about its demands.

These rights are particularly precious at a time when people the world over are
fighting for our freedom and opposing government repression and brutality. No
repressive act can be hidden and no injustice will go unopposed when there is a
truly open and free Internet.

Protecting and broadening that free Internet is our mission as an organization.
Facilitating, defending and protecting our members' data and their ability to
function as activist organizations is at the very core of that mission.

We continue committed to that mission.

---

May First/People Link is a membership organization of left-wing and civil
society organizations and activists who share Internet resources and work on
technology and freedom issues. We are organized in the United States and
Mexico.

